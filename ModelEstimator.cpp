/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ModelEstimator.cpp
 * Author: luis
 * 
 * Created on 23 de agosto de 2018, 05:03 PM
 */

#include "ModelEstimator.h"
#include "LinePointFunctions.hpp"
#include "BasicFunctions.hpp"
#include "Jacobi_eigenvalue_modif.hpp"

#include <opencv2/xfeatures2d.hpp>
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include <opencv2/line_descriptor.hpp>
#include "opencv2/core/utility.hpp"
#include <opencv2/imgproc.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/highgui.hpp>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>
#include <fstream>
#include <string.h>
#include <cstdlib>
#include <math.h>
#include <opencv2/calib3d.hpp>  

using namespace std;

cv::Mat solveAffine(std::vector<cv::Point2f>& pInters_test,
        std::vector<cv::Point2f>& pInters_refe) {

    cv::Point2f arrayPuntos_test[pInters_test.size()];
    cv::Point2f arrayPuntos_refe[pInters_refe.size()];

    for (int hh = 0; hh < pInters_refe.size(); hh++) {
        arrayPuntos_test[hh] = pInters_test[hh];
        arrayPuntos_refe[hh] = pInters_refe[hh];
    } // for 

    /* < Calculando Matriz Afin > */
    cv::Mat mat_AffineTrans = cv::getAffineTransform(arrayPuntos_test, arrayPuntos_refe);
    //            cout << mat_AffineTrans << endl;


    cv::Mat mat_Affine(3, 3, CV_64F);

    for (int ii = 0; ii < mat_AffineTrans.rows; ii++) {
        for (int jj = 0; jj < mat_AffineTrans.cols; jj++) {
            mat_Affine.at<double>(ii, jj) = mat_AffineTrans.at<double>(ii, jj);
        }// for
    }//for
    mat_Affine.at<double>(2, 0) = 0;
    mat_Affine.at<double>(2, 1) = 0;
    mat_Affine.at<double>(2, 2) = 1;

    return mat_Affine;
} //  function 

cv::Mat solveHomography(std::vector<cv::Point2f>& pInters_test,
        std::vector<cv::Point2f>& pInters_refe) {
    // < Bandera para utilizar el metodo manual > 
    bool bandEstimadorHomografiaManual = false;

    if (bandEstimadorHomografiaManual) {
        int numPuntosIntersec = pInters_test.size();
        int nFil = numPuntosIntersec * 2;
        int nCol = 9;

        /* CREANDO MATRIZ A */
        cv::Mat matA = cv::Mat::zeros(nFil, nCol, CV_64F);

        // printPointsVector_varMatlab(pInters_test, "pInters_test");
        // printPointsVector_varMatlab(pInters_refe, "pInters_refe");

        /*Rellenando con los puntos de Interseccion I1*/
        int indMat_impar, indMat_par;
        for (int ii = 0; ii < numPuntosIntersec; ii++) {
            /* Indices auxiliares */
            indMat_par = ii * 2;
            indMat_impar = indMat_par + 1;
            /* Fila par ==> x,y,z*/
            matA.at<double>(indMat_par, 0) = pInters_test[ii].x; // A : x1
            matA.at<double>(indMat_par, 1) = pInters_test[ii].y; // A : y1
            matA.at<double>(indMat_par, 2) = 1; // A : z1
            /*Fila impar ==> x,y,z*/
            matA.at<double>(indMat_impar, 0 + 3) = pInters_test[ii].x; // A : x1
            matA.at<double>(indMat_impar, 1 + 3) = pInters_test[ii].y; // A : y1
            matA.at<double>(indMat_impar, 2 + 3) = 1; // A : z1
            /*Fila par ==> (x'x)/z', (x'y)/z', (x')/z'*/
            matA.at<double>(indMat_par, 0 + 6) = -1 * (pInters_refe[ii].x * pInters_test[ii].x); //
            matA.at<double>(indMat_par, 1 + 6) = -1 * (pInters_refe[ii].x * pInters_test[ii].y); //
            matA.at<double>(indMat_par, 2 + 6) = -1 * (pInters_refe[ii].x); // 
            /*Fila impar ==> (y'x)/z', (y'y)/z', (y')/z'*/
            matA.at<double>(indMat_impar, 0 + 6) = -1 * (pInters_refe[ii].y * pInters_test[ii].x); //
            matA.at<double>(indMat_impar, 1 + 6) = -1 * (pInters_refe[ii].y * pInters_test[ii].y); //
            matA.at<double>(indMat_impar, 2 + 6) = -1 * (pInters_refe[ii].y); // 
        }//for     

        // < Recalculando la homigrafia >
        return calculateHomografia(matA);
    } else {
        return cv::findHomography(pInters_test, pInters_refe, 0);
    } // if - else 
}// function

cv::Mat calculateHomografia(cv::Mat matA) {
    /*-------------- Acomodando las variables para las operaciones --------------*/
    cv::Mat matA_transp;
    cv::Mat matA_original;

    matA_original = matA.clone(); // respaldando  la matriz A    
    matA_transp = matA.t(); // La traspuesta es la matriz A 

    /* Obteniendo el producto de matrices para hacer cuadrada la matriz*/
    cv::Mat matA_cuadrada = matA_transp * matA_original; // matSquare_A = A' * A; 

    int idMetodoObtenerEigenVectores = 0;
    bool bandPrintSomething = false;
    cv::Mat modeloTrans;

    modeloTrans = getEigenVectoresByMethod(idMetodoObtenerEigenVectores, matA_cuadrada, bandPrintSomething);
    return modeloTrans;
}// function

cv::Mat getEigenVectoresByMethod(int idMetodo, cv::Mat& matCuadrada, bool bandPrintSomething) {
    /* Inicia algoritmo */
    cv::Mat modeloTrans;
    std::vector< std::vector<double> > vect_eigenVectores;
    std::vector< std::vector<double> > vect_eigen2D;

    switch (idMetodo) {
        case 0:
        {
            /* Imprimiendo matriz*/
            std::vector<std::vector<double> > vectCuadrada = transform_cvMat2stdVector(matCuadrada);

            int nFil_cuadrada = vectCuadrada.size();
            int nCol_cuadrada = vectCuadrada[0].size();

            double arrayA_cuadrada[nFil_cuadrada * nCol_cuadrada]; // Creando Array del tamaño de la matA_cuadrada pero en 1D
            vecMat2array_porCol(vectCuadrada, arrayA_cuadrada); /* Transformando Por Columna*/

            /* Calculando los EigenValores y EigenVectores*/
            //int it_max = 25; //muy pocas va a fallar
            int it_max = 100;

            int it_num, rot_num; // aqui se guardan los valores 
            double eigenVectores[nFil_cuadrada * nCol_cuadrada]; // 9 * 9
            double eigenValores[nCol_cuadrada]; // 9 

            /* Obtencion de eigenvalores y eigenvectores */
            jacobi_eigenvalue(nFil_cuadrada, arrayA_cuadrada, it_max, eigenVectores, eigenValores, it_num, rot_num);

            /* Ajustando stdVector */
            vect_eigenVectores = array2vecMat_porFil(eigenVectores, nFil_cuadrada, nCol_cuadrada); /* Por Fila*/
            vect_eigenVectores = getMatTranspuesta(vect_eigenVectores);

            /* Obteniendo la fila que representa la homografia */
            std::vector< double > eigenVector(nFil_cuadrada);
            for (int f = 0; f < nFil_cuadrada; f++) {
                eigenVector[f] = vect_eigenVectores[f][0];
            }// for

            /* Formando matriz 3*3 */
            vect_eigen2D = vecOneDim2twoDim(eigenVector, 3, 3);

            /* Normalizando con el ultimo elemento de la matriz*/
            calculateDivisionMat(vect_eigen2D, vect_eigen2D[2][2]);

            /* Transformando a cv::Mat*/
            modeloTrans = transform_stdVector2cvMat(vect_eigen2D);
            break;
        }//case
        case 1:
        {
            cv::Mat matEigenvectors1D, matEigenvectors2D;
            /* < Método 1 >*/
            /* Convirtiendo matriz a formato necesitado */
            cv::Mat matCuadrada_s;
            matCuadrada.convertTo(matCuadrada_s, CV_64FC1);

            /* Obteniendo los eigen-valores y eigen-vectores*/
            cv::Mat matEvals, matEvecs;
            cv::eigen(matCuadrada_s, matEvals, matEvecs);

            /* Obteniendo Fila de eigenvectores */
            int nFil = matEvecs.rows;
            int nCol = matEvecs.cols;
            matEigenvectors1D = cv::Mat(nFil, 1, CV_64F);

            for (int kk = 0; kk < nFil; kk++) {
                matEigenvectors1D.at<double>(kk) = matEvecs.at<double>(kk, 0);
            }// for

            /* Reshaping la matriz 1d */
            matEigenvectors2D = mat1D_To_mat2D(matEigenvectors1D, 3, 3);

            modeloTrans = matEigenvectors2D / matEigenvectors2D.at<double>(2, 2);

            vect_eigenVectores = transform_cvMat2stdVector(matEvecs);
            vect_eigen2D = transform_cvMat2stdVector(matEigenvectors2D);
            break;
        } // case            
    } // switch 

    if (bandPrintSomething) {
        cout << "--------------------------" << endl;
        printVector2D_comoVariableMatlab(vect_eigenVectores, "vect_eigenVectores");
        cout << endl;
        cout << modeloTrans.rows << "*" << modeloTrans.cols << "\n modeloTrans=" << modeloTrans << endl;
        cout << endl;
        cout << "--------------------------" << endl;
    } // if 

    return modeloTrans;
}// function

cv::Mat calculateModel(int idTypeModel4Ransac,
        std::vector<cv::Point2f>& pTest,
        std::vector<cv::Point2f>& pRefe,
        bool bandLuisMethodsHomography) {
    // < Revisando Puntos de entrada >
    if (pTest.size() != pRefe.size()) {
        cout << " Error: Calculando Modelo - El numero de puntos en TEST y REFE debe ser el mismo. " << endl;
        cout << " .... MODELESTIMATOR .... \n " << endl;
        exit(0);
    } // if 

    /* < CALCULATING THE MODEL > */

    //    bool bandConsiderarPerspectivaHomo = 0;    

    cv::Mat Modelo;

    // Obteniendo el numero de correspondencias
    int numMatches = pTest.size();
    ///*
    // Seleccionando caso
    switch (idTypeModel4Ransac) {
        case 0:
        { // < Homografia > 
            if (numMatches >= 4) {
                if (bandLuisMethodsHomography) {
                    // Resolviendo con metodos Luis
                    Modelo = solveHomography(pTest, pRefe);
                } else {
                    // Resolviendo con metodos OPENCV
                    //                    Modelo = cv::findHomography(pTest, pRefe, CV_RANSAC,
                    //                            this->parametros.paramRANSAC.umbralDistancia,
                    //                            cv::noArray(),
                    //                            this->parametros.paramRANSAC.numIteraciones);
                    ////Modelo = cv::getPerspectiveTransform(pTest, pRefe);
                    Modelo = cv::findHomography(pTest, pRefe);

                    // No se encontro el modelo 
                    if (Modelo.data != NULL) {
                        Modelo.convertTo(Modelo, CV_64F);
                    } else {
                        Modelo = cv::Mat::eye(3, 3, CV_64F);
                    } // if - else 
                } // if - else 

            } else {
                //cout << " ** Insuficiente numero de Puntos para calcular la Homografia **" << endl;
                Modelo = solveAffine(pTest, pRefe);
            }// if - else
            break;
        }// case 
        case 1:
        { // < Matriz Afin> 
            Modelo = solveAffine(pTest, pRefe);
            break;
        }//case 
    } //switch 

    if (Modelo.data == NULL) {
        cout << "Homografia nula." << endl;
    }// if

    return Modelo;
} // function 

int evaluaModelo_puntos(cv::Mat& Modelo,
        std::vector<bool>& vectBanderasConsiderarLineas,
        std::vector<cv::Point2f>& puntos_test, std::vector<cv::Point2f>& puntos_refe,
        int umbralDistanciaInliers, std::vector<double>& vectDistancia,
        std::vector<int>&vecIndicesValidosMatch, bool bandPrintSomething,
        std::vector<cv::Point2f>& puntos_tran) {
    // < Numero de inliers de la Homografia >
    int numInliers = 0;

    /* < ######### Obteniendo distancia y angulos ######### >*/
    /* Obteniendo las transformaciones y las lineas*/
    vectDistancia = getPointsTransformationsByModel(Modelo,
            vectBanderasConsiderarLineas,
            puntos_test, puntos_refe, puntos_tran,
            bandPrintSomething);

    /*< ######### Obteniendo Inliers Elementos que cumplen con el angulo y la distancia determinada ######### > */
    vecIndicesValidosMatch = getVectInliners_puntos(
            vectBanderasConsiderarLineas, vectDistancia,
            umbralDistanciaInliers); // obteniendo valores Validos deacuerdo al umbral de distancia

    numInliers = vecIndicesValidosMatch.size();
    return numInliers;
} // function 

int evaluaModelo_lineas(cv::Mat& Modelo, int tipoEvalDistancia,
        std::vector<bool>& vectBanderasConsiderarLineas,
        std::vector<cv::line_descriptor::KeyLine>& lineas_test, std::vector<cv::line_descriptor::KeyLine>& lineas_refe,
        int umbralDistanciaInliers, int umbralAnguloInlier,
        std::vector<double>& vectDistancia, std::vector<double>& vectAngulos,
        std::vector<int>&vecIndicesValidosMatch, bool bandPrintSomething,
        std::vector<cv::line_descriptor::KeyLine>& lineas_tran) {
    // < Numero de inliers de la Homografia >
    int numInliers = 0;

    /* < ######### Obteniendo distancia y angulos ######### > */
    /* < Obteniendo las transformaciones y las lineas > */
    lineas_tran = getLinesTransformationsByModel(tipoEvalDistancia,
            Modelo, vectBanderasConsiderarLineas,
            lineas_test, lineas_refe, vectDistancia, vectAngulos,
            bandPrintSomething);

    /* < ######### Obteniendo Inliers Elementos que cumplen con el angulo y la distancia determinada ######### > */
    vecIndicesValidosMatch = getVectInliners_lineas(
            vectBanderasConsiderarLineas, vectDistancia,
            vectAngulos, umbralDistanciaInliers, umbralAnguloInlier); // obteniendo valores Validos deacuerdo al umbral de distancia

    numInliers = vecIndicesValidosMatch.size();
    return numInliers;
} // function 

std::vector<int> getVectInliners_puntos(
        std::vector<bool>& vectBanderasConsiderarLineas,
        std::vector<double>& vecDistancias, int thresold) {
    int numElementos = vecDistancias.size(); // numero de elementos en el vector
    std::vector<int> vecIndicesValidas;
    // < Verificando >
    for (int ii = 0; ii < numElementos; ii++) {
        if ((vectBanderasConsiderarLineas[ii]) && (vecDistancias[ii] < thresold)) { // Si es menor que el umbral se Guarda en el vector de distancias Validas
            // < Es un Inlier>
            vecIndicesValidas.push_back(ii);
        }// if
    }// for 
    return vecIndicesValidas;
}// function

std::vector<int> getVectInliners_lineas(std::vector<bool>vectorBanderasConsiderarLinea,
        std::vector<double>& vecDistancias, std::vector<double>& vectAngulos, double thDistance, double thAngle) {
    int numElementos = vecDistancias.size(); // numero de elementos en el vector
    std::vector<int> vecIndicesValidas;

    double val_angulo, val_distancia;
    bool bandDistanciaValida, bandAnguloValido;

    for (int ii = 0; ii < numElementos; ii++) {
        val_angulo = vectAngulos[ii];
        val_distancia = vecDistancias[ii];
        bandAnguloValido = ((val_angulo < thAngle) || (val_angulo > 180 - thAngle)); // < Criterios de angulo >
        bandDistanciaValida = (val_distancia < thDistance); // < Criterios de distancia >
        // < Verificando >
        if ((vectorBanderasConsiderarLinea[ii]) && (bandDistanciaValida) &&
                (bandAnguloValido)) { // Si es menor que el umbral se Guarda en el vector de distancias Validas            
            // < Es un Inlier>
            vecIndicesValidas.push_back(ii);
        }// if
    }// for 
    return vecIndicesValidas;
}// function

double getScoreModelo_lineas(std::vector<double>& vectDistancia,
        std::vector<double>& vectAngulos) {
    // Obteniendo sumatorias;
    double sumaDistancias = getSumatoriaDeVector(vectDistancia);
    double sumaAngulos = getSumatoriaDeVector(vectAngulos);

    return ((sumaDistancias + sumaAngulos) / vectDistancia.size());
    //        cout << "\n(" << sumaDistancias << " + " << sumaAngulos << ")/" << vectDistancia.size() << " = " << score << endl;    
} // function 

double getScoreModelo_puntos(std::vector<double>& vectDistancia) {
    // Obteniendo sumatorias;    
    return getMedia(vectDistancia);
} // function 

cv::Mat recalculateHomographyWithIntersectionPoints(
        int idTypeModel4Ransac,
        std::vector<cv::line_descriptor::KeyLine>& vectLines_inliers_test,
        std::vector<cv::line_descriptor::KeyLine>& vectLines_inliers_refe,
        std::vector<cv::Point2f>& vectPuntIntersec_test,
        std::vector<cv::Point2f>& vectPuntIntersec_refe,
        std::vector<cv::line_descriptor::KeyLine>& vectLines_inliers_tran,
        std::vector<cv::Point2f>& vectPuntIntersec_tran,
        int thErrorDist, cv::Point2f ptCentro,
        double radioTolerancia, int numMinMuestras,
        std::vector<int>& vectIndexLineasInliers_UsedToImproveModel,
        std::vector<double>& vectDistError_IP) {
    /* <> */
    cv::Mat Modelo;

    /* <Obteniendo puntos de interseccion de las lineas inliers > */
    std::vector<std::pair<int, int> > vectRealcionLineasPI = generaCorrespondenciaPuntosInterseccionLineas_PI_umbralError(
            vectLines_inliers_test, vectLines_inliers_refe,
            vectPuntIntersec_test, vectPuntIntersec_refe,
            vectLines_inliers_tran, vectPuntIntersec_tran,
            thErrorDist, ptCentro, radioTolerancia,
            vectDistError_IP);

    // <Extrayendo Lineas buenas>    
    vectIndexLineasInliers_UsedToImproveModel = getVectUniqueIndexFromPairVector(vectRealcionLineasPI);

    //    cout << "\nRecalculando Homografia con " << numPuntosInterseccion << " puntos de interseccion" << endl;    
    if (vectIndexLineasInliers_UsedToImproveModel.size() < numMinMuestras) {
        // < No se puede formar el modelo porque no se tiene suficientes lineas >
        Modelo = cv::Mat::eye(3, 3, CV_64F);
    } else {
        /* < Calculando Modelo > */
        Modelo = calculateModel(idTypeModel4Ransac, vectPuntIntersec_test, vectPuntIntersec_refe);
    }
    return Modelo;
} // fuction 

std::pair<int, std::pair<double, double> > getEvaluationModeloPoints(cv::Mat& Modelo,
        std::vector<cv::Point2f>& puntos_test, std::vector<cv::Point2f>& puntos_refe,
        int umbralDistanciaInliers) {
    /* < Para los resultados > */
    std::pair<int, std::pair<double, double> > pairResult;

    /* < Banderas para considerar elementos > */
    int numPuntos = puntos_test.size();
    std::vector<bool> vectBanderasConsiderarLineas = generaVectorTrue(numPuntos);

    /* < Variables auxiliares > */
    std::vector<double> vectDistancia;
    std::vector<int> vecIndicesValidosMatch;
    std::vector<cv::Point2f> puntos_tran;

    /* < Obteniendo evaluacion > */
    pairResult.first = evaluaModelo_puntos(Modelo, vectBanderasConsiderarLineas,
            puntos_test, puntos_refe, umbralDistanciaInliers,
            vectDistancia, vecIndicesValidosMatch,
            false, puntos_tran);
    // Obteniendo la distania de los inliers 
    std::vector<double> vectDist_inliers = getElementsFromVector(vectDistancia, vecIndicesValidosMatch);
    pairResult.second.second = getDesviacionEstandar(vectDist_inliers, pairResult.second.first);

    return pairResult;
} // function 
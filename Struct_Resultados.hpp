/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Struct_Resultados.hpp
 * Author: luis
 *
 * Created on 6 de junio de 2018, 01:55 PM
 */

#ifndef STRUCT_RESULTADOS_HPP
#define STRUCT_RESULTADOS_HPP

//#include "QuadTree_Keypoints.h"

#include <opencv2/line_descriptor.hpp>
#include "opencv2/core/utility.hpp"
#include <opencv2/imgproc.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/highgui.hpp>

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using namespace std;

struct Struct_LineSegments {
    /* <~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~> */
    /*-------------------------------*/
    /* Homografias */
    cv::Mat Model_ip; // < El Mejor de entre los dos >   
    bool bandModeloFound_ip; // Indicador que la homografia se realizo 
    /*-------------------------------*/

    // < Lineas >    
    int numLinesDetec_test;
    int numLinesDetec_refe;
    cv::Mat matLinesDescr_test;
    cv::Mat matLinesDescr_refe;

    /* DMatch - Correspondencia */
    std::vector<cv::DMatch> vectMatches_respaldo;
    std::vector<cv::DMatch> vectMatches;
    std::vector<cv::Scalar> vectColores;

    /* Control de RANSAC */
    double scoreRANSAC_ip;
    std::vector<int> vectIRandomIndexes_lines;

    int numInliers_lines; // < El Mejor de entre los dos >    

    std::vector<double> vectDist_lines; // < El Mejor de entre los dos >   
    std::vector<double> vectAngle_lines; // < El Mejor de entre los dos >    

    std::vector<int> vectIndexMatch_Inliers_lines; // < Indices Inliers >   

    // Lineas detectadas
    std::vector<cv::line_descriptor::KeyLine> vectLinesDetect_test; // < Lineas IMG-TEST detectadas por el algoritmo EDLINES
    std::vector<cv::line_descriptor::KeyLine> vectLinesDetect_refe; // < Lineas IMG-REFE detectadas por el algoritmo EDLINES

    // Lineas Emparejadas    
    std::vector<cv::line_descriptor::KeyLine> vectLinesMatch_test; // < Matching de lineas Test con respecto a Refe
    std::vector<cv::line_descriptor::KeyLine> vectLinesMatch_refe; // < Matching de lineas Refe con respecto a Test

    // Lineas transformadas
    std::vector<cv::line_descriptor::KeyLine> vectLinesMatch_tran; // < Lineas Test-Match transformadas con el Mejor Modelo    

    /* Lineas Inliers */
    std::vector<cv::line_descriptor::KeyLine> vectLinesMatch_Inliers_test; // < Lineas Test-Match que son Inliers
    std::vector<cv::line_descriptor::KeyLine> vectLinesMatch_Inliers_refe; // < Lineas Refe-Match que son Inliers
    std::vector<cv::line_descriptor::KeyLine> vectLinesMatch_Inliers_tran; // < Lineas Test-Match transformadas con el Modelo y que son Inliers

    /* Lineas seleccionadas */
    std::vector<cv::line_descriptor::KeyLine> vectLinesS_test; // < Lineas Test-Match-Inliers seleccionadas para obtener las intersecciones
    std::vector<cv::line_descriptor::KeyLine> vectLinesS_refe; // < Lineas Refe-Match-Inliers seleccionadas para obtener las intersecciones

    // Puntos de interseccion - Lineas Seleccionadas    
    std::vector<cv::line_descriptor::KeyLine> vectLineasSelect_IP_test; // < Lineas Test seleccionadas para obtener los Puntos de interseccion
    std::vector<cv::line_descriptor::KeyLine> vectLineasSelect_IP_refe; // < Lineas Refe seleccionadas para obtener los Puntos de interseccion
    std::vector<cv::line_descriptor::KeyLine> vectLineasSelect_IP_tran; // < Lineas Tran seleccionadas para obtener los Puntos de interseccion

    std::vector<cv::Point2f> vectLS_PI_test; // < Puntos de interseccion de las Lineas Test-Match-Inliers seleccionadas
    std::vector<cv::Point2f> vectLS_PI_refe; // < Puntos de interseccion de las Lineas Refe-Match-Inliers seleccionadas
    std::vector<cv::Point2f> vectLS_PI_tran; // < Puntos de interseccion de las Lineas Tran-Match-Inliers seleccionadas
    std::vector<double> vectDist_LS_PI; // < Error de transformacion entre los IP transformados y los de referencia

    bool bandImprovedModelDone_ip; // < Bandera si el modelo se mejoro
    std::vector<int> vectIndexLineasInliers_UsedToImproveModel; // < Indice Lineas TEST-REFE-TRAN Inliers que fueron seleccionadas para Obtener las Intersecciones 
    /* <~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~> */
};

struct Struct_Points {
    /* <~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~> */
    /*-------------------------------*/
    /* Homografias */
    cv::Mat Model_kp; // < El Mejor de entre los dos >    
    bool bandModeloFound_kp; // Indicador que la homografia se realizo 
    /*-------------------------------*/

    // < Puntos >    
    int numPointsDetec_test;
    int numPointsDetec_refe;
    cv::Mat matPointsDescr_test;
    cv::Mat matPointsDescr_refe;

    /* DMatch - Correspondencia */
    std::vector<cv::DMatch> vectMatches_respaldo;
    std::vector<cv::DMatch> vectMatches;
    std::vector<cv::Scalar> vectColores;

    /* Control de RANSAC */
    double score_RANSAC_kp;
    std::vector<int> vectIRandomIndexes_points;

    int numInliers_points; // < El Mejor de entre los dos >
    std::vector<double> vectDist_points; // < El Mejor de entre los dos >
    std::vector<double> vectAngle_points; // < El Mejor de entre los dos >
    std::vector<int> vectIndexMatch_Inliers_points; // < El Mejor de entre los dos >

    // Puntos detectadas
    std::vector<cv::KeyPoint> vectKeypointsDetect_test;
    std::vector<cv::KeyPoint> vectKeypointsDetect_refe;

    // Puntos Emparejadas    
    std::vector<cv::KeyPoint> vectKeypointsMatch_test;
    std::vector<cv::KeyPoint> vectKeypointsMatch_refe;

    /* Lineas Inliers */
    std::vector<cv::Point2f> vectPointsMatch_Inliers_test;
    std::vector<cv::Point2f> vectPointsMatch_Inliers_refe;
    std::vector<cv::Point2f> vectPointsMatch_Inliers_tran;

    /* Puntos extraidos de los Keypoints*/
    std::vector<cv::Point2f> vectPointsMatch_test;
    std::vector<cv::Point2f> vectPointsMatch_refe;
    std::vector<cv::Point2f> vectPointsMatch_tran;
};

struct Struct_Matching {
    int iterFramePares;
    /* <Variables En Comun>*/
    /* Frames */
    int idFrame_test;
    int idFrame_refe;

    /* Globales */
    int inlinersTh_Homo;
    double tiempoEjecucion;

    // < Bandera para considerar este par de Frames en el mosaico >
    bool bandConsiderarStitching;

    /* Imagenes y matrices de descriptores */
    cv::Mat imageMat_test;
    cv::Mat imageMat_refe;

    /* <~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~> */
    // < Lineas >    
    Struct_LineSegments structLineas;
    // < Puntos >    
    Struct_Points structPuntos;
    /* <~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~> */

    /* <EXTRA> */
    int numDetect_Lineas;
    int numMatches_Lineas;
    int numInliers_Lineas;
    int numDetect_Puntos;
    int numMatches_Puntos;
    int numInliers_Puntos;
    
    /* < Cuando el modelo es encontrado > */
    bool bandModelFound;
    int idModelUsed;
    cv::Mat Model;
    int numFeaturesDetect;
    int numMatches;
    int numInliers;
    int numPuntosValidos;
    double porcentajeMatchInliers;
    double porcentajeDesplModel;

    // Para Lineas o Puntos
    double media_errorDist;
    double sd_errorDist;       

    // Para Lineas o Puntos    
    double feature_media_errorDist;
    double feature_sd_errorDist;
    double feature_media_errorAngle;
    double feature_sd_errorAngle;
};

struct Struct_ResultsMATIO {
    /*Detalles*/
    std::string nomVideo;
    int numFramesTotal;
    double videoDuration;
    double framesPerSecond;
    int numFramesIterations;

    /* ALgoritmos Features */
    std::string nomDetecDesc;
    std::string nomMatcher;
    std::string typeModel;
    std::string typeBlending;

    /*Detalles*/
    //    bool bandMosaicoOK;
    cv::Mat MosaicoImg;
    cv::Mat MosaicoMask;
    //    std::string nombreMosaico;


    /* Evaluacion modelo en cada iteracion */
    std::vector<bool> vectBandModelFound;
    std::vector<int> vectIdModelUsed;

    std::vector<double> vectPorcentajeMatchInliers;
    std::vector<double> vectPorcentajeDesplModel;

    /* < Extra > */
    std::vector<int> vectNumDetect_Lineas; // 
    std::vector<int> vectNumDetect_Puntos; // 
    std::vector<int> vectNumMatch_Lineas; // 
    std::vector<int> vectNumMatch_Puntos; // 
    std::vector<int> vectNumInlier_Lineas; // 
    std::vector<int> vectNumInlier_Puntos; // 
    
    /* < Desempeño en general > */
    std::vector<int> vectNum_Features_iter; // Numero Features
    std::vector<int> vectNum_Matches_iter; // Numero Matches
    std::vector<int> vectNum_Inliers_iter; // Numero inliers
    std::vector<int> vectNum_PuntosValidos_iter; // Numero inliers

    // Error Puntos validos
    std::vector<double> vectMean_distanceInliers_iter; // Error Distancia
    std::vector<double> vectSD_distanceInliers_iter; // Error Distancia
        
    // Error Features 
    std::vector<double> vectMean_Feature_distanceInliers_iter; // Error Distancia
    std::vector<double> vectMean_Feature_angleInliers_iter; // Error Angulo    

    std::vector<double> vectSD_Feature_distanceInliers_iter; // Error Distancia
    std::vector<double> vectSD_Feature_angleInliers_iter; // Error Angulo

    /* Numero imagenes utilizadas en el mosaico */
    int numImgsUtilizadasMosaicing;

    /* Tiempo*/
    std::vector<std::string> vectTiemposEjecucion_label;
    std::vector<std::vector<double> > vectTiemposEjecucion_Total_Iter;
    double runtimeStitchingBlending;
    double totalRunTime;

};

#endif /* STRUCT_RESULTADOS_HPP */


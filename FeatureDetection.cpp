/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FeatureDetection.cpp
 * Author: luis
 * 
 * Created on 6 de junio de 2018, 06:45 PM
 */

#include "FeatureDetection.hpp"
#include "LinePointFunctions.hpp"
#include "BasicFunctions.hpp"
#include "TimeControl.hpp"
#include "Struct_Parametros.hpp"
#include "Struct_Resultados.hpp"


#include <opencv2/xfeatures2d.hpp>
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include <opencv2/line_descriptor.hpp>
#include "opencv2/core/utility.hpp"
#include <opencv2/imgproc.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/highgui.hpp>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>
#include <fstream>
#include <string.h>

#include <time.h>
#include <sys/time.h>

FeatureDetection::FeatureDetection(ParametrosEjecucion& parametros) {
    this->parametros = parametros;
} //

FeatureDetection::FeatureDetection(const FeatureDetection& orig) {
} //

FeatureDetection::~FeatureDetection() {
} //

vector<cv::KeyPoint> selectNKeypointsMaxResponse(int maxNumFeatures, std::vector<cv::KeyPoint>& vectKeypoints) {
    /* <> */
    vector<cv::KeyPoint>::const_iterator first = vectKeypoints.begin();
    vector<cv::KeyPoint>::const_iterator last = vectKeypoints.begin() + maxNumFeatures;
    vector<cv::KeyPoint> newVectKeypoints(first, last);

    return newVectKeypoints;
} //function

std::vector<cv::line_descriptor::KeyLine> selectNKeylinesMaxResponse(int maxNumFeatures,
        std::vector<cv::line_descriptor::KeyLine> vectKeylines) {
    /* <> */
    std::vector<cv::line_descriptor::KeyLine>::const_iterator first = vectKeylines.begin();
    std::vector<cv::line_descriptor::KeyLine>::const_iterator last = vectKeylines.begin() + maxNumFeatures;

    std::vector<cv::line_descriptor::KeyLine> newVect(first, last);
    return newVect;
} // function

double FeatureDetection::detectDescribe_LineSegments(Struct_Matching& structMatch) {
    /*+++++++++++++++++++++++++++++++++++++++*/
    /* Midiendo tiempos */
    TimeControl tc;
    tc.start(); // <--------
    /*+++++++++++++++++++++++++++++++++++++++*/

    /* load image */
    if ((structMatch.imageMat_test.data == NULL) || (structMatch.imageMat_refe.data == NULL)) {
        std::cout << "\n**** Error, images could not be loaded. Please, check their path ****" << std::endl;
        exit(0);
    } // if

    /* <~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~> */
    /* < Deteccion y Descripcion > */
    /* <~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~> */
    std::string nomDetecDesc;
    detectAndDescribe_KeyLines(structMatch.imageMat_test,
            structMatch.imageMat_refe,
            structMatch.structLineas.vectLinesDetect_test,
            structMatch.structLineas.vectLinesDetect_refe,
            structMatch.structLineas.matLinesDescr_test,
            structMatch.structLineas.matLinesDescr_refe,
            structMatch.structLineas.numLinesDetec_test,
            structMatch.structLineas.numLinesDetec_refe,
            nomDetecDesc,
            this->parametros.Features.maxNumFeatures);

    /*+++++++++++++++++++++++++++++++++++++++*/
    return tc.finish(false, "");
    /*+++++++++++++++++++++++++++++++++++++++*/
} // function

double FeatureDetection::detectDescribe_KeyPoints(Struct_Matching& structMatch) {
    /*+++++++++++++++++++++++++++++++++++++++*/
    /* Midiendo tiempos */
    TimeControl tc;
    tc.start(); // <--------
    /*+++++++++++++++++++++++++++++++++++++++*/

    /* load image */
    if ((structMatch.imageMat_test.data == NULL) || (structMatch.imageMat_refe.data == NULL)) {
        std::cout << "\n**** Error, images could not be loaded. Please, check their path ****" << std::endl;
        exit(0);
    } // if

    /* <~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~> */
    /* < Deteccion y Descripcion > */
    /* <~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~> */
    std::string nomDetecDesc;
    detectAndDescribe_KeyPoints(structMatch.imageMat_test,
            structMatch.imageMat_refe,
            structMatch.structPuntos.vectKeypointsDetect_test,
            structMatch.structPuntos.vectKeypointsDetect_refe,
            structMatch.structPuntos.matPointsDescr_test,
            structMatch.structPuntos.matPointsDescr_refe,
            structMatch.structPuntos.numPointsDetec_test,
            structMatch.structPuntos.numPointsDetec_refe,
            nomDetecDesc,
            this->parametros.Features.idDetecDescr,
            this->parametros.Features.maxNumFeatures);

    /*+++++++++++++++++++++++++++++++++++++++*/
    return tc.finish(false, "");
    /*+++++++++++++++++++++++++++++++++++++++*/
} // function

void detectAndDescribe_KeyLines(cv::Mat & imageMat_test,
        cv::Mat & imageMat_refe,
        std::vector<cv::line_descriptor::KeyLine> & keylines_test,
        std::vector<cv::line_descriptor::KeyLine> & keylines_refe,
        cv::Mat & descr_test, cv::Mat & descr_refe,
        int & numDetect_features_test, int & numDetect_features_refe,
        std::string & nomDetecDesc,
        int maxNumFeatures) {
    /* <> */
    bool banRecalcular = false;
    int idDetecDescr = 0; // id Del detector/descriptor

    /* <================================ TEST ================================ > */
    /* Calculando lineas y describiendolas en TEST si es que no se han hecho */
    detectAndDescribe_KeyLines(imageMat_test,
            keylines_test, descr_test, nomDetecDesc,
            idDetecDescr, maxNumFeatures);

    numDetect_features_test = keylines_test.size();
    /* <================================ REFE ================================> */
    /* Calculando lineas y describiendolas en REFE si es que no se han hecho */
    if ((keylines_refe.size() == 0) || banRecalcular) {
        detectAndDescribe_KeyLines(imageMat_refe,
                keylines_refe, descr_refe, nomDetecDesc,
                idDetecDescr, maxNumFeatures);
    } // if    
    numDetect_features_refe = keylines_refe.size();
} // function

void detectAndDescribe_KeyLines(cv::Mat & imageMat,
        std::vector<cv::line_descriptor::KeyLine> & keylines,
        cv::Mat & descr, std::string & nomDetecDesc,
        int idDetecDescr, int maxNumFeatures) {
    // Creando objetos para detectar y describir
    cv::Ptr<cv::line_descriptor::LSDDetector> lsd;
    cv::Ptr<cv::line_descriptor::BinaryDescriptor> bd;

    int scale = 2;
    int numOctaves = 2;

    // < Limpiando vector >
    keylines.clear();

    // Desidiendo que detector/descriptor utilizar 
    switch (idDetecDescr) {
        case 0:
        {
            // < Line Band Descriptor >
            nomDetecDesc = "LBD"; // nombre del detector/descriptor   
            bd = cv::line_descriptor::BinaryDescriptor::createBinaryDescriptor();

            // < Detectando Lineas >
            bd -> detect(imageMat, keylines);
            break;
        } // case
        case 1:
        {
            // < Line Segment Descriptor >
            nomDetecDesc = "LSD"; // nombre del detector/descriptor
            lsd = cv::line_descriptor::LSDDetector::createLSDDetector();

            // < Detectando Lineas >
            lsd -> detect(imageMat, keylines, scale, numOctaves);
            break;
        } // case 
    } // switch 

    /* < Rescantando los n (maxNumFeatures) features mas importantes > */
    if (keylines.size() > maxNumFeatures) {
        keylines = selectNKeylinesMaxResponse(maxNumFeatures, keylines);
    } // if

    /* <compute descriptors> */
    bd -> compute(imageMat, keylines, descr);

} // function 

void detectAndDescribe_KeyPoints(
        cv::Mat& imageMat_test, cv::Mat& imageMat_refe,
        std::vector<cv::KeyPoint>& keypoints_test, std::vector<cv::KeyPoint>& keypoints_refe,
        cv::Mat& descr_test, cv::Mat& descr_refe,
        int & numDetect_features_test, int & numDetect_features_refe,
        std::string& nomDetDesc,
        int idDetecDescr, int maxNumFeatures) {
    /* Variables Auxiliares */
    bool banRecalcular = false;

    /* <================================ TEST ================================> */
    /* Detectar keypoints TEST */
    detectAndDescribe_KeyPoints(imageMat_test,
            keypoints_test, descr_test, nomDetDesc,
            idDetecDescr, maxNumFeatures);

    numDetect_features_test = keypoints_test.size();
    /* <================================ REFE ================================>*/
    /* Calculando puntos y describiendolas en REFE si es que no se han hecho */
    if (keypoints_refe.size() == 0 || banRecalcular) {
        /* Detectar keypoints REFE */
        detectAndDescribe_KeyPoints(imageMat_refe,
                keypoints_refe, descr_refe, nomDetDesc,
                idDetecDescr, maxNumFeatures);
    } // if 

    numDetect_features_refe = keypoints_refe.size();
}// function 

void detectAndDescribe_KeyPoints(cv::Mat& imageMat,
        std::vector<cv::KeyPoint>& keypoints, cv::Mat& descr,
        std::string& nomDetDesc,
        int idDetecDescr, int maxNumFeatures) {
    // < Limpiando vector >
    keypoints.clear();

    /* < Variables Auxiliares > */
    cv::Ptr<cv::Feature2D> detector;

    switch (idDetecDescr) {
        case 0: /* SURF */
        {
            nomDetDesc = "SURF";
            /* Crear variable para detectar keypoints por SURF */
            detector = cv::xfeatures2d::SURF::create();
            break;
        } //case
        case 1: /* SIFT */
        {
            nomDetDesc = "SIFT";
            /* Crear variable para detectar keypoints por SIFT */
            detector = cv::xfeatures2d::SIFT::create(maxNumFeatures);
            break;
        } //case
        default:
        case 2: /* ORB */
        {
            nomDetDesc = "ORB";
            /* Crear variable para detectar keypoints por ORB */
            detector = cv::ORB::create(maxNumFeatures);
            break;
        } //case
    }//switch

    /* < Detectar keypoints TEST > */
    detector->detect(imageMat, keypoints);

    if (keypoints.size() > maxNumFeatures) {
        keypoints = selectNKeypointsMaxResponse(maxNumFeatures, keypoints);
    } //if 

    /* < Calcular los descriptores TEST > */
    detector->compute(imageMat, keypoints, descr);
}// function 
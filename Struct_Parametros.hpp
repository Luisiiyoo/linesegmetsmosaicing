/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Struct_Parametros.hpp
 * Author: luis
 *
 * Created on 1 de noviembre de 2017, 11:20 AM
 */

#ifndef STRUCT_PARAMETROS_HPP
#define STRUCT_PARAMETROS_HPP

#include <iostream>
#include <fstream>
#include <string.h>
#include <vector>

using namespace std;

struct Parametros_RANSAC {
    /* <Parametros de control para Ransac> */
    // <Numero de muestras RANSAC>
    //const static int numMinMuestras = 4; // < Minmimo numero de muestras necesarias para obtener el Modelo (4 para la Homografia y 3 para la matriz afin )
    int numMinMuestras; // < Minmimo numero de muestras necesarias para obtener el Modelo (4 para la Homografia y 3 para la matriz afin )

    // <Tipo de modelos>
    int idTypeModel4Ransac; // < 0: Homography || 1: AffineTransform >    
    std::vector<std::string> vecTypeModel4Ransac; // < 0: Homography || 1: AffineTransform >    

    // <Tolerancia de error>    
    const static double tipoEvalDistancia = 0; // < 0: Distancia minima || 1: Punto medio de la linea >    
    const static double umbralDistancia = 3; // < Maximo error en Distancia permitido para decicir si la muestra es un inlier
    const static double umbralAngulo = 2; // < Maximo error en Angulo permitido para decicir si la muestra es un inlier
    const static double radioDeInliners = 0.10; // < Porcentaje de numero de inliers para decidir si se acepta la iteracion como una valida
    const static double porcentajeInlinersHomo = 0.10; // < Porcentaje de numero de inliers para decidir si se acepta la iteracion como una valida

    // < Puntos de interseccion >
    const static double factorRadioToleranciaPuntoInterseccion = 2.0; // < Factor de radio de tolerancia entre el Punto de Interseccion y el centro de la imagen 
    const static int distanciaRefeTest_PI = 200; // < Distancia entre los PI permitida (NO UTILIZADO)

    // < Comportamiento de RANSAC>
    const static bool bandRansacNormal = true; // < Bandera RANSAC normal (NO UTILIZADA)
    const static bool bandRecalcularModelo = true; // < Variable para Recalcular la homografia con los puntos de interseccion de los inliers obteniedos con un modelo previo

    // <Numero Iteraciones RANSAC>
    const static double probObtenerBuenaMuestra = 0.999; // < Probabilidad deseada de obtener un buen conjunto de muestras (que sean inliers)
    const static double probElegirInlier = 0.40; // < Probabilidad de elegir una muestra y sea inlier
    int numIteraciones; // < Numero de iteraciones a realizar para buscar los INLIERS : getNumberOfIterationsRANSAC(numLineas, probElegirInlier, probObtenerBuenaMuestra);

}; //struct

struct Parametros_ControlVentanas {
    const static int msEsperarMosaico = 0; // < Milisegundos de espera despues de Mostrar el Mosaico (0: Espera tecla del usuario)
    /* Parametros para la resolucion de las imagenes o frames*/
    const static bool bandChangSize = true; // < Bandera para cambiar el tamaño de los frames si exceden un ancho deseado
    const static int widthWanted = 640; // < Ancho deseado de las imagenes
    //    const static double porcentajeDeMuestreoFrames = ;

    const static double msEsperar_mostrarVentana = 10; // < Milisegundos a esperar entre cada iteracion 
    const static bool bandMuestraVentanas_automatico = true; // < Mostrar ventanas automaticamente (No espera la tecla del usuario)

    // ****************************************
    bool bandMuestraVentanasResults; // < Muestra todas las ventanas de resultados
    // ****************************************


    const static bool bandMuestraMascaraMosaico = false; // < Muestra la mascara del mosaico generado
    const static bool bandMuestraMosaicoAcumulado = true; // < Muestra el mosaico acumulado
    const static bool bandMuestraResultadosIndividualmente = false; // < Muestra la deteccion, la correspondencia y los inliers por RANSAC

    const static bool bandMuestraDetecFeatures = true; // <Muestra las ventanas de los Features detectados
    const static bool bandShowMatch_WithWithout_RANSAC = true; // <Muestra las ventanas de la correspondencia antes y despues de RANSAC
    const static bool bandMuestraPuntosInterseccionValidos = true; // <Muestra las ventanas de los puntos de interseccion utilizados en el Modelo
    const static bool bandMuestraTransformacion = true; // <Muestra las ventanas de los features transformados a la Imagen de referencia Ajustada al desplazamiento del Canvas 
    const static bool bandMosaicingTwoImages_iter = 1; // <Muestra el Mosaico generado entre cada par de imagenes (Cada iteracion) que se contemplo en el Stitching Global del Mosaico

    bool bandDynamicKeyFrame; // < Bandera para que se conserve el Frame si su porcentaje de correspondencia esta por arriba de un umbral
    const static double porcMinMantenerImg = 40; // < Umbral de porcentaje minimo de la correspondencia para cambiar el frame o mantenerlo
    const static double porcMaxDesplazamientoModelo = 15; // < Umbral de porcentaje de maximo del desplazamiento del modelo permitidopara cambiar el frame o mantenerlo
}; //struct

struct Parametros_Mosaico {
    /* Parametros para la transformacion de las Lineas una vez ya obtenida la homografia*/

    //const static  bool bandPuntoAPunto = false;
    //const static int numPuntosCheckTrans = 5;   
    const static bool bandMosaicoAcumulado = true;

    bool bandStitching; // < Realizar StitchingBlending
    int idTypeBlending; // < 0: NO || 1: FEATHER || 2: MULTI_BAND > 
    std::vector<std::string> vecTypeBlending; // < 0: NO || 1: FEATHER || 2: MULTI_BAND > 

    const static int limitSizeCanvas = 12000; // < Maximo tamaño aceptado para generar el Mosaico Global de todas las imagenes contempladas en el StitchingBlending
}; //struct

struct Parametros_Features {
    /* Parametros para la compracion de Lineas y Puntos*/
    bool bandHacerConLineas; // < True : Utilizar Segmentos de Linea || False :  Utilizar Keypoints
    const static bool bandUseQuadTreeKP = 1; // < QuadTree de Keypoints

    int idDetecDescr; // < 0:"SURF" || 1:"SIFT"|| 2:"ORB">
    std::vector<std::string> vecNomDetecDescr; // < 0:"SURF" || 1:"SIFT"|| 2:"ORB">

    const static int idMatcher = 0; // < 0:"BruteForceMatcher" || 1:"BinaryDescriptorMatcher"|| 2:"FlannBasedMatcher">
    std::vector<std::string> vecNomMatchers; // < 0:"BruteForceMatcher" || 1:"BinaryDescriptorMatcher"|| 2:"FlannBasedMatcher">

    const static bool bandShowCOUTMessages = true; // < Bandera para mostrar Mensajes de consola     
    const static int maxNumFeatures = 2000; // < Maximo numero de Features permitidos en la Deteccion/Descripcion
};

struct Parametros_GuargarResultadosFileMAT {
    bool bandGuardarResultados; // < Bandera para guardar resultados en un archivo .MAT
    std::string dirResultados_FileMat; // < Ruta donde se almacenaran los resultados
    std::string nameMedia; // < Nombre de los archivos (Video, par de imagenes, conjunto de imagenes) utilizados
    std::string pathOfSourceMedia; // < Ruta del archivos utilizados

    std::string nameExecution; // < Nombre de la ejecucion compuesto por los datos de la ejecucion


    /* Para el par de imagenes*/
    std::string fullPathFuente_imgTest; // < Nombre + direccion Imagen Test (Opcion par de imagenes)
    std::string fullPathFuente_imgRefe; // < Nombre + direccion Imagen Refe (Opcion par de imagenes)
};

struct ParametrosEjecucion {
    Parametros_RANSAC RANSAC;
    Parametros_ControlVentanas ControlVent;
    Parametros_Mosaico Mosaic;
    Parametros_Features Features;
    Parametros_GuargarResultadosFileMAT SaveResults;
    //    Parametros_QuadTreeKP paramQuadTreeKP;
};
// < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ >
// < ~~~~~~~~~~~~~~~~~~ Funciones ~~~~~~~~~~~~~~~~~~ >
// < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ >
std::vector<std::string> opcionesConjuntoImagenes(int idSetImg, std::string dirMedia,
        std::string& stringNombres, std::string& nombreSecuencia,
        double& probElegirInlier);

std::string opcionesVideos(int idVideo, std::string dirMedia, string& fullPathFuente_video,
        double& probElegirInlier);

std::string opcionesParImagenes(int idImg, std::string dirMedia, string& fullPathFuente_imgTest,
        std::string& fullPathFuente_imgRefe, double& probElegirInlier);

#endif /* STRUCT_PARAMETROS_HPP */


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ImageStitching.hpp
 * Author: luis
 *
 * Created on 7 de junio de 2018, 08:02 PM
 */

#ifndef IMAGESTITCHING_HPP
#define IMAGESTITCHING_HPP
#include "Struct_Parametros.hpp"
#include "Struct_Resultados.hpp"

#include <opencv2/xfeatures2d.hpp>
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include <opencv2/line_descriptor.hpp>
#include "opencv2/core/utility.hpp"
#include <opencv2/imgproc.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/highgui.hpp>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "Struct_Parametros.hpp"
#include <iostream>
#include <fstream>
#include <string.h>

#include "opencv2/stitching/detail/autocalib.hpp"
#include "opencv2/stitching/detail/blenders.hpp"
#include "opencv2/stitching/detail/camera.hpp"
#include "opencv2/stitching/detail/exposure_compensate.hpp"
#include "opencv2/stitching/detail/matchers.hpp"
#include "opencv2/stitching/detail/motion_estimators.hpp"
#include "opencv2/stitching/detail/seam_finders.hpp"
#include "opencv2/stitching/detail/util.hpp"
#include "opencv2/stitching/detail/warpers.hpp"

class ImageStitching {
public:
    ImageStitching(ParametrosEjecucion& parametros, int numFramesTotal);
    ImageStitching(const ImageStitching& orig);
    virtual ~ImageStitching();

    cv::Mat stitchingTwoImages(
            cv::Mat& img_test, cv::Mat& img_test_mask,
            cv::Mat& img_refe, cv::Mat& img_refe_mask,
            cv::Mat& matHomografia,
            cv::Mat& imgMosaicMask,
            int opcionBlender, bool try_gpu = false);



    cv::Mat blendingTwoImages(cv::Mat& imgTest, cv::Mat& imgRefe,
            cv::Mat& imgTest_mask, cv::Mat& imgRefe_mask, cv::Mat& blendingMask,
            int opcionBlender, bool try_gpu);



    double ajustarHomografias(Struct_Matching& structMatch_actual);
    void agregarImagenAlMosaico(Struct_Matching& structMatch);

    double crearCanvasDeHomografiasAjustadas(cv::Mat& CanvasImg, cv::Mat& CanvasMask, int & numImagesMosaico);

    /* < Variables donde se almacenaran las Homografias > */
    /* < Desplazamiento > */
    std::vector<double> vectDespIzq_global, vectDespDer_global, vectDespArr_global, vectDespAba_global;
    /* < Imagenes - Homografias > */
    std::vector<int> vectIndexLocal;
    std::vector< cv::Mat > vectImagenes; // Numero de pares + 1 (inicial)   
    std::vector< cv::Mat > vectHomografias_parcial;
    std::vector< cv::Mat > vectHomografias_completas;

    std::vector< cv::Mat > vectTraslacion_completas_mosaicoAcumulado;
    std::vector<double> vectRuntimeMosaicing_mosaicoacumulado;

    /* <Tabla de Relaciones y verificacion de Homografia > */
    std::vector<std::vector<int> > vect2DRelacionHomografias;
    std::vector<bool> vect1DBandModelFound;
    std::vector<bool> vect1DConsiderarStitching;

    cv::Mat MosaicoAculumado_img;
    cv::Mat MosaicoAculumado_mask;
    //    double tiempoMosaicingAculumado;
    //    double numImagenesMosaicingAculumado;
    //    double runtimeMosaicingAcumulado;


    cv::Size getSizeCanvasFromImageTransformations(cv::Mat& matHomografia,
            cv::Size sizeImg_refe, cv::Size sizeImg_test,
            cv::Mat& matTraslacion, std::vector<double>& vectDesplazamiento);

    void createTextFileEspecificResults(std::string fullName,
        std::vector<std::string>& vectTiemposEjecucion_label,
        std::vector<std::vector<double> >& vectTiemposEjecucion_Total_Iter,
        double runtimeStitchingBlending,
        int numFramesTotal, int numImgsUtilizadasMosaicing);
private:

    void checkSizeMosaicGlobal(cv::Size sizeCanvas);

    cv::Mat addImageToMosaic(
            cv::Mat& newImage, cv::Mat& newImage_mask,
            cv::Mat& oldMosaico, cv::Mat& oldMosaico_mask,
            cv::Size& sizeCanvas, cv::Mat& matHomografia_ajustada,
            cv::Mat& matTraslacion_ajustada,
            cv::Mat& newMosaicMask,
            int opcionBlender, bool try_gpu);

    cv::Size getSizeCanvasAndMatTraslacion(cv::Size sizeImagen,
            cv::Mat& matTraslacion, std::vector<double>& vectMaxDesplazamiento);

    cv::Mat stitchingImages(std::vector<cv::Mat>& vectImagenes,
            std::vector<cv::Mat>& vectHomografias, cv::Mat& matTraslacion,
            cv::Size sizeCanvas,
            cv::Mat& CanvasMask, int opcionBlender, bool try_gpu = false);

    cv::Mat blendingImages(cv::Size& sizeCanvas, std::vector<cv::Mat>& vectMatImages,
            std::vector<cv::Mat>& vectMatImages_mask,
            cv::Mat& blendingMask, int opcionBlender, bool try_gpu);

   
    std::vector<cv::Point2f> getCornersImage(int width, int height);

    std::vector<cv::Point2f> transformPointsByMatTransformation(std::vector<cv::Point2f> & points, cv::Mat& matTransformation);

    cv::Size getSizeSquareFromPoints(std::vector<cv::Point2f>& points);

    std::vector<cv::Point2f> transformCornersImageByTransformationMat(int width, int height,
            cv::Mat& matTransformation, cv::Size& sizeImage_trans);

    cv::Size getSizeCanvas_TransformationMat_DisplacementVector(std::vector<cv::Point2f> points,
            cv::Size imgSize, cv::Mat& matTraslacion,
            std::vector<double>& vectDesplazamiento,
            std::vector<double>& vectExtremeCordinates);


    cv::Ptr<cv::detail::Blender> createBlenderObject(int opcionBlender = 1, bool try_gpu = false);

    /* ------------------------------------------- */
    /* ------------------------------------------- */
    /* < Parametros Y estructura para guardar resultados > */
    ParametrosEjecucion parametros;
};


#endif /* IMAGESTITCHING_HPP */


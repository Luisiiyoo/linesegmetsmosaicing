/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "Struct_Parametros.hpp"
#include "LinePointFunctions.hpp"
#include "BasicFunctions.hpp"

#include <iostream>
#include <fstream>
#include <string.h>
#include <sys/stat.h>   // For stat().
#include <sys/types.h>
using namespace std;

std::vector<std::string> opcionesConjuntoImagenes(int idSetImg, std::string dirMedia,
        std::string& stringNombres, std::string& nombreSecuencia,
        double& probElegirInlier) {
    /* Variables */
    std::string subDir = "";
    std::string secuenciaFiles = "";
    stringNombres = "";
    nombreSecuencia = "";

    std::string fullPathFiles;
    std::vector<std::string> vectImagenes;

    //double probElegirInlier; // Depende de cada imagen 
    probElegirInlier = 0.50;

    std::string extraName = "SecuenciaImgs-";
    switch (idSetImg) {
        case 0:
            subDir = "0_INAOE_Cupula/";
            secuenciaFiles = "vuelocupula_%04d.png";
            nombreSecuencia = extraName + "inaoe_vuelocupula";
            break;
        case 1:
            subDir = "1_INAOE_Giro/";
            secuenciaFiles = "vueloinaoe_%04d.png";
            nombreSecuencia = extraName + "inaoe_vuelogiro";
            break;
        case 2:
            subDir = "2_INAOE_Vuelo2/";
            secuenciaFiles = "vuelo2_%04d.png";
            nombreSecuencia = extraName + "inaoe_subvuelo2";
            break;
        case 3:
            subDir = "flir3_completo/";
            secuenciaFiles = "flir3_%04d.png";
            nombreSecuencia = extraName + "inaoe_flir3-completo";
            break;
        case 4:
            subDir = "flir3_part1/";
            secuenciaFiles = "flir3_%04d.png";
            nombreSecuencia = extraName + "inaoe_flir3p1";
            break;
        case 5:
            subDir = "flir3_part2/";
            secuenciaFiles = "flir3_%04d.png";
            nombreSecuencia = extraName + "inaoe_flir3p2";
            break;
        case 6:
            subDir = "flir4/";
            secuenciaFiles = "flir4_%04d.png";
            nombreSecuencia = extraName + "inaoe_flir4-completo";
            break;
        case 7:
            subDir = "pktest02/";
            secuenciaFiles = "frame%05d.jpg";
            nombreSecuencia = extraName + "pktest02";
            break;
        case 8:
            subDir = "Turbina/";
            secuenciaFiles = "inspection_%04d.png";
            nombreSecuencia = extraName + "manual_control";
            break;
        case 9:
            subDir = "Turbina/";
            fullPathFiles = dirMedia + subDir;

            std::vector<std::string> vectNamesImages = getFilesInDirectory(fullPathFiles);

            secuenciaFiles = getNameSequence(fullPathFiles, vectNamesImages);
            nombreSecuencia = extraName + "automaticNames";
            break;
    } // switch

    fullPathFiles = dirMedia + subDir;
    stringNombres = fullPathFiles + secuenciaFiles;

    /* Obteniendo la lista de archivos en el directorio especificado */
    vectImagenes = getFilesInDirectory(fullPathFiles);
    printVectorStrings_Directorios(vectImagenes, fullPathFiles);
    return vectImagenes;
} // function

std::string opcionesParImagenes(int idImg, std::string dirMedia,
        string& fullPathFuente_imgTest,
        std::string& fullPathFuente_imgRefe, double& probElegirInlier) {
    probElegirInlier = 0.50;
    std::string subDir = "";
    std::string namefile_test = "";
    std::string namefile_refe = "";
    std::string nomParImagenes = "";
    switch (idImg) {
        case 0:
            subDir = "0_ParVisible/";
            namefile_test = "im1.jpg";
            namefile_refe = "im2.jpg";
            nomParImagenes = "EdificiosVisible";
            break;
        case 1:
            subDir = "1_ParTermicoGiro/";
            namefile_test = "vueloinaoe_0010.png";
            namefile_refe = "vueloinaoe_0013.png";
            nomParImagenes = "VueloInaoe_0010-0013";
            break;
    }//switch
    fullPathFuente_imgTest = dirMedia + subDir + namefile_test;
    fullPathFuente_imgRefe = dirMedia + subDir + namefile_refe;
    return nomParImagenes;
} //function

std::string opcionesVideos(int idVideo, std::string dirMedia,
        string& fullPathFuente_video,
        double& probElegirInlier) {
    probElegirInlier = 0.50;
    std::string subDir;
    std::string namefile = "";

    switch (idVideo) {
            //        case 0:            
            //            subDir = "0_Cupula/";
            //            namefile = "inaoe_vuelocupula.mp4";
            //            break;
        case 1:
            subDir = "1_Giro/";
            namefile = "inaoe_vuelogiro.mp4";
            break;
        case 2:
            subDir = "2_Vuelo2/";
            namefile = "inaoe_vuelo2.mp4";
            break;
    } //switch    

    fullPathFuente_video = dirMedia + subDir + namefile;
    /* <~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~> */
    return namefile;
}// function

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FeatureMosaicing.cpp
 * Author: luis
 * 
 * Created on 6 de junio de 2018, 01:18 PM
 */

#include "FeatureMosaicing.hpp"
#include "Struct_Parametros.hpp"
#include "Struct_Resultados.hpp"
#include "TimeControl.hpp"
#include "LinePointFunctions.hpp"
#include "BasicFunctions.hpp"

#include "FeatureDetection.hpp"
#include "FeatureMatching.hpp"
#include "RANSAC.hpp"
#include "ImageStitching.hpp"
#include "SavingResultsMatio.hpp"
#include "GraphicalResults.h"

#include "QuadTree_Keypoints.h"

FeatureMosaicing::FeatureMosaicing(ParametrosEjecucion& parametros) {
    // < Recibiendo parametros globales >
    this->parametros = parametros;

    // < Estableciendo numero de muestras de acuerdo al modelo de transformacion>
    switch (this->parametros.RANSAC.idTypeModel4Ransac) {
        case 0:
            // <Homografia>
            this->parametros.RANSAC.numMinMuestras = 4;
            break;
        case 1:
            // <Afin>
            this->parametros.RANSAC.numMinMuestras = 3;
            break;
    } // switch

    /* < Rellenando vectores >*/
    this->parametros.RANSAC.vecTypeModel4Ransac.push_back("Homography");
    this->parametros.RANSAC.vecTypeModel4Ransac.push_back("AffineTransform");

    this->parametros.Features.vecNomMatchers.push_back("BruteForceMatcher");
    this->parametros.Features.vecNomMatchers.push_back("BinaryDescriptorMatcher");
    this->parametros.Features.vecNomMatchers.push_back("FlannBasedMatcher");

    this->parametros.Features.vecNomDetecDescr.push_back("SURF");
    this->parametros.Features.vecNomDetecDescr.push_back("SIFT");
    this->parametros.Features.vecNomDetecDescr.push_back("ORB");

    this->parametros.Mosaic.vecTypeBlending.push_back("No");
    this->parametros.Mosaic.vecTypeBlending.push_back("Feather");
    this->parametros.Mosaic.vecTypeBlending.push_back("MultiBand");

    // < Vector de nombre de las fases en el proceso de creacion de mosaicos >
    std::string arr[] = {"t_ReadFrames", "t_DetecDesc",
        "t_Match", "t_RANSAC", "t_QuadTree",
        "t_AjustModel"}; // 
    this->struResultMatio.vectTiemposEjecucion_label = std::vector<std::string>(arr, arr + sizeof (arr) / sizeof (arr[0])); // conviriendo a std::vector

    // < Creando Objeto para graficar resultados >
    GraphicalResults gr(this->parametros);
    this->graphicalResults = gr;
} // constructor

FeatureMosaicing::~FeatureMosaicing() {
} // destructor

void FeatureMosaicing::estableceNumeroIteracionesRANSAC() {
    /* <Calculando Numero de iteraciones RANSAC> */
    //    parametros.paramRANSAC.numIteraciones = getNumberOfIterationsRANSAC(
    //            parametros.paramRANSAC.numMinMuestras,
    //            parametros.paramRANSAC.probElegirInlier,
    //            parametros.paramRANSAC.probObtenerBuenaMuestra);

    //parametros.RANSAC.numIteraciones = 266;
    parametros.RANSAC.numIteraciones = 143;

} // Function 

void FeatureMosaicing::pasaVariables2Struct(int numFramesTotal, double framesPerSecond) {
    /* <Pasando variables a la estructura de resultados > */
    /* Rellenando parametros */
    this->struResultMatio.nomVideo = parametros.SaveResults.nameMedia;
    this->struResultMatio.numFramesTotal = numFramesTotal; //numeroTotalImagenes
    this->struResultMatio.numFramesIterations = struResultMatio.numFramesTotal - 1; //numParFrames
    this->struResultMatio.framesPerSecond = framesPerSecond;
    this->struResultMatio.videoDuration = struResultMatio.numFramesTotal / struResultMatio.framesPerSecond; //duracionTiempoVideo

    /* <Opteniendo los nombres de los algoritmos seleccionados> */
    this->struResultMatio.nomDetecDesc = ((this->parametros.Features.bandHacerConLineas) ? "LBD" : this->parametros.Features.vecNomDetecDescr[this->parametros.Features.idDetecDescr]);
    this->struResultMatio.nomMatcher = this->parametros.Features.vecNomMatchers[this->parametros.Features.idMatcher];
    this->struResultMatio.typeModel = this->parametros.RANSAC.vecTypeModel4Ransac[this->parametros.RANSAC.idTypeModel4Ransac];
    this->struResultMatio.typeBlending = this->parametros.Mosaic.vecTypeBlending[this->parametros.Mosaic.idTypeBlending];

    // < Vector 2D para tiempos de ejecucion >    
    std::vector<std::vector<double> > vect2dAux(this->struResultMatio.numFramesIterations,
            std::vector<double>(this->struResultMatio.vectTiemposEjecucion_label.size()));
    this->struResultMatio.vectTiemposEjecucion_Total_Iter = vect2dAux;
} // function 

void FeatureMosaicing::passStructMatchResults2StructMatio(Struct_Matching& structMatch) {
    // Definiendo tamaño de los vectores 
    if (structMatch.iterFramePares == 0) {
        int numIteraciones = this->struResultMatio.numFramesIterations;

        this->struResultMatio.vectBandModelFound = std::vector<bool>(numIteraciones);
        this->struResultMatio.vectIdModelUsed = std::vector<int>(numIteraciones);
        this->struResultMatio.vectNum_Features_iter = std::vector<int>(numIteraciones);
        this->struResultMatio.vectNum_Matches_iter = std::vector<int>(numIteraciones);
        this->struResultMatio.vectNum_Inliers_iter = std::vector<int>(numIteraciones);
        this->struResultMatio.vectNum_PuntosValidos_iter = std::vector<int>(numIteraciones);

        // Criterios de evaluacion
        this->struResultMatio.vectPorcentajeMatchInliers = std::vector<double>(numIteraciones);
        this->struResultMatio.vectPorcentajeDesplModel = std::vector<double>(numIteraciones);

        // Error de puntos validos
        this->struResultMatio.vectMean_distanceInliers_iter = std::vector<double>(numIteraciones);
        this->struResultMatio.vectSD_distanceInliers_iter = std::vector<double>(numIteraciones);

        // Error de Features 
        this->struResultMatio.vectMean_Feature_distanceInliers_iter = std::vector<double>(numIteraciones);
        this->struResultMatio.vectMean_Feature_angleInliers_iter = std::vector<double>(numIteraciones);
        this->struResultMatio.vectSD_Feature_distanceInliers_iter = std::vector<double>(numIteraciones);
        this->struResultMatio.vectSD_Feature_angleInliers_iter = std::vector<double>(numIteraciones);

        // < Extra >
        this->struResultMatio.vectNumDetect_Lineas = std::vector<int>(numIteraciones);
        this->struResultMatio.vectNumDetect_Puntos = std::vector<int>(numIteraciones);
        this->struResultMatio.vectNumMatch_Lineas = std::vector<int>(numIteraciones);
        this->struResultMatio.vectNumMatch_Puntos = std::vector<int>(numIteraciones);
        this->struResultMatio.vectNumInlier_Lineas = std::vector<int>(numIteraciones);
        this->struResultMatio.vectNumInlier_Puntos = std::vector<int>(numIteraciones);
    } // 
    /* <Almacenando Resultados> */
    // Guardando variables extra 
    this->struResultMatio.vectNumDetect_Lineas[structMatch.iterFramePares] = structMatch.numDetect_Lineas;
    this->struResultMatio.vectNumDetect_Puntos[structMatch.iterFramePares] = structMatch.numDetect_Puntos;
    this->struResultMatio.vectNumMatch_Lineas[structMatch.iterFramePares] = structMatch.numMatches_Lineas;
    this->struResultMatio.vectNumMatch_Puntos[structMatch.iterFramePares] = structMatch.numMatches_Puntos;
    this->struResultMatio.vectNumInlier_Lineas[structMatch.iterFramePares] = structMatch.numInliers_Lineas;
    this->struResultMatio.vectNumInlier_Puntos[structMatch.iterFramePares] = structMatch.numInliers_Puntos;

    // Indicadores
    this->struResultMatio.vectBandModelFound[structMatch.iterFramePares] = structMatch.bandModelFound;
    this->struResultMatio.vectIdModelUsed[structMatch.iterFramePares] = structMatch.idModelUsed;

    // Criterios de evaluacion
    this->struResultMatio.vectPorcentajeMatchInliers[structMatch.iterFramePares] = structMatch.porcentajeMatchInliers;
    this->struResultMatio.vectPorcentajeDesplModel[structMatch.iterFramePares] = structMatch.porcentajeDesplModel;

    this->struResultMatio.vectNum_Features_iter[structMatch.iterFramePares] = structMatch.numFeaturesDetect;
    this->struResultMatio.vectNum_Matches_iter[structMatch.iterFramePares] = structMatch.numMatches;
    this->struResultMatio.vectNum_Inliers_iter[structMatch.iterFramePares] = structMatch.numInliers;

    this->struResultMatio.vectNum_PuntosValidos_iter[structMatch.iterFramePares] = structMatch.numPuntosValidos;


    // Error de puntos validos
    this->struResultMatio.vectMean_distanceInliers_iter[structMatch.iterFramePares] = structMatch.media_errorDist;
    this->struResultMatio.vectSD_distanceInliers_iter[structMatch.iterFramePares] = structMatch.sd_errorDist;

    // Error de Features
    this->struResultMatio.vectMean_Feature_distanceInliers_iter[structMatch.iterFramePares] = structMatch.feature_media_errorDist;
    this->struResultMatio.vectSD_Feature_distanceInliers_iter[structMatch.iterFramePares] = structMatch.feature_sd_errorDist;

    if (structMatch.idModelUsed == 1) {
        this->struResultMatio.vectMean_Feature_angleInliers_iter[structMatch.iterFramePares] = structMatch.feature_media_errorAngle;
        this->struResultMatio.vectSD_Feature_angleInliers_iter[structMatch.iterFramePares] = structMatch.feature_sd_errorAngle;
    } else {
        this->struResultMatio.vectMean_Feature_angleInliers_iter[structMatch.iterFramePares] = 0;
        this->struResultMatio.vectSD_Feature_angleInliers_iter[structMatch.iterFramePares] = 0;
    } // if - else
}// function

void FeatureMosaicing::getResultsStructMatch(bool bandLines, Struct_Matching& structMatch) {
    if (bandLines) {
        structMatch.idModelUsed = 1;
        structMatch.bandModelFound = structMatch.structLineas.bandModeloFound_ip;
        structMatch.Model = structMatch.structLineas.Model_ip.clone();

        structMatch.numFeaturesDetect = structMatch.structLineas.vectLinesDetect_test.size();
        structMatch.numMatches = structMatch.structLineas.vectMatches.size();
        structMatch.numInliers = structMatch.structLineas.vectLinesMatch_Inliers_test.size();
        structMatch.numPuntosValidos = structMatch.structLineas.vectLS_PI_test.size(); // Puntos de interseccion validos 

        /* Obteniendo Media y Desviacion estandar*/
        if (structMatch.bandModelFound) {
            // Error Distancia (Media y SD) <Lineas>
            std::vector<double>vectErrorDist_Inliers = getElementsFromVector(structMatch.structLineas.vectDist_lines, structMatch.structLineas.vectIndexMatch_Inliers_lines);
            structMatch.feature_sd_errorDist = getDesviacionEstandar(vectErrorDist_Inliers, structMatch.feature_media_errorDist);

            // Error Angulo (Media y SD) <Lineas>
            std::vector<double> vectErrorAngle_Inliers = getElementsFromVector(structMatch.structLineas.vectAngle_lines, structMatch.structLineas.vectIndexMatch_Inliers_lines);
            structMatch.feature_sd_errorAngle = getDesviacionEstandar(vectErrorAngle_Inliers, structMatch.feature_media_errorAngle);

            // Distandia de los puntos de interseccion  <IP>
            structMatch.sd_errorDist = getDesviacionEstandar(structMatch.structLineas.vectDist_LS_PI, structMatch.media_errorDist);

            if (isinf(structMatch.feature_sd_errorAngle) || isnan(structMatch.feature_media_errorAngle)) {
                printVector_comoVariableMatlab(vectErrorAngle_Inliers, "VectAngulos");
            }// if 
            if (isinf(structMatch.feature_sd_errorDist) || isnan(structMatch.feature_media_errorDist)) {
                printVector_comoVariableMatlab(vectErrorDist_Inliers, "VectAngulos");
            }// if 
        }//if
    } else {
        structMatch.idModelUsed = 0;
        structMatch.bandModelFound = structMatch.structPuntos.bandModeloFound_kp;
        structMatch.Model = structMatch.structPuntos.Model_kp.clone();

        structMatch.numFeaturesDetect = structMatch.structPuntos.vectKeypointsDetect_test.size();
        structMatch.numMatches = structMatch.structPuntos.vectMatches.size();
        structMatch.numInliers = structMatch.structPuntos.vectPointsMatch_Inliers_tran.size();

        /* Obteniendo Media y Desviacion estandar*/
        if (structMatch.bandModelFound) {
            // Error Distancia (Media y SD)
            std::vector<double> vectErrorDist_Inliers = getElementsFromVector(structMatch.structPuntos.vectDist_points, structMatch.structPuntos.vectIndexMatch_Inliers_points);
            structMatch.sd_errorDist = getDesviacionEstandar(vectErrorDist_Inliers, structMatch.media_errorDist);

            structMatch.feature_sd_errorDist = structMatch.sd_errorDist;
            structMatch.feature_media_errorDist = structMatch.media_errorDist;

            // Error Angulo (Media y SD)
            structMatch.feature_sd_errorAngle = 0;
            structMatch.feature_media_errorAngle = 0;
        }// if 
    } // if - else

    /* < Obteniendo porcentajes >*/
    std::pair<double, double > pair = getPorcentajesModel(structMatch.bandModelFound,
            structMatch.Model, this->averageSizeImage,
            structMatch.numMatches, structMatch.numInliers);

    structMatch.porcentajeMatchInliers = pair.first;
    structMatch.porcentajeDesplModel = pair.second;

    /* < Si no se encontro el modelo entonces : > */
    if (!structMatch.bandModelFound) {
        structMatch.sd_errorDist = INFINITY;
        structMatch.media_errorDist = INFINITY;

        structMatch.feature_sd_errorAngle = INFINITY;
        structMatch.feature_media_errorAngle = INFINITY;
        structMatch.feature_sd_errorDist = INFINITY;
        structMatch.feature_media_errorDist = INFINITY;
    } // if - else 
} // function 

double FeatureMosaicing::assignFrames(int ii, cv::VideoCapture& vidCapt,
        Struct_Matching& structMatch,
        Struct_Matching & structMatch_anterior) {
    /*+++++++++++++++++++++++++++++++++++++++*/
    TimeControl tc_iter_0;
    tc_iter_0.start(); // <--------    
    /*+++++++++++++++++++++++++++++++++++++++*/

    // Asignando el numero de iteracion 
    structMatch.iterFramePares = ii;

    // Tomando acciones de acuerdo a la iteracion 
    if (structMatch.iterFramePares == 0) {
        printf("Porcentaje Match (Inicial) = %0.3g \n", 100.00);
        structMatch.idFrame_refe = vidCapt.get(CV_CAP_PROP_POS_FRAMES);
        vidCapt >> structMatch.imageMat_refe; // El frame anterior o el que se mantuvo es REFE

        this->averageSizeImage = (structMatch.imageMat_refe.cols + structMatch.imageMat_refe.rows) / 2;

        structMatch.idFrame_test = vidCapt.get(CV_CAP_PROP_POS_FRAMES);
        vidCapt >> structMatch.imageMat_test; // el frame actual es TEST


        /* ~~~~~~~~~~~~~~~~~~~ */
        // <Anterior>
        structMatch_anterior.porcentajeMatchInliers = 100;
        // Imagen TEST
        structMatch_anterior.idFrame_test = structMatch.idFrame_test;
        structMatch_anterior.imageMat_test = structMatch.imageMat_test.clone();

        // <Homografia>
        if (this->parametros.Features.bandHacerConLineas) {
            structMatch_anterior.structLineas.Model_ip = cv::Mat::eye(3, 3, CV_64F);
        } else {
            structMatch_anterior.structPuntos.Model_kp = cv::Mat::eye(3, 3, CV_64F);
        } // if - else

    } else {
        printf("Porcentaje Match (Anterior Iteracion) = %0.3g \n", structMatch_anterior.porcentajeMatchInliers);

        bool bandCambiarFrame = (structMatch_anterior.bandConsiderarStitching);

        structMatch.idFrame_refe = ((bandCambiarFrame) ? structMatch_anterior.idFrame_test : structMatch_anterior.idFrame_refe); // idFrame
        structMatch.imageMat_refe = ((bandCambiarFrame) ? structMatch_anterior.imageMat_test.clone() : structMatch_anterior.imageMat_refe.clone()); // imagen

        // < Eligiendo Por Lineas o por Puntos >
        if (this->parametros.Features.bandHacerConLineas) {
            structMatch.structLineas.vectLinesDetect_refe = ((bandCambiarFrame) ? structMatch_anterior.structLineas.vectLinesDetect_test : structMatch_anterior.structLineas.vectLinesDetect_refe); // Features
            structMatch.structLineas.matLinesDescr_refe = ((bandCambiarFrame) ? structMatch_anterior.structLineas.matLinesDescr_test.clone() : structMatch_anterior.structLineas.matLinesDescr_refe.clone()); // Descriptor        
            structMatch.structLineas.numLinesDetec_refe = ((bandCambiarFrame) ? structMatch_anterior.structLineas.vectLinesDetect_test.size() : structMatch_anterior.structLineas.vectLinesDetect_refe.size()); // numFeatures
        } else {
            structMatch.structPuntos.vectKeypointsDetect_refe = ((bandCambiarFrame) ? structMatch_anterior.structPuntos.vectKeypointsDetect_test : structMatch_anterior.structPuntos.vectKeypointsDetect_refe); // Features
            structMatch.structPuntos.matPointsDescr_refe = ((bandCambiarFrame) ? structMatch_anterior.structPuntos.matPointsDescr_test.clone() : structMatch_anterior.structPuntos.matPointsDescr_refe.clone()); // Descriptor        
            structMatch.structPuntos.numPointsDetec_refe = ((bandCambiarFrame) ? structMatch_anterior.structPuntos.vectKeypointsDetect_test.size() : structMatch_anterior.structPuntos.vectKeypointsDetect_refe.size()); // numFeatures
        } // else if


        structMatch.idFrame_test = vidCapt.get(CV_CAP_PROP_POS_FRAMES);
        vidCapt >> structMatch.imageMat_test; // Siempre se va a estar actualizando 
    } // if-else

    /* Check is not empty the frames */
    if ((structMatch.imageMat_test.data == NULL) || (structMatch.imageMat_refe.data == NULL)) {
        cout << "\n Error: Null data in ImgTest or ImgRede.\n" << endl;
        exit(0);
    } else {
        /* <Modificiando Tamaño de los frames> */
        modificaResImg_EnBaseACols(this->parametros.ControlVent.bandChangSize,
                structMatch.imageMat_test, structMatch.imageMat_test, this->parametros.ControlVent.widthWanted);
        modificaResImg_EnBaseACols(this->parametros.ControlVent.bandChangSize,
                structMatch.imageMat_refe, structMatch.imageMat_refe, this->parametros.ControlVent.widthWanted);
    }// else - if

    cout << "Frame " << structMatch.idFrame_test << " (test) --- Frame " << structMatch.idFrame_refe << " (refe)." << endl;
    /*+++++++++++++++++++++++++++++++++++++++*/
    return tc_iter_0.finish(false, "");
    /*+++++++++++++++++++++++++++++++++++++++*/
    /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
} // function

Struct_ResultsMATIO FeatureMosaicing::Mosaicing_VideoSequence(int idMedia, std::string dirMedia) {
    // <Obteniendo Detalles del recurso elegido >
    double probElegirInlier;
    parametros.SaveResults.nameMedia = opcionesVideos(idMedia, dirMedia,
            parametros.SaveResults.pathOfSourceMedia, probElegirInlier);

    // check if exist the source 
    if (parametros.SaveResults.pathOfSourceMedia.empty()) {
        cout << "\n Error: El idMedia proporcionado no existe." << endl;
        exit(0);
    }// if

    /* < Establece numero iteraciones RANSAC>*/
    estableceNumeroIteracionesRANSAC();

    /* <Load the video>*/
    cv::VideoCapture vidCapt;
    vidCapt.open(parametros.SaveResults.pathOfSourceMedia);
    cout << "Video: " << parametros.SaveResults.pathOfSourceMedia <<
            "\n has frames size of : " << vidCapt.get(CV_CAP_PROP_FRAME_HEIGHT) <<
            " * " << vidCapt.get(CV_CAP_PROP_FRAME_WIDTH);
    cout << " and " << (int) vidCapt.get(CV_CAP_PROP_FRAME_COUNT) << " frames." << endl;

    parametros.SaveResults.nameMedia = checkNameFile(parametros.SaveResults.nameMedia);

    // <Llamando a la funcion que controla >
    Mosaicing_VideoCapture(vidCapt);

    return this->struResultMatio;
} // function 

Struct_ResultsMATIO FeatureMosaicing::Mosaicing_SetOfImages(int idMedia, std::string dirMedia) {
    // <Obteniendo Detalles del recurso elegido >
    double probElegirInlier;

    opcionesConjuntoImagenes(idMedia, dirMedia, parametros.SaveResults.pathOfSourceMedia,
            parametros.SaveResults.nameMedia, probElegirInlier);

    // check if exist the source 
    if (parametros.SaveResults.pathOfSourceMedia.empty()) {
        cout << "\n Error: El idMedia proporcionado no existe." << endl;
        exit(0);
    }// if

    /* < Establece numero iteraciones RANSAC>*/
    estableceNumeroIteracionesRANSAC();

    /* <Load the video>*/
    cv::VideoCapture vidCapt;
    vidCapt.open(parametros.SaveResults.pathOfSourceMedia);
    cout << "Set of Images : " << parametros.SaveResults.pathOfSourceMedia <<
            "\n has frames size of : " << vidCapt.get(CV_CAP_PROP_FRAME_HEIGHT) <<
            " * " << vidCapt.get(CV_CAP_PROP_FRAME_WIDTH);
    cout << " and " << (int) vidCapt.get(CV_CAP_PROP_FRAME_COUNT) << " frames." << endl;

    // < Modificando nombre del video >
    parametros.SaveResults.nameMedia = checkNameFile(parametros.SaveResults.nameMedia);

    // <Llamando a la funcion que controla >
    Mosaicing_VideoCapture(vidCapt);

    return this->struResultMatio;
} // function 

void FeatureMosaicing::printDetallesIteracion(Struct_Matching & structMatch) {
    /* <Mostrar mensajes >*/
    if (this->parametros.Features.bandShowCOUTMessages) {
        int numFeatures_test = ((this->parametros.Features.bandHacerConLineas) ? structMatch.structLineas.vectLinesDetect_test.size() : structMatch.structPuntos.vectKeypointsDetect_test.size());
        int numFeatures_refe = ((this->parametros.Features.bandHacerConLineas) ? structMatch.structLineas.vectLinesDetect_refe.size() : structMatch.structPuntos.vectKeypointsDetect_refe.size());
        int numDimDescritor = ((this->parametros.Features.bandHacerConLineas) ? structMatch.structLineas.matLinesDescr_refe.cols : structMatch.structPuntos.matPointsDescr_refe.cols);
        int numMatches = ((this->parametros.Features.bandHacerConLineas) ? structMatch.structLineas.vectMatches.size() : structMatch.structPuntos.vectMatches.size());
        int numInliers = ((this->parametros.Features.bandHacerConLineas) ? structMatch.structLineas.numInliers_lines : structMatch.structPuntos.numInliers_points);

        cout << "\nFeatures Detected: #test = " << numFeatures_test;
        cout << "  #refe = " << numFeatures_refe << endl;
        cout << "Detector/Descriptor: " << struResultMatio.nomDetecDesc;
        cout << " of size N * " << numDimDescritor << "." << endl;
        cout << "Alg. Matcher: " << struResultMatio.nomMatcher << "." << endl;
        cout << "Num. Iterations RANSAC: " << this->parametros.RANSAC.numIteraciones << "." << endl;
        cout << "Num. Matches = " << numMatches;
        cout << "  Num. Inliers = " << numInliers << endl;
        cout << "porcentajeMatch = " << structMatch.porcentajeMatchInliers << endl;
        cout << "porceDespModelo = " << structMatch.porcentajeDesplModel << endl;
    } //  if 
} // function 

std::string FeatureMosaicing::makeExecutionName() {
    // <> 
    std::string cadenaQuadTree = "QT-" + this->parametros.Features.vecNomDetecDescr[this->parametros.Features.idDetecDescr];
    std::string cs = "_";
    std::string cc = "-";
    std::string c0, c1, c2, c3, c4;
    c0 = parametros.SaveResults.nameMedia + cc +
            ((parametros.Features.bandUseQuadTreeKP) ? cadenaQuadTree : "") + cs +
            ((parametros.Features.bandHacerConLineas) ? "Lineas" : "Puntos");
    c1 = int2string(struResultMatio.numImgsUtilizadasMosaicing) + "De"
            + int2string(struResultMatio.numFramesTotal) + "Imgs" + cc +
            this->struResultMatio.nomDetecDesc + cc + this->struResultMatio.nomMatcher;
    c2 = int2string(parametros.ControlVent.porcMinMantenerImg) + "(porcMinMatch)";
    c3 = int2string(parametros.RANSAC.numIteraciones) + "(iters)" + cc +
            this->struResultMatio.typeModel + cc +
            int2string(parametros.RANSAC.umbralDistancia) + "(thDist)" + cc +
            int2string(parametros.RANSAC.umbralAngulo) + "(thAngle)";
    c4 = this->struResultMatio.typeBlending + "Blending";

    return c0 + cs + c1 + cs + c2 + cs + c3 + cs + c4;
} // function 

void FeatureMosaicing::savingResults(ImageStitching & stitcher) {
    /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
    /* < Generando nombre de la iteracion > */
    /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
    this->parametros.SaveResults.nameExecution = makeExecutionName();

    /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
    /* < Guardando Resultados en un archivo .MAT > */
    /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
    if (parametros.SaveResults.bandGuardarResultados) {
        // Generando y guardando los resultados en un archivo .mat
        SavingResultsMatio srMatio(this->parametros);
        srMatio.saveResults(struResultMatio);

        // < Especificando ruta y archivo  para el Texto>
        std::string fullName = this->parametros.SaveResults.dirResultados_FileMat +
                this->parametros.SaveResults.nameExecution + ".txt";

        // Guardando resultados en el archivo de texto
        stitcher.createTextFileEspecificResults(fullName, this->struResultMatio.vectTiemposEjecucion_label,
                this->struResultMatio.vectTiemposEjecucion_Total_Iter, this->struResultMatio.runtimeStitchingBlending,
                struResultMatio.numFramesTotal,
                struResultMatio.numImgsUtilizadasMosaicing);
    } // if 
    /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
    /* < Guardando Imagen del Mosaico > */
    /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
    if (this->struResultMatio.MosaicoImg.data != NULL) {
        // Ruta y nombre del mosaico
        std::string fullNameMosaico = this->parametros.SaveResults.dirResultados_FileMat +
                this->parametros.SaveResults.nameExecution;

        cout << "\t** Guardando Mosaico **" << endl;
        cout << fullNameMosaico << endl;

        // Verificando que tipo de mosaico se creo, el acumulado o el generado al final del proceso
        if (this->parametros.Mosaic.bandMosaicoAcumulado) {
            cv::imwrite(fullNameMosaico + "_acumulado_img.tif", stitcher.MosaicoAculumado_img);
            cv::imwrite(fullNameMosaico + "_acumulado_mask.tif", stitcher.MosaicoAculumado_mask);
        } else {
            cv::imwrite(fullNameMosaico + "_img.tif", this->struResultMatio.MosaicoImg);
            cv::imwrite(fullNameMosaico + "_mask.tif", this->struResultMatio.MosaicoMask);
        }// if - else 
    } // if 
} // function 

Struct_ResultsMATIO FeatureMosaicing::Mosaicing_PairOfImages(int idMedia, std::string dirMedia) {
    // < Opteniendo detalles de media seleccionada >
    // <Modelo>
    cv::Mat Modelo;

    double probElegirInlier;
    parametros.SaveResults.nameMedia = opcionesParImagenes(idMedia, dirMedia,
            parametros.SaveResults.fullPathFuente_imgTest,
            parametros.SaveResults.fullPathFuente_imgRefe,
            probElegirInlier);

    /* < Establece numero iteraciones RANSAC>*/
    estableceNumeroIteracionesRANSAC();

    /* < Asignando variables a la structura de resultados > */
    pasaVariables2Struct(2, -1);

    // < Creando Stitcher >
    ImageStitching stitcher(this->parametros, this->struResultMatio.numFramesTotal);

    /* <--------------------------------------------------------------------> */
    /* <--------------------------------------------------------------------> */
    /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
    /* < Lectura / Asignacion de Frames >*/
    /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
    /*+++++++++++++++++++++++++++++++++++++++*/
    TimeControl tc_iter;
    tc_iter.start(); // <--------    
    /*+++++++++++++++++++++++++++++++++++++++*/
    Struct_Matching structMatch;
    structMatch.idFrame_test = 1;
    structMatch.idFrame_refe = 0;

    // < Cargando Imagenes >
    structMatch.imageMat_refe = cv::imread(parametros.SaveResults.fullPathFuente_imgRefe, 1);
    cout << "Image of Reference: " << parametros.SaveResults.fullPathFuente_imgRefe <<
            " \n has size of : " << structMatch.imageMat_refe.rows << " * " << structMatch.imageMat_refe.cols << endl;

    structMatch.imageMat_test = cv::imread(parametros.SaveResults.fullPathFuente_imgTest, 1);
    cout << "Image of Test: " << parametros.SaveResults.fullPathFuente_imgTest <<
            "\n has size of : " << structMatch.imageMat_test.rows << " * " << structMatch.imageMat_test.cols << endl;
    /*+++++++++++++++++++++++++++++++++++++++*/
    this->struResultMatio.vectTiemposEjecucion_Total_Iter[0][0] = tc_iter.finish(false, "");
    /*+++++++++++++++++++++++++++++++++++++++*/

    /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
    /* < Deteccion / Descripcion > */
    /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
    FeatureDetection featureDet(this->parametros);
    // < ¿ Lineas o Puntos ? >
    if (this->parametros.Features.bandHacerConLineas) {
        this->struResultMatio.vectTiemposEjecucion_Total_Iter[0][1] = featureDet.detectDescribe_LineSegments(structMatch);
    } else {
        this->struResultMatio.vectTiemposEjecucion_Total_Iter[0][1] = featureDet.detectDescribe_KeyPoints(structMatch);
    } // if - else 

    /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
    /* < Matching > */
    /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
    FeatureMatching featureMatch(this->parametros);
    // < ¿ Lineas o Puntos ? >
    if (this->parametros.Features.bandHacerConLineas) {
        this->struResultMatio.vectTiemposEjecucion_Total_Iter[0][2] = featureMatch.matching_LineSegments(structMatch);
    } else {
        this->struResultMatio.vectTiemposEjecucion_Total_Iter[0][2] = featureMatch.matching_KeyPoints(structMatch);
    } // if - else

    /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
    /* < RANSAC > */
    /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
    RANSAC ransac(this->parametros);
    // < ¿ Lineas o Puntos ? >
    if (this->parametros.Features.bandHacerConLineas) {
        // < Modelo por Lineas >
        this->struResultMatio.vectTiemposEjecucion_Total_Iter[0][3] = ransac.ransac_LineSegments(structMatch);
        Modelo = structMatch.structLineas.Model_ip.clone();
    } else {
        // < Modelo por Puntos >
        this->struResultMatio.vectTiemposEjecucion_Total_Iter[0][3] = ransac.ransac_KeyPoints(structMatch);
        Modelo = structMatch.structPuntos.Model_kp.clone();
    } // if - else

    /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
    /* < Obteniendo los resultados > */
    /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
    getResultsStructMatch(this->parametros.Features.bandHacerConLineas, structMatch);

    /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
    /* < QUADTREE > */
    /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
    if (this->parametros.Features.bandUseQuadTreeKP && this->parametros.Features.bandHacerConLineas) {
        /* < QUADTREE > */
        Struct_QuadTreeKP structQuadTree;
        this->struResultMatio.vectTiemposEjecucion_Total_Iter[0][4] = createQuadTreeUsingPI_Keypoints(
                structMatch, this->parametros, structQuadTree);
        //cv::Mat newHomography = structQuadTree.Model.clone();

        cv::Mat img_test_mask = cv::Mat(structMatch.imageMat_test.size(), CV_8UC1, cv::Scalar(255)); // creando mascara para copiar los elementos 
        cv::Mat img_refe_mask = cv::Mat(structMatch.imageMat_refe.size(), CV_8UC1, cv::Scalar(255)); // creando mascara para copiar los elementos        

        cv::Mat MosaicoImg, MosaicoMask;
        MosaicoImg = stitcher.stitchingTwoImages(
                structMatch.imageMat_test, img_test_mask,
                structMatch.imageMat_refe, img_refe_mask,
                structMatch.Model, MosaicoMask,
                this->parametros.Mosaic.idTypeBlending);

        std::string titleMosaic = "Model using Line Segments and KeyPoints";
        muestraVentanaCanvas(MosaicoImg, titleMosaic, true);
        this->graphicalResults.plotingQuadTreeResults(structQuadTree);

    } else {
        this->struResultMatio.vectTiemposEjecucion_Total_Iter[0][4] = 0;
    } // if - else   

    /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
    /* < Ajuste de Homografia > */
    /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
    this->struResultMatio.vectTiemposEjecucion_Total_Iter[0][5] = 0;

    /*  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  */
    /* <Mostrar mensajes >*/
    printDetallesIteracion(structMatch);
    /*  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  */

    /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
    /* <                            GENERANDO MOSAICO                       > */
    /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
    /*+++++++++++++++++++++++++++++++++++++++*/
    TimeControl tc_iterMosaico;
    tc_iterMosaico.start(); // <--------    
    /*+++++++++++++++++++++++++++++++++++++++*/
    bool bandModeloEncontrado = ((this->parametros.Features.bandHacerConLineas) ? structMatch.structLineas.bandModeloFound_ip : structMatch.structPuntos.bandModeloFound_kp);
    if (bandModeloEncontrado) {
        /* < Realizando Stitching de las DOS Imagenes > */
        struResultMatio.numImgsUtilizadasMosaicing = 2;
        cv::Mat img_test_mask = cv::Mat(structMatch.imageMat_test.size(), CV_8UC1, cv::Scalar(255)); // creando mascara para copiar los elementos 
        cv::Mat img_refe_mask = cv::Mat(structMatch.imageMat_refe.size(), CV_8UC1, cv::Scalar(255)); // creando mascara para copiar los elementos        

        struResultMatio.MosaicoImg = stitcher.stitchingTwoImages(
                structMatch.imageMat_test, img_test_mask,
                structMatch.imageMat_refe, img_refe_mask,
                Modelo, struResultMatio.MosaicoMask,
                this->parametros.Mosaic.idTypeBlending);

        this->parametros.SaveResults.nameExecution = makeExecutionName();
    }// if 
    /*+++++++++++++++++++++++++++++++++++++++*/
    this->struResultMatio.runtimeStitchingBlending = tc_iterMosaico.finish(false, "");
    /*+++++++++++++++++++++++++++++++++++++++*/

    // <------------------------------------------->
    // <------------------------------------------->
    this->struResultMatio.totalRunTime = getSumatoriaDeVector2D(this->struResultMatio.vectTiemposEjecucion_Total_Iter) + struResultMatio.runtimeStitchingBlending;
    printf(" \n\n  Tiempo Total : %.4f s.\n", this->struResultMatio.totalRunTime);

    /* <Muestra mosaico> */
    if (this->struResultMatio.MosaicoImg.data != NULL) {

        //        this->graphicalResults.plotingFeaturesDetected(structMatch, this->struResultMatio);
        //        this->graphicalResults.plotingMatchesRANSAC(structMatch, this->struResultMatio);
        //        this->graphicalResults.plotingFeaturesInliersTransformed(stitcher, structMatch);

        cout << "\t** Mostrando Mosaico **" << endl;
        cout << this->parametros.SaveResults.nameExecution << endl;

        /* Mostrando Canvas */
        muestraVentanaCanvas(this->struResultMatio.MosaicoImg,
                this->parametros.SaveResults.nameExecution, true);
        cv::waitKey(this->parametros.ControlVent.msEsperarMosaico);
    } // if 

    return this->struResultMatio;
} // function 

void FeatureMosaicing::Mosaicing_VideoCapture(cv::VideoCapture & vidCapt) {
    /* < Asignando variables a la structura de resultados > */
    pasaVariables2Struct((int) vidCapt.get(CV_CAP_PROP_FRAME_COUNT),
            (double) vidCapt.get(CV_CAP_PROP_FPS));

    /* < Comprobando el numero de frames que trabajara el algoritmo > */
    if (struResultMatio.numFramesIterations < 1) {
        std::cout << "\n**** Error, the number of frames is not sufficient  ****" << std::endl;
        exit(0);
    } // if 

    /* <--------------------------------------------------------------- > */
    /* < Estructuras para almacenar los Resultados > */
    Struct_Matching structMatch_anterior;
    // Creando objeto para transformar las imagenes 
    ImageStitching stitcher(this->parametros, this->struResultMatio.numFramesTotal);
    std::vector<double> vectTiemposEjecucion_value; // Vector donde se almacenan los tiempos de ejecucion en cada etapa 
    /* <--------------------------------------------------------------- > */
    for (int ii = 0; ii < struResultMatio.numFramesIterations; ii++) {
        // < Creando Structura de iteracion actual >
        Struct_Matching structMatch;

        cout << "\n ############## Iteracion =  " << ii << " de " << struResultMatio.numFramesIterations << " ############## " << endl;
        /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
        /* Midiendo tiempo de ejecucion por Frame*/
        /*+++++++++++++++++++++++++++++++++++++++*/
        /* Vector donde se almacenan los tiempos de ejecucion de las diferentes fases */
        vectTiemposEjecucion_value = getVectorOfVals(this->struResultMatio.vectTiemposEjecucion_label.size(), 0);

        /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
        /* < Lectura / Asignacion de Frames >*/
        /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
        vectTiemposEjecucion_value[0] = assignFrames(ii, vidCapt,
                structMatch, structMatch_anterior);

        /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
        /* < Deteccion / Descripcion > */
        /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
        FeatureDetection featureDet(this->parametros);
        // < ¿ Lineas o Puntos ? >
        if (this->parametros.Features.bandHacerConLineas) {
            vectTiemposEjecucion_value[1] = featureDet.detectDescribe_LineSegments(structMatch);
            structMatch.numDetect_Lineas = structMatch.structLineas.vectLinesDetect_test.size(); // < Contemplando lineas
        } else {
            vectTiemposEjecucion_value[1] = featureDet.detectDescribe_KeyPoints(structMatch);
            structMatch.numDetect_Puntos = structMatch.structPuntos.vectKeypointsDetect_test.size(); // < Contemplando puntos
        } // if - else         

        /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
        /* < Matching > */
        /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
        FeatureMatching featureMatch(this->parametros);
        // < ¿ Lineas o Puntos ? >
        if (this->parametros.Features.bandHacerConLineas) {
            vectTiemposEjecucion_value[2] = featureMatch.matching_LineSegments(structMatch);
            structMatch.numMatches_Lineas = structMatch.structLineas.vectLinesMatch_test.size();
        } else {
            vectTiemposEjecucion_value[2] = featureMatch.matching_KeyPoints(structMatch);
            structMatch.numMatches_Puntos = structMatch.structPuntos.vectPointsMatch_test.size();
        } // if - else      

        /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
        /* < RANSAC > */
        /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
        // Creando objeto de la calse RANSAC 
        RANSAC ransac(this->parametros);
        // < ¿ Lineas o Puntos ? >
        if (this->parametros.Features.bandHacerConLineas) {
            /* < Pool de Segmentos de Linea > */
            vectTiemposEjecucion_value[3] = ransac.ransac_LineSegments(structMatch);
            structMatch.numInliers_Lineas = structMatch.structLineas.vectLinesMatch_Inliers_test.size();
        } else {
            vectTiemposEjecucion_value[3] = ransac.ransac_KeyPoints(structMatch);
            structMatch.numInliers_Puntos = structMatch.structPuntos.vectPointsMatch_Inliers_test.size();
        } // if - else        

        /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
        /* < Obteniendo los resultados > */
        /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
        getResultsStructMatch(this->parametros.Features.bandHacerConLineas, structMatch);

        /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
        /* < QuadTree Searching Keypoints > */
        /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
        if (this->parametros.Features.bandUseQuadTreeKP && this->parametros.Features.bandHacerConLineas) {
            /* < QUADTREE > */
            Struct_QuadTreeKP structQuadTree;
            bool bandHacerBuscarPuntosApoyoSoloEnQuadtrees = true; // <Bandera para obtener keypoints en las regiones determinadas por quadtrees>
            vectTiemposEjecucion_value[4] = createQuadTreeUsingPI_Keypoints(
                    structMatch, this->parametros, structQuadTree, bandHacerBuscarPuntosApoyoSoloEnQuadtrees);

            /* < Mostrando QuadTree > */
            if (this->parametros.ControlVent.bandMuestraVentanasResults) {
                this->graphicalResults.plotingQuadTreeResults(structQuadTree);
            } // if 
        } else {
            vectTiemposEjecucion_value[4] = 0;
        } // if - else         

        /* < ~~~~~~ Revisando modelo si fue encontrado ~~~~~~ >*/
        if (!structMatch.bandModelFound) {
            cout << " *** Model " << struResultMatio.typeModel << " not found.*** ";
            cv::waitKey(0);
            exit(0);
        } // if 

        /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
        /* < Pasa MatchResults A StructMatio para guardar los resultados> */
        /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
        passStructMatchResults2StructMatio(structMatch);

        /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
        /* < Seleccion Dinamica de KeyFrame y Ajuste de Modelo > */
        /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
        // Revisando si se realizara el Stitching
        if (this->parametros.Mosaic.bandStitching) {
            // Tomando decision si se considera frame < Seleccion Dinamica de Keyframe >
            structMatch.bandConsiderarStitching = DynamicKeyFrame(structMatch.Model,
                    structMatch.porcentajeMatchInliers, structMatch.porcentajeDesplModel,
                    structMatch.imageMat_refe.size(), stitcher);

            // Ajuste de Homografia 
            vectTiemposEjecucion_value[5] = stitcher.ajustarHomografias(structMatch);
        } else {
            structMatch.bandConsiderarStitching = false;
            vectTiemposEjecucion_value[5] = 0;
        } // if - else         

        /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
        /* < Mosaicing Acumulado > */
        /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
        if (parametros.Mosaic.bandMosaicoAcumulado && structMatch.bandConsiderarStitching) {
            // <Agregando imagen al mosaico>
            stitcher.agregarImagenAlMosaico(structMatch);
        } // if 

        /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
        /* < Obteniendo Tiempo de Ejecucion de la iteracion  > */
        /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
        /*+++++++++++++++++++++++++++++++++++++++*/
        this->struResultMatio.vectTiemposEjecucion_Total_Iter[ii] = vectTiemposEjecucion_value; // Asignado el Vector1D al 2D
        structMatch.tiempoEjecucion = printTimes(vectTiemposEjecucion_value, this->struResultMatio.vectTiemposEjecucion_label, ii + 1);
        /*+++++++++++++++++++++++++++++++++++++++*/

        /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
        /* < Respaldando Resultados > */
        /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
        structMatch_anterior = structMatch;

        /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
        /* < Mostrar Ventanas > */
        /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
        if (this->parametros.ControlVent.bandMuestraVentanasResults) {
            this->graphicalResults.controlaVentanas(structMatch, stitcher, this->struResultMatio);
        } // if 
        cout << endl;
        //        /* <----------------------------------------------- > */
        //        /* <         Mostrando Detalles de la iteracion     > */
        //                printDetallesIteracion(structMatch);
        //        /* <----------------------------------------------- > */
        //                if (structMatch.bandConsiderarStitching) {
        //                    cv::waitKey(0);
        //                } // if 
    } // for
    /* <~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~> */
    /* <~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~> */
    /* <~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~> */
    // < Liberando espacio >
    vidCapt.release();

    /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
    /* < Genera el mosaico con las homografias obtenidas   > */
    /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
    // Variables de resultados despues de realizar el mosaico    
    if (parametros.Mosaic.bandMosaicoAcumulado) {
        struResultMatio.numImgsUtilizadasMosaicing = stitcher.vectRuntimeMosaicing_mosaicoacumulado.size() + 1;
        struResultMatio.runtimeStitchingBlending = getSumatoriaDeVector(stitcher.vectRuntimeMosaicing_mosaicoacumulado);
        struResultMatio.MosaicoImg = stitcher.MosaicoAculumado_img;
        struResultMatio.MosaicoMask = stitcher.MosaicoAculumado_mask;

    } else {
        struResultMatio.numImgsUtilizadasMosaicing = 0;
        if (this->parametros.Mosaic.bandStitching) {
            struResultMatio.runtimeStitchingBlending = stitcher.crearCanvasDeHomografiasAjustadas(
                    struResultMatio.MosaicoImg, struResultMatio.MosaicoMask,
                    struResultMatio.numImgsUtilizadasMosaicing);
        } // if 
    } // if - else

    /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
    /* < Guardando Resultados > */
    /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
    /* < Obteniendo Tiempo de Ejecucion Total > */
    this->struResultMatio.totalRunTime = getSumatoriaDeVector2D(this->struResultMatio.vectTiemposEjecucion_Total_Iter) + struResultMatio.runtimeStitchingBlending;
    savingResults(stitcher);

    /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
    /* < Mostrando Tiempo de Ejecucion > */
    /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
    printf(" \t************************************\n");
    int factor = 1000; // para mostrar como milisegundos
    double tiempoTot_DDR_s = (this->struResultMatio.totalRunTime - this->struResultMatio.runtimeStitchingBlending);
    double tiempoProm_DDR_ms = (tiempoTot_DDR_s / this->struResultMatio.numFramesTotal) * factor;
    double tiempoTot_SB_s = this->struResultMatio.runtimeStitchingBlending;
    double tiempoProm_SB_ms = (tiempoTot_SB_s / this->struResultMatio.numImgsUtilizadasMosaicing) * factor;
    printf("  T.Tot Det/Des/RANSAC: %.4f s ~ T.Prom: %.3f ms (%i imgs).\n", tiempoTot_DDR_s, tiempoProm_DDR_ms, this->struResultMatio.numFramesTotal);
    printf("  T.Tot Stitch/Blend  : %.4f s ~ T.Prom: %.3f ms (%i imgs).\n", tiempoTot_SB_s, tiempoProm_SB_ms, this->struResultMatio.numImgsUtilizadasMosaicing);
    printf("  -----------------------------------------\n");
    printf("  Tiempo Total : %.4f s.\n", this->struResultMatio.totalRunTime);
    printf("  Tiempo Promedio : %.4f s.\n", (tiempoProm_DDR_ms + tiempoProm_SB_ms) / 1000);

    /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
    /* < Mostrando Mosaico y su mascara > */
    /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
    if (this->parametros.Mosaic.bandMosaicoAcumulado) {
        // < Esperando (Manteniendo las ventanas) >
        cv::waitKey(this->parametros.ControlVent.msEsperarMosaico);
    } else {
        if (this->struResultMatio.MosaicoImg.data != NULL) {
            cout << "\t** Mostrando Mosaico **" << endl;
            // <Mostrando Mosaico img>
            muestraVentanaCanvas(this->struResultMatio.MosaicoImg,
                    this->parametros.SaveResults.nameExecution + "_img", true);
            // <Mostrando Mosaico mask>
            if (this->parametros.ControlVent.bandMuestraMascaraMosaico) {

                muestraVentanaCanvas(this->struResultMatio.MosaicoMask,
                        this->parametros.SaveResults.nameExecution + "_mask", true);
            } // if
            // < Esperando (Manteniendo las ventanas) >
            cv::waitKey(this->parametros.ControlVent.msEsperarMosaico);
        } // if 
    } // if -else
}// function 

bool FeatureMosaicing::DynamicKeyFrame(cv::Mat& matModelo,
        double porcentajeMatchModel, double porcentajeDesplModel,
        cv::Size sizeImg,
        ImageStitching & stitcher) {
    /* < > */
    if (!this->parametros.ControlVent.bandDynamicKeyFrame) {
        return true; // Si no es seleccion dinamica entonces todos los frames son considerados
    } //if 

    /* < Obteniendo el tamaño del mosaico > */
    std::vector<double> vectDesp_parImagenes;
    cv::Mat matT_parImagenes;
    cv::Size sizeMosaicoParImg = stitcher.getSizeCanvasFromImageTransformations(
            matModelo, sizeImg, sizeImg, matT_parImagenes, vectDesp_parImagenes);

    // < ¿Se considerará el frame? >    
    bool bandCriterio_pMatchModel = (porcentajeMatchModel <= this->parametros.ControlVent.porcMinMantenerImg);
    bool bandCriterio_pDesplModel = (porcentajeDesplModel > this->parametros.ControlVent.porcMaxDesplazamientoModelo);
    bool bandConsiderarFrame = bandCriterio_pMatchModel || bandCriterio_pDesplModel;

    /* ---------------------------------------------- */
    // < Revisando el tamaño del Mosaico >
    int factor = 3;
    bool bandLimiteWidht = (sizeMosaicoParImg.width > sizeImg.width * factor);
    bool bandLimiteHeight = (sizeMosaicoParImg.height > sizeImg.height * factor);

    if (bandLimiteWidht && bandLimiteHeight) {
        cout << "\n** El mosaico excede el ancho o el alto permitido por el mosaico, modifique este parametro.**" << endl;
        cout << sizeMosaicoParImg << endl << endl;
        //        this->graphicalResults.controlaVentanas(structMatch, stitcher, this->struResultMatio);
        cv::waitKey(0);
        exit(0); // <-AQUI        
    } // if 
    /* ---------------------------------------------- */
    return bandConsiderarFrame;
} //fucntion


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   TimeControl.hpp
 * Author: luis
 *
 * Created on 8 de enero de 2018, 04:55 PM
 */

#ifndef TIMECONTROL_HPP
#define TIMECONTROL_HPP
#include <time.h>
#include <sys/time.h>
#include <string.h>
#include <iostream>

using namespace std;

class TimeControl {
public:
    TimeControl();
    virtual ~TimeControl();

    void start();
    double finish(bool bandShowTime = true, std::string cadena = "");
private:
    /* Funciones */
    double timeval_diff(struct timeval *a, struct timeval *b);

    /* Variables */
    struct timeval iniTime, endTime;
};
#endif /* TIMECONTROL_HPP */


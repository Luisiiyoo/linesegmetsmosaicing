/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ModelEstimator.h
 * Author: luis
 *
 * Created on 23 de agosto de 2018, 05:03 PM
 */

#ifndef MODELESTIMATOR_H
#define MODELESTIMATOR_H

#include <opencv2/xfeatures2d.hpp>
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include <opencv2/line_descriptor.hpp>
#include "opencv2/core/utility.hpp"
#include <opencv2/imgproc.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/highgui.hpp>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

cv::Mat solveAffine(std::vector<cv::Point2f>& pInters_test,
        std::vector<cv::Point2f>& pInters_refe);

cv::Mat solveHomography(std::vector<cv::Point2f>& pInters_test,
        std::vector<cv::Point2f>& pInters_refe);

cv::Mat calculateHomografia(cv::Mat matA);

cv::Mat getEigenVectoresByMethod(int idMetodo, cv::Mat& matCuadrada, bool bandPrintSomething);

cv::Mat calculateModel(int idTypeModel4Ransac,
        std::vector<cv::Point2f>& pTest, std::vector<cv::Point2f>& pRefe,
        bool bandLuisMethodsHomography = true);

int evaluaModelo_puntos(cv::Mat& Homografia,
        std::vector<bool>& vectBanderasConsiderarLineas,
        std::vector<cv::Point2f>& puntos_test, std::vector<cv::Point2f>& puntos_refe,
        int umbralDistanciaInliers, std::vector<double>& vectDistancia,
        std::vector<int>&vecIndicesValidosMatch, bool bandPrintSomething,
        std::vector<cv::Point2f>& puntos_transformadas);

int evaluaModelo_lineas(cv::Mat& Homografia, int tipoEvalDistancia,
        std::vector<bool>& vectBanderasConsiderarLineas,
        std::vector<cv::line_descriptor::KeyLine>& lineas_test, std::vector<cv::line_descriptor::KeyLine>& lineas_refe,
        int umbralDistanciaInliers, int umbralAnguloInlier,
        std::vector<double>& vectDistancia, std::vector<double>& vectAngulos,
        std::vector<int>&vecIndicesValidosMatch, bool bandPrintSomething,
        std::vector<cv::line_descriptor::KeyLine>& lineas_transformadas);

std::vector<int> getVectInliners_puntos(std::vector<bool>& vectBanderasConsiderarLineas,
        std::vector<double>& vecDistancias, int thresold);

std::vector<int> getVectInliners_lineas(std::vector<bool>vectorBanderasConsiderarLinea,
        std::vector<double>& vecDistancias, std::vector<double>& vectAngulos,
        double thDistance, double thAngle);

double getScoreModelo_lineas(std::vector<double>& vectDistancia,
        std::vector<double>& vectAngulos);

double getScoreModelo_puntos(std::vector<double>& vectDistancia);

cv::Mat recalculateHomographyWithIntersectionPoints(
        int idTypeModel4Ransac,
        std::vector<cv::line_descriptor::KeyLine>& vectLines_inliers_test,
        std::vector<cv::line_descriptor::KeyLine>& vectLines_inliers_refe,
        std::vector<cv::Point2f>& vectPuntIntersec_test,
        std::vector<cv::Point2f>& vectPuntIntersec_refe,
        std::vector<cv::line_descriptor::KeyLine>& vectLines_inliers_tran,
        std::vector<cv::Point2f>& vectPuntIntersec_tran,
        int thErrorDist, cv::Point2f ptCentro,
        double radioTolerancia, int numMinMuestras,
        std::vector<int>& vectIndexLineasInliers_UsedToImproveModel,
        std::vector<double>& vectDistError_IP);

std::pair<int, std::pair<double, double> > getEvaluationModeloPoints(cv::Mat& Modelo,
        std::vector<cv::Point2f>& puntos_test, std::vector<cv::Point2f>& puntos_refe,
        int umbralDistanciaInliers);

#endif /* MODELESTIMATOR_H */


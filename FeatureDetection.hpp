/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FeatureDetection.hpp
 * Author: luis
 *
 * Created on 6 de junio de 2018, 06:45 PM
 */

#ifndef FEATUREDETECTION_HPP
#define FEATUREDETECTION_HPP

#include "Struct_Parametros.hpp"
#include "Struct_Resultados.hpp"

class FeatureDetection {
public:
    FeatureDetection(ParametrosEjecucion& parametros);
    FeatureDetection(const FeatureDetection& orig);
    virtual ~FeatureDetection();

    double detectDescribe_LineSegments(Struct_Matching& structMatch);
    double detectDescribe_KeyPoints(Struct_Matching& structMatch);

private:
    ParametrosEjecucion parametros;
};

void detectAndDescribe_KeyLines(cv::Mat & imageMat,
        std::vector<cv::line_descriptor::KeyLine> & keylines,
        cv::Mat & descr, std::string & nomDetecDesc,
        int idDetecDescr, int maxNumFeatures);

void detectAndDescribe_KeyLines(cv::Mat & imageMat_test,
        cv::Mat & imageMat_refe,
        std::vector<cv::line_descriptor::KeyLine> & keylines_test,
        std::vector<cv::line_descriptor::KeyLine> & keylines_refe,
        cv::Mat & descr_test, cv::Mat & descr_refe,
        int & numDetect_features_test, int & numDetect_features_refe,
        std::string & nomDetecDesc,
        int maxNumFeatures);

void detectAndDescribe_KeyPoints(cv::Mat& imageMat,
        std::vector<cv::KeyPoint>& keypoints, cv::Mat& descr,
        std::string& nomDetDesc,
        int idDetecDescr, int maxNumFeatures);

void detectAndDescribe_KeyPoints(
        cv::Mat& imageMat_test, cv::Mat& imageMat_refe,
        std::vector<cv::KeyPoint>& keypoints_test, std::vector<cv::KeyPoint>& keypoints_refe,
        cv::Mat& descr_test, cv::Mat& descr_refe,
        int & numDetect_features_test, int & numDetect_features_refe,
        std::string& nomDetDesc,
        int idDetecDescr, int maxNumFeatures);

std::vector<cv::line_descriptor::KeyLine> selectNKeylinesMaxResponse(int maxNumFeatures,
        std::vector<cv::line_descriptor::KeyLine> vectKeylines);

std::vector<cv::KeyPoint> selectNKeypointsMaxResponse(int maxNumFeatures,
        std::vector<cv::KeyPoint>& vectKeypoints);
#endif /* FEATUREDETECTION_HPP */


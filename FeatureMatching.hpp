/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FeatureMatching.hpp
 * Author: luis
 *
 * Created on 6 de junio de 2018, 08:37 PM
 */

#ifndef FEATUREMATCHING_HPP
#define FEATUREMATCHING_HPP

#include "Struct_Parametros.hpp"
#include "Struct_Resultados.hpp"

class FeatureMatching {
public:
    FeatureMatching(ParametrosEjecucion& parametros);
    FeatureMatching(const FeatureMatching& orig);
    virtual ~FeatureMatching();

    double matching_LineSegments(Struct_Matching& structMatch);
    double matching_KeyPoints(Struct_Matching& structMatch);

private:
    ParametrosEjecucion parametros;
};

void chooseMatchDescriptors(int idMatcher, int idDetecDescr,
        cv::Mat& descr_test, cv::Mat& descr_refe, std::vector<cv::DMatch>& matches);

std::vector< cv::DMatch > removeMatchesFactorMinimo(std::vector<cv::DMatch>& matches, int factDist);

std::vector<double> getVectDistanciasDMacth(std::vector<cv::DMatch>& matches);

void ajustarMatch_Keylines(std::vector<cv::DMatch> matches,
        std::vector<cv::DMatch>& matches_ajust,
        std::vector<cv::line_descriptor::KeyLine> keylines_test,
        std::vector<cv::line_descriptor::KeyLine> keylines_refe,
        std::vector<cv::line_descriptor::KeyLine>& keylines_test_ajust,
        std::vector<cv::line_descriptor::KeyLine>& keylines_refe_ajust);

void ajustarMatch_Keypoints(std::vector<cv::DMatch> matches,
        std::vector<cv::DMatch>& matches_ajust,
        std::vector<cv::KeyPoint> keypoints_test,
        std::vector<cv::KeyPoint> keypoints_refe,
        std::vector<cv::KeyPoint>& keypoints_test_ajust,
        std::vector<cv::KeyPoint>& keypoints_refe_ajust);

void matching_LineSegments(int idMatcher,
        cv::Mat& descr_test, cv::Mat& descr_refe,
        std::vector<cv::line_descriptor::KeyLine>& keylines_test,
        std::vector<cv::line_descriptor::KeyLine>& keylines_refe,
        std::vector<cv::DMatch>& matches);

void matching_KeyPoints(int idMatcher, int idDetecDescr,
        cv::Mat& descr_test, cv::Mat& descr_refe,
        std::vector<cv::DMatch>& matches,
        std::vector<cv::KeyPoint>& keypoints_test,
        std::vector<cv::KeyPoint>& keypoints_refe,
        std::vector<cv::Point2f>& puntos_test,
        std::vector<cv::Point2f>& puntos_refe);

#endif /* FEATUREMATCHING_HPP */


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SavingResultsMatio.hpp
 * Author: luis
 *
 * Created on 10 de junio de 2018, 02:04 PM
 */

#ifndef SAVINGRESULTSMATIO_HPP
#define SAVINGRESULTSMATIO_HPP

#include "Struct_Parametros.hpp"
#include "Struct_Resultados.hpp"
#include <matio.h>

#include <iostream>
#include <fstream>
#include <string.h>

class SavingResultsMatio {
public:
    SavingResultsMatio(ParametrosEjecucion& parametros);
    SavingResultsMatio(const SavingResultsMatio& orig);
    virtual ~SavingResultsMatio();

    void saveResults(Struct_ResultsMATIO& strutResultsMatio);

private:

    void saveImportantVariables(mat_t* matio_FilePointer,Struct_ResultsMATIO& strutResultsMatio);

    char* string2char(std::string cadena);

    void closeMatio_FilePointer(mat_t* matio_FilePointer);
    mat_t* createMatio_FilePointer(std::string nombreArchivo);

    void write_Int(string nombreVar, int valorVariable, mat_t *matio_FilePointer);
    void write_Int_vec1D(string nombreVar, std::vector<int>& vec1D_valorVariable, mat_t *matio_FilePointer);

    void write_Double(string nombreVar, double valorVariable, mat_t *matio_FilePointer);
    void write_Double_vec1D(string nombreVar, std::vector<double>& vec1D_valorVariable, mat_t *matio_FilePointer);

    void write_Bool(string nombreVar, bool valorVariable, mat_t *matio_FilePointer);
    void write_Bool_vec1D(string nombreVar, std::vector<bool>& vec1D_valorVariable, mat_t *matio_FilePointer);

    void write_String(string nombreVar, string valorVariable, mat_t *matio_FilePointer);


    /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
    ParametrosEjecucion parametros;
};

#endif /* SAVINGRESULTSMATIO_HPP */


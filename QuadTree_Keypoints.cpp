/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Quadtree.cpp
 * Author: luis
 * 
 * Created on 15 de agosto de 2018, 10:57 PM
 */


#include "QuadTree_Keypoints.h"
#include "BasicFunctions.hpp"
#include "LinePointFunctions.hpp"
#include "Struct_Resultados.hpp"
//#include "Struct_Parametros.hpp"
#include "FeatureDetection.hpp"
#include "FeatureMatching.hpp"
#include "RANSAC.hpp"
#include "ModelEstimator.h"
#include "GraphicalResults.h"
#include "TimeControl.hpp"

#include <opencv2/xfeatures2d.hpp>
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"

#include "string.h"
#include <string>
#include <sstream>
#include <fstream>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <complex>

using namespace std;

QuadTree::~QuadTree() {
}

QuadTree::QuadTree(const cv::Size & sizeImg) {
    this->sizeImg = sizeImg;

    // Bandera indicadora de si es HOJA o RAMA
    this->bandIsLeft = false; // Por default en este constructor <NO> es una hoja
    this->numElementsInThisQuad = 0; // por default ningun elemento pertenece a este Quad

    // Nivel 
    this->level = 0; // < Nivel 0 - Imagen de resolucion normal (Sin hacer la division) >
    this->code = "A"; // < NW = 'A' > || < NE = 'B' > || < SW = 'C' > || < SE = 'D' >

    // Llenando Puntos 
    this->ptSupIzq.x = 0; // Punto Superior Izquierdo que comprende el Quad
    this->ptSupIzq.y = 0;
    this->ptInfDer.x = sizeImg.width - 1; // Punto Inferior Derecho que comprende el Quad
    this->ptInfDer.y = sizeImg.height - 1;

    // Calculando tamaño del Quad Actual
    int factor_aux = std::pow(2, this->level);
    this->width = sizeImg.width / factor_aux; // tendra el mismo tamaño
    this->height = sizeImg.height / factor_aux; // tendra el mismo tamaño

    // Otros QUADS
    this->NW = NULL; // En <NULL> porque no se ha dividio 
    this->NE = NULL;
    this->SW = NULL;
    this->SE = NULL;
} // constructor

QuadTree::QuadTree(const cv::Size & sizeImg,
        cv::Point2f ptSupIzq, cv::Point2f ptInfDer,
        int level, std::string code) {
    this->sizeImg = sizeImg; // Resolucion original de la imagen

    // Bandera indicadora de si es HOJA o RAMA
    this->bandIsLeft = false; // Por default en este constructor <NO> es una hoja
    this->numElementsInThisQuad = 0; // por default ningun elemento pertenece a este Quad

    // Nivel 
    this->level = level; // < in the range of 0 to maxLevel >
    this->code = code; // < NW = 'A' > || < NE = 'B' > || < SW = 'C' > || < SE = 'D' >

    // Llenando Puntos 
    this->ptSupIzq = ptSupIzq;
    this->ptInfDer = ptInfDer;

    // Calculando tamaño del Quad Actual
    int factor_aux = std::pow(2, this->level);
    this->width = sizeImg.width / factor_aux; // Obteniendo el tamaño del Quad
    this->height = sizeImg.height / factor_aux; //  Deberia tener el mismo tamaño si se calcula utiliznaod "ptSupIzq" y "ptInfDer"

    // Otros QUADS
    this->NW = NULL; // En <NULL> porque no se ha dividio 
    this->NE = NULL;
    this->SW = NULL;
    this->SE = NULL;
} // constructor

void QuadTree::createQuadTree(cv::Mat& imgMat,
        std::vector<cv::Point2f>& vectPoints,
        std::vector<bool>& vectBandValidPoints,
        std::vector<std::string>& vectColdePointsQuad,
        const int& minObjectsToSplitQuad, const int& maxLevel,
        std::vector<QuadTree>& vectQuads) {
    // Mostrando rectangulo
    cv::rectangle(imgMat, this->ptSupIzq, this->ptInfDer, cv::Scalar(255, 255, 255), 1);

    // < Contando cuantos elementos No han sido localizados en los QUAD>
    // Buscando Puntos en este QUAD y Contandolos
    std::vector<int> vectIndex_PointsInThisQuad = findElementosInThisQuad(vectPoints, vectBandValidPoints);

    // < Comprobando si es una hoja o un nodo >
    bool bandStop = isALeft(maxLevel) || (vectIndex_PointsInThisQuad.size() <= minObjectsToSplitQuad);

    // <Revisando si se detiene o continua dividiendo>
    if (bandStop) {
        // < Es una hoja (se llego al nivel maximo) o Ya no hay suficientes Objetos en el Quad>
        this->bandIsLeft = true;

        // <Guardando numero de elementos en este Quad >        
        this->numElementsInThisQuad = vectIndex_PointsInThisQuad.size();

        // < Marcando los puntos encontrados en este Quad >
        setCodeOfPointsInThisQuad(vectIndex_PointsInThisQuad, vectBandValidPoints, vectColdePointsQuad);

        // < Imprimiendo detalles de este Quad>
        //        printQuadDetails();

        // < Guardando Quad en el vector>
        vectQuads.push_back(*this);
        return;
    } else {
        // < Es una rama >
        this->bandIsLeft = false;

        // < Dividiendo Quad >
        cv::Point2f ptSupIzq_aux;
        cv::Point2f ptInfDer_aux;

        double midWidth = this->width / 2.0f;
        double midHeight = this->height / 2.0f;

        std::string code_newQuad;
        // < ------------- NW : (A) ------------- >         
        code_newQuad = this->code + "A";
        ptSupIzq_aux = cv::Point2f(this->ptSupIzq.x, this->ptSupIzq.y);
        ptInfDer_aux = cv::Point2f(this->ptInfDer.x - midWidth, this->ptInfDer.y - midHeight);

        this->NW = new QuadTree(this->sizeImg,
                ptSupIzq_aux, ptInfDer_aux, level + 1, code_newQuad);

        this->NW->createQuadTree(imgMat, vectPoints, vectBandValidPoints,
                vectColdePointsQuad, minObjectsToSplitQuad, maxLevel, vectQuads);
        // < ------------- NE : (B) ------------- >  
        code_newQuad = this->code + "B";
        ptSupIzq_aux = cv::Point2f(this->ptSupIzq.x + midWidth, this->ptSupIzq.y);
        ptInfDer_aux = cv::Point2f(this->ptInfDer.x, this->ptInfDer.y - midHeight);

        this->NE = new QuadTree(this->sizeImg,
                ptSupIzq_aux, ptInfDer_aux, level + 1, code_newQuad);

        this->NE->createQuadTree(imgMat, vectPoints, vectBandValidPoints,
                vectColdePointsQuad, minObjectsToSplitQuad, maxLevel, vectQuads);
        // < ------------- SW : (C) ------------- >       
        code_newQuad = this->code + "C";
        ptSupIzq_aux = cv::Point2f(this->ptSupIzq.x, this->ptSupIzq.y + midHeight);
        ptInfDer_aux = cv::Point2f(this->ptInfDer.x - midWidth, this->ptInfDer.y);

        this->SW = new QuadTree(this->sizeImg,
                ptSupIzq_aux, ptInfDer_aux, level + 1, code_newQuad);

        this->SW->createQuadTree(imgMat, vectPoints, vectBandValidPoints,
                vectColdePointsQuad, minObjectsToSplitQuad, maxLevel, vectQuads);
        // < ------------- SE : (D) ------------- >
        code_newQuad = this->code + "D";
        ptSupIzq_aux = cv::Point2f(this->ptSupIzq.x + midWidth, this->ptSupIzq.y + midHeight);
        ptInfDer_aux = cv::Point2f(this->ptInfDer.x, this->ptInfDer.y);

        this->SE = new QuadTree(this->sizeImg,
                ptSupIzq_aux, ptInfDer_aux, level + 1, code_newQuad);

        this->SE->createQuadTree(imgMat, vectPoints, vectBandValidPoints,
                vectColdePointsQuad, minObjectsToSplitQuad, maxLevel, vectQuads);
    } // if - else 

} // function 

std::vector<int> QuadTree::findElementosInThisQuad(std::vector<cv::Point2f>& vectPoints,
        std::vector<bool>& vectBandValidPoints) {
    // Vector de resultados 
    std::vector<int> vectIndexInThisQuad;

    bool band_SinAsignar;
    bool bandPoint_InThisArea;
    int numElementos = vectPoints.size();

    for (int ii = 0; ii < numElementos; ii++) {
        band_SinAsignar = vectBandValidPoints[ii]; // False = Ya se encontro en otro QUAD

        // < Revisando si no se encontro en otro Quad >
        if (band_SinAsignar) {

            bandPoint_InThisArea = isThePointInThisQuad(vectPoints[ii]); //bandPoint_InThisArea = isThePointInThisQuad(pt); // 

            // < Verificando punto>
            if (bandPoint_InThisArea) {
                vectIndexInThisQuad.push_back(ii);
            } // if 

        } // if 
    } // for 
    return vectIndexInThisQuad;
} //function 

void QuadTree::setCodeOfPointsInThisQuad(std::vector<int>& vectIndex_PointsInThisQuad,
        std::vector<bool>& vectBandValidPoints, std::vector<std::string>& vectColdePointsQuad) {

    int numElementsInThisQuad = vectIndex_PointsInThisQuad.size();
    int index;

    for (int ii = 0; ii < numElementsInThisQuad; ii++) {
        index = vectIndex_PointsInThisQuad[ii]; // obteniendo indice de punto en este Quad

        // cambiando estado y asignando codigo a punto 
        vectBandValidPoints[index] = false;
        vectColdePointsQuad[index] = this->code;
    } // for 
} // function 

bool QuadTree::isThePointInThisQuad(cv::Point2f & point) {
    bool bandX = (point.x >= ptSupIzq.x) && (point.x <= ptInfDer.x);
    bool bandY = (point.y >= ptSupIzq.y) && (point.y <= ptInfDer.y);

    return (bandX && bandY);
} // function 

bool QuadTree::isALeft(const int& maxLevel) {

    bool bandHoja = (this->level >= maxLevel);
    //bool bandSizeOK = (this->width >= minWidth) && (this->heigth >= minHeight);

    if (this->bandIsLeft || bandHoja) {
        return true; // una hoja
    } else {
        return false; // Un rama
    }// if - else

} // function

void QuadTree::printQuadDetails() {
    cout << " ~~~~~~~~~ Quad ~~~~~~~~~ " << endl;
    cout << " level :  " << level << " --- code : " << code << endl;
    cout << " bandIsLeft :  " << bandIsLeft << " --- numElementsInThisQuad : " << numElementsInThisQuad << endl;
    cout << " sizeImg (Original):  " << sizeImg << endl;
    cout << " ptSupIzq :  " << ptSupIzq << " --- ptInfDer : " << ptInfDer << endl;
    cout << " width :  " << width << " --- heigth : " << height << endl;
} // function 

std::vector<int> searchQuadsWithLeastNElements(std::vector<QuadTree> vectQuads, int value) {
    std::vector<int> vectIndex;

    for (int ii = 0; ii < vectQuads.size(); ii++) {
        QuadTree quad = vectQuads[ii];
        if (quad.numElementsInThisQuad <= value) {
            vectIndex.push_back(ii); // añadiendo indice
        }// if
    } // for 

    return vectIndex;
} // function

double getPorcentajeIntersectionPointsInQuadTree(std::vector<QuadTree>& vectQuads,
        std::vector<int>& vectIndexesQuad) {
    double porcIP = 0;
    int index;
    for (int qq = 0; qq < vectIndexesQuad.size(); qq++) {
        index = vectIndexesQuad[qq];
        QuadTree qt = vectQuads[index];
        porcIP = porcIP + (100 / std::pow(4, qt.level));
    } // for 

    // Porcentaje de Intersection Points
    return 100 - porcIP;
} // function 

std::pair< int, int > generateKeypointMatchesQuad(
        std::vector<QuadTree>& vectQuads_test, std::vector<QuadTree>& vectQuads_refe,
        std::vector<int>& vectIndex_test, std::vector<int>& vectIndex_refe,
        cv::Mat& imgMat_test, cv::Mat& imgMat_refe,
        int idDetecDescr, int maxNumFeatures, int idMatcher,
        std::vector<cv::DMatch>& matches,
        std::vector<cv::Point2f> & puntos_test, std::vector<cv::Point2f> & puntos_refe) {
    /* < Keypoints in the Quads of the Test Image >*/
    std::vector<cv::KeyPoint> keypoints_test;
    cv::Mat descrKP_test;


    // Obteniendo puntos 
    int numFeaturesDetec_test = generatePoolOfKeyPointsQuad(vectQuads_test, vectIndex_test, imgMat_test,
            idDetecDescr, maxNumFeatures, keypoints_test, descrKP_test);

    /* < Keypoints in the Quads of the Refe Image >*/
    std::vector<cv::KeyPoint> keypoints_refe;
    cv::Mat descrKP_refe;
    // Obteniendo puntos 
    int numFeaturesDetec_refe = generatePoolOfKeyPointsQuad(vectQuads_refe, vectIndex_refe, imgMat_refe,
            idDetecDescr, maxNumFeatures, keypoints_refe, descrKP_refe);


    //    cout<<" -- Matching KeyPoints QuadTree -- " <<endl;
    /* < Getting the keypoint matches >*/
    matching_KeyPoints(idMatcher, idDetecDescr,
            descrKP_test, descrKP_refe, matches,
            keypoints_test, keypoints_refe,
            puntos_test, puntos_refe);

    //    /* <> */
    //    double max_dist = 0;
    //    double min_dist = 100;
    //
    //    //-- Quick calculation of max and min distances between keypoints
    //    for (int jj = 0; jj < matches.size(); jj++) {
    //        double dist = matches[jj].distance;
    //        if (dist < min_dist) min_dist = dist;
    //        if (dist > max_dist) max_dist = dist;
    //    } // for 
    //
    //    /* Encontrando Matches entre un radio */
    //    double factorMatchDist = 2.5;
    //    std::vector< cv::DMatch > good_matches;
    //    std::vector< int > vectIndexDMatch;
    //
    //    for (int kk = 0; kk < matches.size(); kk++) {
    //        if (matches[kk].distance <= max(factorMatchDist * min_dist, 0.02)) {
    //            good_matches.push_back(matches[kk]);
    //            vectIndexDMatch.push_back(kk);
    //        }// if 
    //    } // for 
    //
    //    // Nuevos Matches
    //    matches = good_matches;
    //
    //    /* <Extrayendo good Matches > */
    //    puntos_test = getSpecificPointsFromVector(puntos_test, vectIndexDMatch);
    //    puntos_refe = getSpecificPointsFromVector(puntos_refe, vectIndexDMatch);


    /* Resultados */
    std::pair< int, int > pairResults;
    pairResults.first = (numFeaturesDetec_test + numFeaturesDetec_refe) / 2;
    pairResults.second = matches.size();

    return pairResults;
} // function 

int generatePoolOfKeyPointsQuad(std::vector<QuadTree>& vectQuads,
        std::vector<int>& vectIndex, cv::Mat& imgMat,
        int idDetecDescr, int maxNumFeatures,
        std::vector<cv::KeyPoint> & keypoints, cv::Mat & descrKP) {
    // Limpiando vector 
    keypoints.clear();

    //    descrKP = NULL;
    /* < Variables Auxiliares > */
    cv::Ptr<cv::Feature2D> detector;
    std::string nomDetDesc;

    int maxNumKP_Quad;
    for (int ii = 0; ii < vectIndex.size(); ii++) {
        /* Obteniendo el quad <TEST> */
        QuadTree quad = vectQuads[vectIndex[ii]];

        // Calculando el numero maximo de caracterisitcas maximo a detectar en este quad
        maxNumKP_Quad = maxNumFeatures / std::pow(4, quad.level);
        switch (idDetecDescr) {
            case 0: /* SURF */
            {
                nomDetDesc = "SURF";
                /* Crear variable para detectar keypoints por SURF */
                detector = cv::xfeatures2d::SURF::create(maxNumKP_Quad);
                break;
            } //case
            case 1: /* SIFT */
            {
                nomDetDesc = "SIFT";
                /* Crear variable para detectar keypoints por SIFT */
                detector = cv::xfeatures2d::SIFT::create(maxNumKP_Quad);
                break;
            } //case
            default:
            case 2: /* ORB */
            {
                nomDetDesc = "ORB";
                /* Crear variable para detectar keypoints por ORB */
                detector = cv::ORB::create(maxNumKP_Quad);
                break;
            } //case
        }//switch   

        /* Mostrando codigo QUAD */
        //      cout << ii + 1 << " Quad : " << quad.code << endl;

        /* < Obteniendo una subImagen con los puntos del Quad > */
        // Extrae subimagen
        cv::Rect roi = cv::Rect(quad.ptSupIzq, quad.ptInfDer); // especificando region de interes
        cv::Mat subMat = imgMat(roi); // extrayendo la subimagen

        // Extrae keypoints subimagen
        std::vector<cv::KeyPoint> kp_subMat; // vector de KP de la subImagen

        /* < Detectar keypoints TEST > */
        detector->detect(subMat, kp_subMat);

        if (kp_subMat.size() > maxNumKP_Quad) {
            kp_subMat = selectNKeypointsMaxResponse(maxNumKP_Quad, kp_subMat);
        } //if 

        /* < Ajustando los puntos de la SubImagen a la Imagen global > */
        for (int jj = 0; jj < kp_subMat.size(); jj++) {
            cv::KeyPoint kp_subMat_ajustado;

            // <Ajustando cada punto a la imagen global >
            kp_subMat_ajustado = kp_subMat[jj];
            kp_subMat_ajustado.pt.x = kp_subMat_ajustado.pt.x + quad.ptSupIzq.x;
            kp_subMat_ajustado.pt.y = kp_subMat_ajustado.pt.y + quad.ptSupIzq.y;

            keypoints.push_back(kp_subMat_ajustado); // almacenando el punto con el ajuste a la imagen global            
        } // for         
    } //  for 

    /* < Calcular los descriptores TEST > */
    detector->compute(imgMat, keypoints, descrKP);

    return keypoints.size();
} // function 

double createQuadTreeUsingPI_Keypoints(Struct_Matching& structMatch,
        ParametrosEjecucion parametros, Struct_QuadTreeKP& structQuadTree,
        bool bandGenerateQuadTrees) {
    /*+++++++++++++++++++++++++++++++++++++++*/
    /* Midiendo tiempos */
    TimeControl tc;
    tc.start(); // <--------    
    /*+++++++++++++++++++++++++++++++++++++++*/
    /* <Structura para resultados QuadTree > */
    std::vector<int > vectNumInlierModel(3); // <Vector de  Numero de inliers >
    std::vector<std::pair< int, int > > vectPairDetectMatch(3); // <Vector de pares (Número de puntos detectados y num. correspondencias) >
    // <Llenando imformación de líneas>
    vectPairDetectMatch[1].first = (structMatch.structLineas.vectLinesDetect_refe.size() + structMatch.structLineas.vectLinesDetect_test.size()) / 2;
    vectPairDetectMatch[1].second = structMatch.structLineas.vectMatches.size();
    vectNumInlierModel[1] = structMatch.structLineas.vectLinesMatch_Inliers_test.size();

    cout << "\n----- QuadTree ----- " << endl;
    bool bandDetectarPuntosDeApoyo = false;

    /* < Parametros > */
    structQuadTree.idDetecDescrQuad = parametros.Features.idDetecDescr; /* < 0:"SURF" || 1:"SIFT"|| 2:"ORB"> */
    structQuadTree.maxNumFeatures = parametros.Features.maxNumFeatures; // < Maximo numero de KeyPoints a extraer en el Quad >    
    structQuadTree.minObjectsToSplitQuad = parametros.RANSAC.numMinMuestras; // < Minimo Numero de Puntos de interseccion para No dividir el Quad>
    structQuadTree.maxLevel = 3; // < Maximo Nivel del Arbol >
    structQuadTree.porcIP4UseKP = 40; // < Porcentaje para buscar KP >
    structQuadTree.numElemtsToSearchQuad = structQuadTree.minObjectsToSplitQuad; // < Busqueda de Quads que tengan menos de N elementos para extraer KeyPoints>    

    // <Clonando Imagenes>
    structQuadTree.imgMat_test = structMatch.imageMat_test.clone();
    structQuadTree.imgMat_refe = structMatch.imageMat_refe.clone();

    // < Obteniendo Puntos de interseccion de las lineas inliers >    
    structQuadTree.vectIntersecP_test = structMatch.structLineas.vectLS_PI_test;
    structQuadTree.vectIntersecP_refe = structMatch.structLineas.vectLS_PI_refe;

    // < Obteniendo numero de PI >
    int numPuntosInters = structQuadTree.vectIntersecP_test.size();

    /* < ================= Iniciando QuadTrees ================= >*/
    // Iniciando Quad
    QuadTree qt_test(structQuadTree.imgMat_test.size());
    QuadTree qt_refe(structQuadTree.imgMat_refe.size());

    if (bandGenerateQuadTrees) {
        // < ~~~~~~~~~~~~~~~~~ QuadTree TEST ~~~~~~~~~~~~~~~~~ >             
        // Variables auxiliares
        structQuadTree.vectBandValidPoints_test = generaVectorTrue(numPuntosInters);
        structQuadTree.vectColdePointsQuad_test = getVectorOfVals(numPuntosInters, "");
        // Formando Arbol - <TEST>
        qt_test.createQuadTree(structQuadTree.imgMat_test,
                structQuadTree.vectIntersecP_test, structQuadTree.vectBandValidPoints_test,
                structQuadTree.vectColdePointsQuad_test,
                structQuadTree.minObjectsToSplitQuad, structQuadTree.maxLevel,
                structQuadTree.vectQuads_test);

        // < ~~~~~~~~~~~~~~~~~ QuadTree REFE ~~~~~~~~~~~~~~~~~ >                       
        // Variables auxiliares
        structQuadTree.vectBandValidPoints_refe = generaVectorTrue(numPuntosInters);
        structQuadTree.vectColdePointsQuad_refe = getVectorOfVals(numPuntosInters, "");
        // Formando Arbol - <REFE>    
        qt_refe.createQuadTree(structQuadTree.imgMat_refe,
                structQuadTree.vectIntersecP_refe, structQuadTree.vectBandValidPoints_refe,
                structQuadTree.vectColdePointsQuad_refe,
                structQuadTree.minObjectsToSplitQuad, structQuadTree.maxLevel,
                structQuadTree.vectQuads_refe);

        /* < ############################################################### > */
        // <Buscando Quads que tengan menos de N-elementos>       
        structQuadTree.vectIndexQuad_test = searchQuadsWithLeastNElements(structQuadTree.vectQuads_test, structQuadTree.numElemtsToSearchQuad);
        structQuadTree.vectIndexQuad_refe = searchQuadsWithLeastNElements(structQuadTree.vectQuads_refe, structQuadTree.numElemtsToSearchQuad);

        structQuadTree.porcIP_test = getPorcentajeIntersectionPointsInQuadTree(structQuadTree.vectQuads_test,
                structQuadTree.vectIndexQuad_test);

        structQuadTree.porcIP_refe = getPorcentajeIntersectionPointsInQuadTree(structQuadTree.vectQuads_refe,
                structQuadTree.vectIndexQuad_refe);

        cout << " Num. Quads TEST : " << structQuadTree.vectQuads_test.size() <<
                "  %IP_test : " << structQuadTree.porcIP_test << endl;
        cout << " Num. Quads REFE : " << structQuadTree.vectQuads_refe.size() <<
                "  %IP_refe : " << structQuadTree.porcIP_refe << endl;
        /* < ############################################################### > */
    } else {
        /* <Cuando no se utiliza QuadTrees > */
        // Añadiendo Quad Nivel 0 al Vector de QuadTrees
        structQuadTree.vectQuads_test.push_back(qt_test);
        structQuadTree.vectQuads_refe.push_back(qt_refe);
        // Indices validos (0 es el unico )
        structQuadTree.vectIndexQuad_test.push_back(0);
        structQuadTree.vectIndexQuad_refe.push_back(0);
        // Porcentaje de IP- validos
        structQuadTree.porcIP_test = 0;
        structQuadTree.porcIP_refe = 0;
    } // if - else

    // < Verificando si es necesario extraer puntos de apoyo >
    bandDetectarPuntosDeApoyo = structQuadTree.porcIP_test < structQuadTree.porcIP4UseKP ||
            structQuadTree.porcIP_refe < structQuadTree.porcIP4UseKP;

    if (bandDetectarPuntosDeApoyo) {
        /* < Creando correspondencia de Puntos > */
        vectPairDetectMatch[0] = generateKeypointMatchesQuad(structQuadTree.vectQuads_test, structQuadTree.vectQuads_refe,
                structQuadTree.vectIndexQuad_test, structQuadTree.vectIndexQuad_refe,
                structMatch.imageMat_test, structMatch.imageMat_refe,
                structQuadTree.idDetecDescrQuad, structQuadTree.maxNumFeatures,
                parametros.Features.idMatcher,
                structMatch.structPuntos.vectMatches,
                structMatch.structPuntos.vectPointsMatch_test,
                structMatch.structPuntos.vectPointsMatch_refe);

        /* < Solo si se encontro correspondencia de Puntos > */
        if (structMatch.structPuntos.vectMatches.size() > 0) {
            /* < RANSAC > */
            int fact = (structMatch.structLineas.bandImprovedModelDone_ip) ? 2 : 1;
            parametros.RANSAC.numIteraciones = parametros.RANSAC.numIteraciones / fact; // 
            RANSAC ransac_keypoints(parametros);
            ransac_keypoints.ransac_KeyPoints(structMatch);

            /* <Guardando numero de inliers> */
            vectNumInlierModel[0] = structMatch.structPuntos.vectPointsMatch_Inliers_test.size();

            // Puntos + lineas
            vectPairDetectMatch[2].first = (vectPairDetectMatch[0].first + vectPairDetectMatch[1].first) / 2;
            vectPairDetectMatch[2].second = (vectPairDetectMatch[0].second + vectPairDetectMatch[1].second) / 2;
            vectNumInlierModel[2] = (vectNumInlierModel[0] + vectNumInlierModel[1]) / 2;


            /* < Asignando los Keypoints a la Estructura Si fue encontrado el modelo > */
            int numInliersLineas = structMatch.structLineas.vectLinesMatch_Inliers_test.size();
            int numInliersPuntos = structMatch.structPuntos.vectPointsMatch_Inliers_test.size();
            if (structMatch.structPuntos.bandModeloFound_kp && numInliersPuntos >= numInliersLineas) {
                structQuadTree.vectKeyP_test = structMatch.structPuntos.vectPointsMatch_Inliers_test;
                structQuadTree.vectKeyP_refe = structMatch.structPuntos.vectPointsMatch_Inliers_refe;

                /* < Comprar Modelos > */
                std::vector<cv::Mat> vectModelos;
                std::vector<std::pair<int, std::pair<double, double > > > vectEvalModels;

                // Obteniendo el mejor modelo
                int idBestModel = getTheBestModel(structMatch, parametros.RANSAC.umbralDistancia,
                        10, vectModelos, vectEvalModels);

                // Pasando resultados a estructura
                setStructWithTheBestModel(idBestModel, structMatch,
                        vectModelos, vectEvalModels, vectPairDetectMatch,
                        vectNumInlierModel);
            } // if 
        } // if 
    } // if

    // <Obteniendo detalles de los puntos de apoyo>
    if (bandDetectarPuntosDeApoyo) {       
        structMatch.numDetect_Puntos = vectPairDetectMatch[0].first;
        structMatch.numMatches_Puntos = vectPairDetectMatch[0].second;
        structMatch.numInliers_Puntos = vectNumInlierModel[0];
    } else {
        structMatch.numDetect_Puntos = 0;
        structMatch.numMatches_Puntos = 0;
        structMatch.numInliers_Puntos = 0;
    } // if else

    /*+++++++++++++++++++++++++++++++++++++++*/
    return tc.finish(false, "");
    /*+++++++++++++++++++++++++++++++++++++++*/
} // function 

int getTheBestModel(Struct_Matching& structMatch,
        double umbralError, double porcentajeBaseLine,
        std::vector<cv::Mat>& vectModelos,
        std::vector<std::pair<int, std::pair<double, double > > > &vectEvalModels) {
    /* < Evaluacion de los modelos > */
    vectModelos = std::vector<cv::Mat>(3);
    vectModelos[0] = structMatch.structPuntos.Model_kp; // Modelo de Puntos 
    vectModelos[1] = structMatch.structLineas.Model_ip; // Modelo de Lineas  
    vectModelos[2] = (structMatch.structLineas.Model_ip + structMatch.structPuntos.Model_kp) / 2; // Modelo de puntos y lineas


    int idBestModel = 1;
    std::pair<int, std::pair<double, double> > eval_IP = std::make_pair<int, std::pair<double, double> >(0, std::make_pair<double, double>(INFINITY, INFINITY));
    std::pair<int, std::pair<double, double> > eval_KP = std::make_pair<int, std::pair<double, double> >(0, std::make_pair<double, double>(INFINITY, INFINITY));
    std::pair<int, std::pair<double, double> > eval_IP_KP = std::make_pair<int, std::pair<double, double> >(0, std::make_pair<double, double>(INFINITY, INFINITY));

    // -----
    std::vector<cv::Point2f> vectPoolPoints_test = structMatch.structPuntos.vectPointsMatch_test;
    std::vector<cv::Point2f> vectPoolPoints_refe = structMatch.structPuntos.vectPointsMatch_refe;

    // < ~~~~~~~~~~~~ IP ~~~~~~~~~~~~ >      
    // Obteniendo media y desviacion esandar y numero de Intersection Points inliers    
    eval_IP = getEvaluationModeloPoints(vectModelos[1],
            vectPoolPoints_test, vectPoolPoints_refe, umbralError);

    // Definiendo limites (IP es considerado como baseline)
    int limInf = round(eval_IP.first - (eval_IP.first * (porcentajeBaseLine / 100)));
    int limSup = round(eval_IP.first + (eval_IP.first * (porcentajeBaseLine / 100)));

    // < ~~~~~~~~~~~~ KP ~~~~~~~~~~~~ >        
    std::vector<double> vectDistInliers_kp = getElementsFromVector(structMatch.structPuntos.vectDist_points,
            structMatch.structPuntos.vectIndexMatch_Inliers_points);
    // Obteniendo media y SD
    eval_KP.first = structMatch.structPuntos.vectIndexMatch_Inliers_points.size();
    eval_KP.second.second = getDesviacionEstandar(vectDistInliers_kp, eval_KP.second.first);

    /* < =================================== > */

    /* < =================================== > */
    if (eval_KP.first < limInf) {
        // <Utiliza Modelo de Lineas>       
        idBestModel = 1;

    } else if (eval_KP.first >= limInf && eval_KP.first <= limSup) {
        // <Probar la combinacion de lineas + puntos>                           
        /* < Combinando Modelos (Promedio punto) > */
        // <Evaluando modelo >        
        eval_IP_KP = getEvaluationModeloPoints(vectModelos[2],
                vectPoolPoints_test, vectPoolPoints_refe, umbralError);

        /* < Comparando los Resultados de los modelos > */
        if (eval_IP_KP.first > limSup) {
            // <Utiliza Modelo de Lineas +  Puntos>     
            idBestModel = 2;

        } else if (eval_IP_KP.first >= limInf && eval_IP_KP.first <= limSup) {
            // Buscando entre IP , KP y IP + KP (SD)
            if ((eval_KP.second.second < eval_IP.second.second) && (eval_KP.second.second < eval_IP_KP.second.second)) {
                // <Utiliza Modelo de  Puntos>     
                idBestModel = 0;
            } else if ((eval_IP_KP.second.second < eval_IP.second.second) && (eval_IP_KP.second.second < eval_KP.second.second)) {
                // <Utiliza Modelo de Lineas + Puntos>     
                idBestModel = 2;
            } else {
                // <Utiliza Modelo de Lineas >  
                idBestModel = 1;
            }// if - else if - else
        } else {
            // Buscando entre IP y KP (SD)
            if (eval_KP.second.second < eval_IP.second.second) {
                // <Utiliza Modelo de  Puntos>
                idBestModel = 0;
            } else {
                // <Utiliza Modelo de Lineas>
                idBestModel = 1;
            } // if - else
        }// if - else if - else
    } else {
        // <Utiliza Modelo de Puntos>
        idBestModel = 0;
    } // if - if else - else

    /* Guardando Evaluaciones*/
    vectEvalModels.clear();
    vectEvalModels.push_back(eval_KP);
    vectEvalModels.push_back(eval_IP);
    vectEvalModels.push_back(eval_IP_KP);

    cout << "idBestModel = " << idBestModel << endl;
    cout << "0 - Puntos. #Inliers = " << eval_KP.first << "  SD = " << eval_KP.second.second << endl;
    cout << "1 - Lineas. #Inliers = " << eval_IP.first << "  SD = " << eval_IP.second.second << endl;
    cout << "2 - Lineas+Puntos. #Inliers = " << eval_IP_KP.first << "  SD = " << eval_IP_KP.second.second << endl;

    return idBestModel;
}// function 

void setStructWithTheBestModel(int idBestModel, Struct_Matching& structMatch,
        std::vector<cv::Mat>& vectModelos,
        std::vector<std::pair<int, std::pair<double, double > > > & vectEvalModels,
        std::vector<std::pair< int, int > > & vectPairDetectMatch,
        std::vector<int >& vectNumInlierModel) {
    // <Buscando el mejor modelo>
    std::pair<double, double > pair;
    double averageSizeImage = (structMatch.imageMat_refe.cols + structMatch.imageMat_refe.rows) / 2;


    /* Obteniendo el mejor modelo */
    structMatch.idModelUsed = idBestModel;
    structMatch.bandModelFound = true;
    structMatch.Model = vectModelos[idBestModel].clone(); // Modelo de puntos

    structMatch.numPuntosValidos = vectEvalModels[idBestModel].first; // numero de inliers            
    structMatch.media_errorDist = vectEvalModels[idBestModel].second.first; // Media Error
    structMatch.sd_errorDist = vectEvalModels[idBestModel].second.second; // SD Error

    structMatch.numFeaturesDetect = vectPairDetectMatch[idBestModel].first;
    structMatch.numMatches = vectPairDetectMatch[idBestModel].second;
    structMatch.numInliers = vectNumInlierModel[idBestModel];

    // Porcentaje Desplazamiento y Porcentaje Match
    pair = getPorcentajesModel(structMatch.bandModelFound,
            structMatch.Model, averageSizeImage,
            structMatch.numMatches, structMatch.numInliers);
    structMatch.porcentajeMatchInliers = pair.first;
    structMatch.porcentajeDesplModel = pair.second;

    //    std::vector<double> vectDist_aux;
    //    std::vector<double> vectAngle_aux;
    //
    //    double dist_media_aux = 0;
    //    double dist_sd_aux = 0;
    //    double angle_media_aux = 0;
    //    double angle_sd_aux = 0;

    switch (idBestModel) {
        case 0:
            structMatch.feature_media_errorDist = vectEvalModels[idBestModel].second.first; // Media Error
            structMatch.feature_sd_errorDist = vectEvalModels[idBestModel].second.second; // SD Error

            // ANGULO
            structMatch.feature_media_errorAngle = 0; // Media Error
            structMatch.feature_sd_errorAngle = 0; // SD Error   

            // numPuntos validos 
            structMatch.numPuntosValidos = structMatch.numInliers;
            break;
        case 1:
            /* < Lineas >*/
            // DISTANCIA                        
            structMatch.feature_media_errorDist = structMatch.feature_media_errorDist; // Media Error
            structMatch.feature_sd_errorDist = structMatch.feature_sd_errorDist; // SD Error

            // ANGULO            
            structMatch.feature_media_errorAngle = structMatch.feature_media_errorAngle; // Media Error
            structMatch.feature_sd_errorAngle = structMatch.feature_sd_errorAngle; // SD Error           
            break;
        case 2:
            /* < Puntos Y Lineas >*/
            structMatch.feature_media_errorDist = vectEvalModels[idBestModel].second.first; // Media Error
            structMatch.feature_sd_errorDist = vectEvalModels[idBestModel].second.second; // SD Error

            // ANGULO            
            structMatch.feature_media_errorAngle = 0; // Media Error
            structMatch.feature_sd_errorAngle = 0; // SD Error        
            break;
    } // switch
} // function 

void getBestModel2(Struct_Matching& structMatch,
        double umbralError, double porcentajeBaseLine) {
    /* < Evaluacion de los modelos > */
    int idBestModel = 1;
    std::pair<int, std::pair<double, double> > eval_IP = std::make_pair<int, std::pair<double, double> >(0, std::make_pair<double, double>(INFINITY, INFINITY));
    std::pair<int, std::pair<double, double> > eval_KP = std::make_pair<int, std::pair<double, double> >(0, std::make_pair<double, double>(INFINITY, INFINITY));
    std::pair<int, std::pair<double, double> > eval_IP_KP = std::make_pair<int, std::pair<double, double> >(0, std::make_pair<double, double>(INFINITY, INFINITY));

    // -----
    std::vector<cv::Point2f> vectPoolPoints_test;
    std::vector<cv::Point2f> vectPoolPoints_refe;
    //    std::vector<int> vectIndexInliers_ipkp;
    cv::Mat Model_IP_KP;

    // < ~~~~~~~~~~~~ IP ~~~~~~~~~~~~ >      
    // Obteniendo media y desviacion esandar y numero de Intersection Points inliers
    eval_IP.first = structMatch.structLineas.vectDist_LS_PI.size(); // numInliers
    eval_IP.second.second = getDesviacionEstandar(structMatch.structLineas.vectDist_LS_PI, eval_IP.second.first); // Media y SD

    // Definiendo limites (IP es considerado como baseline)
    int limInf = round(eval_IP.first - (eval_IP.first * (porcentajeBaseLine / 100)));
    int limSup = round(eval_IP.first + (eval_IP.first * (porcentajeBaseLine / 100)));

    // < ~~~~~~~~~~~~ KP ~~~~~~~~~~~~ >        
    std::vector<double> vectDistInliers_kp = getElementsFromVector(structMatch.structPuntos.vectDist_points,
            structMatch.structPuntos.vectIndexMatch_Inliers_points);
    // Obteniendo media y SD
    eval_KP.first = structMatch.structPuntos.vectIndexMatch_Inliers_points.size();
    eval_KP.second.second = getDesviacionEstandar(vectDistInliers_kp, eval_KP.second.first);

    /* < =================================== > */

    /* < =================================== > */
    if (eval_KP.first < limInf) {
        // <Utiliza Modelo de Lineas>       
        idBestModel = 1;

    } else if (eval_KP.first >= limInf && eval_KP.first <= limSup) {
        // <Probar la combinacion de lineas + puntos>      
        //        // Combinando los IP y KP
        vectPoolPoints_test = mergePointVectors(structMatch.structLineas.vectLS_PI_test, structMatch.structPuntos.vectPointsMatch_Inliers_test);
        vectPoolPoints_refe = mergePointVectors(structMatch.structLineas.vectLS_PI_refe, structMatch.structPuntos.vectPointsMatch_Inliers_refe);
        //        /* < Recalculando Modelo > */
        //        Model_IP_KP = calculateModel(idTypeModel4Ransac, vectPoolPoints_test, vectPoolPoints_refe);

        /* < Combinando Modelos (Promedio punto) > */
        Model_IP_KP = (structMatch.structLineas.Model_ip + structMatch.structPuntos.Model_kp) / 2;
        // <Evaluando modelo >        
        eval_IP_KP = getEvaluationModeloPoints(Model_IP_KP,
                vectPoolPoints_test, vectPoolPoints_refe, umbralError);

        /* < Comparando los Resultados de los modelos > */
        if (eval_IP_KP.first > limSup) {
            // <Utiliza Modelo de Lineas +  Puntos>     
            idBestModel = 2;

        } else if (eval_IP_KP.first >= limInf && eval_IP_KP.first <= limSup) {
            // Buscando entre IP , KP y IP + KP (SD)
            if ((eval_KP.second.second < eval_IP.second.second) && (eval_KP.second.second < eval_IP_KP.second.second)) {
                // <Utiliza Modelo de  Puntos>     
                idBestModel = 0;
            } else if ((eval_IP_KP.second.second < eval_IP.second.second) && (eval_IP_KP.second.second < eval_KP.second.second)) {
                // <Utiliza Modelo de Lineas + Puntos>     
                idBestModel = 2;
            } else {
                // <Utiliza Modelo de Lineas >  
                idBestModel = 1;
            }// if - else if - else
        } else {
            // Buscando entre IP y KP (SD)
            if (eval_KP.second.second < eval_IP.second.second) {
                // <Utiliza Modelo de  Puntos>
                idBestModel = 0;
            } else {
                // <Utiliza Modelo de Lineas>
                idBestModel = 1;
            } // if - else
        }// if - else if - else
    } else {
        // <Utiliza Modelo de Puntos>
        idBestModel = 0;
    } // if - if else - else

    // <Buscando el mejor modelo>
    std::pair<double, double > pair;
    double averageSizeImage = (structMatch.imageMat_refe.cols + structMatch.imageMat_refe.rows) / 2;

    cout << "idBestModel = " << idBestModel << endl;
    cout << "0 - Puntos. #Inliers = " << eval_KP.first << "  SD = " << eval_KP.second.second << endl;
    cout << "1 - Lineas. #Inliers = " << eval_IP.first << "  SD = " << eval_IP.second.second << endl;
    cout << "2 - Lineas+Puntos. #Inliers = " << eval_IP_KP.first << "  SD = " << eval_IP_KP.second.second << endl;
    switch (idBestModel) {
        case 0:
            //            cout << "idBestModel = Puntos. #Inliers = " << eval_KP.first << "  SD = " << eval_KP.second.second << endl;
            structMatch.idModelUsed = idBestModel;
            structMatch.bandModelFound = structMatch.structPuntos.bandModeloFound_kp;
            structMatch.Model = structMatch.structPuntos.Model_kp.clone(); // Modelo de puntos
            structMatch.numPuntosValidos = eval_KP.first;
            structMatch.media_errorDist = eval_KP.second.first;
            structMatch.sd_errorDist = eval_KP.second.second;

            pair = getPorcentajesModel(structMatch.bandModelFound,
                    structMatch.Model, averageSizeImage,
                    structMatch.structPuntos.vectPointsMatch_test.size(),
                    structMatch.structPuntos.vectPointsMatch_Inliers_test.size());

            structMatch.porcentajeMatchInliers = pair.first;
            structMatch.porcentajeDesplModel = pair.second;
            //            structQuadTree.vectPoints_test = structMatch.structPuntos.vectPointsMatch_Inliers_test; // Keypoints
            //            structQuadTree.vectPoints_refe = structMatch.structPuntos.vectPointsMatch_Inliers_refe; // Keypoints
            break;
        case 1:
            //            cout << "idBestModel = Lineas. #Inliers = " << eval_IP.first << "  SD = " << eval_IP.second.second << endl;
            structMatch.idModelUsed = idBestModel;
            structMatch.bandModelFound = structMatch.structLineas.bandModeloFound_ip;
            structMatch.Model = structMatch.structLineas.Model_ip.clone(); // Modelo de puntos
            structMatch.numPuntosValidos = eval_IP.first;
            structMatch.media_errorDist = eval_IP.second.first;
            structMatch.sd_errorDist = eval_IP.second.second;

            pair = getPorcentajesModel(structMatch.bandModelFound,
                    structMatch.Model, averageSizeImage,
                    structMatch.structLineas.vectLinesMatch_test.size(),
                    structMatch.structLineas.vectLinesMatch_Inliers_test.size());

            structMatch.porcentajeMatchInliers = pair.first;
            structMatch.porcentajeDesplModel = pair.second;
            //            structQuadTree.vectPoints_test = structMatch.structLineas.vectLS_PI_test; // Puntos de interseccion
            //            structQuadTree.vectPoints_refe = structMatch.structLineas.vectLS_PI_refe; // Puntos de interseccion
            break;
        case 2:
            //            cout << "idBestModel = Lineas+Puntos. #Inliers = " << eval_IP_KP.first << "  SD = " << eval_IP_KP.second.second << endl;
            structMatch.idModelUsed = idBestModel;
            structMatch.bandModelFound = true;
            structMatch.Model = Model_IP_KP; // Modelo de Lineas + Puntos
            structMatch.numPuntosValidos = eval_IP_KP.first;
            structMatch.media_errorDist = eval_IP_KP.second.first;
            structMatch.sd_errorDist = eval_IP_KP.second.second;

            pair = getPorcentajesModel(structMatch.bandModelFound,
                    structMatch.Model, averageSizeImage,
                    vectPoolPoints_refe.size(), eval_IP_KP.first);

            structMatch.porcentajeMatchInliers = pair.first;
            structMatch.porcentajeDesplModel = pair.second;
            //            structQuadTree.vectPoints_test = getSpecificPointsFromVector(vectPoolPoints_test, vectIndexInliers_ipkp);
            //            structQuadTree.vectPoints_refe = getSpecificPointsFromVector(vectPoolPoints_refe, vectIndexInliers_ipkp);
            break;
    } // switch
}// function 

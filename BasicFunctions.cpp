/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "BasicFunctions.hpp"

#include <iostream>     // std::cout
#include <algorithm>    // std::random_shuffle
#include <vector>       // std::vector
#include <ctime>        // std::time
#include <cstdlib>      // std::rand, std::srand

#include <fstream>
#include <cstdlib>
#include <string>
#include <math.h>
#include <cmath>
#include <string.h>
#include <opencv2/core/cvdef.h>

#include <dirent.h> 
#include <iosfwd> 
#include <sys/stat.h> 
#include <dirent.h>
#include <errno.h>




using namespace std;

bool renameFiles(std::string newName_Format, int numZeros_Format,
        std::string fullPathFiles, std::vector<std::string>& vectNamesImages) {

    bool bandOK = true;
    // < Checking if one element of the list has the same name desired >
    std::string cadAux = vectNamesImages[0];

    // Buscando nombre
    std::size_t found = cadAux.find(newName_Format);
    if (found != std::string::npos) {
        std::cout << " The files have relative name with the newName: \"" << newName_Format << "\"\n";
        bandOK = false;
    } else {
        // finding extension
        found = cadAux.find('.');
        std::string extension = cadAux.substr(found, cadAux.length() - found);

        // Rename      
        std::string oldName;
        std::string newName;
        std::string numberIter;

        cout << " ------------------- " << endl;
        for (int ff = 0; ff < vectNamesImages.size(); ff++) {
            oldName = vectNamesImages[ff]; // getting the old name

            // Converting number to string             
            numberIter = int2string(ff);

            newName = newName_Format + std::string(numZeros_Format - numberIter.length(), '0') +
                    numberIter + extension;

            // rename
            cout << ff << " : oldName: " << oldName << " ... newName: " << newName << endl;
            oldName = fullPathFiles + oldName;
            newName = fullPathFiles + newName;

            int result = rename(oldName.c_str(), newName.c_str());
            if (result == 0) {
                puts("\t File successfully renamed");
            } else {
                perror("Error renaming file");
                return false;
            }// if - else             
        } // for 
    }// if - else
    return bandOK;
} // function 

int findString(std::string cad1, std::string cad2) {
    std::size_t found = cad1.find(cad2);
    if (found == std::string::npos) {
        return -1;
    } else {
        return found;
    }
}// function

std::string getNameSequence(std::string& fullPathFiles, std::vector<std::string>& vectNamesImages) {
    // < Important: the name of the image sequence must be similar to below > 
    // < name_%04d.extension>
    std::string nameImageSequence = "";

    int numImages = vectNamesImages.size();

    if (numImages < 2) {
        cout << " Error : No enought number of images. " << endl;
        //    } else if (numImages == 2) {
        //        cout << " Image Mosaicing using two images. " << endl;
    } else {
        cout << " Image Mosaicing using " << int2string(numImages) << " images. " << endl;

        // obteniendo primeras cadenas
        std::string cad_1 = vectNamesImages[0];
        std::string cad_2 = vectNamesImages[1];

        // obteniendo nombre de la secuencia
        std::string cadAux, extension;

        // Conditions to check if the image secuence names are valid
        bool bandLengthSimilar = (cad_1.length() == cad_2.length()); // < Longitud similar >

        int posSlash_1 = findString(cad_1, "_");
        int posSlash_2 = findString(cad_2, "_");
        bool bandSlash = (posSlash_1 == posSlash_2); // < misma posicion de Slash >


        std::string subcad_1 = cad_1.substr(0, posSlash_1 + 1);
        std::string subcad_2 = cad_2.substr(0, posSlash_2 + 1);
        bool bandSameSubstring = !(subcad_1.compare(subcad_2)); // < misma substring >

        int posExtension_1 = findString(cad_1, ".");
        int posExtension_2 = findString(cad_2, ".");
        bool bandSameExtension = (posExtension_1 == posExtension_2); // < Misma extension >       

        // < Revisando si inicia en 0 o en 1 >
        int numDigitos = posExtension_1 - posSlash_1 - 1;
        std::string primeraNumeracion = cad_1.substr(posSlash_1 + 1, numDigitos);
        bool c0 = !primeraNumeracion.compare(std::string(numDigitos, '0'));
        bool c1 = !primeraNumeracion.compare(std::string(numDigitos - 1, '0') + "1");
        bool bandInicioBien = c0 || c1;


        bool isOK = false;
        extension = cad_1.substr(posExtension_1, cad_1.length() - posExtension_1);

        if (bandLengthSimilar && bandSlash && bandSameSubstring && bandSameExtension && bandInicioBien) {
            cadAux = cad_1.substr(0, posSlash_1 + 1); // subCadena
            isOK = true;

            // Checking all the files if they have relative names
            int valIni = (c0) ? 0 : 1;
            int indicadorLimite = 10;
            int contIndicadorLimite = 1;
            for (int ff = 0; ff < vectNamesImages.size(); ff++) {
                //if (isItInTheString(vectNamesImages[ff], cadAux) < 0) {

                if (ff >= indicadorLimite) {
                    indicadorLimite = indicadorLimite * 10;
                    contIndicadorLimite++;
                }// if 

                std::string cadd = cadAux + std::string(numDigitos - contIndicadorLimite, '0') + int2string(valIni)+extension;
                if (vectNamesImages[ff].compare(cadd)) {
                    isOK = false;
                    break;
                } // if 

                valIni++;
            } // for 
        }// if 

        // < ---------------------------------------- >

        if (isOK) {
            // std::string(numZeros_Format - numberIter.length(), '0') +
            //            cout<<vectNamesImages[0]<<endl;
            //            cout<<posExtension_1<< " ----- "<< posSlash_1<<endl;
            nameImageSequence = cadAux + "%0" + int2string(numDigitos) + "d" + extension;
            cout << "\n Name of image sequence : " << nameImageSequence << endl;
        } else {
            // < Renombrar >
            bool bandInvalidResponse = true;

            char response;
            while (bandInvalidResponse) {
                cout << " There is not a valid name sequence. \n Do you want to rename all the files with \"imgseq_%04."<<extension<<"\" (Y/n)?" << endl;
                cin>>response;

                // < Positive response >
                if (response == 'y' || response == 'Y') {
                    // < Renombrar >
                    numDigitos = 4;
                    std::string cadRename;
                    bool bandErrorRename = true;
                    int contIntentos = 0;
                    while (bandErrorRename) {
                        cadRename = "imgseq" + int2string(contIntentos) + "_";
                        bandErrorRename = !(renameFiles(cadRename, numDigitos, fullPathFiles, vectNamesImages));
                        contIntentos++;
                    }// while 
                    nameImageSequence = cadRename + "%0" + int2string(numDigitos) + extension;
                    bandInvalidResponse = false;
                }// if 

                    // < Negative response >
                else if (response == 'n' || response == 'N') {
                    // < Salir >
                    bandInvalidResponse = false;
                    nameImageSequence = "Error";
                } // if else
            } //while 
        }// if - else
    } // if - else if  - else

    return nameImageSequence;
} // function


bool existDirectory(std::string directory) {
    bool bandDirectoryOK;

    DIR* dir = opendir(directory.c_str());
    if (dir) {
        /* Directory exists. */
        closedir(dir);
        bandDirectoryOK = true; // < Ya existe el directorio >

    } else {
        bandDirectoryOK = false; // <No existe el directorio>
    }// if - else if - else

    return bandDirectoryOK;
} // function

bool makeSureDirectoryExist(std::string directory) {
    bool bandDirectoryOK;

    DIR* dir = opendir(directory.c_str());
    if (dir) {
        /* Directory exists. */
        closedir(dir);
        bandDirectoryOK = true; // < Ya existe el directorio >

    } else if (ENOENT == errno) {
        /* Directory does not exist. */
        mkdir(directory.c_str(), 0777);
        bandDirectoryOK = true; // < Se creo el directorio  >

    } else {
        bandDirectoryOK = false; // <No se creo el directorio>

    }// if - else if - else

    return bandDirectoryOK;
} // function 

std::vector<std::string> getFilesInDirectory(const std::string directory) {
    /* Vector de Directorios */
    std::vector<std::string> vectFullFileName;
    std::vector<std::string> vectFileName;
#ifdef WINDOWS
    HANDLE dir;
    WIN32_FIND_DATA file_data;

    if ((dir = FindFirstFile((directory + "/*").c_str(), &file_data)) == INVALID_HANDLE_VALUE)
        return; /* No files found */

    do {
        const string file_name = file_data.cFileName;
        const string full_file_name = directory + "/" + file_name;
        const bool is_directory = (file_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0;

        if (file_name[0] == '.')
            continue;

        if (is_directory)
            continue;

        vectFullFileName.push_back(full_file_name);
    } while (FindNextFile(dir, &file_data));

    FindClose(dir);
#else
    DIR *dir;
    class dirent *ent;
    class stat st;

    dir = opendir(directory.c_str());
    while ((ent = readdir(dir)) != NULL) {
        const std::string file_name = ent->d_name;
        const std::string full_file_name = directory + "/" + file_name;

        /* Si es un archivo que comienza con . lo omite*/
        if (file_name[0] == '.')
            continue;

        if (stat(full_file_name.c_str(), &st) == -1)
            continue;

        const bool is_directory = (st.st_mode & S_IFDIR) != 0;

        /* Si es un subDirectorio lo omite*/
        if (is_directory)
            continue;

        vectFullFileName.push_back(full_file_name);
        vectFileName.push_back(file_name);
    }
    closedir(dir);

    /* Ordenando el vector para trabjar deacuerdo al nombre especificado por cada archivo*/
    std::sort(vectFullFileName.begin(), vectFullFileName.end());
    std::sort(vectFileName.begin(), vectFileName.end());

    return vectFileName;
#endif
} // GetFilesInDirectory

std::vector<int> sortAndTrackIndices(std::vector<double>& vectorVals) {
    int numElem = vectorVals.size();
    std::vector<int> vectorIndices(numElem);
    // Comienza el algoritmo
    std::vector<std::pair<double, int> >a;
    // Llenando vector de pares 
    for (int ii = 0; ii < numElem; ii++) {
        a.push_back(make_pair(vectorVals[ii], ii)); // k = value, i = original index
    } // for 

    // Ordenando 
    sort(a.begin(), a.end());

    for (int ii = 0; ii < numElem; ii++) {
        vectorVals[ii] = a[ii].first;
        vectorIndices[ii] = a[ii].second;
    } // for 

    // Regresando indices del ordenamiento 
    return vectorIndices;
} // function 

std::vector<int> sortAndTrackIndices(std::vector<int>& vectorVals) {
    int numElem = vectorVals.size();
    std::vector<int> vectorIndices(numElem);
    // Comienza el algoritmo
    std::vector<std::pair<int, int> >a;
    // Llenando vector de pares 
    for (int ii = 0; ii < numElem; ii++) {
        a.push_back(make_pair(vectorVals[ii], ii)); // k = value, i = original index
    } // for 

    // Ordenando 
    sort(a.begin(), a.end());

    for (int ii = 0; ii < numElem; ii++) {
        vectorVals[ii] = a[ii].first;
        vectorIndices[ii] = a[ii].second;
    } // for 

    // Regresando indices del ordenamiento 
    return vectorIndices;
} // function 

std::string checkNameFile(std::string nombreArchivo) {
    //    std::string str2;
    //    //std::size_t foundVal1 = nombreArchivo.find("/", 10);
    //
    //    std::size_t foundVal = nombreArchivo.find("%");
    //    if (foundVal == std::string::npos) {
    //        str2 = nombreArchivo;
    //    } else {
    //        //str2 = nombreArchivo.substr(0, foundVal - 1);
    //        str2 = "secuencia";
    //    } // if -else
    //    return str2;
    bool band = true;
    while (band) {
        std::size_t foundDiagonal = nombreArchivo.find("\\");
        if (foundDiagonal == std::string::npos) {
            band = false;
        } else {
            nombreArchivo = nombreArchivo.substr(foundDiagonal + 1, nombreArchivo.length());
        }
    }//while 

    std::string str2;
    std::size_t foundVal = nombreArchivo.find(".");
    if (foundVal == std::string::npos) {
        str2 = nombreArchivo;
    } else {
        str2 = nombreArchivo.substr(0, foundVal);
    } // if -else
    return str2;
} //function

void printVectorDistanciaAngulo_comoVariableMatlab(std::vector<double>& vectDist,
        std::vector<double>& vectAngu, std::vector<int>vectIndicesValidos, std::string nombre) {
    /* Creando vector de vanderas validas */
    std::vector<bool>vectBands = generaVectorBanderas(vectDist.size(), vectIndicesValidos, true);

    cout << nombre << " = {";
    for (int c = 0; c < vectDist.size(); c++) {
        cout << c << " , " << vectDist[c] << " , " << vectAngu[c] << " , " << ((vectBands[c]) ? "\'Si\'" : "\'No\'");
        if (c < vectDist.size() - 1) {
            cout << ";" << endl;
        }//if
    }//for c
    cout << "};" << endl;
} //function

int getNumberOfIterationsRANSAC(int numMuestrasModelo, double probElegirInlier, double probObtenerBuenaMuestra) {
    int numIteraciones;
    /* Variables auxiliares */
    double lg_probObtenerMalaMuestra = log10(1 - probObtenerBuenaMuestra);
    double lg_probElegirMuestraOutliersEnN_Iteraciones = log10(1 - pow(probElegirInlier, numMuestrasModelo));

    /* Calculando numero de iteraciones ransac */
    numIteraciones = round(lg_probObtenerMalaMuestra / lg_probElegirMuestraOutliersEnN_Iteraciones);

    return numIteraciones;
} // function 

std::vector<double> getColFromVector2D(std::vector<std::vector<double> >& vect2D, int col) {
    int numF = vect2D.size();
    int numC = vect2D[0].size();

    std::vector<double> vect1D(numF);

    for (int ii = 0; ii < numF; ii++) {
        vect1D[ii] = vect2D[ii][col];
    } // for
    return vect1D;
} // function

std::vector<double> getRowFromVector2D(std::vector<std::vector<double> >& vect2D, int row) {
    int numF = vect2D.size();
    int numC = vect2D[0].size();

    std::vector<double> vect1D(numC);

    for (int ii = 0; ii < numC; ii++) {
        vect1D[ii] = vect2D[row][ii];
    } // for
    return vect1D;
} // function

std::vector<int> getRowFromVector2D(std::vector<std::vector<int> >& vect2D, int row) {
    int numF = vect2D.size();
    int numC = vect2D[0].size();

    std::vector<int> vect1D(numC);

    for (int ii = 0; ii < numC; ii++) {
        vect1D[ii] = vect2D[row][ii];
    } // for
    return vect1D;
} // function

double printTimes(std::vector<double>& vectTiempo, std::vector<std::string>& vectEtiquetas, int iter) {
    int numE = vectTiempo.size();
    double total = getSumatoriaDeVector(vectTiempo);

    if (true) {
        cout << "\t ------>> RunTime <<------" << endl;
        for (int ii = 0; ii < numE; ii++) {
            printf("%s = %.4f%s\n", vectEtiquetas[ii].c_str(), vectTiempo[ii]*1000, " ms");
        } // for     
        printf(" ->TiempoIter_%i = %.5f %s = %.5f %s\n", iter, total * 1000, "ms", total, "sec");
    } // if 

    return total;
} //function

std::vector<std::string> getVectorOfVals(int numElementos, std::string val) {
    std::vector<std::string> vect(numElementos);

    for (int ii = 0; ii < numElementos; ii++) {
        vect[ii] = val;
    } // for 

    return vect;
} // function

std::vector<double> getVectorOfVals(int numElementos, double val) {
    std::vector<double> vect(numElementos);

    for (int ii = 0; ii < numElementos; ii++) {
        vect[ii] = val;
    } // for 

    return vect;
} // function

void printVectorStrings_Directorios(std::vector<std::string> vect, std::string cad) {
    cout << cad << endl;
    for (int ii = 0; ii < vect.size(); ii++) {
        cout << ii << " : " << vect[ii] << endl;
    } // for
} // function 

bool existElementVector(std::vector<int>& vect, int val) {
    int numElementos = vect.size();

    for (int ii = 0; ii < numElementos; ii++) {
        if (vect[ii] == val) {
            return true;
        } // if
    } // for

    return false;
} // function 

std::vector<int> getElementsVectUnicos(std::vector<int>& vect) {
    int numElementos = vect.size();
    std::vector<int> vectR;
    int val;

    for (int ii = 0; ii < numElementos; ii++) {
        val = vect[ii];
        if (existElementVector(vectR, val) == false) {
            vectR.push_back(val);
        } // if
    } // for

    return vectR;
} // function 

std::vector<double> agregaElementosVector(std::vector<double>& vectorBase, std::vector<double>& vectorExtraElementos) {
    int numExtraElementos = vectorExtraElementos.size();

    std::vector<double> vectorResultados = cloneVector(vectorBase);

    for (int ii = 0; ii < numExtraElementos; ii++) {
        vectorResultados.push_back(vectorExtraElementos[ii]);
    }//for 

    return vectorResultados;
} // fucntion

std::vector<int> agregaElementosVector(std::vector<int>& vectorBase, std::vector<int>& vectorExtraElementos) {
    int numExtraElementos = vectorExtraElementos.size();

    std::vector<int> vectorResultados = cloneVector(vectorBase);

    for (int ii = 0; ii < numExtraElementos; ii++) {
        vectorResultados.push_back(vectorExtraElementos[ii]);
    }//for 

    return vectorResultados;
} // fucntion

std::vector<bool> generaVectorFalse(int numElementos) {
    std::vector<bool> vectBanderas(numElementos);
    for (int ii = 0; ii < numElementos; ii++) {
        vectBanderas[ii] = false;
    } //for
    return vectBanderas;
} // fucntion

std::vector<bool> generaVectorTrue(int numElementos) {
    std::vector<bool> vectBanderas(numElementos);
    for (int ii = 0; ii < numElementos; ii++) {
        vectBanderas[ii] = true;
    } //for
    return vectBanderas;
} // fucntion

std::vector<bool> generaVectorBanderas(int numElementos, std::vector<int>& vecIndicesTrue, bool valBool) {
    std::vector<bool> vectBanderas;
    if (valBool) {
        vectBanderas = generaVectorFalse(numElementos);
    } else {
        vectBanderas = generaVectorTrue(numElementos);
    } //if

    for (int ii = 0; ii < vecIndicesTrue.size(); ii++) {
        vectBanderas[vecIndicesTrue[ii]] = valBool;
    }
    return vectBanderas;
} // function

std::vector<double> cloneVector(std::vector<double>& vect) {
    int numElementos = vect.size();
    std::vector<double> vect2(numElementos);

    for (int ii = 0; ii < numElementos; ii++) {
        vect2[ii] = vect[ii];
    } // for

    return vect2;
} // funtion

std::vector<int> cloneVector(std::vector<int>& vect) {
    int numElementos = vect.size();
    std::vector<int> vect2(numElementos);

    for (int ii = 0; ii < numElementos; ii++) {
        vect2[ii] = vect[ii];
    } // for

    return vect2;
} // funtion

std::vector<int> getElementsFromVector(std::vector<int>& vector, std::vector<int>& vect_index) {
    int numElementos = vect_index.size();
    std::vector<int> validosVector_byIndex(numElementos);
    int index;
    for (int jj = 0; jj < numElementos; jj++) {
        index = vect_index[jj];
        validosVector_byIndex[jj] = vector[index];
    } // for

    return validosVector_byIndex;
} // for

std::vector<bool> getElementsFromVector(std::vector<bool>& vector, std::vector<int>& vect_index) {
    int numElementos = vect_index.size();
    std::vector<bool> validosVector_byIndex(numElementos);
    int index;
    for (int jj = 0; jj < numElementos; jj++) {
        index = vect_index[jj];
        validosVector_byIndex[jj] = vector[index];
    } // for

    return validosVector_byIndex;
} // for

std::vector<std::pair<int, int> > getElementsFromPairVector(std::vector<std::pair<int, int> >& vector, std::vector<int>& vect_index) {
    int numElementos = vect_index.size();
    std::vector<std::pair<int, int> > validosVector_byIndex(numElementos);
    int index;
    for (int jj = 0; jj < numElementos; jj++) {
        index = vect_index[jj];
        validosVector_byIndex[jj] = vector[index];
    } // for

    return validosVector_byIndex;
} // for

std::vector<double> getElementsFromVector(std::vector<double>& vector, std::vector<int>& vect_index) {
    int numElementos = vect_index.size();
    std::vector<double> validosVector_byIndex(numElementos);
    int index;
    for (int jj = 0; jj < numElementos; jj++) {
        index = vect_index[jj];
        validosVector_byIndex[jj] = vector[index];
    } // for

    return validosVector_byIndex;
} // for

bool satisfaceCondicion(int valVector, std::string expresion, double otroVal) {
    if (strcmp(expresion.c_str(), "==") == 0) {
        if (valVector == otroVal) {
            return true;
        } // if
    } else if (strcmp(expresion.c_str(), ">") == 0) {
        if (valVector > otroVal) {
            return true;
        } // if
    }// else if
    else if (strcmp(expresion.c_str(), "<") == 0) {
        if (valVector < otroVal) {
            return true;
        } // if
    }// else if
    else if (strcmp(expresion.c_str(), ">=") == 0) {
        if (valVector >= otroVal) {
            return true;
        } // if
    }// else if
    else if (strcmp(expresion.c_str(), "<=") == 0) {
        if (valVector <= otroVal) {
            return true;
        } // if
    }// else if
    else if (strcmp(expresion.c_str(), "!=") == 0) {
        if (valVector != otroVal) {
            return true;
        } // if
    }// else if

    /* Regresa False si no coincidio con las anteriores sentencias */
    return false;
} // function 

std::vector<int> findElementosSatisfacenExpresion(std::vector<int>& vector, std::string expresion, double val) {
    std::vector<int> indexValidos;
    int numElementos = vector.size();

    for (int jj = 0; jj < numElementos; jj++) {
        /* Revisando Si satisface condicion */
        if (satisfaceCondicion(vector[jj], expresion, val)) {
            indexValidos.push_back(jj);
        } // if

    }// for 
    return indexValidos;
} // function 

std::vector<int> findElementosSatisfacenDobleExpresion(std::vector<int>& vector,
        std::string expresion1, double val1,
        std::string expresion2, double val2) {

    std::vector<int> indexValidos;
    int numElementos = vector.size();

    for (int jj = 0; jj < numElementos; jj++) {
        /* Revisando Si satisface condicion */
        if (satisfaceCondicion(vector[jj], expresion1, val1) && satisfaceCondicion(vector[jj], expresion2, val2)) {
            indexValidos.push_back(jj);
        } // if

    }// for 
    return indexValidos;
} // function 

bool satisfaceCondicion(double valVector, std::string expresion, double otroVal) {
    if (strcmp(expresion.c_str(), "==") == 0) {
        if (valVector == otroVal) {
            return true;
        } // if
    } else if (strcmp(expresion.c_str(), ">") == 0) {
        if (valVector > otroVal) {
            return true;
        } // if
    }// else if
    else if (strcmp(expresion.c_str(), "<") == 0) {
        if (valVector < otroVal) {
            return true;
        } // if
    }// else if
    else if (strcmp(expresion.c_str(), ">=") == 0) {
        if (valVector >= otroVal) {
            return true;
        } // if
    }// else if
    else if (strcmp(expresion.c_str(), "<=") == 0) {
        if (valVector <= otroVal) {
            return true;
        } // if
    }// else if
    else if (strcmp(expresion.c_str(), "!=") == 0) {
        if (valVector != otroVal) {
            return true;
        } // if
    }// else if

    /* Regresa False si no coincidio con las anteriores sentencias */
    return false;
} // function 

std::vector<int> findElementosSatisfacenExpresion(std::vector<bool>& vector) {
    std::vector<int> indexValidos;
    int numElementos = vector.size();

    for (int jj = 0; jj < numElementos; jj++) {
        /* Revisando Si satisface condicion */
        if (vector[jj]) {
            indexValidos.push_back(jj);
        } // if

    }// for 
    return indexValidos;
} // function 

int count_True_ElementsVect(std::vector<bool>& vector) {
    int numElem_true = 0;
    for (int ii = 0; ii < vector.size(); ii++) {
        if (vector[ii]) {
            numElem_true++;
        } // if 
    } // for 
    return numElem_true;
} // function 

int count_False_ElementsVect(std::vector<bool>& vector) {
    int numElem_false = 0;
    for (int ii = 0; ii < vector.size(); ii++) {
        if (!vector[ii]) {
            numElem_false++;
        } // if 
    } // for 
    return numElem_false;
} // function

std::vector<int> findElementos_No_SatisfacenExpresion(std::vector<bool>& vector) {
    std::vector<int> indexValidos;
    int numElementos = vector.size();

    for (int jj = 0; jj < numElementos; jj++) {
        /* Revisando Si satisface condicion */
        if (!vector[jj]) {
            indexValidos.push_back(jj);
        } // if

    }// for 
    return indexValidos;
} // function 

std::vector<int> findElementosSatisfacenExpresion(std::vector<double>& vector, std::string expresion, double val) {
    std::vector<int> indexValidos;
    int numElementos = vector.size();

    for (int jj = 0; jj < numElementos; jj++) {
        /* Revisando Si satisface condicion */
        if (satisfaceCondicion(vector[jj], expresion, val)) {
            indexValidos.push_back(jj);
        } // if

    }// for 
    return indexValidos;
} // function 

std::vector<int> findElementosSatisfacenDobleExpresion(std::vector<double>& vector,
        std::string expresion1, double val1,
        std::string expresion2, double val2) {

    std::vector<int> indexValidos;
    int numElementos = vector.size();

    for (int jj = 0; jj < numElementos; jj++) {
        /* Revisando Si satisface condicion */
        if (satisfaceCondicion(vector[jj], expresion1, val1) && satisfaceCondicion(vector[jj], expresion2, val2)) {
            indexValidos.push_back(jj);
        } // if

    }// for 
    return indexValidos;
} // function 

std::vector<int> findElementVector(bool val, std::vector<bool> vector) {
    std::vector<int> vectPosiciones;

    for (int ii = 0; ii < vector.size(); ii++) {
        if (vector[ii] == val) {
            vectPosiciones.push_back(ii);
        }//if 
    } //for 
    return vectPosiciones;
} //function

int findIndexOfValInVector(int val, std::vector<int> vector) {
    int index = -1;

    for (int ii = 0; ii < vector.size(); ii++) {
        if (vector[ii] == val) {
            return ii;
        }//if 
    } //for 
    return index;
} //function

std::vector<int> findElementVector(int val, std::vector<int> vector) {
    std::vector<int> vectPosiciones;

    for (int i = 0; i < vector.size(); i++) {
        if (vector[i] == val) {
            vectPosiciones.push_back(i);
        }//if 
    } //for 
    return vectPosiciones;
} //function

std::vector<double> eraseValVector(double val, std::vector<double>& vector) {
    std::vector<double> vect_new;

    for (int ii = 0; ii < vector.size(); ii++) {
        if (vector[ii] != val) {
            vect_new.push_back(vector[ii]);
        }//if 
    } //for 
    return vect_new;
} //function

std::vector<int> eraseValVector(int val, std::vector<int>& vector) {
    std::vector<int> vect_new;

    for (int ii = 0; ii < vector.size(); ii++) {
        if (vector[ii] != val) {
            vect_new.push_back(vector[ii]);
        }//if 
    } //for 
    return vect_new;
} //function

std::vector<int> generaVecNumAleatorios(int iniVal, int endVal, int numDatos) {
    std::vector<int> vectorResults;
    int opcion = 1;

    switch (opcion) {
        case 1:
        {
            vectorResults = generaVecNumAleatorios_aux1(iniVal, endVal, numDatos);
            break;
        } // case
        case 2:
        {
            vectorResults = generaVecNumAleatorios_aux2(iniVal, endVal, numDatos);
            break;
        } // case
    } // switch
    return vectorResults;
} // function

std::vector<int> generaVecNumAleatorios_aux2(int iniVal, int endVal, int numDatos) {
    std::vector<int> vect;
    int valRandom;

    //std::srand( ( std::time(NULL))); //<- Esto hace que siempre salgan los mismos numeros
    int valRand_aux;
    while (vect.size() < numDatos) {
        /* Obteniendo valor aleatorio */
        valRand_aux = rand();
        //        cout << "rand()  = " << valRand_aux << endl;
        valRandom = valRand_aux % endVal + iniVal; // valRandom in the range iniVal to endVal

        /* Obteniendo las posiciones donde se encuentra el elemento */
        std::vector<int> vectPosicionesElemento = findElementVector(valRandom, vect);

        if (vectPosicionesElemento.empty()) {
            /*No esta ese elemento en el vector*/
            vect.push_back(valRandom);
        } //if
    }//while
    return vect;
} //function

std::vector<int> generaVecNumAleatoriosDeVectorIndices(std::vector<int> vecIndicesValidos, int numDatos) {
    /* Voy a obtener los valores de vecIndicesValidos esos son los indices aleatorios*/
    int numElementos = vecIndicesValidos.size();

    std::vector<int> indicesAleatorios_local = generaVecNumAleatorios(0, numElementos, numDatos);

    std::vector<int> indicesAleatorios_validos = getElementsFromVector(vecIndicesValidos, indicesAleatorios_local);

    return indicesAleatorios_validos;
}// for 

std::vector<int> generaVecNumAleatorios_aux1(int iniVal, int endVal, int numDatos) {

    std::vector<int> myvector;

    myvector = generaVectorNumConsecutivos(iniVal, endVal);

    // using built-in random generator:
    std::random_shuffle(myvector.begin(), myvector.end());

    // using myrandom:
    std::random_shuffle(myvector.begin(), myvector.end(), myrandom);

    std::vector<int> vecIndAleatorios(numDatos);
    for (int i = 0; i < numDatos; ++i) {
        vecIndAleatorios[i] = myvector[i];
    }//for

    return vecIndAleatorios;
}// function

int myrandom(int i) {
    return std::rand() % i;
}

std::vector<int> generaVectorNumConsecutivos(int iniVal, int endVal) {
    std::vector<int> myvector;
    for (int i = iniVal; i < endVal; ++i) {
        myvector.push_back(i); // 1 2 3 4 5 6 7 8 9
    } //for 
    return myvector;
} // function

int factorial(int n) {
    //    int fact = 1;
    //    for (int i = 1; i <= n; i++) {
    //        fact = fact* i;
    //    }// for
    //    return fact;
    return (n == 1 || n == 0) ? 1 : factorial(n - 1) * n;
} // function

std::vector< std::vector<double> > getMatTranspuesta(std::vector< std::vector<double> >& mat) {
    int fil = mat.size();
    int col = mat[0].size();

    std::vector< std::vector<double> > matTrans(col, std::vector<double>(fil));

    for (int f = 0; f < fil; f++) {
        for (int c = 0; c < col; c++) {
            matTrans[c][f] = mat[f][c];
        }// for
    }//for
    return matTrans;
}//function

std::vector< std::vector<double> > calcularProductoMatrices(std::vector< std::vector<double> >& matA, std::vector< std::vector<double> >& matB) {
    int fil_A = matA.size();
    int col_A = matA[0].size();
    int fil_B = matB.size();
    int col_B = matB[0].size();

    std::vector< std::vector<double> > matR(fil_A, std::vector<double>(col_B));

    if (col_A == fil_B) {
        for (int f = 0; f < fil_A; f++) {
            for (int c = 0; c < col_B; c++) {
                matR[f][c] = 0;
                for (int i = 0; i < fil_B; i++) {
                    matR[f][c] = matR[f][c]+ (matA[f][i] * matB[i][c]);
                }//for
            }// for
        }//for
    } else {
        cout << "Error: las matrices no tienen el tamaño valido para realizar el producto."
                "\n MatA[" << fil_A << " * " << col_A << "], MatB[" << fil_B << " * " << col_B << "]" << endl;
    }//else
    return matR;
}//function

void printVector2D_comoVariableMatlab(std::vector< std::vector<int> >& mat, std::string nombre) {
    cout << nombre << " = [";
    for (int f = 0; f < mat.size(); f++) {
        for (int c = 0; c < mat[0].size(); c++) {
            cout << mat[f][c];
            if (c < mat[0].size() - 1) {
                cout << ", ";
            }//if
        }//for c
        if (f < mat.size() - 1) {
            cout << ";\n";
        }

    }//for f
    cout << "];" << endl;
} //function

void printVectorPares_comoVariableMatlab(std::vector< std::pair<int, int> >& vect, std::string nombre) {
    cout << nombre << " = [";
    for (int c = 0; c < vect.size(); c++) {
        cout << vect[c].first << ", " << vect[c].second;
        if (c < vect.size() - 1) {
            cout << ";\n ";
        }//if
    }//for c
    cout << "];" << endl;
} //function

void printVector2D_comoVariableMatlab(std::vector< std::vector<double> >& mat, std::string nombre) {
    cout << nombre << " = [";
    for (int f = 0; f < mat.size(); f++) {
        for (int c = 0; c < mat[0].size(); c++) {
            cout << mat[f][c];
            if (c < mat[0].size() - 1) {
                cout << ", ";
            }//if
        }//for c
        if (f < mat.size() - 1) {
            cout << ";\n";
        }

    }//for f
    cout << "];" << endl;
} //function

void writeVector2D_comoVariableMatlab(std::vector< std::vector<double> >& mat, std::string nombre, std::ofstream& outfile) {
    outfile << nombre << " = [";
    for (int f = 0; f < mat.size(); f++) {
        for (int c = 0; c < mat[0].size(); c++) {
            outfile << mat[f][c];
            if (c < mat[0].size() - 1) {
                outfile << ", ";
            }//if
        }//for c
        if (f < mat.size() - 1) {
            outfile << ";\n";
        }

    }//for f
    outfile << "];" << endl;
} //function

void writeVector2D_comoVariableMatlab(std::vector< std::vector<int> >& mat, std::string nombre, std::ofstream& outfile) {
    outfile << nombre << " = [";
    for (int f = 0; f < mat.size(); f++) {
        for (int c = 0; c < mat[0].size(); c++) {
            outfile << mat[f][c];
            if (c < mat[0].size() - 1) {
                outfile << ", ";
            }//if
        }//for c
        if (f < mat.size() - 1) {
            outfile << ";\n";
        }

    }//for f
    outfile << "];" << endl;
} //function

void writeVector_comoVariableMatlab(std::vector<int>& vect, std::string nombre, std::ofstream& outfile) {
    outfile << nombre << " = [";
    for (int c = 0; c < vect.size(); c++) {
        outfile << vect[c];
        if (c < vect.size() - 1) {
            outfile << ", ";
        }//if
    }//for c
    outfile << "];" << endl;
} //function

void writeVector_comoVariableMatlab(std::vector<double>& vect, std::string nombre, std::ofstream& outfile) {
    outfile << nombre << " = [";
    for (int c = 0; c < vect.size(); c++) {
        outfile << vect[c];
        if (c < vect.size() - 1) {
            outfile << ", ";
        }//if
    }//for c
    outfile << "];" << endl;
} //function

void writeVector_comoVariableMatlab(std::vector<bool>& vect, std::string nombre, std::ofstream& outfile) {
    outfile << nombre << " = [";
    for (int c = 0; c < vect.size(); c++) {
        outfile << ((vect[c]) ? "true" : "false");
        if (c < vect.size() - 1) {
            outfile << ", ";
        }//if
    }//for c
    outfile << "];" << endl;
} //function

void writeVector_comoVariableMatlab(std::vector<std::string>& vect, std::string nombre, std::ofstream& outfile) {
    outfile << nombre << " = {";
    for (int c = 0; c < vect.size(); c++) {
        outfile << "'" << vect[c] << "'";
        if (c < vect.size() - 1) {
            outfile << ", ";
        }//if
    }//for c
    outfile << "};" << endl;
} //function

void printVector_comoVariableMatlab(std::vector<double>& vect, std::string nombre) {
    cout << nombre << " = [";
    for (int c = 0; c < vect.size(); c++) {
        cout << vect[c];
        if (c < vect.size() - 1) {
            cout << ", ";
        }//if
    }//for c
    cout << "];" << endl;
} //function

void printVector_comoVariableMatlab(std::vector<int>& vect, std::string nombre) {
    cout << nombre << " = [";
    for (int c = 0; c < vect.size(); c++) {
        cout << vect[c];
        if (c < vect.size() - 1) {
            cout << ", ";
        }//if
    }//for c
    cout << "];" << endl;
} //function

void printVector_comoVariableMatlab(std::vector<bool>& vect, std::string nombre) {
    cout << nombre << " = [";
    for (int c = 0; c < vect.size(); c++) {
        cout << vect[c];
        if (c < vect.size() - 1) {
            cout << ", ";
        }//if
    }//for c
    cout << "];" << endl;
} //function

void calculateDivisionMat(std::vector< std::vector<double> >& mat, double val) {
    int fil = mat.size();
    int col = mat[0].size();

    for (int f = 0; f < fil; f++) {
        for (int c = 0; c < col; c++) {
            mat[f][c] = mat[f][c] / val;
        }//for
    } // for 
} //function

void vecMat2array_porFil(std::vector< std::vector<double> >& A, double *arr) {
    int nFil = A.size();
    int nCol = A[0].size();
    int index;

    for (int f = 0; f < nFil; f++) {
        for (int c = 0; c < nCol; c++) {
            /*Por Columna*/
            index = ((nFil) * c) + f;

            arr[index] = A[f][c];
            //            cout << "  [" << index << "]=" << arr[index];
            //cout << " **arr[" << index << "] = A[" << f << "][" << c << "]";
        } //for
        //        cout << endl;
    }//for
}//function 

std::vector< std::vector<double> > array2vecMat_porFil(double *array, int nFil, int nCol) {
    std::vector< std::vector<double> > mat(nCol, std::vector<double>(nFil));
    int f, c;
    for (int ind = 0; ind < nFil * nCol; ind++) {
        f = ind / nFil;
        c = ind % nFil;
        mat[f][c] = array[ind];
    }//for
    return mat;
}//function

void vecMat2array_porCol(std::vector< std::vector<double> >& A, double *arr) {
    //std::vector< std::vector<double> > Mat
    int nFil = A.size();
    int nCol = A[0].size();

    //    cout<<"nFil = "<<nFil<<"  nCol = "<<nCol<<endl;
    int index;
    for (int f = 0; f < nFil; f++) {
        for (int c = 0; c < nCol; c++) {
            /*Por Fila*/
            index = ((nCol) * f) + c;

            arr[index] = A[f][c];
            //            cout << "  [" << index << "]=" << arr[index];
            //cout << " **arr[" << index << "] = A[" << f << "][" << c << "]";
        } //for
        //        cout << endl;
    }//for
}//function 

std::vector< std::vector<double> > array2vecMat_porCol(double *array, int nFil, int nCol) {
    //std::vector< std::vector<double> > mat(nFil, std::vector<double>(nCol));
    std::vector< std::vector<double> > mat(nFil, std::vector<double>(nCol));
    int f, c;
    for (int ind = 0; ind < nFil * nCol; ind++) {
        f = ind / nCol;
        c = ind % nCol;
        mat[f][c] = array[ind];
        //               cout << "  [" << f << "][" << c << "]=" << array[ind]<<endl;
    }//for
    return mat;
}//function

std::vector< std::vector<double> > vecOneDim2twoDim(std::vector<double>& array, int nFil, int nCol) {
    std::vector< std::vector<double> > mat(nCol, std::vector<double>(nFil));
    int f, c;
    for (int ind = 0; ind < nFil * nCol; ind++) {
        f = ind / nFil;
        c = ind % nFil;
        mat[f][c] = array[ind];
    }//for
    return mat;
}//function

int findMaxElementPosicion(std::vector<double> & vect) {
    double maxElement = getMaxElementVec(vect); // Obteniendo el numero Maximo de un vector
    for (int ii = 0; ii < vect.size(); ii++) {
        if (maxElement == vect[ii]) {
            return ii;
        }
    }// for
} // function

int findMinElementPosicion(std::vector<int> & vect) {
    int minElement = getMinElementVec(vect); // Obteniendo el numero Minimo de un vector
    for (int ii = 0; ii < vect.size(); ii++) {
        if (minElement == vect[ii]) {
            return ii;
        }
    }// for
} // function

int findMaxElementPosicion(std::vector<int> & vect) {
    double maxElement = getMaxElementVec(vect); // Obteniendo el numero Maximo de un vector
    for (int ii = 0; ii < vect.size(); ii++) {
        if (maxElement == vect[ii]) {
            return ii;
        }
    }// for
} // function

int findMinElementPosicion(std::vector<double> & vect) {
    double minElement = getMinElementVec(vect); // Obteniendo el numero Minimo de un vector
    for (int ii = 0; ii < vect.size(); ii++) {
        if (minElement == vect[ii]) {
            return ii;
        }
    }// for
} // function

double getMaxElementVec(std::vector<double> & vect) {
    double maxVal = 0;
    for (int i = 0; i < vect.size(); i++) {
        if (vect[i] > maxVal) {
            maxVal = vect[i];
        }//if
    }// for
    return maxVal;
}//Function

int getMaxElementVec(std::vector<int> & vect) {
    int maxVal = 0;
    for (int i = 0; i < vect.size(); i++) {
        if (vect[i] > maxVal) {
            maxVal = vect[i];
        }//if
    }// for
    return maxVal;
}//Function

double getMinElementVec(std::vector<double> & vect) {
    double minVal = INFINITY;
    for (int i = 0; i < vect.size(); i++) {
        if (vect[i] < minVal) {
            minVal = vect[i];
        }//if
    }// for
    return minVal;
}//Function

int getMinElementVec(std::vector<int> & vect) {
    int minVal = 99999999;
    for (int i = 0; i < vect.size(); i++) {
        if (vect[i] < minVal) {
            minVal = vect[i];
        }//if
    }// for
    return minVal;
}//Function

double getMedia(std::vector<double>& vect) {
    int numElementos = vect.size();
    double media = 0;
    double sumatoria = 0;
    for (int ii = 0; ii < numElementos; ii++) {
        sumatoria = sumatoria + vect[ii];
    } //for
    media = sumatoria / numElementos;
    return media;
} // function

double getMedia(std::vector<int>& vect) {
    int numElementos = vect.size();
    double media = 0;
    double sumatoria = 0;
    for (int ii = 0; ii < numElementos; ii++) {
        sumatoria = sumatoria + vect[ii];
    } //for
    media = sumatoria / numElementos;
    return media;
} // function

double getDesviacionEstandar(std::vector<double>& vect, double& media) {
    media = getMedia(vect);

    int numElementos = vect.size();
    double sumatoria_diferencias = 0;

    double aux = 0;
    for (int ii = 0; ii < numElementos; ii++) {
        aux = (std::pow((vect[ii] - media), 2)) / numElementos;
        sumatoria_diferencias = sumatoria_diferencias + aux;
    } //for

    return std::sqrt(sumatoria_diferencias);
} // function

double getDesviacionEstandar(std::vector<int>& vect) {
    int numElementos = vect.size();
    double media = getMedia(vect);
    double sumatoria_diferencias = 0;

    double aux = 0;
    for (int ii = 0; ii < numElementos; ii++) {
        aux = (std::pow((vect[ii] - media), 2)) / numElementos;
        sumatoria_diferencias = sumatoria_diferencias + aux;
    } //for

    return std::sqrt(sumatoria_diferencias);
} // function

double getMediana(std::vector<double> vect) {
    double media;
    int numElementos = vect.size();
    /* Ordenano de forma ascendente */
    std::sort(vect.begin(), vect.end());

    double div = numElementos / 2;

    if (numElementos % 2 == 0) {
        /* Es par */
        int posIzq = div - 1;
        int posDer = div;
        /* Obteniendo el promedio de los dos lados */
        media = (vect[posIzq] + vect[posDer]) / 2;

    } else {
        /* Es impar*/
        media = vect[std::floor(div)];
    }
    return media;
} //function

double calculateVectDotProduct(std::vector<double> V1, std::vector<double> V2) {
    double result = 0;
    int longitudVector = V1.size();

    for (int i = 0; i < longitudVector; i++) {
        result = result + (V1[i] * V2[i]);
    } //for 
    return result;
}//function 

std::vector<double> calculateVectSubtraction(std::vector<double> V1, std::vector<double> V2) {
    int longitudVector = V1.size();
    std::vector<double> vectR(longitudVector);

    for (int i = 0; i < longitudVector; i++) {
        vectR[i] = V1[i] - V2[i];
    } //for 
    return vectR;
} //function 

std::vector<double> calculateVectSum(std::vector<double> V1, std::vector<double> V2) {
    int longitudVector = V1.size();
    std::vector<double> vectR(longitudVector);

    for (int i = 0; i < longitudVector; i++) {
        vectR[i] = V1[i] + V2[i];
    } //for 
    return vectR;
} //function 

std::vector<double> calculateVectMultiplication(std::vector<double> V1, std::vector<double> V2) {
    int longitudVector = V1.size();
    std::vector<double> vectR(longitudVector);

    for (int i = 0; i < longitudVector; i++) {
        vectR[i] = V1[i] * V2[i];
    } //for 
    return vectR;
} //function 

std::vector<double> calculateVectDivision(std::vector<double> V1, std::vector<double> V2) {
    int longitudVector = V1.size();
    std::vector<double> vectR(longitudVector);

    for (int i = 0; i < longitudVector; i++) {
        vectR[i] = V1[i] / V2[i];
    } //for 
    return vectR;
} //function 

std::vector<double> multiplicateVectByVal(std::vector<double> vector, double val) {
    int longitudVector = vector.size();
    std::vector<double> vectR(longitudVector);

    for (int i = 0; i < longitudVector; i++) {
        vectR[i] = vector[i] * val;
    } //for 
    return vectR;
} //function

std::vector<double> divideVectByVal(std::vector<double> vector, double val) {
    int longitudVector = vector.size();
    std::vector<double> vectR(longitudVector);

    for (int i = 0; i < longitudVector; i++) {
        vectR[i] = vector[i] / val;
    } //for 
    return vectR;
} //function

std::vector<double> sumVectByVal(std::vector<double> vector, double val) {
    int longitudVector = vector.size();
    std::vector<double> vectR(longitudVector);

    for (int i = 0; i < longitudVector; i++) {
        vectR[i] = vector[i] + val;
    } //for 
    return vectR;
} //function

std::vector<int> sumVectByVal(std::vector<int> vector, int val) {
    int longitudVector = vector.size();
    std::vector<int> vectR(longitudVector);

    for (int i = 0; i < longitudVector; i++) {
        vectR[i] = vector[i] + val;
    } //for 
    return vectR;
} //function

std::vector<double> subtractVectByVal(std::vector<double> vector, double val) {
    int longitudVector = vector.size();
    std::vector<double> vectR(longitudVector);

    for (int i = 0; i < longitudVector; i++) {
        vectR[i] = vector[i] - val;
    } //for 
    return vectR;
} //function

double calculateNormVect(std::vector<double> vector) {
    int longitudVector = vector.size();
    double norma = 0;

    for (int i = 0; i < longitudVector; i++) {
        norma = norma + (vector[i] * vector[i]);
    } //for
    norma = std::sqrt(norma);
    return norma;
}// function 

/* __________________________________________________________________________ */
int getNumCombinacionesD2(int numElementos) {
    int n = numElementos; // elementos a formar combinaciones de dos
    int r = 2;

    int numerador = factorial(n);
    int denominador = (factorial(r) * factorial(n - r));

    //    if (numerador == 0 || denominador == 0) {
    //        /* Es un numero muy grande que no se pudo almacenar en las variables */
    //        return -1;
    //    }
    return numerador / denominador;
} // function

std::vector< std::vector<int> > calculateMatCombinacionesD2(int numElementos, int numCombinacionesd2) {

    std::vector< std::vector<int> > matCombinaciones(numCombinacionesd2, std::vector<int>(2));

    /* Formando matriz con los indices de n combinaciones de 2 */
    int indComb = 0;
    for (int ii = 0; ii < numElementos; ii++) {
        for (int jj = (ii + 1); jj < numElementos; jj++) {
            matCombinaciones[indComb][0] = ii;
            matCombinaciones[indComb][1] = jj;
            indComb++;
        }// for columnas
    }// for filas
    return matCombinaciones;
}// function

int calculateNumCombinacionesD2_Deseadas(int numElementos_Deseados) {
    int numPares = 0;
    int n = 3; // elementos a formar combinaciones de dos
    const int r = 2;

    while (numElementos_Deseados >= numPares) {

        numPares = factorial(n) / (factorial(r) * factorial(n - r));
        /* Incrementando N porque no es suficiente */
        //        cout << numElementos_Deseados << " ::  numPares = " << numPares << "     n: " << n << endl;
        n++;
    } //while 
    //    cout<<endl;
    return numPares;
} //function

double radians2degrees(double val) {
    double PI = (double) std::atan(1)*4;

    return ( 180 / PI)*val;
} // function 

double degrees2radians(double val) {
    double PI = (double) std::atan(1)*4;

    return ( PI / 180) * val;
} // function 

std::vector< std::vector<int> > calculateMatCombinacionesD2_Deseadas(int numElementos_Deseados) {
    /*  obteniendo el numero de combinaciones Parejas que son necesarias para cumplir con el numero de elementos deseados */
    //    int numPares = calculateNumCombinacionesD2_Deseadas(numElementos_Deseados);

    /* Variables para almacenar las combinaciones */
    std::vector< std::vector<int> > matCombinaciones(numElementos_Deseados, std::vector<int>(2));
    /* Subconjunto de matCombinaciones que contiene el numero de Elementos Deseados */
    std::vector< std::vector<int> > matCombinacionesEspecificas(numElementos_Deseados, std::vector<int>(2));

    /* Formando combinaciones de 2 */
    int indComb = 0;
    for (int ii = 0; ii < numElementos_Deseados; ii++) {
        for (int jj = (ii + 1); jj < numElementos_Deseados; jj++) {
            matCombinaciones[indComb][0] = ii;
            matCombinaciones[indComb][1] = jj;
            indComb++;
            if (indComb >= numElementos_Deseados) {
                break;
            }
        }// for columnas
        if (indComb >= numElementos_Deseados) {
            break;
        }
    }// for filas
    /* Quedandome solamente con los n-Elementos Deseados */
    //    for (int kk = 0; kk < numElementos_Deseados; kk++) {
    //        matCombinacionesEspecificas[kk][0] = matCombinaciones[kk][0];
    //        matCombinacionesEspecificas[kk][1] = matCombinaciones[kk][1];
    //    } //for 
    /* Regresando la matriz de combinaciones*/
    return matCombinaciones;
}// function

double getSumatoriaDeVector(std::vector<double>& vector) {
    double sumatoria = 0;

    for (int ii = 0; ii < vector.size(); ii++) {
        sumatoria = sumatoria + vector[ii];
    } // for 

    return sumatoria;
}// function 

double getSumatoriaDeVector2D(std::vector<std::vector<double> >& vector2D) {
    double sumatoria = 0;

    for (int ii = 0; ii < vector2D.size(); ii++) {
        for (int jj = 0; jj < vector2D[0].size(); jj++) {
            sumatoria = sumatoria + vector2D[ii][jj];
        } //for
    } // for 

    return sumatoria;
}// function 

std::vector<int> getVectUniqueIndexFromPairVector(std::vector<std::pair<int, int> > vectPares) {
    // <Extrayendo indices No repeditos >    
    std::vector<int> vectIndicesUnicos;

    for (int kk = 0; kk < vectPares.size(); kk++) {
        // Primer elemento del par
        if (!existElementVector(vectIndicesUnicos, vectPares[kk].first)) {
            vectIndicesUnicos.push_back(vectPares[kk].first);
        } // if         
        // Segundo elemento del par
        if (!existElementVector(vectIndicesUnicos, vectPares[kk].second)) {
            vectIndicesUnicos.push_back(vectPares[kk].second);
        } // if         
    } // for 

    return vectIndicesUnicos;
} // fucntion 
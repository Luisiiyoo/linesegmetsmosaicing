/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   RANSAC.hpp
 * Author: luis
 *
 * Created on 6 de junio de 2018, 09:07 PM
 */

#ifndef RANSAC_HPP
#define RANSAC_HPP
#include "Struct_Parametros.hpp"
#include "Struct_Resultados.hpp"

#include <opencv2/xfeatures2d.hpp>
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include <opencv2/line_descriptor.hpp>
#include "opencv2/core/utility.hpp"
#include <opencv2/imgproc.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/highgui.hpp>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

class RANSAC {
public:
    RANSAC(ParametrosEjecucion& parametros);
    RANSAC(const RANSAC& orig);
    virtual ~RANSAC();

    double ransac_KeyPoints(Struct_Matching& structMatch);
    double ransac_LineSegments(Struct_Matching& structMatch);
    //    double ransac_PI_LineSegments(Struct_Matching& structMatch);

private:
    cv::Mat simpleCalculateHomographyAndEvaluation_puntos(
            std::vector<int>& vecIndMatchSeleccionados,
            std::vector<bool>& vectBanderasConsiderarLineas,
            std::vector<cv::Point2f>& puntos_test, std::vector<cv::Point2f>& puntos_refe,
            int idTypeModel4Ransac, double umbralDistanciaInliers,
            std::vector<double>& vecDistancias, std::vector<int>& vecIndicesInliersMatch,
            int& numInliers, std::vector<cv::Point2f>& puntos_transformados);
 
    cv::Mat simpleCalculateHomographyAndEvaluation_lineas(
            std::vector<int>& vecIndMatchSeleccionados,
            std::vector<bool>& vectBanderasConsiderarLineas,
            std::vector<cv::line_descriptor::KeyLine>& lineas_test,
            std::vector<cv::line_descriptor::KeyLine>& lineas_refe,
            int idTypeModel4Ransac, int tipoEvalDistancia,
            double umbralDistanciaInliers, double umbralAnguloInliers,
            std::vector<double>& vecDistancias, std::vector<double>& vectAngulos,
            std::vector<int>& vecIndicesInliersMatch,
            int& numInliers,
            std::vector<cv::line_descriptor::KeyLine>& lineas_transformadas,
            std::vector<cv::line_descriptor::KeyLine>& lineas_select_test,
            std::vector<cv::line_descriptor::KeyLine>& lineas_select_refe,
            std::vector<cv::Point2f>& pointsFromSelectLines_test,
            std::vector<cv::Point2f>& pointsFromSelectLines_refe);
         
    void ransac_LineSegmets_improveModel(Struct_Matching& structMatch);

    ParametrosEjecucion parametros;
    double umbralNumInliers;
    std::vector<int> vectNumInliersIteraciones;
    std::vector<int> vect_IteracionesValidas_ransac;

    int iter_BH; // mejor iteracion
    std::vector<int> vectIndAleatorios_BH; // mejor combinacion de lineas

    double radioTolerancia;
    cv::Point2f puntoCentroImg;

    cv::Size sizeImg;
};

#endif /* RANSAC_HPP */


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Quadtree.h
 * Author: luis
 *
 * Created on 15 de agosto de 2018, 10:57 PM
 */

#ifndef QUADTREE_KEYPOINTS_H
#define QUADTREE_KEYPOINTS_H

#include "Struct_Resultados.hpp"
#include "Struct_Parametros.hpp"

#include <opencv2/xfeatures2d.hpp>
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"

#include <string>
#include <cstdlib> 

class QuadTree {
public:
    //    Quadtree(const Quadtree& orig);
    QuadTree(const cv::Size & sizeImg);
    QuadTree(const cv::Size & sizeImg,
            cv::Point2f ptSupIzq, cv::Point2f ptInfDer,
            int level, std::string code);
    virtual ~QuadTree();

    void createQuadTree(cv::Mat& imgMat, std::vector<cv::Point2f>& vectPoints,
            std::vector<bool>& vectBandValidPoints,
            std::vector<std::string>& vectColdePointsQuad,
            const int& minObjetosParaDividir, const int& maxLevel,
            std::vector<QuadTree>& vectQuads);


    // < Atributos Quad    
    int level;
    cv::Point2f ptSupIzq;
    cv::Point2f ptInfDer;
    std::string code;
    int numElementsInThisQuad;

    bool bandIsLeft;
    double width, height;
    cv::Size sizeImg;

    // < Apuntadores para otros Quad >
    QuadTree * NW;
    QuadTree * NE;
    QuadTree * SW;
    QuadTree * SE;

private:

    void printQuadDetails();


    bool isThePointInThisQuad(cv::Point2f & point);

    void asignaCodigoQuad(std::vector<bool>& vectBandValidPoints,
            std::vector<std::string>& vectColdePointsQuad,
            std::vector<int>& vectIndex);

    std::vector<int> findElementosInThisQuad(std::vector<cv::Point2f>& vectPoints,
            std::vector<bool>& vectBandValidPoints);

    void setCodeOfPointsInThisQuad(std::vector<int>& vectIndex_PointsInThisQuad,
            std::vector<bool>& vectBandValidPoints, std::vector<std::string>& vectColdePointsQuad);

    bool isALeft(const int& maxLevel);


};

struct Struct_QuadTreeKP {
    // Homografia Obtenida en el uso del QuadTree
    //    cv::Mat Model_Best;
    //    bool bandModelFound;
    //    int idBestModel;    

    // Imagenes con Grid de QuadTree
    cv::Mat imgMat_test;
    cv::Mat imgMat_refe;

    int porcIP4UseKP; // < Porcentaje de IP para utilizar KP
    int minObjectsToSplitQuad; // < Minimo Numero de Puntos de interseccion para No dividir el Quad>
    int maxLevel; // < Maximo Nivel del Arbol >
    int numElemtsToSearchQuad; // < Busqueda de Quads que tengan menos de N elementos para extraer KeyPoints>

    int idDetecDescrQuad; /* < 0:"SURF" || 1:"SIFT"|| 2:"ORB"> */
    int maxNumFeatures; // < Maximo numero de KeyPoints a extraer en el Quad >    

    // Vector de Quads Formados por el Arbol
    std::vector<QuadTree> vectQuads_test;
    std::vector<QuadTree> vectQuads_refe;

    // Vector de indices de los Quads de interes
    std::vector<int> vectIndexQuad_test;
    std::vector<int> vectIndexQuad_refe;

    // Porcentaje - Presencia de Puntos de interseccion
    double porcIP_test;
    double porcIP_refe;

    // Vector auxiliar para buscar keypoints
    std::vector<bool> vectBandValidPoints_test;
    std::vector<bool> vectBandValidPoints_refe;

    // Codigo para cada Keypoint en el Quad correspondiente
    std::vector<std::string> vectColdePointsQuad_test;
    std::vector<std::string> vectColdePointsQuad_refe;

    // Puntos de interseccion 
    std::vector<cv::Point2f>vectIntersecP_test;
    std::vector<cv::Point2f>vectIntersecP_refe;
    //    std::vector<cv::Point2f>vectIntersecP_tran;

    // Keypoints
    std::vector<cv::Point2f>vectKeyP_test;
    std::vector<cv::Point2f>vectKeyP_refe;
    //    std::vector<cv::Point2f>vectKeyP_tran;

    // Colores 
    std::vector<cv::Scalar>vectIntersecP_colores;
    std::vector<cv::Scalar>vectKeyP_colores;
    //    std::vector<cv::Scalar>vectPointsInliers_colores;

    // IP + KP
    //    std::vector<cv::Point2f>vectPoints_test;
    //    std::vector<cv::Point2f>vectPoints_refe;
    //    std::vector<cv::Point2f>vectPoints_tran;
};


std::vector<int> searchQuadsWithLeastNElements(std::vector<QuadTree> vectQuads, int value);

double getPorcentajeIntersectionPointsInQuadTree(std::vector<QuadTree>& vectQuads,
        std::vector<int>& vectIndexesQuad);

double createQuadTreeUsingPI_Keypoints(Struct_Matching& structMatch,
        ParametrosEjecucion parametros, Struct_QuadTreeKP& structQuadTree,
        bool bandGenerateQuadTrees = true);

int getTheBestModel(Struct_Matching& structMatch,
        double umbralError, double porcentaje, 
        std::vector<cv::Mat>& vectModelos,
        std::vector<std::pair<int, std::pair<double, double > > > &vectEval);

void getBestModel2(Struct_Matching& structMatch,
        double umbralError, double porcentajeBaseLine);
std::pair< int, int > generateKeypointMatchesQuad(
        std::vector<QuadTree>& vectQuads_test, std::vector<QuadTree>& vectQuads_refe,
        std::vector<int>& vectIndex_test, std::vector<int>& vectIndex_refe,
        cv::Mat& imgMat_test, cv::Mat& imgMat_refe,
        int idDetecDescr, int maxNumFeatures, int idMatcher,
        std::vector<cv::DMatch>& matches,
        std::vector<cv::Point2f> & puntos_test,
        std::vector<cv::Point2f> & puntos_refe);

int generatePoolOfKeyPointsQuad(std::vector<QuadTree>& vectQuads,
        std::vector<int>& vectIndex, cv::Mat& imgMat,
        int idDetecDescr, int maxNumFeatures,
        std::vector<cv::KeyPoint> & keypoints, cv::Mat & descrKP);

void setStructWithTheBestModel(int idBestModel, Struct_Matching& structMatch,
        std::vector<cv::Mat>& vectModelos,
        std::vector<std::pair<int, std::pair<double, double > > > & vectEvalModels,
        std::vector<std::pair< int, int > > & vectPairDetectMatch,
        std::vector<int >& vectNumInlierModel);

#endif /* QUADTREE_H */


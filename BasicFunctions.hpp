/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   BasicFunctions.hpp
 * Author: luis
 *
 * Created on 13 de septiembre de 2017, 04:05 PM
 */

#ifndef BASICFUNCTIONS_HPP
#define BASICFUNCTIONS_HPP

#include <vector>       // std::vector
#include <string>
#include <iostream>
#include <fstream>

bool renameFiles(std::string newName_Format, int numZeros_Format,
        std::string fullPathFiles, std::vector<std::string>& vectNamesImages);

int findString(std::string cad1, std::string cad2);

std::string getNameSequence(std::string& fullPathFiles, std::vector<std::string>& vectNamesImages);

bool existDirectory(std::string directory);
bool makeSureDirectoryExist(std::string directory);

std::vector<std::string> getFilesInDirectory(const std::string directory);
std::vector<int> sortAndTrackIndices(std::vector<double>& vectorVals);
std::vector<int> sortAndTrackIndices(std::vector<int>& vectorVals);

std::string checkNameFile(std::string nombreArchivo);

void printVectorDistanciaAngulo_comoVariableMatlab(std::vector<double>& vectDist,
        std::vector<double>& vectAngu, std::vector<int>vectIndicesValidos, std::string nombre);

int getNumberOfIterationsRANSAC(int numMuestrasModelo, double probElegirInlier, double probObtenerBuenaMuestra);

std::vector<double> getColFromVector2D(std::vector<std::vector<double> >& vect2D, int col);
std::vector<double> getRowFromVector2D(std::vector<std::vector<double> >& vect2D, int row);
std::vector<int> getRowFromVector2D(std::vector<std::vector<int> >& vect2D, int row);

double printTimes(std::vector<double>& vectTiempo, std::vector<std::string>& vectEtiquetas, int iter);

double getSumatoriaDeVector(std::vector<double>& vector);
double getSumatoriaDeVector2D(std::vector<std::vector<double> >& vector2D);

std::vector<std::string> getVectorOfVals(int numElementos, std::string val);
std::vector<double> getVectorOfVals(int numElementos, double val);

void printVectorStrings_Directorios(std::vector<std::string> vect, std::string cad);

bool existElementVector(std::vector<int>& vect, int val);
std::vector<int> getElementsVectUnicos(std::vector<int>& vect);

std::vector<double> agregaElementosVector(std::vector<double>& vectorBase, std::vector<double>& vectorExtraElementos);
std::vector<int> agregaElementosVector(std::vector<int>& vectorBase, std::vector<int>& vectorExtraElementos);

std::vector<bool> generaVectorFalse(int numElementos);
std::vector<bool> generaVectorTrue(int numElementos);
std::vector<bool> generaVectorBanderas(int numElementos, std::vector<int>& vecIndicesTrue, bool valBool);

std::vector<int> cloneVector(std::vector<int>& vect);
std::vector<double> cloneVector(std::vector<double>& vect);

int findIndexOfValInVector(int val, std::vector<int> vector);

//std::vector<int> createVectNumAleatorios(int iniVal, int endVal, int numDatos);
std::vector<int> findElementVector(int val, std::vector<int> vector);
std::vector<int> findElementVector(bool val, std::vector<bool> vector);

std::vector<int> eraseValVector(int val, std::vector<int>& vector);
std::vector<double> eraseValVector(double val, std::vector<double>& vector);

std::vector<bool> getElementsFromVector(std::vector<bool>& vector, std::vector<int>& vect_index);
std::vector<int> getElementsFromVector(std::vector<int>& vector, std::vector<int>& vect_index);
std::vector<double> getElementsFromVector(std::vector<double>& vector, std::vector<int>& vect_index);

std::vector<std::pair<int, int> > getElementsFromPairVector(std::vector<std::pair<int, int> >& vector, std::vector<int>& vect_index);

bool satisfaceCondicion(int valVector, std::string expresion, double otroVal);
std::vector<int> findElementosSatisfacenExpresion(std::vector<int>& vector, std::string expresion, double val);
std::vector<int> findElementosSatisfacenDobleExpresion(std::vector<int>& vector,
        std::string expresion1, double val1,
        std::string expresion2, double val2);

int count_True_ElementsVect(std::vector<bool>& vector);
int count_False_ElementsVect(std::vector<bool>& vector);

bool satisfaceCondicion(double valVector, std::string expresion, double otroVal);
std::vector<int> findElementos_No_SatisfacenExpresion(std::vector<bool>& vector);
std::vector<int> findElementosSatisfacenExpresion(std::vector<bool>& vector);
std::vector<int> findElementosSatisfacenExpresion(std::vector<double>& vector, std::string expresion, double val);
std::vector<int> findElementosSatisfacenDobleExpresion(std::vector<double>& vector,
        std::string expresion1, double val1,
        std::string expresion2, double val2);

std::vector<int> generaVecNumAleatoriosDeVectorIndices(std::vector<int> vecIndicesValidos, int numDatos);
std::vector<int> generaVecNumAleatorios(int iniVal, int endVal, int numDatos);
std::vector<int> generaVecNumAleatorios_aux2(int iniVal, int endVal, int numDatos);
std::vector<int> generaVecNumAleatorios_aux1(int iniVal, int endVal, int numDatos);

int myrandom(int i);
std::vector<int> generaVectorNumConsecutivos(int iniVal, int endVal);
int factorial(int n);

int getNumCombinacionesD2(int numElementos);
int calculateNumCombinacionesD2_Deseadas(int numElementos_Deseados);
std::vector< std::vector<int> > calculateMatCombinacionesD2(int numElementos, int numCombinacionesd2);
std::vector< std::vector<int> > calculateMatCombinacionesD2_Deseadas(int numElementos_Deseados);

void printVectorPares_comoVariableMatlab(std::vector< std::pair<int, int> >& vect, std::string nombre);
void printVector2D_comoVariableMatlab(std::vector< std::vector<int> >& mat, std::string nombre);
void printVector2D_comoVariableMatlab(std::vector< std::vector<double> >& mat, std::string nombre);

void writeVector2D_comoVariableMatlab(std::vector< std::vector<double> >& mat, std::string nombre, std::ofstream& outfile);
void writeVector2D_comoVariableMatlab(std::vector< std::vector<int> >& mat, std::string nombre, std::ofstream& outfile);

void writeVector_comoVariableMatlab(std::vector<double>& vect, std::string nombre, std::ofstream& outfile);
void writeVector_comoVariableMatlab(std::vector<int>& vect, std::string nombre, std::ofstream& outfile);
void writeVector_comoVariableMatlab(std::vector<bool>& vect, std::string nombre, std::ofstream& outfile);
void writeVector_comoVariableMatlab(std::vector<std::string>& vect, std::string nombre, std::ofstream& outfile);


void printVector_comoVariableMatlab(std::vector<double>& vect, std::string nombre);
void printVector_comoVariableMatlab(std::vector<bool>& vect, std::string nombre);
void printVector_comoVariableMatlab(std::vector<int>& vect, std::string nombre);

double getMediana(std::vector<double> vect);
std::vector< std::vector<double> > getMatTranspuesta(std::vector< std::vector<double> >& mat);
std::vector< std::vector<double> > calcularProductoMatrices(std::vector< std::vector<double> >& matA, std::vector< std::vector<double> >& matB);
double calculateVectDotProduct(std::vector<double> S1, std::vector<double> S2);
std::vector<double> calculateVectSubtraction(std::vector<double> V1, std::vector<double> V2);
std::vector<double> calculateVectSum(std::vector<double> V1, std::vector<double> V2);
std::vector<double> calculateVectMultiplication(std::vector<double> V1, std::vector<double> V2);
std::vector<double> calculateVectDivision(std::vector<double> V1, std::vector<double> V2);

std::vector<double> subtractVectByVal(std::vector<double> vector, double val);
std::vector<double> sumVectByVal(std::vector<double> vector, double val);
std::vector<int> sumVectByVal(std::vector<int> vector, int val);
std::vector<double> divideVectByVal(std::vector<double> vector, double val);
std::vector<double> multiplicateVectByVal(std::vector<double> vector, double val);

double calculateNormVect(std::vector<double> vector);

void calculateDivisionMat(std::vector< std::vector<double> >& mat, double val);


void vecMat2array_porFil(std::vector< std::vector<double> >& matA, double *arrayA);
void vecMat2array_porCol(std::vector< std::vector<double> >& matA, double *arrayA);
std::vector< std::vector<double> > array2vecMat_porFil(double *array, int nFil, int nCol);
std::vector< std::vector<double> > array2vecMat_porCol(double *array, int nFil, int nCol);
std::vector< std::vector<double> > vecOneDim2twoDim(std::vector<double>& array, int nFil, int nCol);

int findMaxElementPosicion(std::vector< double >& vect);
int findMinElementPosicion(std::vector< double >& vect);

int findMaxElementPosicion(std::vector< int >& vect);
int findMinElementPosicion(std::vector< int >& vect);

double getMaxElementVec(std::vector<double> & vect);
int getMaxElementVec(std::vector<int> & vect);
double getMinElementVec(std::vector<double> & vect);
int getMinElementVec(std::vector<int> & vect);

double getMedia(std::vector<double>& vect);
double getDesviacionEstandar(std::vector<double>& vect, double& media);

double getMedia(std::vector<int>& vect);
double getDesviacionEstandar(std::vector<int>& vect);

double degrees2radians(double val);
double radians2degrees(double val);


std::vector<int> getVectUniqueIndexFromPairVector(std::vector<std::pair<int, int> > vectPares);
#endif /* BASICFUNCTIONS_HPP */


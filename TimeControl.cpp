/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   TimeControl.cpp
 * Author: luis
 * 
 * Created on 8 de enero de 2018, 04:55 PM
 */

#include "TimeControl.hpp"

#include <iostream>
#include <fstream>
#include <string.h>
#include <time.h>
#include <sys/time.h>

using namespace std;

TimeControl::TimeControl() {
} //

TimeControl::~TimeControl() {
} //desctructor

double TimeControl::timeval_diff(struct timeval *a, struct timeval *b) {
    return
    (double) (a->tv_sec + (double) a->tv_usec / 1000000) -
            (double) (b->tv_sec + (double) b->tv_usec / 1000000);
} // function 

void TimeControl::start() {
    /* Creando estructura para almacenar el tiempo */
    gettimeofday(&iniTime, NULL);
} // function 

double TimeControl::finish(bool bandShowTime, std::string cadena) {
    /* Creando estructura para almacenar el tiempo */
    gettimeofday(&endTime, NULL);

    double tiempoSeg = timeval_diff(&endTime, &iniTime);

    if (bandShowTime) {
        printf(cadena.c_str());
        printf("Tiempo : %.8g segundos\n", tiempoSeg);
    } // if

    /* Obteniendo el timpo transcurrido */
    return tiempoSeg;
} // function 


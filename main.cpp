/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: luis gonzalez guzman 
 *
 * Created on 6 de junio de 2018, 10:30 AM
 */

#include <dirent.h> 
#include <iosfwd> 
#include <sys/stat.h> 
#include <dirent.h>
#include <errno.h>

#include <iostream>
#include <fstream>
#include <string.h>
#include <cstdlib> 
#include "Struct_Parametros.hpp"
#include "FeatureMosaicing.hpp"
#include "BasicFunctions.hpp"

using namespace std;

/*****************************************************/
/* <Creando estructuras de parametros> */
ParametrosEjecucion parametros;
/******************** PARAMETROS ********************/
bool bandRandomSeed = 1;
bool bandMuestraVentanasResults = true;
/* -------------------------------------------------------------------------- */
int opcionEjecucion = 2; // < 0 :TwoImagenes || 1 : VideoFrames|| 2 : SetImagenes>
/* _____________________________________________________________ */
bool bandDynamicKeyFrame = 0; // Bandera para utilizar seleccion Dinamica de frame o no
/* _____________________________________________________________ */
/* Seleccion de imagenes y videos predefinicidos*/
int idVideo = 1; // Check ExecutionParameters Function
int idImges = 1; // Check ExecutionParameters Function
int idSetImges = 8; // Check ExecutionParameters Function
/* _____________________________________________________________ */
int idTypeModel4Ransac = 1; // < 0: Homography || 1: AffineTransform >    
bool bandHacerConLineas = 0;
int idDetecDescr = 0; // < 0:"SURF" || 1:"SIFT"|| 2:"ORB">
/* _____________________________________________________________ */
/* Parametros para la guardar resultados en archivo .MAT*/
bool bandGuardarResultados = 1; // Bandera para almacer resultados al finalizar la ejecucion 

std::string dirRoot;
std::string dirRootResults;
std::string dirRootDB_SecImgs;
std::string dirRootDB_Videos;
std::string dirRootDB_ParImgs;

static void help() {
    std::cout << " =========== Line Segment Mosaicing =========== " << std::endl;
    std::cout << "Este codigo implementa el Algoritmo Edlines y Line Band Descriptor para \n "
            "extraer Segmentos de Lineas en un conjunto de imagenes para obtener las corresondencias \n"
            "entre estos segmentos. Posteriormente se aplica una modificacion del algoritmo RANSAC para \n"
            "segmentos de lineas con el objetivo de eliminar incorrectos matches generados en el proceso anterior, \n"
            "ademas se obtiene la homografia en este proceso." << std::endl << std::endl;

    std::cout << " Desarrollado por:  Luis González Guzmán" << std::endl;
    std::cout << " Año: 2017" << std::endl;
    std::cout << " E-mail: luis-gonzman@inaoep.mx  \n" << std::endl;

}// help

static void checkRootMediaDirectory(std::string& dirRoot,
        std::string& dirRootResults, std::string& dirRootDB_SecImgs,
        std::string& dirRootDB_Videos,
        std::string& dirRootDB_ParImgs) {

    std::vector<std::string> vectDirRoot;
    vectDirRoot.push_back("/home/luis/Documentos/"); // <-
    vectDirRoot.push_back("/home/luisgonzalez/Documents/"); // <-

    std::string dirRootDataset;
    bool bandDirFound = false;

    for (int dd = 0; dd < vectDirRoot.size(); dd++) {
        dirRoot = vectDirRoot[dd];
        dirRootDataset = dirRoot + "Datasets_Mosaicos/INAOE_DATASET/"; // <-

        if (existDirectory(dirRoot) && existDirectory(dirRootDataset)) {
            // <verificando directorio de resultados>
            dirRootResults = dirRoot + "LineSegmentsMosaicing_Results/"; // <-
            bool bandDirResults = makeSureDirectoryExist(dirRootResults);
            if (!bandDirResults) {
                cout << "Error loading the directories - Results folder. \n" << endl;
                exit(0);
            } // if - else

            // < Directorios alternativos>
            dirRootDB_SecImgs = dirRootDataset + "INAOE_SecuenciaImagenes/";
            dirRootDB_Videos = dirRootDataset + "INAOE_Videos/";
            dirRootDB_ParImgs = dirRootDataset + "INAOE_ParImagenes/";

            if (existDirectory(dirRootDB_SecImgs) && existDirectory(dirRootDB_Videos)
                    && existDirectory(dirRootDB_ParImgs)) {
                // todos los directorios existen 
                bandDirFound = true;
                break;
            } // if 
        }// if
    }// for 

    if (bandDirFound == false) {
        cout << "Error loading the directories. \n" << endl;
        exit(0);
    } // if
}// function 

int main(int argc, char** argv) {
    //help();
    int idMedia;
    /* <~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~> */
    checkRootMediaDirectory(dirRoot,
            dirRootResults, dirRootDB_SecImgs,
            dirRootDB_Videos, dirRootDB_ParImgs);

    parametros.ControlVent.bandMuestraVentanasResults = bandMuestraVentanasResults;

    parametros.RANSAC.idTypeModel4Ransac = idTypeModel4Ransac;
    parametros.SaveResults.bandGuardarResultados = bandGuardarResultados;
    parametros.SaveResults.dirResultados_FileMat = dirRootResults;

    parametros.Mosaic.bandStitching = true;
    parametros.Mosaic.idTypeBlending = 0;

    parametros.Features.bandHacerConLineas = bandHacerConLineas;
    parametros.Features.idDetecDescr = idDetecDescr;

    parametros.ControlVent.bandDynamicKeyFrame = bandDynamicKeyFrame;
    /* <~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~> */

    FeatureMosaicing mosaicing(parametros);

    /* Definiendo Semilla para generar los numeros aleatorios */
    if (bandRandomSeed) {
        time_t seconds;
        time(&seconds);
        srand((unsigned int) seconds);
    } // if     

    switch (opcionEjecucion) {
        case 0:
            idMedia = idImges;
            mosaicing.Mosaicing_PairOfImages(idMedia, dirRootDB_ParImgs);
            break;
        case 1:
            idMedia = idVideo;
            mosaicing.Mosaicing_VideoSequence(idMedia, dirRootDB_Videos);
            break;
        case 2:
            idMedia = idSetImges;
            mosaicing.Mosaicing_SetOfImages(idMedia, dirRootDB_SecImgs);
            break;
        default:
            cout << " Opcion de Ejecucion No Valida" << endl;
            break;
    } // switch


    /* ---------------------------------------------- */
    /*   Esperando tecla para finalizar ejecucion   */
    cout << "\n\nEjecucion Finalizada correctamente." << endl;
    /* ---------------------------------------------- */

    return 0;
} // main 


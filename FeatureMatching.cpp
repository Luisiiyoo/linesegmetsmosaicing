/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FeatureMatching.cpp
 * Author: luis
 * 
 * Created on 6 de junio de 2018, 08:37 PM
 */

#include "FeatureMatching.hpp"
#include "LinePointFunctions.hpp"
#include "BasicFunctions.hpp"
#include "TimeControl.hpp"
#include "Struct_Parametros.hpp"
#include "Struct_Resultados.hpp"


#include <opencv2/xfeatures2d.hpp>
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include <opencv2/line_descriptor.hpp>
#include "opencv2/core/utility.hpp"
#include <opencv2/imgproc.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/highgui.hpp>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "Struct_Parametros.hpp"
#include <iostream>
#include <fstream>
#include <string.h>

#include <time.h>
#include <sys/time.h>

FeatureMatching::FeatureMatching(ParametrosEjecucion& parametros) {
    this->parametros = parametros;
} //

FeatureMatching::FeatureMatching(const FeatureMatching& orig) {
} //

FeatureMatching::~FeatureMatching() {
} //

double FeatureMatching::matching_LineSegments(Struct_Matching& structMatch) {
    /*+++++++++++++++++++++++++++++++++++++++*/
    TimeControl tc;
    tc.start(); // <--------
    /*+++++++++++++++++++++++++++++++++++++++*/
    /* <~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~> */
    /* < Matching > */
    /* <~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~> */
    /* Realizando Correspondencia de segementos de Linea */
    chooseMatchDescriptors(this->parametros.Features.idMatcher, 2,
            structMatch.structLineas.matLinesDescr_test,
            structMatch.structLineas.matLinesDescr_refe,
            structMatch.structLineas.vectMatches_respaldo);

    /* <Comprobando si el Algoritmo encontro correspondencias> */
    if (structMatch.structLineas.vectMatches_respaldo.empty()) {
        cout << "Error: El algoritmo de correspondencias no encontro Matches.\n" << endl;
        exit(0);
    }

    /* Imprimiento DMatch */
    // printDMatch(matches_respaldo);

    /* Ajustando Keylines al Match */
    ajustarMatch_Keylines(structMatch.structLineas.vectMatches_respaldo,
            structMatch.structLineas.vectMatches,
            structMatch.structLineas.vectLinesDetect_test,
            structMatch.structLineas.vectLinesDetect_refe,
            structMatch.structLineas.vectLinesMatch_test,
            structMatch.structLineas.vectLinesMatch_refe);

    /*+++++++++++++++++++++++++++++++++++++++*/
    return tc.finish(false, "");
    /*+++++++++++++++++++++++++++++++++++++++*/
} // function 

double FeatureMatching::matching_KeyPoints(Struct_Matching& structMatch) {
    /*+++++++++++++++++++++++++++++++++++++++*/
    TimeControl tc;
    tc.start(); // <--------
    /*+++++++++++++++++++++++++++++++++++++++*/
    /* <~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~> */
    /* < Matching > */
    /* <~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~> */
    /* Realizando Correspondencia de segementos de Linea */
    chooseMatchDescriptors(this->parametros.Features.idMatcher, this->parametros.Features.idDetecDescr,
            structMatch.structPuntos.matPointsDescr_test,
            structMatch.structPuntos.matPointsDescr_refe,
            structMatch.structPuntos.vectMatches_respaldo);

    /* <Comprobando si el Algoritmo encontro correspondencias> */
    if (structMatch.structPuntos.vectMatches_respaldo.empty()) {
        cout << "Error: El algoritmo de correspondencias no encontro Matches.\n" << endl;
        exit(0);
    }

    /* Imprimiento DMatch */
    // printDMatch(matches_respaldo);

    /* Ajustando Keylines al Match */
    ajustarMatch_Keypoints(structMatch.structPuntos.vectMatches_respaldo,
            structMatch.structPuntos.vectMatches,
            structMatch.structPuntos.vectKeypointsDetect_test,
            structMatch.structPuntos.vectKeypointsDetect_refe,
            structMatch.structPuntos.vectKeypointsMatch_test,
            structMatch.structPuntos.vectKeypointsMatch_refe);

    /* Obteniendo los Puntos de los keypoints*/
    extraePointsOfKeypoints(structMatch.structPuntos.vectKeypointsMatch_test,
            structMatch.structPuntos.vectKeypointsMatch_refe,
            structMatch.structPuntos.vectPointsMatch_test,
            structMatch.structPuntos.vectPointsMatch_refe);

    /*+++++++++++++++++++++++++++++++++++++++*/
    return tc.finish(false, "");
    /*+++++++++++++++++++++++++++++++++++++++*/
} // function 

void chooseMatchDescriptors(int idMatcher, int idDetecDescr,
        cv::Mat& descr_test, cv::Mat& descr_refe, std::vector<cv::DMatch>& matches) {
    // Verificando si hay elementos en los dos conjuntos para comparar 
    if ((descr_test.rows == 0) || (descr_refe.rows == 0)) {
        return; // N 
    } // if 
//    cout << "descr_test :" << descr_test.cols << "*" << descr_test.rows << endl;
//    cout << "descr_refe :" << descr_refe.cols << "*" << descr_refe.rows << endl;

    switch (idMatcher) {
        case 0: /* create a Brute-Force matcher */
        {
            cv::BFMatcher matcher;
            bool bandCrossCheck = true;
            if (idDetecDescr == 2) {
                /*Si se selecciono ORB entonce se utiliza distancia hamming o para las lineas por BinaryLineDescriptr se va a utilizar este espacio*/
                cv::BFMatcher matcher_aux(cv::NORM_HAMMING, bandCrossCheck);
                matcher = matcher_aux;
            } else {
                cv::BFMatcher matcher_aux(cv::NORM_L2, bandCrossCheck);
                matcher = matcher_aux;
            }//else                                
            matcher.match(descr_test, descr_refe, matches); // Query, Train            
            break;
        } // case
        case 1: /* Buscar correspondencias con FLANN matcher*/
        {
            descr_test.convertTo(descr_test, CV_32F);
            descr_refe.convertTo(descr_refe, CV_32F);
            cv::FlannBasedMatcher matcher;
            matcher.match(descr_test, descr_refe, matches);
            break;
        }//case 
        case 2: /* create a BinaryDescriptorMatcher matcher */
        {
            cv::Ptr<cv::line_descriptor::BinaryDescriptorMatcher> matcher = cv::line_descriptor::BinaryDescriptorMatcher::createBinaryDescriptorMatcher();
            matcher->match(descr_test, descr_refe, matches);
            break;
        } // case
    } //switch 
}// fuction

std::vector< cv::DMatch > removeMatchesFactorMinimo(std::vector<cv::DMatch>& matches, int factDist) {
    double min_dist_match = 5000000;
    double max_dist_match = 0;
    for (int i = 0; i < matches.size(); i++) {
        double dist = matches[i].distance;
        if (dist < min_dist_match) min_dist_match = dist;
        if (dist > max_dist_match) max_dist_match = dist;
    }//for 
    bool bandOK = false;
    std::vector< cv::DMatch > good_matches;

    while (bandOK == false) {
        double umbralDistMinima = 0;
        umbralDistMinima = factDist * min_dist_match;


        for (int i = 0; i < matches.size(); i++) {
            if (matches[i].distance <= umbralDistMinima) {
                good_matches.push_back(matches[i]);
            }//for
        }//for

        if (good_matches.size() >= (matches.size()*0.05)) {
            bandOK = true;
        } else {
            good_matches.clear();
            factDist++;
            cout << "\t*****factDist incrementado : " << factDist << " *****" << endl;
        }//else
    }//while

    return good_matches;
}//function

std::vector<double> getVectDistanciasDMacth(std::vector<cv::DMatch>& matches) {
    int numElementos = matches.size();
    std::vector<double> vecDistanciasDMatch(numElementos);

    for (int dd = 0; dd < numElementos; dd++) {
        vecDistanciasDMatch[dd] = (double) matches[dd].distance;
    }//for 
    return vecDistanciasDMatch;
} //function

void ajustarMatch_Keypoints(std::vector<cv::DMatch> matches, std::vector<cv::DMatch>& matches_ajust,
        std::vector<cv::KeyPoint> keypoints_test, std::vector<cv::KeyPoint> keypoints_refe,
        std::vector<cv::KeyPoint>& keypoints_test_ajust, std::vector<cv::KeyPoint>& keypoints_refe_ajust) {
    int numMatches = matches.size();
    int indMatchImg_query;
    int indMatchImg_train;

    // Creando clon del DMatch
    matches_ajust = std::vector<cv::DMatch>(matches);

    // creando variables vacias
    keypoints_test_ajust = std::vector<cv::KeyPoint>(numMatches); // Sobreescribiendo Variable
    keypoints_refe_ajust = std::vector<cv::KeyPoint>(numMatches); // Sobreescribiendo Variable

    //    cout << "AjusteMatch  (" << numMatches << " Matches): " << endl;

    for (int ii = 0; ii < numMatches; ii++) {
        indMatchImg_query = matches_ajust[ii].queryIdx;
        indMatchImg_train = matches_ajust[ii].trainIdx;

        keypoints_test_ajust[ii] = keypoints_test[ indMatchImg_query];
        keypoints_refe_ajust[ii] = keypoints_refe[ indMatchImg_train];

        matches_ajust[ii].queryIdx = ii;
        matches_ajust[ii].trainIdx = ii;
        //        cout << "[" << ii << "] : Query = " << matches[ii].queryIdx << "  Train = " << matches[ii].trainIdx << endl;
    } //for
}//function

void ajustarMatch_Keylines(std::vector<cv::DMatch> matches, std::vector<cv::DMatch>& matches_ajust,
        std::vector<cv::line_descriptor::KeyLine> keylines_test, std::vector<cv::line_descriptor::KeyLine> keylines_refe,
        std::vector<cv::line_descriptor::KeyLine>& keylines_test_ajust, std::vector<cv::line_descriptor::KeyLine>& keylines_refe_ajust) {
    int numMatches = matches.size();
    int indMatchImg_query;
    int indMatchImg_train;

    // Creando clon del DMatch
    matches_ajust = std::vector<cv::DMatch>(matches);

    // creando variables vacias
    keylines_test_ajust = std::vector<cv::line_descriptor::KeyLine>(numMatches); // Sobreescribiendo Variable
    keylines_refe_ajust = std::vector<cv::line_descriptor::KeyLine>(numMatches); // Sobreescribiendo Variable

    //    cout << "AjusteMatch  (" << numMatches << " Matches): " << endl;

    for (int ii = 0; ii < numMatches; ii++) {
        indMatchImg_query = matches_ajust[ii].queryIdx;
        indMatchImg_train = matches_ajust[ii].trainIdx;

        keylines_test_ajust[ii] = keylines_test[ indMatchImg_query];
        keylines_refe_ajust[ii] = keylines_refe[ indMatchImg_train];

        matches_ajust[ii].queryIdx = ii;
        matches_ajust[ii].trainIdx = ii;
        //        cout << "[" << ii << "] : Query = " << matches[ii].queryIdx << "  Train = " << matches[ii].trainIdx << endl;
    } //for

}//function

void matching_LineSegments(int idMatcher,
        cv::Mat& descr_test, cv::Mat& descr_refe,
        std::vector<cv::line_descriptor::KeyLine>& keylines_test,
        std::vector<cv::line_descriptor::KeyLine>& keylines_refe,
        std::vector<cv::DMatch>& matches) {
    /* <~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~> */
    /* < Matching > */
    /* <~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~> */
    /* Realizando Correspondencia de segementos de Linea */
    chooseMatchDescriptors(idMatcher, 2,
            descr_test, descr_refe, matches);

    /* <Comprobando si el Algoritmo encontro correspondencias> */
    if (matches.empty()) {
        cout << "Error: El algoritmo de correspondencias no encontro Matches.\n" << endl;
        //        exit(0);
    }

    /* Ajustando Keylines al Match */
    ajustarMatch_Keylines(matches, matches,
            keylines_test, keylines_refe,
            keylines_test, keylines_refe);
} // function 

void matching_KeyPoints(int idMatcher, int idDetecDescr,
        cv::Mat& descr_test, cv::Mat& descr_refe,
        std::vector<cv::DMatch>& matches,
        std::vector<cv::KeyPoint>& keypoints_test,
        std::vector<cv::KeyPoint>& keypoints_refe,
        std::vector<cv::Point2f>& puntos_test,
        std::vector<cv::Point2f>& puntos_refe) {
    /* <~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~> */
    /* < Matching > */
    /* <~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~> */
    /* Realizando Correspondencia de segementos de Linea */
    chooseMatchDescriptors(idMatcher, idDetecDescr,
            descr_test, descr_refe, matches);

    /* <Comprobando si el Algoritmo encontro correspondencias> */
    if (matches.empty()) {
        cout << "Error: El algoritmo de correspondencias no encontro Matches.\n" << endl;
        //        exit(0);
    }

    /* Ajustando Keylines al Match */
    ajustarMatch_Keypoints(matches, matches,
            keypoints_test, keypoints_refe,
            keypoints_test, keypoints_refe);

    /* Obteniendo los Puntos de los keypoints*/
    extraePointsOfKeypoints(keypoints_test, keypoints_refe,
            puntos_test, puntos_refe);

} // function 


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FeatureMosaicing.hpp
 * Author: luis
 *
 * Created on 6 de junio de 2018, 01:18 PM
 */

#ifndef FEATUREMOSAICING_HPP
#define FEATUREMOSAICING_HPP
#include "Struct_Parametros.hpp"
#include "Struct_Resultados.hpp"

#include "ImageStitching.hpp"

#include "GraphicalResults.h"

#include <opencv2/line_descriptor.hpp>
#include "opencv2/core/utility.hpp"
#include <opencv2/imgproc.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/highgui.hpp>

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

class FeatureMosaicing {
public:
    FeatureMosaicing(ParametrosEjecucion& parametros);

    virtual ~FeatureMosaicing();

    Struct_ResultsMATIO Mosaicing_PairOfImages(int idMedia, std::string dirMedia);
    Struct_ResultsMATIO Mosaicing_VideoSequence(int idMedia, std::string dirMedia);
    Struct_ResultsMATIO Mosaicing_SetOfImages(int idMedia, std::string dirMedia);
    void Mosaicing_ImageSequence(std::string nameVideo, cv::VideoCapture& vidCapt);

private:
    void estableceNumeroIteracionesRANSAC();

    void passStructMatchResults2StructMatio(Struct_Matching& structMatch);

    void printDetallesIteracion(Struct_Matching& structMatch);
    std::string makeExecutionName();

    void savingResults(ImageStitching& stitcher);

    void pasaVariables2Struct(int numFramesTotal, double framesPerSecond);

    double assignFrames(int ii,
            cv::VideoCapture& vidCapt,
            Struct_Matching& structMatch,
            Struct_Matching& structMatch_anterior);

    void Mosaicing_VideoCapture(cv::VideoCapture& vidCapt);

    // <Parametros >
    ParametrosEjecucion parametros;

    // < Estructura de resultados>
    Struct_ResultsMATIO struResultMatio;
    GraphicalResults graphicalResults;
    double averageSizeImage;

    // <Otras Variables >

    //    std::vector<std::string> vectTiemposEjecucion_label;
    //    std::vector<double> vectTiemposEjecucion_value;

    bool DynamicKeyFrame(cv::Mat& matModelo,
            double porcentajeMatchModel, double porcentajeDesplModel,
            cv::Size sizeImg,
            ImageStitching& stitcher);


    void getResultsStructMatch(bool bandLines, Struct_Matching& structMatch);
};

#endif /* FEATUREMOSAICING_HPP */


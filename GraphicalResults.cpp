/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   GraphicalResults.cpp
 * Author: luis
 * 
 * Created on 20 de agosto de 2018, 06:19 PM
 */

#include "GraphicalResults.h"

#include "Struct_Parametros.hpp"
#include "Struct_Resultados.hpp"
#include "TimeControl.hpp"
#include "LinePointFunctions.hpp"
#include "BasicFunctions.hpp"
#include "ImageStitching.hpp"
#include "SavingResultsMatio.hpp"
#include "QuadTree_Keypoints.h"

GraphicalResults::GraphicalResults() {
    //    this->parametros = NULL;

    /* Inicia la ejecucion con los frames del video */
    this->bandNoWaitFirstWindow = true; // hace un delay para acomodar las ventanas antes de ejecutar automaticamente     
} // constructor 

GraphicalResults::GraphicalResults(ParametrosEjecucion& parametros) {
    this->parametros = parametros;

    /* Inicia la ejecucion con los frames del video */
    this->bandNoWaitFirstWindow = true; // hace un delay para acomodar las ventanas antes de ejecutar automaticamente     
} // constructor 

GraphicalResults::~GraphicalResults() {
} //desctructor

void GraphicalResults::plotingFeaturesDetected(Struct_Matching & structMatch, Struct_ResultsMATIO & struResultMatio) {
    if (this->parametros.ControlVent.bandMuestraDetecFeatures) {
        std::vector<std::string> vEti_refe, vEti_test;
        cv::Mat mo_refe, mo_test;
        std::vector<cv::Scalar> vcolor_refe, vcolor_test;

        int numDetect_features_test;
        int numDetect_features_refe;

        if (this->parametros.Features.bandHacerConLineas) {
            plotLineas(structMatch.imageMat_refe, mo_refe, structMatch.structLineas.vectLinesDetect_refe, vcolor_refe); // Test
            plotLineas(structMatch.imageMat_test, mo_test, structMatch.structLineas.vectLinesDetect_test, vcolor_test); // Refe
            numDetect_features_test = structMatch.structLineas.vectLinesDetect_refe.size();
            numDetect_features_refe = structMatch.structLineas.vectLinesDetect_test.size();

        } else {
            plotPuntos(structMatch.imageMat_refe, mo_refe, structMatch.structPuntos.vectKeypointsDetect_refe, vcolor_refe, vEti_refe);
            plotPuntos(structMatch.imageMat_test, mo_test, structMatch.structPuntos.vectKeypointsDetect_test, vcolor_test, vEti_test);
            numDetect_features_test = structMatch.structPuntos.vectKeypointsDetect_test.size();
            numDetect_features_refe = structMatch.structPuntos.vectKeypointsDetect_refe.size();

        } // if - else       

        /* Añadiendo texto a las imagenes de match*/
        cv::Point punto(10, 15);
        cv::Scalar color_refe(0, 255, 0);
        cv::Scalar color_test(255, 0, 0);
        addTextoEnPuntoImagen(mo_refe, int2string(numDetect_features_refe) + " Features Detected (Refe)- " + struResultMatio.nomDetecDesc, punto, color_refe);
        addTextoEnPuntoImagen(mo_test, int2string(numDetect_features_test) + " Features Detected (Test)- " + struResultMatio.nomDetecDesc, punto, color_test);

        /* Mostrando ventana features detectados en imagen de refe y test */
        cv::Mat Img_Output = concatenaMatriz_Horizontal(mo_refe, mo_test);
        /* Reduciendo el tamaño de la imagen para que se pueda mostrar en la pantalla */
        modificaResImg_EnBaseAFils(true, Img_Output, Img_Output, this->parametros.ControlVent.widthWanted / 2);
        /* Mostrando ventana concatenada */
        cv::imshow("Features detecten in Refe-Image(left) and Features detecten in Test-Image(rigth) - " + struResultMatio.nomDetecDesc, Img_Output);

    }// if    
} // function

void GraphicalResults::plotingMatchesRANSAC(Struct_Matching & structMatch, Struct_ResultsMATIO& struResultMatio) {
    /* <ploting  matches >*/
    if (this->parametros.ControlVent.bandShowMatch_WithWithout_RANSAC) {
        /* */
        cv::Mat Img_output_sin_ransac, Img_output_con_ransac;
        //std::vector<char> mask(structMatch.matches.size(), 1);
        std::vector<char> mask;

        int numMatches, numInliers;

        /* Dibujando de correspondencia sin ransac */
        if (this->parametros.Features.bandHacerConLineas) {
            numMatches = structMatch.structLineas.vectMatches.size();
            numInliers = structMatch.structLineas.numInliers_lines;
            mask = std::vector<char>(numMatches, 1);

            /* <--- Sin Ransac ---> */
            Img_output_sin_ransac = plotMatchesInliners_lineas(1, true, true,
                    structMatch.imageMat_refe, structMatch.structLineas.vectLinesMatch_refe,
                    structMatch.imageMat_test, structMatch.structLineas.vectLinesMatch_test,
                    structMatch.structLineas.vectColores);
            /* <--- Con Ransac ---> */
            Img_output_con_ransac = plotMatchesInliners_lineas(1, true, true,
                    structMatch.imageMat_refe, structMatch.structLineas.vectLinesMatch_Inliers_refe,
                    structMatch.imageMat_test, structMatch.structLineas.vectLinesMatch_Inliers_test,
                    structMatch.structLineas.vectColores);
        } else {
            numMatches = structMatch.structPuntos.vectMatches.size();
            numInliers = structMatch.structPuntos.numInliers_points;
            mask = std::vector<char>(numMatches, 1);

            /* <--- Sin Ransac ---> */
            Img_output_sin_ransac = plotMatchesInliners_puntos(1, true, true,
                    structMatch.imageMat_refe, structMatch.structPuntos.vectPointsMatch_refe,
                    structMatch.imageMat_test, structMatch.structPuntos.vectPointsMatch_test,
                    structMatch.structPuntos.vectColores);
            /* <--- Con Ransac ---> */
            Img_output_con_ransac = plotMatchesInliners_puntos(1, true, true,
                    structMatch.imageMat_refe, structMatch.structPuntos.vectPointsMatch_Inliers_refe,
                    structMatch.imageMat_test, structMatch.structPuntos.vectPointsMatch_Inliers_test,
                    structMatch.structPuntos.vectColores);
        } // if - else 


        /* Añadiendo texto a las imagenes de match */
        cv::Point punto_1(10, 15), punto_2(10 + structMatch.imageMat_refe.cols, 15);
        cv::Scalar color_1(0, 0, 255), color_2(0, 255, 255);

        std::string numMatches_sin_ransac = int2string(numMatches);
        std::string numMatches_con_ransac = int2string(numInliers);

        /* <--- Sin Ransac ---> */
        addTextoEnPuntoImagen(Img_output_sin_ransac,
                numMatches_sin_ransac + " Line Matches Without RANSAC - " + struResultMatio.nomDetecDesc,
                punto_1, color_1);
        addTextoEnPuntoImagen(Img_output_sin_ransac,
                " RefeFrame : " + int2string(structMatch.idFrame_refe) + " TestFrame : " + int2string(structMatch.idFrame_test),
                punto_2, color_2);

        /* <--- Con Ransac ---> */
        addTextoEnPuntoImagen(Img_output_con_ransac,
                numMatches_con_ransac + " Line Matches With RANSAC - " + struResultMatio.nomDetecDesc,
                punto_1, color_1);
        addTextoEnPuntoImagen(Img_output_con_ransac,
                " RefeFrame : " + int2string(structMatch.idFrame_refe) + " TestFrame : " + int2string(structMatch.idFrame_test),
                punto_2, color_2);

        /* Mat Output RANSAC */
        //        cv::Mat& imgRANSAC;
        //        modificaResImg_EnBaseAFils(true, Img_output_con_ransac, imgRANSAC, this -> paramResImg.widthWanted);

        /* Mostrando ventana sin ransac y con ransac */
        cv::Mat Img_Output = concatenaMatriz_Vertical(Img_output_sin_ransac, Img_output_con_ransac);

        /* Reduciendo el tamaño de la imagen para que se pueda mostrar en la pantalla */
        modificaResImg_EnBaseAFils(true, Img_Output, Img_Output, this->parametros.ControlVent.widthWanted);

        /* Mostrando ventana concatenada */
        cv::imshow(" Line Matches (UP) and Inliers RANSAC (DOWN) - " + struResultMatio.nomDetecDesc, Img_Output);
    } // if       
} // function 

void GraphicalResults::ploting_ValidIntersectionPoints(Struct_Matching & structMatch) {
    /* <~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~> */
    /* < Ploting intersection points > */
    if (this->parametros.ControlVent.bandMuestraPuntosInterseccionValidos &&
            this->parametros.Features.bandHacerConLineas) {
        //        std::vector<cv::line_descriptor::KeyLine> vectLines_OK_tran = getSpecificLinesFromVector(
        //                structMatch.structLineas.keylines_tran_BH_NoMejorada,
        //                structMatch.structLineas.vectIndexLineasInliers_UsedToImproveModel);
        //
        //        std::vector<cv::line_descriptor::KeyLine> vectLines_OK_refe = getSpecificLinesFromVector(
        //                structMatch.structLineas.keylines_refe_Match_Inliners,
        //                structMatch.structLineas.vectIndexLineasInliers_UsedToImproveModel);

        // Creando Imagen con fondo blanco 
        cv::Mat matBackground;
        matBackground.create(structMatch.imageMat_refe.cols, structMatch.imageMat_refe.rows, CV_8UC1);
        matBackground.setTo(255);

        // Variables auxiliares 
        std::vector<cv::Scalar> vectColores_lineas, vectColores_pi;
        std::vector<std::string> vecEtiquetas_pi;

        int R_refe = 31, G_refe = 97, B_refe = 141;
        //int R_tran = 243, G_tran = 156, B_tran = 18;
        int R_tran = 243, G_tran = 18, B_tran = 18;

        int tamPunto = 10;
        bool bandShowEti_PI = true;

        // Lineas Inliers Refe
        vectColores_lineas = generateVectorSpecificColor(structMatch.structLineas.vectLineasSelect_IP_refe.size(), R_refe, G_refe, B_refe);
        plotLineasRectas(matBackground, matBackground, structMatch.structLineas.vectLineasSelect_IP_refe, vectColores_lineas, "_", !bandShowEti_PI);

        // Lineas Inliers Tran
        vectColores_lineas = generateVectorSpecificColor(structMatch.structLineas.vectLineasSelect_IP_tran.size(), R_tran, G_tran, B_tran);
        plotLineasRectas(matBackground, matBackground, structMatch.structLineas.vectLineasSelect_IP_tran, vectColores_lineas, "'", !bandShowEti_PI);

        // Puntos Inliers Tran        
        vectColores_pi = generateVectorSpecificColor(structMatch.structLineas.vectLineasSelect_IP_tran.size(), R_tran, G_tran, B_tran);
        plotPuntos(matBackground, matBackground, structMatch.structLineas.vectLS_PI_tran, vectColores_pi, vecEtiquetas_pi, "'", bandShowEti_PI, tamPunto);

        // Puntos Inliers Refe
        vectColores_pi = generateVectorSpecificColor(structMatch.structLineas.vectLineasSelect_IP_refe.size(), R_refe, G_refe, B_refe);
        plotPuntos(matBackground, matBackground, structMatch.structLineas.vectLS_PI_refe, vectColores_pi, vecEtiquetas_pi, "_", bandShowEti_PI, tamPunto);


        /* Añadiendo texto a las imagenes de match */
        cv::Point punto_1(10, 15);
        cv::Scalar color_1(0, 0, 0);
        addTextoEnPuntoImagen(matBackground,
                int2string(structMatch.structLineas.vectLineasSelect_IP_tran.size()) + " Intersection Points (" +
                int2string(structMatch.structLineas.vectLineasSelect_IP_tran.size()) + " Lines)",
                punto_1, color_1);

        modificaResolucionImg(matBackground, matBackground, structMatch.imageMat_refe.cols, structMatch.imageMat_refe.rows);
        std::string titleFig = "Valid Intersection Points of Lines Inliers for Improving the Model";
        cv::imshow(titleFig, matBackground);
    }// if 
    /* <~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~> */
} // function 

void GraphicalResults::plotingFeaturesInliersTransformed(ImageStitching& stitcher, Struct_Matching & structMatch) {
    /* **************************** TRANSFORMACIONES PARA LINEAS **************************** */
    if (this->parametros.ControlVent.bandMuestraTransformacion) {
        /* <~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~> */
        cv::Mat imageInliers;
        std::string titleFigure;
        std::string titleSpecialFeature;
        cv::Point punto(10, 15);
        cv::Scalar color(0, 0, 255);
        cv::Scalar colorBlack(255, 255, 255);

        /* Mostrando Lineas Inliers */
        int numInliers = ((this->parametros.Features.bandHacerConLineas) ? structMatch.structLineas.numInliers_lines : structMatch.structPuntos.numInliers_points);
        titleFigure = "Inliners in the Reference Image";
        titleSpecialFeature = int2string(numInliers) + " Inlier Matches (" + double2string(structMatch.porcentajeMatchInliers) + " % Match)- Reference Image";

        // Convirtiendo a escala de grises 
        cv::cvtColor(structMatch.imageMat_refe, imageInliers, cv::COLOR_RGB2GRAY);
        /*--------------------------------------------*/
        /* <~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~> */
        /* Obteniendo tamaño del canvas entre las dos imágenes */
        cv::Mat matModelo = ((this->parametros.Features.bandHacerConLineas) ? structMatch.structLineas.Model_ip.clone() : structMatch.structPuntos.Model_kp.clone());
        cv::Mat matTraslacion;
        std::vector<double> vectDesplazamiento;

        cv::Size sizeCanvas = stitcher.getSizeCanvasFromImageTransformations(matModelo,
                structMatch.imageMat_test.size(), structMatch.imageMat_refe.size(),
                matTraslacion, vectDesplazamiento);

        // < Refe Transformation >
        cv::warpPerspective(imageInliers, imageInliers, matTraslacion, sizeCanvas); // <Aplicando ajuste para que se pueda visualizar (Traslacion solamente)>

        /* <~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~> */

        /* < ~~~~~~~~~~~~~~ Mostrando Features ~~~~~~~~~~~~~~ >*/
        if (this->parametros.Features.bandHacerConLineas) {
            std::vector<cv::line_descriptor::KeyLine> vectLineasInliers_refe_traslacion;
            std::vector<cv::line_descriptor::KeyLine> vectLineasInliers_tran_traslacion;

            // < Ajustando Lineas Refe >
            perspectiveTransform_vectKeyLines(
                    structMatch.structLineas.vectLinesMatch_Inliers_refe,
                    vectLineasInliers_refe_traslacion, matTraslacion);

            // < Ajustando Lineas Trans >
            perspectiveTransform_vectKeyLines(
                    structMatch.structLineas.vectLinesMatch_Inliers_tran,
                    vectLineasInliers_tran_traslacion, matTraslacion);

            plotLineasRectas(imageInliers, imageInliers, vectLineasInliers_refe_traslacion, structMatch.structLineas.vectColores, "_");
            plotLineasRectas(imageInliers, imageInliers, vectLineasInliers_tran_traslacion, structMatch.structLineas.vectColores, "'");

            /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
            //            printKeylinesAsPointsVector_varMatlab(structMatch.structLineas.keylines_refe_Match_Inliners, "LineasInliers_refe");
            //            printKeylinesAsPointsVector_varMatlab(structMatch.structLineas.keylines_tran_Match_Inliners, "LineasInliers_tran");
            //
            //            std::vector<double> vectError_Angle = getElementsFromVector(structMatch.vectAngle_BH, structMatch.vecIndicesInliersMatch_BH);
            //            std::vector<double> vectError_Dist = getElementsFromVector(structMatch.vectDist_BH, structMatch.vecIndicesInliersMatch_BH);
            //            printVector_comoVariableMatlab(vectError_Angle, "vectError_Angle");
            //            printVector_comoVariableMatlab(vectError_Dist, "vectError_Dist");
            /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

        } else {
            cv::Mat matHomografia_ajustada = matTraslacion * matModelo;
            std::vector<cv::Point2f> vectPointsInliers_refe_trans, vectPointsInliers_test_trans;
            cv::perspectiveTransform(structMatch.structPuntos.vectPointsMatch_Inliers_refe, vectPointsInliers_refe_trans, matTraslacion);
            cv::perspectiveTransform(structMatch.structPuntos.vectPointsMatch_Inliers_test, vectPointsInliers_test_trans, matHomografia_ajustada);

            std::vector<std::string> vecEtiquetas;
            plotPuntos(imageInliers, imageInliers, vectPointsInliers_refe_trans, structMatch.structPuntos.vectColores, vecEtiquetas);
            plotPuntos(imageInliers, imageInliers, vectPointsInliers_test_trans, structMatch.structPuntos.vectColores, vecEtiquetas, "'");
        } // if 

        /* Añadiendo texto a las imagenes de match*/
        addTextoEnPuntoImagen(imageInliers, titleSpecialFeature, punto, color, 1, 4);
        addTextoEnPuntoImagen(imageInliers, titleSpecialFeature, punto, colorBlack, 1, 1);
        /* Muestra Transformacion */
        //cv::imshow(titleFigure, imageInliers);
        muestraVentanaCanvas(imageInliers, titleFigure, true);
        /* <~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~> */
    } // if
} // function 

void GraphicalResults::plotingMosaicoParImagenes(ImageStitching & stitcher, Struct_Matching& structMatch) {
    /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
    int iterFrame_actual = structMatch.iterFramePares + 1;
    if (this->parametros.ControlVent.bandMosaicingTwoImages_iter && stitcher.vect1DConsiderarStitching[iterFrame_actual]) {
        std::string nombreVentanaCanvas;
        // < Realizando Stitching de las DOS Imagenes > 

        //        int indx_refe = stitcher.vect2DRelacionHomografias[iterFrame_actual][2];
        //
        //        int indx_Test_local = findIndexOfValInVector(iterFrame_actual, stitcher.vectIndexLocal);
        //        int indx_Refe_local = findIndexOfValInVector(indx_refe, stitcher.vectIndexLocal);

        cv::Mat Canvas, Mask;
        cv::Mat img_test_mask = cv::Mat(structMatch.imageMat_test.size(), CV_8UC1, cv::Scalar(255)); // creando mascara para copiar los elementos        
        cv::Mat img_refe_mask = cv::Mat(structMatch.imageMat_refe.size(), CV_8UC1, cv::Scalar(255)); // creando mascara para copiar los elementos 

        /* < Obteniendo Modelo de acuerdo al tipo de primitiva> */
        cv::Mat matModelo = ((this->parametros.Features.bandHacerConLineas) ? structMatch.structLineas.Model_ip.clone() : structMatch.structPuntos.Model_kp.clone());

        Canvas = stitcher.stitchingTwoImages(
                structMatch.imageMat_test, img_test_mask,
                structMatch.imageMat_refe, img_refe_mask,
                matModelo,
                Mask, 0);
        // Mostrando Canvas 
        nombreVentanaCanvas = "Mosaicing Results of Two Images";
        muestraVentanaCanvas(Canvas, nombreVentanaCanvas);
        this->bandNoWaitFirstWindow = false;
        //            bandVentanasAutomaticas = false;
    } // if         
    /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
} // function 

void GraphicalResults::controlaVentanas(Struct_Matching& structMatch, ImageStitching & stitcher, Struct_ResultsMATIO& struResultMatio) {
    if (this->parametros.ControlVent.bandMuestraVentanasResults) {
        /* < Mostrando Resultados Individualmente o no > */
        //+++++++++++++++++++++++++++++++++++++++++++++++      
        if (this->parametros.ControlVent.bandMuestraResultadosIndividualmente) {
            plotingFeaturesDetected(structMatch, struResultMatio);
            plotingMatchesRANSAC(structMatch, struResultMatio);
            plotingFeaturesInliersTransformed(stitcher, structMatch);
            plotingMosaicoParImagenes(stitcher, structMatch);
            ploting_ValidIntersectionPoints(structMatch);

        } else {
            // < Comprando el tipo de caracteristica utilizada >
            if (this->parametros.Features.bandHacerConLineas) {
                cv::Mat imgOutput = plotResults_lineas(structMatch.imageMat_refe, structMatch.imageMat_test,
                        structMatch.idFrame_refe, structMatch.idFrame_test,
                        structMatch.structLineas.vectLinesDetect_refe, structMatch.structLineas.vectLinesDetect_test,
                        structMatch.structLineas.vectLinesMatch_refe, structMatch.structLineas.vectLinesMatch_test,
                        structMatch.structLineas.vectLinesMatch_Inliers_refe, structMatch.structLineas.vectLinesMatch_Inliers_test);

                modificaResImg_EnBaseAFils(true, imgOutput, imgOutput, this->parametros.ControlVent.widthWanted / 2.5);
                cv::imshow("Reference (left) and Test (rigth) Lines : Yellow-No_Match. Red-Wrong_Match. Green-Good_Match", imgOutput);
                //cv::waitKey(parametros.paramControlVent.msEsperar_mostrarVentana);
            } else {
                cv::Mat imgOutput = plotResults_puntos(structMatch.imageMat_refe, structMatch.imageMat_test,
                        structMatch.idFrame_refe, structMatch.idFrame_test,
                        structMatch.structPuntos.vectKeypointsDetect_refe, structMatch.structPuntos.vectKeypointsDetect_test,
                        structMatch.structPuntos.vectPointsMatch_refe, structMatch.structPuntos.vectPointsMatch_test,
                        structMatch.structPuntos.vectPointsMatch_Inliers_refe, structMatch.structPuntos.vectPointsMatch_Inliers_test);

                //modificaResImg_EnBaseAFils(true, imgOutput, imgOutput, this->parametros.paramControlVent.widthWanted / 1.8);
                modificaResImg_EnBaseAFils(true, imgOutput, imgOutput, this->parametros.ControlVent.widthWanted / 2.5);
                cv::imshow("Reference (left) and Test (rigth) Points : Yellow-No_Match. Red-Wrong_Match. Green-Good_Match", imgOutput);
            } // if - else
        } // if - else

        /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        /* < Si se realizo el mosaico acumulado > */
        if (this->parametros.Mosaic.bandMosaicoAcumulado && structMatch.bandConsiderarStitching
                && this->parametros.ControlVent.bandMuestraMosaicoAcumulado) {
            // <Mostrando Mosaico img>                
            muestraVentanaCanvas(stitcher.MosaicoAculumado_img, "Accumulated mosaic Img", 1, this->parametros.ControlVent.widthWanted, this->parametros.ControlVent.widthWanted);

            // <Mostrando Mosaico mask>
            if (this->parametros.ControlVent.bandMuestraMascaraMosaico) {
                muestraVentanaCanvas(stitcher.MosaicoAculumado_mask, "Accumulated mosaic Mask", 1);
            } // if                   

            //        // < Esperando (Manteniendo las ventanas) >
            //        cv::waitKey(this->parametros.paramControlVent.msEsperar_mostrarVentana); //  * 10
            //                cv::waitKey(0);
            //            cv::destroyWindow(nombreVentanaCanvas);
        } // if 
        /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

        //+++++++++++++++++++++++++++++++++++++++++++++++
        /* < Control de ventanas > */
        /* Esperando tecla o tiempo*/
        if (this->parametros.ControlVent.bandMuestraVentanas_automatico && this->bandNoWaitFirstWindow) {
            cv::waitKey((this->parametros.ControlVent.msEsperar_mostrarVentana));
        } else {
            cv::waitKey();
            this->bandNoWaitFirstWindow = true;
        } //if-else


        // <------------------------------------------------> 
        if (this->parametros.ControlVent.bandMuestraResultadosIndividualmente &&
                this->parametros.ControlVent.bandMosaicingTwoImages_iter) {
            cv::destroyWindow("Mosaicing Results of Two Images");
        } // if
    }//if
} // function

void GraphicalResults::plotingQuadTreeResults(Struct_QuadTreeKP& structQuadTree) {
    /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
    // < Ploting Puntos de Intersecion >    
    int numPuntosInters = structQuadTree.vectIntersecP_test.size();
    int numPuntosKey = structQuadTree.vectKeyP_test.size();
    //    int numPuntosInliers = structQuadTree.vectPoints_test.size();

    std::vector<std::string> vectCadena;
    structQuadTree.vectIntersecP_colores = generateVectorSpecificColor(numPuntosInters, 0, 255, 0);
    structQuadTree.vectKeyP_colores = generateVectorSpecificColor(numPuntosKey, 255, 0, 255);
    //    structQuadTree.vectPointsInliers_colores = generateVectorSpecificColor(numPuntosInliers, 0, 0, 255);

    int sizePoint = 8;
    bool bandEtiquetaNum_IP = false;
    bool bandEtiquetaNum_KP = false;
    bool bandEtiquetaNum_Inliers = false;
    cv::Mat matOutput;
    //  < TEST >
    // Puntos interseccion
    plotPuntos(structQuadTree.imgMat_test, structQuadTree.imgMat_test,
            structQuadTree.vectIntersecP_test, structQuadTree.vectIntersecP_colores,
            vectCadena, "", bandEtiquetaNum_IP, sizePoint);
    // KeyPoints
    plotPuntos(structQuadTree.imgMat_test, structQuadTree.imgMat_test,
            structQuadTree.vectKeyP_test,
            structQuadTree.vectKeyP_colores,
            vectCadena, "", bandEtiquetaNum_KP, sizePoint);
    //    // Inliers
    //    plotPuntos(structQuadTree.imgMat_test, structQuadTree.imgMat_test,
    //            structQuadTree.vectPoints_test,
    //            structQuadTree.vectPointsInliers_colores,
    //            vectCadena, "", bandEtiquetaNum_Inliers, sizePoint);

    //  < REFE >    
    // Puntos interseccion
    plotPuntos(structQuadTree.imgMat_refe, structQuadTree.imgMat_refe,
            structQuadTree.vectIntersecP_refe, structQuadTree.vectIntersecP_colores,
            vectCadena, "", bandEtiquetaNum_IP, sizePoint);
    // KeyPoints
    plotPuntos(structQuadTree.imgMat_refe, structQuadTree.imgMat_refe,
            structQuadTree.vectKeyP_refe,
            structQuadTree.vectKeyP_colores,
            vectCadena, "", bandEtiquetaNum_KP, sizePoint);
    //    // KeyPoints
    //    plotPuntos(structQuadTree.imgMat_refe, structQuadTree.imgMat_refe,
    //            structQuadTree.vectPoints_refe,
    //            structQuadTree.vectPointsInliers_colores,
    //            vectCadena, "", bandEtiquetaNum_Inliers, sizePoint);

    // Concatenando Matriz
    int colsSpace = 25;
    cv::Mat m_space = cv::Mat(structQuadTree.imgMat_refe.rows, colsSpace,
            structQuadTree.imgMat_refe.type(), cv::Scalar(255, 255, 255));

    matOutput = concatenaMatriz_Horizontal(structQuadTree.imgMat_refe, m_space);
    matOutput = concatenaMatriz_Horizontal(matOutput, structQuadTree.imgMat_test);

    //    matOutput = concatenaMatriz_Horizontal(structQuadTree.imgMat_refe, structQuadTree.imgMat_test);

    // añadiendo etiqueta
    cv::Point punto_refe(10, 15);
    cv::Point punto_test(colsSpace + 10 + structQuadTree.imgMat_refe.cols, 15);
    std::string label_refe = "Num Intersection Points = " + int2string(numPuntosInters);
    std::string label_test = "Num KeyPoints QuadTree = " + int2string(numPuntosKey);

    addTextoEnPuntoImagen(matOutput, label_refe, punto_refe, cv::Scalar(0, 255, 0), 1, 6);
    addTextoEnPuntoImagen(matOutput, label_refe, punto_refe, cv::Scalar(0, 0, 0), 1, 2);

    addTextoEnPuntoImagen(matOutput, label_test, punto_test, cv::Scalar(255, 0, 255), 1, 6);
    addTextoEnPuntoImagen(matOutput, label_test, punto_test, cv::Scalar(0, 0, 0), 1, 2);


    std::string titulo = "QuadTree - Intersection Points from KeyLines (LBD) and KeyPoints (" + parametros.Features.vecNomDetecDescr[structQuadTree.idDetecDescrQuad] + ")";
    modificaResImg_EnBaseAFils(true, matOutput, matOutput, parametros.ControlVent.widthWanted / 2.5);

    // <Mostrando ventana >
    cv::imshow(titulo, matOutput);
    //        muestraVentanaCanvas(matOutput, titulo, true, 640, structQuadTree.imgMat_refe.rows);
    /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
} // function

void muestraVentanaCanvas(cv::Mat& Canvas, std::string nombreVentanaCanvas,
        bool bandAplicarSiempre, int anchoDeseado, int altoDeseado) {

    if ((Canvas.cols > anchoDeseado) || (Canvas.rows > altoDeseado) || bandAplicarSiempre) {
        /* SI es mayor que el tamaño deseado entonces realiza el ajuste de la ventana */
        cv::namedWindow(nombreVentanaCanvas, cv::WINDOW_NORMAL);
        cv::imshow(nombreVentanaCanvas, Canvas);
        cv::resizeWindow(nombreVentanaCanvas, anchoDeseado, altoDeseado);
    } else {
        cv::imshow(nombreVentanaCanvas, Canvas);
    } // if - else

} // function 
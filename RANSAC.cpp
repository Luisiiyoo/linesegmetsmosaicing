/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   RANSAC.cpp
 * Author: luis
 * 
 * Created on 6 de junio de 2018, 09:07 PM
 */

#include "RANSAC.hpp"
#include "LinePointFunctions.hpp"
#include "BasicFunctions.hpp"
#include "TimeControl.hpp"
#include "Struct_Parametros.hpp"
#include "Struct_Resultados.hpp"
#include "ModelEstimator.h"

#include <opencv2/xfeatures2d.hpp>
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include <opencv2/line_descriptor.hpp>
#include "opencv2/core/utility.hpp"
#include <opencv2/imgproc.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/highgui.hpp>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "Struct_Parametros.hpp"
#include <iostream>
#include <fstream>
#include <string.h>
#include <cstdlib>
#include <math.h>
#include <opencv2/calib3d.hpp>  

#include <time.h>
#include <sys/time.h>
#include <vector>

RANSAC::RANSAC(ParametrosEjecucion& parametros) {
    this->parametros = parametros;
} //

RANSAC::RANSAC(const RANSAC& orig) {
} //

RANSAC::~RANSAC() {
} //

cv::Mat RANSAC::simpleCalculateHomographyAndEvaluation_puntos(
        std::vector<int>& vecIndMatchSeleccionados,
        std::vector<bool>& vectBanderasConsiderarLineas,
        std::vector<cv::Point2f>& puntos_test, std::vector<cv::Point2f>& puntos_refe,
        int idTypeModel4Ransac, double umbralDistanciaInliers,
        std::vector<double>& vecDistancias, std::vector<int>& vecIndicesInliersMatch,
        int& numInliers, std::vector<cv::Point2f>& puntos_transformados) {
    /* Variables auxiliares */
    std::vector<cv::Point2f> puntos_select_test; // Vector de KeyLines Test
    std::vector<cv::Point2f> puntos_select_refe; // Vector de KeyLines Refe

    /* < 2.- Obtener la Homografia del conjunto seleccionado >*/
    /* Homografia de que se calculara */
    cv::Mat Modelo;

    /* <Obteniendo segementos de linea que seran utilizados> */
    puntos_select_test = getSpecificPointsFromVector(puntos_test, vecIndMatchSeleccionados);
    puntos_select_refe = getSpecificPointsFromVector(puntos_refe, vecIndMatchSeleccionados);

    // calculando modelo 
    cv::Mat modelo = calculateModel(idTypeModel4Ransac, puntos_select_test, puntos_select_refe);

    /* < 3.- Transformando todos los puntos a la imagen de Referencia y calculando distancia > */
    /* < 4.- Obteniendo Inliers y contandolos >*/
    numInliers = evaluaModelo_puntos(modelo,
            vectBanderasConsiderarLineas,
            puntos_test, puntos_refe,
            umbralDistanciaInliers,
            vecDistancias, vecIndicesInliersMatch,
            false,
            puntos_transformados);

    // return the homography
    return modelo;
} // function 

cv::Mat RANSAC::simpleCalculateHomographyAndEvaluation_lineas(
        std::vector<int>& vecIndMatchSeleccionados,
        std::vector<bool>& vectBanderasConsiderarLineas,
        std::vector<cv::line_descriptor::KeyLine>& lineas_test,
        std::vector<cv::line_descriptor::KeyLine>& lineas_refe,
        int idTypeModel4Ransac, int tipoEvalDistancia,
        double umbralDistanciaInliers, double umbralAnguloInliers,
        std::vector<double>& vecDistancias, std::vector<double>& vectAngulos,
        std::vector<int>& vecIndicesInliersMatch,
        int& numInliers,
        std::vector<cv::line_descriptor::KeyLine>& lineas_transformadas,
        std::vector<cv::line_descriptor::KeyLine>& lineas_select_test,
        std::vector<cv::line_descriptor::KeyLine>& lineas_select_refe,
        std::vector<cv::Point2f>& pointsFromSelectLines_test,
        std::vector<cv::Point2f>& pointsFromSelectLines_refe) {

    /* < 2.- Obtener la Homografia del conjunto seleccionado >*/
    /* Obteniendo segementos de linea que seran utilizados */
    lineas_select_test = getSpecificLinesFromVector(lineas_test, vecIndMatchSeleccionados);
    lineas_select_refe = getSpecificLinesFromVector(lineas_refe, vecIndMatchSeleccionados);

    /* Obteniendo interseccion  de lineas */
    generaCorrespondenciaPuntosInterseccionLineas(lineas_select_test, lineas_select_refe,
            pointsFromSelectLines_test, pointsFromSelectLines_refe,
            this->puntoCentroImg, this->radioTolerancia);

    /* < Calculando Modelo > */
    cv::Mat model = calculateModel(idTypeModel4Ransac, pointsFromSelectLines_test, pointsFromSelectLines_refe);


    /* < 3.- Transformando todos los puntos a la imagen de Referencia y calculando distancia > */
    /* < 4.- Obteniendo Inliers y contandolos >*/
    numInliers = evaluaModelo_lineas(model,
            tipoEvalDistancia, vectBanderasConsiderarLineas,
            lineas_test, lineas_refe,
            umbralDistanciaInliers, umbralAnguloInliers,
            vecDistancias, vectAngulos, vecIndicesInliersMatch,
            false,
            lineas_transformadas);
    return model;
} // function 

double RANSAC::ransac_KeyPoints(Struct_Matching& structMatch) {
    /*+++++++++++++++++++++++++++++++++++++++*/
    /* Midiendo tiempos */
    TimeControl tc;
    tc.start(); // <--------
    /*+++++++++++++++++++++++++++++++++++++++*/
    this->sizeImg = structMatch.imageMat_refe.size();

    /* <~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~> */
    /* < RANSAC > */
    /* <~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~> */
    /* Variables */
    structMatch.structPuntos.bandModeloFound_kp = false; // < Inicialmente No es FALSO de que es buena homografia >
    structMatch.structPuntos.numInliers_points = 0;

    this->vectNumInliersIteraciones = std::vector<int>(this->parametros.RANSAC.numIteraciones);
    int numMatches = structMatch.structPuntos.vectMatches.size();

    /* Umbral de numero de Inliners*/
    this->umbralNumInliers = round(this->parametros.RANSAC.radioDeInliners * numMatches); // umbral de inliners

    /* Vector para considerar todos los matches */
    std::vector<bool> vectBanderasConsiderarLineas = generaVectorTrue(numMatches); // considera todos    
    this->iter_BH = -1;

    std::vector<cv::Point2f> puntos_transformadas_iter;

    double score_iter;
    bool bandHomografiaBien_iter;
    std::vector<double> vectDistancias_iter;
    std::vector<int> vectIndicesInliers_iter;
    std::vector<int> vectIndAleatorios_iter;

    bool bandIteracionMejorQue_BH;


    for (int ii = 0; ii < this->parametros.RANSAC.numIteraciones; ii++) {
        /* <Cuando no se cuenta con suficiente numero de matches para generar el modelo  Termina la ejecucion >*/
        if (numMatches < this->parametros.RANSAC.numMinMuestras) {
            break;
        } // if        

        /* < 1.- Seleccionar N-muestras aleatoriamente >*/
        vectIndAleatorios_iter = generaVecNumAleatorios(0, numMatches, this->parametros.RANSAC.numMinMuestras); // Obteniendo vector de numeros aleatorios entre el rango especificado                     

        /* < 2,3,4.- Calculando Homografia y evaluando las lineas  > */
        cv::Mat HomografiaFromRandomData = simpleCalculateHomographyAndEvaluation_puntos(
                vectIndAleatorios_iter, vectBanderasConsiderarLineas,
                structMatch.structPuntos.vectPointsMatch_test, structMatch.structPuntos.vectPointsMatch_refe,
                this->parametros.RANSAC.idTypeModel4Ransac, this->parametros.RANSAC.umbralDistancia,
                vectDistancias_iter, vectIndicesInliers_iter, this->vectNumInliersIteraciones[ii],
                puntos_transformadas_iter);

        /*____________________________________________________________________________________________*/
        /* < 5.- Revisando si la Homografia de la iteracion generó buenos resultado > */
        if ((this->vectNumInliersIteraciones[ii] >= this->umbralNumInliers) &&
                (this->vectNumInliersIteraciones[ii] >= this->parametros.RANSAC.numMinMuestras)) {
            bandHomografiaBien_iter = true;
        } else {
            bandHomografiaBien_iter = false;
        } // if -else

        // Obteniendo SCORE de la iteracion 
        score_iter = getScoreModelo_puntos(vectDistancias_iter);

        // Comparando iteraciones
        if (ii == 0) {
            // < Primera iteracion >
            bandIteracionMejorQue_BH = true;
        } else if (bandHomografiaBien_iter) { // SI cumple con la condicion se acepta la Homografia            
            // < SI Es mayor que el umbral y si cumple cuenta con almenos el minimo numero de muestras >

            if (this->vectNumInliersIteraciones[ii] > structMatch.structPuntos.numInliers_points) {
                // < SI el numero de inliers es mayor, ese es el mejor hasta el momento >
                bandIteracionMejorQue_BH = true;

            } else if (this->vectNumInliersIteraciones[ii] == structMatch.structPuntos.numInliers_points) {
                /* < SI el numero de inliers es Igual, se desempata con el menor SCORE > */

                if (score_iter < structMatch.structPuntos.score_RANSAC_kp) {
                    bandIteracionMejorQue_BH = true;
                } // if 
            } // if - else if
            vect_IteracionesValidas_ransac.push_back(ii);

        } else {
            bandIteracionMejorQue_BH = false;
        } // if - else if - else

        // <Reemplazando datos>
        if (bandIteracionMejorQue_BH) {
            this->iter_BH = ii;
            this->vectIndAleatorios_BH = vectIndAleatorios_iter;

            structMatch.structPuntos.bandModeloFound_kp = bandHomografiaBien_iter;
            structMatch.structPuntos.score_RANSAC_kp = score_iter;
            structMatch.structPuntos.numInliers_points = this->vectNumInliersIteraciones[ii];
            structMatch.structPuntos.Model_kp = HomografiaFromRandomData.clone();

            structMatch.structPuntos.vectDist_points = vectDistancias_iter;
            structMatch.structPuntos.vectIndexMatch_Inliers_points = vectIndicesInliers_iter;

            structMatch.structPuntos.vectPointsMatch_tran = puntos_transformadas_iter;
        } // if 
    }//for
    /*____________________________________________________________________________________________*/
    /*____________________________________________________________________________________________*/
    /*____________________________________________________________________________________________*/

    /* < Obteniendo los Inliers > */
    structMatch.structPuntos.vectPointsMatch_Inliers_test = getSpecificPointsFromVector(structMatch.structPuntos.vectPointsMatch_test, structMatch.structPuntos.vectIndexMatch_Inliers_points);
    structMatch.structPuntos.vectPointsMatch_Inliers_refe = getSpecificPointsFromVector(structMatch.structPuntos.vectPointsMatch_refe, structMatch.structPuntos.vectIndexMatch_Inliers_points);
    structMatch.structPuntos.vectPointsMatch_Inliers_tran = getSpecificPointsFromVector(structMatch.structPuntos.vectPointsMatch_tran, structMatch.structPuntos.vectIndexMatch_Inliers_points);

    /*____________________________________________________________________________________________*/
    if (structMatch.structPuntos.bandModeloFound_kp != true) {
        //        printf("\n---------------------------------------------------------- \n");
        //        printf("\t!!No se obtuvo la homografia!! \n");
        //        printf("\n---------------------------------------------------------- \n");
        cout << " Error: No se encontro Modelo de transformacion utilizando Puntos.\n" << endl;
        structMatch.structPuntos.numInliers_points = 0;
    } else {
        /*____________________________________________________________________________________________*/

        /* < Mejorando Homografia  > */
        //    if (this->parametros.paramRANSAC.bandRecalcularModelo) {
        //        ransac_KeyPoints_improveModel(structMatch);
        //    } // if 
    }//else     

    //    cout << "Iter_BH = " << iter_BH << endl;
    /*+++++++++++++++++++++++++++++++++++++++*/
    return tc.finish(false, "");
    /*+++++++++++++++++++++++++++++++++++++++*/
} // function

double RANSAC::ransac_LineSegments(Struct_Matching& structMatch) {
    /*+++++++++++++++++++++++++++++++++++++++*/
    /* Midiendo tiempos */
    TimeControl tc;
    tc.start(); // <--------
    /*+++++++++++++++++++++++++++++++++++++++*/
    this->sizeImg = structMatch.imageMat_refe.size();

    /* <Obteniendo Umbral para considerar punto de interseccion valido > */
    this->radioTolerancia = getRadioTolerancia(structMatch.imageMat_refe.size(),
            this->parametros.RANSAC.factorRadioToleranciaPuntoInterseccion, this->puntoCentroImg);
    /* <~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~> */
    /* < RANSAC > */
    /* <~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~> */
    /* <Variables> */
    this->iter_BH = -1; // < Indice de la mejor iteracion >
    structMatch.structLineas.bandModeloFound_ip = false; // < Inicialmente es FALSO >
    structMatch.structLineas.numInliers_lines = 0; // < Inicialmente ningun inlier fue encontrado >

    this->vectNumInliersIteraciones = std::vector<int>(this->parametros.RANSAC.numIteraciones); // < Vector que almacena el numero de inliers en cada iteracion >
    int numMatches = structMatch.structLineas.vectMatches.size(); // < Numero de correspondencias >

    /* <Umbral de numero de Inliners> */
    this->umbralNumInliers = round(this->parametros.RANSAC.radioDeInliners * numMatches); // umbral de inliners para considerar la iteracion
    structMatch.inlinersTh_Homo = umbralNumInliers;

    /* <Vector indicador para considerar cada correspondencia > */
    std::vector<bool> vectBanderasConsiderarLineas = generaVectorTrue(numMatches); // <considera todos las correspondencias en la busqueda de inliers>

    /* < Vector auxiliar en cada iteracion de las lineas TEST transformadas > */
    std::vector<cv::line_descriptor::KeyLine> lineas_transformadas_iter;

    /* < Variables para evaluar CADA ITERACION >*/
    double score_iter; // Mejor Puntuacion 
    bool bandHomografiaBien_iter; // Si pasa el umbral establecido para considerar la iteracion 
    std::vector<double> vectAngulos_iter; // Angulo : Error de transformacion 
    std::vector<double> vectDistancias_iter; // Distancia : Error de transformacion 
    std::vector<int> vectIndicesInliers_iter; // Vector de indices de los inliers 
    std::vector<int> vectIndAleatorios_iter; // Indices aleatorios seleccionados

    bool bandIteracionMejorQue_BH; // < Bandera indicadora que la actual iteracion es la mejor hasta el momento >

    // < ~~~~~~~~~~~~~~~~~~ Iniciando RANSAC ~~~~~~~~~~~~~~~~~~ >
    for (int ii = 0; ii < this->parametros.RANSAC.numIteraciones; ii++) {
        /* <Cuando no se cuenta con suficiente numero de matches para generar el modelo  Termina la ejecucion >*/
        if (numMatches < this->parametros.RANSAC.numMinMuestras) {
            break; // < Terminando el ciclo FOR >
        } // if        

        /* < 1.- Seleccionar N-muestras aleatoriamente >*/
        vectIndAleatorios_iter = generaVecNumAleatorios(0, numMatches, this->parametros.RANSAC.numMinMuestras); // Obteniendo vector de numeros aleatorios entre el rango especificado             
        //        static const int arr[] = {53, 90, 94, 84};
        //        vecIndAleatoriosMatch = vector<int> (arr, arr + sizeof (arr) / sizeof (arr[0]));

        /* < 2,3,4.- Calculando Homografia y evaluando las lineas  > */
        std::vector<cv::line_descriptor::KeyLine> lineas_select_test; // <Lineas aleatorias seleccionadas en TEST>
        std::vector<cv::line_descriptor::KeyLine> lineas_select_refe; // <Lineas aleatorias seleccionadas en REFE>
        std::vector<cv::Point2f> pointsFromSelectLines_test; // < Puntos de interseccion generados con las Lineas Aleatorias de TEST >
        std::vector<cv::Point2f> pointsFromSelectLines_refe; // < Puntos de interseccion generados con las Lineas Aleatorias de REFE >

        cv::Mat HomografiaFromRandomData = simpleCalculateHomographyAndEvaluation_lineas(
                vectIndAleatorios_iter, vectBanderasConsiderarLineas,
                structMatch.structLineas.vectLinesMatch_test, structMatch.structLineas.vectLinesMatch_refe,
                this->parametros.RANSAC.idTypeModel4Ransac, this->parametros.RANSAC.tipoEvalDistancia,
                this->parametros.RANSAC.umbralDistancia, this->parametros.RANSAC.umbralAngulo,
                vectDistancias_iter, vectAngulos_iter,
                vectIndicesInliers_iter, this->vectNumInliersIteraciones[ii],
                lineas_transformadas_iter,
                lineas_select_test, lineas_select_refe,
                pointsFromSelectLines_test, pointsFromSelectLines_refe);

        /*____________________________________________________________________________________________*/
        /* < 5.- Revisando si la Homografia de la iteracion generó buenos resultado > */
        if ((this->vectNumInliersIteraciones[ii] >= this->umbralNumInliers) &&
                (this->vectNumInliersIteraciones[ii] >= this->parametros.RANSAC.numMinMuestras)) {
            // < Registrando esta itearion como valida >
            vect_IteracionesValidas_ransac.push_back(ii);
            bandHomografiaBien_iter = true;
        } else {
            bandHomografiaBien_iter = false;
        } // if -else


        // < Comparando iteraciones >
        if (ii == 0) {
            // < Primera iteracion se considera como la mejor >           
            score_iter = getScoreModelo_lineas(vectDistancias_iter, vectAngulos_iter); // Obteniendo SCORE de la iteracion 
            bandIteracionMejorQue_BH = true;

        } else if (bandHomografiaBien_iter) { // SI cumple con la condicion se acepta la Homografia            

            // < SI Es mayor que el umbral y si cumple cuenta con almenos el minimo numero de muestras >
            if (this->vectNumInliersIteraciones[ii] > structMatch.structLineas.numInliers_lines) {
                // < SI el numero de inliers es mayor, ese es el mejor hasta el momento >                
                score_iter = getScoreModelo_lineas(vectDistancias_iter, vectAngulos_iter); // Obteniendo SCORE de la iteracion 
                bandIteracionMejorQue_BH = true;

            } else if (this->vectNumInliersIteraciones[ii] == structMatch.structLineas.numInliers_lines) {
                /* < SI el numero de inliers es Igual, se desempata con el menor SCORE > */
                score_iter = getScoreModelo_lineas(vectDistancias_iter, vectAngulos_iter); // Obteniendo SCORE de la iteracion 

                // Desempatando con el SCORE
                if (score_iter < structMatch.structLineas.scoreRANSAC_ip) {
                    bandIteracionMejorQue_BH = true;
                } // if 
            } // if - else if

        } else {
            // < Esta iteracion no es la mejor >
            bandIteracionMejorQue_BH = false;
        } // if - else if - else

        // <Reemplazando datos Por la mejor Homografia hasta el momento >
        if (bandIteracionMejorQue_BH) {
            this->iter_BH = ii; // Numero de iteracion donde se encontro el mejor modelo 
            structMatch.structLineas.vectIRandomIndexes_lines = vectIndAleatorios_iter; // Indices aleatorios que sirvieron para formar el conjunto

            structMatch.structLineas.bandModeloFound_ip = bandHomografiaBien_iter; // Bandera de que se encontro Homografia
            structMatch.structLineas.scoreRANSAC_ip = score_iter; // Score del 
            structMatch.structLineas.numInliers_lines = this->vectNumInliersIteraciones[ii]; // Numero de inliers 

            structMatch.structLineas.Model_ip = HomografiaFromRandomData.clone(); // Homografia

            structMatch.structLineas.vectAngle_lines = vectAngulos_iter; // Error en angulo de las lineas
            structMatch.structLineas.vectDist_lines = vectDistancias_iter; // Error en distancia de las lineas
            structMatch.structLineas.vectIndexMatch_Inliers_lines = vectIndicesInliers_iter; // Indices considerados como Inliers 

            structMatch.structLineas.vectLinesMatch_tran = lineas_transformadas_iter; // Conjunto de lineas transformadas por el modelo

            structMatch.structLineas.vectLinesS_test = lineas_select_test; // Lineas seleccionadas aleatoriamente
            structMatch.structLineas.vectLinesS_refe = lineas_select_refe; // Lineas seleccionadas aleatoriamente
            structMatch.structLineas.vectLS_PI_test = pointsFromSelectLines_test; // Lineas seleccionadas
            structMatch.structLineas.vectLS_PI_refe = pointsFromSelectLines_refe; // Lineas seleccionadas
        } // if 
    }//for
    /*____________________________________________________________________________________________*/
    /*____________________________________________________________________________________________*/
    /*____________________________________________________________________________________________*/

    /* < Obteniendo los Inliers > */
    structMatch.structLineas.vectLinesMatch_Inliers_test = getSpecificLinesFromVector(structMatch.structLineas.vectLinesMatch_test, structMatch.structLineas.vectIndexMatch_Inliers_lines);
    structMatch.structLineas.vectLinesMatch_Inliers_refe = getSpecificLinesFromVector(structMatch.structLineas.vectLinesMatch_refe, structMatch.structLineas.vectIndexMatch_Inliers_lines);
    structMatch.structLineas.vectLinesMatch_Inliers_tran = getSpecificLinesFromVector(structMatch.structLineas.vectLinesMatch_tran, structMatch.structLineas.vectIndexMatch_Inliers_lines);

    /*____________________________________________________________________________________________*/
    if (structMatch.structLineas.bandModeloFound_ip != true) {
        //        printf("\n---------------------------------------------------------- \n");
        //        printf("\t!!No se obtuvo la homografia!! \n");
        //        printf("\n---------------------------------------------------------- \n");
        cout << " Error: No se encontro Modelo de transformacion utilizando Lineas.\n" << endl;
        structMatch.structLineas.numInliers_lines = 0;
    } else {
        /*____________________________________________________________________________________________*/
        /* < Mejorando Homografia  > */
        ransac_LineSegmets_improveModel(structMatch);
    }//else 
    /*+++++++++++++++++++++++++++++++++++++++*/
    return tc.finish(false, "");
    /*+++++++++++++++++++++++++++++++++++++++*/
} // function

void RANSAC::ransac_LineSegmets_improveModel(Struct_Matching& structMatch) {
    /* < Crea Homografia con los inliers y SI actualiza los indices de inliers > */
    // Ibteniendo puntos de Interseccion
    std::vector<int> vectIndexLineasInliers_UsedToImproveModel;

    /* < Transformando los puntos a Refe> */
    cv::perspectiveTransform(structMatch.structLineas.vectLS_PI_test,
            structMatch.structLineas.vectLS_PI_tran, structMatch.structLineas.Model_ip);

    std::vector<cv::Point2f> vectLS_PI_test_SiMejorada, vectLS_PI_test_NoMejorada; // Puntos de interseccion TEST
    std::vector<cv::Point2f> vectLS_PI_refe_SiMejorada, vectLS_PI_refe_NoMejorada; // Puntos de interseccion REFE
    std::vector<cv::Point2f> vectLS_PI_tran_SiMejorada, vectLS_PI_tran_NoMejorada; // Puntos de interseccion TRAN   

    /* <Vars Si-No Mejorada> */
    cv::Mat Homografia_mat_SiMejorada, Homografia_mat_NoMejorada;
    int numInliers_BH_SiMejorada, numInliers_BH_NoMejorada;
    std::vector<double> vectDist_BH_SiMejorada, vectDist_BH_NoMejorada;
    std::vector<double> vectAngle_BH_SiMejorada, vectAngle_BH_NoMejorada;
    std::vector<int> vecIndicesInliersMatch_BH_SiMejorada, vecIndicesInliersMatch_BH_NoMejorada;
    std::vector<cv::line_descriptor::KeyLine> keylines_tran_BH_SiMejorada, keylines_tran_BH_NoMejorada;

    if (this->parametros.RANSAC.bandRecalcularModelo) {
        /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
        // < Respaldando Datos Sin Mejora >
        Homografia_mat_NoMejorada = structMatch.structLineas.Model_ip.clone();
        numInliers_BH_NoMejorada = structMatch.structLineas.vectIndexMatch_Inliers_lines.size();
        vectDist_BH_NoMejorada = std::vector<double> (structMatch.structLineas.vectDist_lines.begin(), structMatch.structLineas.vectDist_lines.end());
        vectAngle_BH_NoMejorada = std::vector<double>(structMatch.structLineas.vectAngle_lines.begin(), structMatch.structLineas.vectAngle_lines.end());
        vecIndicesInliersMatch_BH_NoMejorada = std::vector<int>(structMatch.structLineas.vectIndexMatch_Inliers_lines.begin(), structMatch.structLineas.vectIndexMatch_Inliers_lines.end());
        keylines_tran_BH_NoMejorada = std::vector<cv::line_descriptor::KeyLine> (structMatch.structLineas.vectLinesMatch_tran.begin(), structMatch.structLineas.vectLinesMatch_tran.end());

        //  Puntos de interseccion validos
        int numPuntosIntersec = structMatch.structLineas.vectLineasSelect_IP_tran.size();
        cv::Point2f p_tran, p_refe, p_test;
        for (int pp = 0; pp < numPuntosIntersec; pp++) {
            // Pasando los puntos 
            p_tran = structMatch.structLineas.vectLS_PI_tran[pp];
            p_refe = structMatch.structLineas.vectLS_PI_refe[pp];
            p_test = structMatch.structLineas.vectLS_PI_test[pp];

            if (getDistanciaEntrePuntos(p_tran, p_refe) <= 3) {
                vectLS_PI_test_NoMejorada.push_back(p_test);
                vectLS_PI_tran_NoMejorada.push_back(p_tran);
                vectLS_PI_refe_NoMejorada.push_back(p_refe);
            } // if 
        } // for 

        /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
        Homografia_mat_SiMejorada = recalculateHomographyWithIntersectionPoints(
                this->parametros.RANSAC.idTypeModel4Ransac,
                structMatch.structLineas.vectLinesMatch_Inliers_test,
                structMatch.structLineas.vectLinesMatch_Inliers_refe,
                vectLS_PI_test_SiMejorada, vectLS_PI_refe_SiMejorada,
                structMatch.structLineas.vectLinesMatch_Inliers_tran,
                vectLS_PI_tran_SiMejorada,
                this->parametros.RANSAC.umbralDistancia, this->puntoCentroImg,
                this->radioTolerancia, this->parametros.RANSAC.numMinMuestras,
                vectIndexLineasInliers_UsedToImproveModel,
                structMatch.structLineas.vectDist_LS_PI);

        /* Se evaluara utilizando todas las lineas */
        std::vector<bool> vectBanderasConsiderarLineas = generaVectorTrue(structMatch.structLineas.vectMatches.size());

        numInliers_BH_SiMejorada = evaluaModelo_lineas(Homografia_mat_SiMejorada,
                this->parametros.RANSAC.tipoEvalDistancia, vectBanderasConsiderarLineas,
                structMatch.structLineas.vectLinesMatch_test,
                structMatch.structLineas.vectLinesMatch_refe,
                this->parametros.RANSAC.umbralDistancia, this->parametros.RANSAC.umbralAngulo,
                vectDist_BH_SiMejorada, vectAngle_BH_SiMejorada,
                vecIndicesInliersMatch_BH_SiMejorada,
                false, keylines_tran_BH_SiMejorada);
        /* ================================================= */
        /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
        // < Decidiendo que Homografia es mejor >
        //        cout << " ** Lineas Inliers: PSA (" << numInliers_BH_SiMejorada << ") - TPI (" << numInliers_BH_NoMejorada << ") **" << endl;
        //        //        cout << " ** PI Inliers: ¿Mejorada (" << vectLS_PI_test_Mejorada.size() << ") > No Mejorada (" << vectLS_PI_test_No_Mejorada.size() << ")? **" << endl;

        int score_Mejorado = numInliers_BH_SiMejorada;
        int score_No_Mejorado = numInliers_BH_NoMejorada;

        structMatch.structLineas.bandImprovedModelDone_ip = score_Mejorado >= score_No_Mejorado;
    } else {
        structMatch.structLineas.bandImprovedModelDone_ip = false;
    } // if - else
    /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
    /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
    // < Vericando si el nuevo Modelo es mejor que el anteior >
    if (structMatch.structLineas.bandImprovedModelDone_ip) {
        // <SI se mejoro El MODELO>
        structMatch.structLineas.numInliers_lines = vecIndicesInliersMatch_BH_SiMejorada.size();
        structMatch.structLineas.Model_ip = Homografia_mat_SiMejorada.clone();
        structMatch.structLineas.vectDist_lines = vectDist_BH_SiMejorada;
        structMatch.structLineas.vectAngle_lines = vectAngle_BH_SiMejorada;
        structMatch.structLineas.vectIndexMatch_Inliers_lines = vecIndicesInliersMatch_BH_SiMejorada;
        structMatch.structLineas.vectLinesMatch_tran = keylines_tran_BH_SiMejorada;

        /* < Obteniendo los Inliers > */
        structMatch.structLineas.vectLinesMatch_Inliers_test = getSpecificLinesFromVector(structMatch.structLineas.vectLinesMatch_test, structMatch.structLineas.vectIndexMatch_Inliers_lines);
        structMatch.structLineas.vectLinesMatch_Inliers_refe = getSpecificLinesFromVector(structMatch.structLineas.vectLinesMatch_refe, structMatch.structLineas.vectIndexMatch_Inliers_lines);
        structMatch.structLineas.vectLinesMatch_Inliers_tran = getSpecificLinesFromVector(structMatch.structLineas.vectLinesMatch_tran, structMatch.structLineas.vectIndexMatch_Inliers_lines);

        /* Pasando los puntos de interseccion*/
        structMatch.structLineas.vectLS_PI_test = vectLS_PI_test_SiMejorada;
        structMatch.structLineas.vectLS_PI_refe = vectLS_PI_refe_SiMejorada;
        structMatch.structLineas.vectLS_PI_tran = vectLS_PI_tran_SiMejorada;

        structMatch.structLineas.vectIndexLineasInliers_UsedToImproveModel = vectIndexLineasInliers_UsedToImproveModel;

        structMatch.structLineas.vectLineasSelect_IP_refe = getSpecificLinesFromVector(
                structMatch.structLineas.vectLinesMatch_Inliers_refe,
                structMatch.structLineas.vectIndexLineasInliers_UsedToImproveModel);
        structMatch.structLineas.vectLineasSelect_IP_test = getSpecificLinesFromVector(
                structMatch.structLineas.vectLinesMatch_Inliers_test,
                structMatch.structLineas.vectIndexLineasInliers_UsedToImproveModel);
        structMatch.structLineas.vectLineasSelect_IP_tran = getSpecificLinesFromVector(
                structMatch.structLineas.vectLinesMatch_Inliers_tran,
                structMatch.structLineas.vectIndexLineasInliers_UsedToImproveModel);

    } else {
        // < Inicialmente >            
        structMatch.structLineas.vectIndexLineasInliers_UsedToImproveModel = structMatch.structLineas.vectIRandomIndexes_lines;
        // Puntos de interseccion Test transformados al sistema de coordenadas REFE

        //        cout << "Homografia = \n" << structMatch.Homografia_mat << endl;
        //        cout << " #PI_test = " << structMatch.structLineas.vectLS_PI_test.size() << endl;
        //        cout << " #PI_tran = " << structMatch.structLineas.vectLS_PI_tran.size() << endl;


        structMatch.structLineas.vectLineasSelect_IP_refe = getSpecificLinesFromVector(
                structMatch.structLineas.vectLinesMatch_refe,
                structMatch.structLineas.vectIndexLineasInliers_UsedToImproveModel);
        structMatch.structLineas.vectLineasSelect_IP_test = getSpecificLinesFromVector(
                structMatch.structLineas.vectLinesMatch_test,
                structMatch.structLineas.vectIndexLineasInliers_UsedToImproveModel);
        perspectiveTransform_vectKeyLines(structMatch.structLineas.vectLineasSelect_IP_test,
                structMatch.structLineas.vectLineasSelect_IP_tran,
                structMatch.structLineas.Model_ip);

        /* < ESTOR REVISANDO > */
        /* Pasando los puntos de interseccion*/
        //        structMatch.structLineas.vectLS_PI_test = vectLS_PI_test_NoMejorada;
        //        structMatch.structLineas.vectLS_PI_refe = vectLS_PI_refe_NoMejorada;
        //        structMatch.structLineas.vectLS_PI_tran = vectLS_PI_tran_NoMejorada;

        structMatch.structLineas.vectLS_PI_test = vectLS_PI_test_SiMejorada;
        structMatch.structLineas.vectLS_PI_refe = vectLS_PI_refe_SiMejorada;
        structMatch.structLineas.vectLS_PI_tran = vectLS_PI_tran_SiMejorada;

    }// if - else        
}// function
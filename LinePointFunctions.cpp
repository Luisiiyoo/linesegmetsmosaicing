/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *  */
#include "LinePointFunctions.hpp"
#include "BasicFunctions.hpp"

#include <opencv2/line_descriptor.hpp>
#include "opencv2/core/utility.hpp"
#include <opencv2/imgproc.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/highgui.hpp>

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <complex>
#include <math.h>  
#include <sstream>

using namespace std;

std::pair<double, double > getPorcentajesModel(bool bandModelFound,
        cv::Mat& Model, double averageSizeImage,
        int numMatches, int numInliers) {
    /* < Obteniendo porcentajes > */
    std::pair<double, double > porcentajes; // Par de porcentajes 
    double porcentajeMatchModel;
    double porcentajeDesplModel;

    if (bandModelFound) {
        // Porcentaje correspondencia 
        porcentajeMatchModel = (double) ((numInliers * 100) / numMatches);
        // Porcentaje desplazamiento 
        double promedioDesplazamiento = (std::abs(Model.at<double>(0, 2)) + std::abs(Model.at<double>(1, 2))) / 2;
        porcentajeDesplModel = (promedioDesplazamiento * 100) / (averageSizeImage);

    } else {
        porcentajeMatchModel = 0;
        porcentajeDesplModel = 0;
    } // if - else

    // Creando par 
    porcentajes = std::make_pair<double, double>(porcentajeMatchModel, porcentajeDesplModel);
    return porcentajes;
} // function 

void perspectiveTransform_vectKeyLines(std::vector<cv::line_descriptor::KeyLine> vectKeylines_input,
        std::vector<cv::line_descriptor::KeyLine>& vectKeylines_output, cv::Mat& matTransformacion) {
    // < Lineas A Puntos>
    std::vector<cv::Point2f> vectPoints_IniFin = vectLineSegments2vectPoints(vectKeylines_input);

    // < Transformando Puntos >
    std::vector<cv::Point2f> vectPoints_IniFin_transformados;
    cv::perspectiveTransform(vectPoints_IniFin, vectPoints_IniFin_transformados, matTransformacion);

    // < Puntos A Lineas >
    vectKeylines_output = vectPoints2vectLineSegments(vectPoints_IniFin_transformados);
} // function 

cv::line_descriptor::KeyLine perspectiveTransform_KeyLine(cv::line_descriptor::KeyLine& linea, cv::Mat& matTransformacion) {
    std::vector<cv::Point2f> vectPunto_IniFin(2);
    vectPunto_IniFin[0] = linea.getStartPoint(); // Punto Inicial
    vectPunto_IniFin[1] = linea.getEndPoint(); // Punto Final

    std::vector<cv::Point2f> vectPunto_IniFin_transformados;
    cv::perspectiveTransform(vectPunto_IniFin, vectPunto_IniFin_transformados, matTransformacion);


    cv::line_descriptor::KeyLine linea_transformada;
    // Punto Inicial Transformado
    linea_transformada.startPointX = vectPunto_IniFin_transformados[0].x;
    linea_transformada.startPointY = vectPunto_IniFin_transformados[0].y;

    // Punto Final Transformado
    linea_transformada.endPointX = vectPunto_IniFin_transformados[1].x;
    linea_transformada.endPointY = vectPunto_IniFin_transformados[1].y;

    return linea_transformada;
}// function 

cv::Mat mat1D_To_mat2D(cv::Mat& mat1D, int nFil, int nCol) {
    cv::Mat mat2D(nFil, nCol, CV_64F);
    int f, c;
    for (int ind = 0; ind < nFil * nCol; ind++) {
        f = ind / nFil;
        c = ind % nFil;
        mat2D.at<double>(f, c) = mat1D.at<double>(ind);
    }//for
    return mat2D;
}//function

std::vector< std::vector< double > > transform_cvMat2stdVector(cv::Mat mat) {
    int numFil = mat.rows;
    int numCol = mat.cols;

    std::vector< std::vector< double > > vect(numFil, std::vector< double >(numCol));

    for (int ii = 0; ii < numFil; ii++) {
        for (int jj = 0; jj < numCol; jj++) {
            vect[ii][jj] = mat.at<double>(ii, jj);
        }//for
    }//for

    return vect;
}//function

cv::Mat transform_stdVector2cvMat(std::vector< std::vector< double > > vect) {
    int numFil = vect.size();
    int numCol = vect[0].size();

    const int mySizes[3] = {numFil, numCol};
    cv::Mat m(2, mySizes, cv::DataType<double>::type);

    for (int ii = 0; ii < numFil; ii++) {
        for (int jj = 0; jj < numCol; jj++) {

            m.at<double>(ii, jj) = vect[ii][jj];
        }//for
    }//for
    return m;
}//function

std::vector<std::vector<int> > getSpecific_STDVector2d_FromVector(std::vector<std::vector<int> >& vect2D, std::vector<int> vecIndices) {
    int numElementos = vecIndices.size();
    std::vector<std::vector<int> > vect2D_sub(numElementos);

    int indice;
    for (int ii = 0; ii < numElementos; ii++) {
        indice = vecIndices[ii];
        vect2D_sub[ii] = vect2D[indice];
    } // for   
    return vect2D_sub;
} // for 

std::vector<cv::Mat> getSpecific_CVMat_FromVector(std::vector<cv::Mat>& vectMat, std::vector<int> vecIndices) {
    int numElementos = vecIndices.size();
    std::vector<cv::Mat> vectMat_sub(numElementos);

    int indice;
    for (int ii = 0; ii < numElementos; ii++) {
        indice = vecIndices[ii];
        vectMat_sub[ii] = vectMat[indice].clone();
    } // for 
    return vectMat_sub;
} // for 

std::vector<cv::line_descriptor::KeyLine> getSpecificLinesFromVector(std::vector<cv::line_descriptor::KeyLine>& vectKeylines, std::vector<int> vecIndices) {
    int numElementos = vecIndices.size();
    std::vector<cv::line_descriptor::KeyLine> vectLineasElegidas(numElementos);

    int indice;
    for (int ii = 0; ii < numElementos; ii++) {
        indice = vecIndices[ii];
        vectLineasElegidas[ii] = vectKeylines[indice];
    } // for 

    return vectLineasElegidas;
} // for 

std::vector<cv::Point2f> getSpecificPointsFromVector(std::vector<cv::Point2f>& vectPuntos, std::vector<int> vecIndices) {
    int numElementos = vecIndices.size();
    std::vector<cv::Point2f> vectPuntosElegidos(numElementos);

    int indice;
    for (int ii = 0; ii < numElementos; ii++) {
        indice = vecIndices[ii];
        vectPuntosElegidos[ii] = vectPuntos[indice];
    } // for 

    return vectPuntosElegidos;
} // for 

double getDistanciaEvalucionInlier_Linea(int tipoEval, cv::line_descriptor::KeyLine& linea_tran, cv::line_descriptor::KeyLine& linea_refe) {
    double distancia;
    switch (tipoEval) {
        case 1:
        { /* Obteniendo punto medio de la linea y calculando la distancia entre los segmetos */
            /* Obteniendo punto central */
            cv::Point2f puntoMidL_trans = getPuntoMedioLinea(linea_tran);
            cv::Point2f puntoMidL_refer = getPuntoMedioLinea(linea_refe);

            distancia = getDistanciaEntrePuntos(puntoMidL_trans, puntoMidL_refer);
            break;
        } //case

        default:
        case 0:
        { /* Distancia Minima entre segementos de linea */
            distancia = getMinDistanciaEntreSegmentosLinea(linea_tran, linea_refe);
            break;
        } //case
    }// switch
}// Function

std::vector<cv::line_descriptor::KeyLine> getLinesTransformationsByModel(int tipoEvalDistancia,
        cv::Mat& H,
        std::vector<bool>vectorBanderasConsiderarLinea,
        std::vector<cv::line_descriptor::KeyLine>& lineas_test,
        std::vector<cv::line_descriptor::KeyLine>& lineas_refe,
        std::vector<double>& vectDistancias,
        std::vector<double>& vectAngulos,
        bool bandPrintSomething) {
    /* Incia algoritmo */
    int numLineas = lineas_test.size();
    vectDistancias = std::vector<double>(numLineas);
    vectAngulos = std::vector<double>(numLineas);

    /* Creando un clon de los puntos que seran transformados*/
    std::vector<cv::line_descriptor::KeyLine> lineas_tran(lineas_test.begin(), lineas_test.end());

    //    cv::Mat matPunto_Ini(3, 1, CV_64F);
    //    cv::Mat matPunto_Fin(3, 1, CV_64F);

    for (int ii = 0; ii < numLineas; ii++) {
        if (vectorBanderasConsiderarLinea[ii]) {
            //            /* Linea i-esimo de la Imagen 1 - consta de dos puntos (inicial y final)*/
            //            /* Rellenando el vector de Punto Inicial*/
            //            matPunto_Ini.at<double>(0, 0) = (double) lineas_test[ii].startPointX;
            //            matPunto_Ini.at<double>(1, 0) = (double) lineas_test[ii].startPointY;
            //            matPunto_Ini.at<double>(2, 0) = (double) 1;
            //
            //            matPunto_Ini = H * matPunto_Ini; // Aplicando la transformacion a la coordenada
            //            matPunto_Ini = matPunto_Ini / matPunto_Ini.at<double>(2, 0); // Normalizando (dividiendo entre Z))
            //
            //            lineas_tran[ii].startPointX = matPunto_Ini.at<double>(0, 0); // Asignando X transformada
            //            lineas_tran[ii].startPointY = matPunto_Ini.at<double>(1, 0); // Asignando Y transformada        
            //
            //            /* Rellenando el vector de Punto Final*/
            //            matPunto_Fin.at<double>(0, 0) = (double) lineas_test[ii].endPointX;
            //            matPunto_Fin.at<double>(1, 0) = (double) lineas_test[ii].endPointY;
            //            matPunto_Fin.at<double>(2, 0) = (double) 1;
            //
            //            matPunto_Fin = H * matPunto_Fin; // Aplicando la transformacion a la coordenada
            //            matPunto_Fin = matPunto_Fin / matPunto_Fin.at<double>(2, 0); // Normalizando (dividiendo entre Z))                     
            //
            //            lineas_tran[ii].endPointX = matPunto_Fin.at<double>(0, 0); // Asignando X transformada
            //            lineas_tran[ii].endPointY = matPunto_Fin.at<double>(1, 0); // Asignando Y transformada

            // <Transformando Linea>
            lineas_tran[ii] = perspectiveTransform_KeyLine(lineas_test[ii], H);


            /* Obteniendo la distancia de la transformacion */
            vectDistancias[ii] = getDistanciaEvalucionInlier_Linea(tipoEvalDistancia, lineas_tran[ii], lineas_refe[ii]);

            /* Obteniendo Angulo de la linea transformada con respecto a la referencia */
            vectAngulos[ii] = getAngleBetween2Lines(lineas_tran[ii], lineas_refe[ii], bandPrintSomething);

            if (bandPrintSomething) {
                //cout << " m1 = " << (lineas_tran[ii].endPointY - lineas_tran[ii].startPointY) / (lineas_tran[ii].endPointX - lineas_tran[ii].startPointX) << endl;
                cout << "lineas_test = [" << lineas_test[ii].startPointX << ", " << lineas_test[ii].startPointY << ";\n"
                        << lineas_test[ii].endPointX << ", " << lineas_test[ii].endPointY << "];\n";
                cout << "lineas_tran = [" << lineas_tran[ii].startPointX << ", " << lineas_tran[ii].startPointY << ";\n"
                        << lineas_tran[ii].endPointX << ", " << lineas_tran[ii].endPointY << "];\n";
                cout << "lineas_refe =[" << lineas_refe[ii].startPointX << ", " << lineas_refe[ii].startPointY << ";\n"
                        << lineas_refe[ii].endPointX << ", " << lineas_refe[ii].endPointY << "];\n";
                cout << "Distancia = " << vectDistancias[ii] << ";   Angulo = " << vectAngulos[ii] << ";" << endl;
                cout << endl;
            } // if
        } else {
            vectDistancias[ii] = std::numeric_limits<double>::infinity();
            vectAngulos[ii] = std::numeric_limits<double>::infinity();
            //vectDistancias[ii] = 9999999;
            //vectAngulos[ii] = 9999999;
            //cv::line_descriptor::KeyLine liniaNull;
            lineas_tran[ii] = cv::line_descriptor::KeyLine();
        } // If - else
    }//for    

    return lineas_tran;
} // function

std::vector<double> getPointsTransformationsByModel(cv::Mat& H,
        std::vector<bool>vectorBanderasConsiderarLinea,
        std::vector<cv::Point2f>& puntos_test,
        std::vector<cv::Point2f>& puntos_refe,
        std::vector<cv::Point2f>& puntos_tran,
        bool bandPrintSomething) {
    /* Incia algoritmo */
    int numPuntos = puntos_test.size();
    std::vector<double> vectDistancias(numPuntos);

    /* Creando un clon de los puntos que seran transformados*/
    puntos_tran = std::vector<cv::Point2f>(puntos_test.begin(), puntos_test.end());

    cv::Mat matPunto(3, 1, CV_64F);

    for (int ii = 0; ii < numPuntos; ii++) {
        if (vectorBanderasConsiderarLinea[ii]) {
            /* Rellenando la matriz de Punto */
            matPunto.at<double>(0, 0) = (double) puntos_test[ii].x;
            matPunto.at<double>(1, 0) = (double) puntos_test[ii].y;
            matPunto.at<double>(2, 0) = (double) 1;

            /* Transformando matriz de puntos */
            matPunto = H * matPunto; // Aplicando la transformacion a la coordenada
            matPunto = matPunto / matPunto.at<double>(2, 0); // Normalizando (dividiendo entre Z))

            puntos_tran[ii].x = matPunto.at<double>(0, 0); // Asignando X transformada
            puntos_tran[ii].y = matPunto.at<double>(1, 0); // Asignando Y transformada        

            /* Obteniendo la distancia de la transformacion */
            vectDistancias[ii] = getDistanciaEntrePuntos(puntos_tran[ii], puntos_refe[ii]);

            if (bandPrintSomething) {
                cout << "punto_test = [" << puntos_test[ii].x << ", " << puntos_test[ii].y << "];\n";
                cout << "punto_tran = [" << puntos_tran[ii].x << ", " << puntos_tran[ii].y << "];\n";
                cout << "punto_refe = [" << puntos_refe[ii].x << ", " << puntos_refe[ii].y << "];\n";

                cout << "Distancia = " << vectDistancias[ii] << endl;
                cout << endl;
            } // if
        } else {
            vectDistancias[ii] = 9999999;
            puntos_tran[ii] = cv::Point2f();
        } // If - else
    }//for    

    return vectDistancias;
} // function

void printPointsVector_varMatlab(std::vector<cv::Point2f>& puntos, std::string nomVar) {
    std::vector<std::vector<double> > matP = transformPoints2Mat(puntos);
    printVector2D_comoVariableMatlab(matP, nomVar);
} // function

void printKeylinesAsPointsVector_varMatlab(std::vector<cv::line_descriptor::KeyLine>& lineas, std::string nomVar) {
    std::vector<cv::Point2f> puntos = vectLineSegments2vectPoints(lineas);
    printPointsVector_varMatlab(puntos, nomVar);
}// function

std::vector<std::vector<double> > transformPoints2Mat(std::vector<cv::Point2f>& vectPoints) {
    int numElementos = vectPoints.size();
    std::vector<std::vector<double> > matPoints(numElementos, std::vector<double>(2));

    for (int ii = 0; ii < numElementos; ii++) {
        matPoints[ii][0] = (double) vectPoints[ii].x;
        matPoints[ii][1] = (double) vectPoints[ii].y;
    } //for 
    return matPoints;
}// for 

std::vector<cv::Point2f> vectLineSegments2vectPoints(std::vector<cv::line_descriptor::KeyLine>& vectKeylines) {
    int numLineas = vectKeylines.size();
    int numPuntos = numLineas * 2;
    std::vector<cv::Point2f> vectPoints(numPuntos);

    for (int ii = 0; ii < numLineas; ii++) {
        vectPoints[ii * 2] = vectKeylines[ii].getStartPoint();
        vectPoints[(ii * 2) + 1] = vectKeylines[ii].getEndPoint();
        //        vectPoints.push_back();
        //        vectPoints.push_back(vectKeylines[ii].getEndPoint());
    } //for
    return vectPoints;
}//function 

std::vector<cv::line_descriptor::KeyLine> vectPoints2vectLineSegments(std::vector<cv::Point2f>& vectPointsIniFin) {
    int numLineas = vectPointsIniFin.size() / 2;
    std::vector<cv::line_descriptor::KeyLine> vectKeylines(numLineas);

    cv::Point2f iniPoint, finPoint;
    int index;
    for (int ii = 0; ii < numLineas; ii++) {
        index = ii * 2;

        //        cout << "L" << ii << " -- P" << index << "  y  P" << index + 1 << endl;
        /* Punto Inicial */
        iniPoint = vectPointsIniFin[index];

        vectKeylines[ii].startPointX = iniPoint.x;
        vectKeylines[ii].startPointY = iniPoint.y;

        /* Punto Final */
        finPoint = vectPointsIniFin[index + 1];

        vectKeylines[ii].endPointX = finPoint.x;
        vectKeylines[ii].endPointY = finPoint.y;
    } //for
    return vectKeylines;
}//function 

//void getValidIntersectionPointsFromLinesTransformation(cv::Size sizeImg, double validRatio,
//        std::vector<cv::line_descriptor::KeyLine>& vectLines_test,
//        std::vector<cv::line_descriptor::KeyLine>& vectLines_refe,
//        std::vector<cv::line_descriptor::KeyLine>& vectLines_tran,
//        std::vector<cv::Point2f>& vectPuntIntersec_test,
//        std::vector<cv::Point2f>& vectPuntIntersec_refe,
//        std::vector<cv::Point2f>& vectPuntIntersec_tran,
//        bool bandCheck = true) {
//    /* <> */
//    vectPuntIntersec_test.clear(); // limpiando vector de puntos de interseccion 
//    vectPuntIntersec_refe.clear(); // limpiando vector de puntos de interseccion 
//    vectPuntIntersec_tran.clear(); // limpiando vector de puntos de interseccion 
//
//    cv::Point2f pCentralImg;
//    pCentralImg.x = sizeImg.width / 2;
//    pCentralImg.y = sizeImg.height / 2;
//
//    int numLines = vectLines_test.size();
//    cv::Point2f pIntersec_test, pIntersec_refe, pIntersec_tran;
//    double distPoints_test, distPoints_refe, distPoints_tran;
//
//    /* Obteniendo puntos de interseccion */
//    for (int ii = 0; ii < numLines - 1; ii++) {
//        for (int jj = ii + 1; jj < numLines; jj++) {
//            /* <Obteniendo punto de interseccion> */
//            pIntersec_test = getPuntoIntersecLineas(vectLines_test[ii], vectLines_test[jj]); // <Test>                            
//            pIntersec_refe = getPuntoIntersecLineas(vectLines_refe[ii], vectLines_refe[jj]); // <Refe>                            
//            pIntersec_tran = getPuntoIntersecLineas(vectLines_tran[ii], vectLines_tran[jj]); // <Tran>                
//
//            /* < ¿Revisar? > */
//            if (!bandCheck) {
//                vectPuntIntersec_test.push_back(pIntersec_test);
//                vectPuntIntersec_refe.push_back(pIntersec_refe);
//                vectPuntIntersec_tran.push_back(pIntersec_tran);
//                continue;
//            } // if 
//
//            /* < Revisando punto de interseccion > */
//            if (isAValidPoint(pIntersec_test) && isAValidPoint(pIntersec_refe) && isAValidPoint(pIntersec_tran)) {
//                // obteniendo distancia                
//                distPoints_test = getDistanciaEntrePuntos(pIntersec_test, pCentralImg); // <Test>                
//                distPoints_refe = getDistanciaEntrePuntos(pIntersec_refe, pCentralImg); // <Refe>
//                distPoints_tran = getDistanciaEntrePuntos(pIntersec_tran, pCentralImg); // <Tran>
//
//                //if ((distPoints_test <= validRatio) &&(distPoints_refe <= validRatio) &&(distPoints_tran <= validRatio)) {
//                if ((distPoints_refe <= validRatio) && (distPoints_tran <= validRatio)) {
//                    /* < Guardando punto de interseccion > */
//                    // <Line ii and Line jj have a valid intersection point>                     
//                    vectPuntIntersec_test.push_back(pIntersec_test); // <Test>                                    
//                    vectPuntIntersec_refe.push_back(pIntersec_refe); // <Refe>                                    
//                    vectPuntIntersec_tran.push_back(pIntersec_tran); // <Tran>                
//                } // if (distancia )
//
//            } // if !(NaN || Inf)
//
//        } // for jj        
//    } // for ii
//
//} // function 

bool isAValidPoint(cv::Point2f& punto) {
    if ((!isinf(punto.x)) && (!isnan(punto.x)) &&
            (!isinf(punto.y)) && (!isnan(punto.y))) {
        return true; // The point has numeric values 
    } else {
        return false; // The point hasn't numeric values 
    } // if - else 

} // function 

double getAngleBetween2Lines(cv::line_descriptor::KeyLine& linea_test, cv::line_descriptor::KeyLine& linea_refe, bool bandShowMess) {
    double angulo = 0;

    double m1, b1; // Pendiente y b linea 1
    double m2, b2; // Pendiente y b linea 2
    /* Parametros linea_test */
    getParametrosEcuacionRecta_Explicitos(m1, b1, linea_test);

    /* Parametros linea_refe */
    getParametrosEcuacionRecta_Explicitos(m2, b2, linea_refe);

    double numerador = m1 - m2; // toma en consideracion la linea_refe como la base

    /* Lineas que nunca se intersecctan -- <tienen la misma pendiente> */
    if (numerador == 0) {
        return std::numeric_limits<double>::infinity(); // < Misma Pendiente >
    }// if

    double denominador = 1 + (m1 * m2);

    double val = numerador / denominador;
    angulo = radians2degrees(std::atan(val));


    /* Haciendo el angulo positivo*/
    if (angulo < 0) {
        angulo = 180 + angulo;
    } // if

    // Obteniendo el minimo angulo entre las dos lineas 
    angulo = ((angulo < (180 - angulo)) ? angulo : (180 - angulo));

    if (bandShowMess) {
        cout << "m1_c = " << m1 << "; b1_c = " << b1 << ";" << endl;
        cout << "m2_c = " << m2 << "; b2_c = " << b2 << ";" << endl;
        cout << endl;
    } // if

    return angulo;
} // function 

cv::Point2f getPuntoIntersecLineas(cv::line_descriptor::KeyLine& Linea1,
        cv::line_descriptor::KeyLine& Linea2) {
    cv::Point2f p_intersec;
    double m1, b1; // Pendiente y b linea 1
    double m2, b2; // Pendiente y b linea 2

    /* Parametros primera linea*/
    getParametrosEcuacionRecta_Explicitos(m1, b1, Linea1);

    /* Parametros segunda linea*/
    getParametrosEcuacionRecta_Explicitos(m2, b2, Linea2);

    /* CALCULANDO Punto interseccion*/
    double aux;
    aux = (b2 - b1) / (m1 - m2);

    p_intersec.y = (m1 * aux) + b1;
    p_intersec.x = aux;

    /*
     if (bandShowMess) {
        cout << " Linea1.Ps = [" << Linea1.sPointInOctaveX << " , " << Linea1.sPointInOctaveY << " ]; Linea1.Pe = [ " << Linea1.ePointInOctaveX << " , " << Linea1.ePointInOctaveY << " ];" << endl;
        cout << " % m1=" << m1 << " b1=" << b1 << endl;
        cout << " Linea2.Ps = [" << Linea2.sPointInOctaveX << " , " << Linea2.sPointInOctaveY << " ]; Linea2.Pe = [ " << Linea2.ePointInOctaveX << " , " << Linea2.ePointInOctaveY << " ];" << endl;
        cout << " % m2=" << m2 << " b2=" << b2 << endl;
        cout << " % Punto interseccion: x=" << p_intersec.x << " y=" << p_intersec.y << endl << endl;
    }// if 
     */
    return p_intersec;
}// function

void getParametrosEcuacionRecta_Explicitos(double &pendiente, double &b, cv::line_descriptor::KeyLine & Linea) {
    // Punto iniacial
    double x1 = (double) Linea.startPointX;
    double y1 = (double) Linea.startPointY;
    // Punto final
    double x2 = (double) Linea.endPointX;
    double y2 = (double) Linea.endPointY;

    // Calculando la pendiente
    //pendiente = (y2 - y1) / (x2 - x1);
    double val = (x2 - x1);
    pendiente = (y2 - y1) / ((val == 0) ? (0.001) : (val)); // Por si la linea es Vertical

    // Encontrando b
    b = (-1 * (pendiente * x1)) + y1;

}// function    

void getParametrosEcuacionRecta_Implicitos(cv::line_descriptor::KeyLine& Linea, double &A, double &B, double &C) {
    double pendiente;
    double b;
    /* Obteniendo parametros Explicitos*/
    getParametrosEcuacionRecta_Explicitos(pendiente, b, Linea);
    /* Obteniendo parametros Implicitos*/
    A = pendiente * 1;
    B = -1;
    C = b;
}// function    

double getDistanciaEntrePuntoRecta(cv::Point2f& punto, cv::line_descriptor::KeyLine & Linea) {
    double dist = 0;
    /* Obteniendo parametros Implicitos*/
    double A, B, C;
    getParametrosEcuacionRecta_Implicitos(Linea, A, B, C);
    /* Obteniendo la distancia  */
    dist = std::abs(A * punto.x + B * punto.y + C);
    dist = dist / std::sqrt(std::pow(A, 2) + std::pow(B, 2));

    return dist;
} // function 

std::vector<double> getVectDistanciaEntrePuntosRecta(std::vector<cv::Point2f>& vecPuntos, cv::line_descriptor::KeyLine & Linea) {
    int numElem = vecPuntos.size();
    std::vector<double> vectDist(numElem);
    for (int i = 0; i < numElem; i++) {
        vectDist[i] = getDistanciaEntrePuntoRecta(vecPuntos[i], Linea);
    }//for 
    return vectDist;
}// function 

std::vector<cv::Point2f> muestreaNPuntosRespectoLineas(int numPuntos, cv::line_descriptor::KeyLine & Linea) {
    std::vector<cv::Point2f> vecPuntos(numPuntos);
    float pi_x = Linea.startPointX;
    float pi_y = Linea.startPointY;

    float pj_x = Linea.endPointX;
    float pj_y = Linea.endPointY;

    float pk_x;
    float pk_y;
    float aux = 0;
    for (int k = 1; k <= numPuntos; k++) {
        aux = (float) ((float) (k - 1) / (float) (numPuntos - 1));

        pk_x = pi_x + aux * (pj_x - pi_x);
        pk_y = pi_y + aux * (pj_y - pi_y);

        cv::Point2f punto;
        punto.x = pk_x;
        punto.y = pk_y;

        vecPuntos[k - 1] = punto;
    }//for 
    return vecPuntos;
}//function 

std::vector<cv::Scalar> generaVectorColorDuplicado(int numDuplicas, cv::Scalar color) {
    std::vector<cv::Scalar> vec_colores(numDuplicas);
    for (int i = 0; i < numDuplicas; i++) {
        vec_colores[i] = color;
    }// for 
    return vec_colores;
}// function 

void plotPuntoInterseccion(cv::Mat& imageInput, cv::Mat& imageOutput, std::vector<cv::Point2f>& puntosIntersec,
        std::vector< std::vector<int> >& matLineasIntersec) {
    int grosorLinea = 4;
    int grosorTexto = 2;
    int tamTexto = 1;
    int numPuntos = matLineasIntersec.size();

    /* draw lines extracted from octave 0 */

    cv::Mat output = imageInput.clone();

    // Convirtiendo en escala de grises
    if (output.channels() == 1) {
        cv::cvtColor(output, output, cv::COLOR_GRAY2BGR);
    } // if

    for (size_t i = 0; i < numPuntos; i++) {
        int B = (rand() % (int) (255 + 1));

        cv::Scalar colorAletorio(0, 0, 0);

        cv::Point2f punto = puntosIntersec[i];
        cout << "plotPuntoInteseccion=> Lineas " << matLineasIntersec[i][0] << " & " << matLineasIntersec[i][1] << " :: Punto[" << i << "] : x = " << punto.x << "   y = " << punto.y << endl;

        /* draw line */
        cv::line(output, punto, punto, colorAletorio, grosorLinea);

        /* show text */

        string label = int2string(matLineasIntersec[i][0]) + "&" + int2string(matLineasIntersec[i][1]);
        cv::putText(output, label, punto, cv::FONT_HERSHEY_PLAIN, tamTexto, colorAletorio, grosorTexto);
    }// for
    imageOutput = output;
}// function

void removeElementosDeVectorAleatorio(std::vector<int>& vecIndAleatoriosMatch, std::vector<cv::DMatch>& vecMatches) {
    int numIndices = vecIndAleatoriosMatch.size();
    //printVector(vecIndAleatoriosMatch); //Imprime el vector desordenado
    std::sort(vecIndAleatoriosMatch.begin(), vecIndAleatoriosMatch.end(), std::greater<int>()); // ordenando de forma descendiente
    cout << "Removiendo los siguientes indices (ordenados)  del vector de DMatch: " << endl;
    printVector_comoVariableMatlab(vecIndAleatoriosMatch, "vecIndAleatoriosMatch"); //Imprime el vector ordenado

    int currentIndex; // Variable auxiliar para obtener el indice del Dmatch
    for (int i = 0; i < numIndices; i++) {
        currentIndex = vecIndAleatoriosMatch[i]; // obtengo el indice aleatorio del vector vecIndAleatoriosMatch
        vecMatches.erase(vecMatches.begin() + currentIndex);
        //cout<<"tam = "<< vecMatches.size()<<endl;
    } // for 
}// function

cv::Point2f getPuntoMedioLinea(cv::line_descriptor::KeyLine & Linea) {
    cv::Point2f punto;
    punto.x = (Linea.startPointX + Linea.endPointX) / 2;
    punto.y = (Linea.startPointY + Linea.endPointY) / 2;
    return punto;
}//function 

double getDistanciaEntrePuntos(cv::Point2f& punto1, cv::Point2f & punto2) {
    double d = sqrt((pow((punto1.x - punto2.x), 2) + pow((punto1.y - punto2.y), 2)));
    //    cout<<"\n "<< d <<" = ((("<<punto1.x<<"-"<<punto2.x<<")^2) + (("<<punto1.y <<"-"<< punto2.y<<")^2)) ^(1/2)";
    return d;
}// function

std::vector<double> getDistanciaEntrePuntos(std::vector<cv::Point2f>& vectPuntos1, std::vector<cv::Point2f> & vectPuntos2) {
    int numElementos = vectPuntos1.size();
    std::vector<double> vectDist(numElementos);
    for (int ii = 0; ii < numElementos; ii++) {
        vectDist[ii] = getDistanciaEntrePuntos(vectPuntos1[ii], vectPuntos2[ii]);
    }// for 
    return vectDist;
}// function

std::vector<double> getVectDistanciaEntrePuntos(std::vector<cv::Point2f>& vectPuntos_test, std::vector<cv::Point2f>& vectPuntos_refe) {
    std::vector<double> vectDistancia(vectPuntos_test.size());

    cv::Point2f punto_test, punto_refe;
    for (int ii = 0; ii < vectPuntos_test.size(); ii++) {
        punto_test = vectPuntos_test[ii];
        punto_refe = vectPuntos_refe[ii];
        vectDistancia[ii] = getDistanciaEntrePuntos(punto_test, punto_refe);
    } // for 
    return vectDistancia;
} // function 

double getMinDistanciaEntreSegmentosLinea(cv::line_descriptor::KeyLine& linea_tran, cv::line_descriptor::KeyLine & linea_refe) {
    std::vector<double> S1_ps, S1_pe; /* <Linea 1> */
    S1_ps.push_back((double) linea_tran.startPointX);
    S1_ps.push_back((double) linea_tran.startPointY);

    S1_pe.push_back((double) linea_tran.endPointX);
    S1_pe.push_back((double) linea_tran.endPointY);

    //    printf("Linea1.Ps=[%.4g,%.4g]; Linea1.Pe=[%.4g,%.4g];\n", S1_ps[0], S1_ps[1], S1_pe[0], S1_pe[1]);

    std::vector<double> S2_ps, S2_pe; /* <Linea 2> */
    S2_ps.push_back((double) linea_refe.startPointX);
    S2_ps.push_back((double) linea_refe.startPointY);

    S2_pe.push_back((double) linea_refe.endPointX);
    S2_pe.push_back((double) linea_refe.endPointY);

    //    printf("Linea2.Ps=[%.4g,%.4g]; Linea2.Pe=[%.4g,%.4g];\n", S2_ps[0], S2_ps[1], S2_pe[0], S2_pe[1]);

    /*___________________________________________________*/
    /* Obteniendo vectores de direcciones */
    std::vector<double> u = calculateVectSubtraction(S1_pe, S1_ps);
    std::vector<double> v = calculateVectSubtraction(S2_pe, S2_ps);
    std::vector<double> w = calculateVectSubtraction(S1_ps, S2_ps);

    /* Variables auxiliares */
    double a = calculateVectDotProduct(u, u); // always >= 0
    double b = calculateVectDotProduct(u, v);
    double c = calculateVectDotProduct(v, v); // always >= 0
    double d = calculateVectDotProduct(u, w);
    double e = calculateVectDotProduct(v, w);

    /* Denominador ecuaciones */
    double D = (a * c) - (b * b); // always >= 0
    double sc, sN, sD = D; // sc = sN / sD, default sD = D >= 0
    double tc, tN, tD = D; // tc = tN / tD, default tD = D >= 0

    double SMALL_NUM = 0.00000001;

    // compute the line parameters of the two closest points
    if (D < SMALL_NUM) { // the lines are almost parallel
        sN = 0.0; // force using point P0 on segment S1
        sD = 1.0; // to prevent possible division by 0.0 later
        tN = e;
        tD = c;
    } else { // get the closest points on the infinite lines
        sN = (b * e - c * d);
        tN = (a * e - b * d);
        if (sN < 0.0) { // sc < 0 => the s=0 edge is visible
            sN = 0.0;
            tN = e;
            tD = c;
        } else if (sN > sD) { // sc > 1  => the s=1 edge is visible
            sN = sD;
            tN = e + b;
            tD = c;
        }// else-if
    }//if 
    /*______________________________________*/
    if (tN < 0.0) { // tc < 0 => the t=0 edge is visible
        tN = 0.0;
        // recompute sc for this edge
        if (-d < 0.0)
            sN = 0.0;
        else if (-d > a)
            sN = sD;
        else {
            sN = -d;
            sD = a;
        }
    } else if (tN > tD) { // tc > 1  => the t=1 edge is visible
        tN = tD;
        // recompute sc for this edge
        if ((-d + b) < 0.0)
            sN = 0;
        else if ((-d + b) > a)
            sN = sD;
        else {
            sN = (-d + b);
            sD = a;
        }//else
    } // else-if

    // finally do the division to get sc and tc
    sc = (std::abs(sN) < SMALL_NUM ? 0.0 : sN / sD);
    tc = (std::abs(tN) < SMALL_NUM ? 0.0 : tN / tD);

    // get the difference of the two closest points
    //Vector dP = w + (sc * u) - (tc * v); // =  S1(sc) - S2(tc)
    std::vector<double> dP = calculateVectSubtraction(calculateVectSum(w, multiplicateVectByVal(u, sc)), multiplicateVectByVal(v, tc));

    /*Vector W -- conecta a S1 y S2 con la menor distancia*/
    std::vector<double> vectW = dP;
    /* Puntos mas cercanos */
    std::vector<double> S1_pc = calculateVectSum(S1_ps, multiplicateVectByVal(u, sc)); // < Closest point on Segment 1 >
    std::vector<double> S2_pc = calculateVectSum(S2_ps, multiplicateVectByVal(v, tc)); // < Closest point on Segment 2 >

    return calculateNormVect(dP); // return the closest distance
}//

int string2int(std::string cadena) {
    int num;
    //    stringstream(cadena) >> num;
    //    
    std::stringstream s_str(cadena.c_str());

    s_str >> num;

    return num;
} // function

std::string int2string(int n) {
    std::stringstream ss;
    ss << n;
    return ss.str();
}//

std::string double2string(double n) {
    std::stringstream ss;
    ss << n;
    return ss.str();
}//

std::vector<std::string> vecNum2vecString(std::vector<int> vecN) {
    int numElem = vecN.size();
    std::vector<string> vecStr(numElem);
    for (int i = 0; i < numElem; i++) {
        vecStr[i] = int2string(vecN[i]);
    }//for
    return vecStr;
}//

void getVectKeyLineasInliners(std::vector<int>& vecIndicesValidos_BH,
        std::vector<cv::line_descriptor::KeyLine>& keylines_test_Match,
        std::vector<cv::line_descriptor::KeyLine>& keylines_refe_Match,
        std::vector<cv::line_descriptor::KeyLine>& keylines_test_Match_Inliners,
        std::vector<cv::line_descriptor::KeyLine>& keylines_refe_Match_Inliners) {
    /*Declarando Variables auxiliares*/
    int numInliners = vecIndicesValidos_BH.size();
    std::vector<cv::line_descriptor::KeyLine> kl_test(numInliners);
    std::vector<cv::line_descriptor::KeyLine> kl_refe(numInliners);
    int index;
    for (int ii = 0; ii < numInliners; ii++) {
        index = vecIndicesValidos_BH[ii];
        /* Obteniendo las lineas del vector de KeyLines*/
        kl_test[ii] = keylines_test_Match[index]; //Line inliner
        kl_refe[ii] = keylines_refe_Match[index]; //Line inliner
    }//for
    keylines_test_Match_Inliners = kl_test;
    keylines_refe_Match_Inliners = kl_refe;
}// function

void getVectPointsInliners(std::vector<int>& vecIndicesValidos_BH,
        std::vector<cv::Point2f>& puntos_test_Match,
        std::vector<cv::Point2f>& puntos_refe_Match,
        std::vector<cv::Point2f>& puntos_test_Match_Inliners,
        std::vector<cv::Point2f>& puntos_refe_Match_Inliners) {
    /*Declarando Variables auxiliares*/
    int numInliners = vecIndicesValidos_BH.size();
    std::vector<cv::Point2f> kl_test(numInliners);
    std::vector<cv::Point2f> kl_refe(numInliners);
    int index;
    for (int ii = 0; ii < numInliners; ii++) {
        index = vecIndicesValidos_BH[ii];
        /* Obteniendo las lineas del vector de KeyLines*/
        kl_test[ii] = puntos_test_Match[index]; //Line inliner
        kl_refe[ii] = puntos_refe_Match[index]; //Line inliner
    }//for
    puntos_test_Match_Inliners = kl_test;
    puntos_refe_Match_Inliners = kl_refe;
}// function

void plotLineasRectas(cv::Mat imageInput, cv::Mat& imageOutput,
        std::vector<cv::line_descriptor::KeyLine> lines,
        std::vector<cv::Scalar>& vec_colores,
        std::string extraTitulo, bool bandLabel, int factorLineaRecta) {
    /* Definiendo Propiedades de la linea y el texto que sera dibujada */
    int grosorLinea = 1;
    int grosorTexto = 2;
    int tamTexto = 1;
    int numLineas = lines.size();

    bool banAddTovecColores = false;
    if (vec_colores.size() != numLineas) {
        banAddTovecColores = true;
        vec_colores.clear();
    }//if

    /* draw lines extracted from octave 0 */

    cv::Mat output = imageInput.clone();

    // Convirtiendo en escala de grises
    if (output.channels() == 1) {
        cv::cvtColor(output, output, cv::COLOR_GRAY2BGR);
    } // if

    for (size_t ii = 0; ii < numLineas; ii++) {
        cv::line_descriptor::KeyLine kl = lines[ii];

        /* get a random color */
        int R = (rand() % (int) (255 + 1));
        int G = (rand() % (int) (255 + 1));
        int B = (rand() % (int) (255 + 1));

        cv::Scalar colorAletorio(B, G, R);
        if (banAddTovecColores) {
            vec_colores.push_back(colorAletorio); // añadiendo color al vector de Colores
        } else {
            colorAletorio = vec_colores[ii];
        } //else


        /* get extremes of line */
        //        double diferenceX = kl.startPointX -kl.endPointX;
        //        double diferenceY = kl.startPointY -kl.endPointY;
        //double x_Start=kl.startPointX  +factorLineaRecta*()
        //double y_Start=

        cv::Point2f Ps = kl.getStartPoint();
        cv::Point2f Pe = kl.getEndPoint();

        cv::Point2f LR_Pe;
        cv::Point2f LR_Ps;
        //        LR_Pe.x = float(Pe.x + (float(factorLineaRecta) * float(Ps.x - Pe.x)) );
        //        LR_Pe.y = float(Pe.y + (float(factorLineaRecta) * float(Ps.y - Pe.y)) );
        //
        //        LR_Ps.x = float(Ps.x + (float(factorLineaRecta) * float(Pe.x - Ps.x)) );
        //        LR_Ps.y = float(Ps.y + (float(factorLineaRecta) * float(Pe.y - Ps.y)) );

        LR_Pe = Pe + (factorLineaRecta * (Ps - Pe));
        LR_Ps = Ps + (factorLineaRecta * (Pe - Ps));

        /* draw line */
        cv::line(output, Ps, Pe, colorAletorio, 2);
        cv::line(output, LR_Ps, LR_Pe, colorAletorio, grosorLinea);

        //line(output, pt1, pt2, cv::Scalar(B, G, R), grosorLinea);

        /* show text */
        if (bandLabel) {
            std::string label = int2string(ii) + extraTitulo;
            cv::putText(output, label, Ps, cv::FONT_HERSHEY_PLAIN, tamTexto, colorAletorio, grosorTexto);
        }//if 
    }// for
    imageOutput = output.clone();
}// function

void plotLineas(cv::Mat imageInput, cv::Mat& imageOutput,
        std::vector<cv::line_descriptor::KeyLine> lines,
        std::vector<cv::Scalar>& vec_colores,
        std::string extraTitulo, bool bandShowLabel) {
    /* Definiendo Propiedades de la linea y el texto que sera dibujada */
    int grosorLinea = 4;
    int grosorTexto = 2;
    int tamTexto = 1;
    int numLineas = lines.size();

    bool banAddTovecColores = false;
    if (vec_colores.size() != numLineas) {
        banAddTovecColores = true;
        vec_colores.clear();
    }//if

    /* draw lines extracted from octave 0 */

    cv::Mat output = imageInput.clone();

    // Convirtiendo en escala de grises
    if (output.channels() == 1) {
        cv::cvtColor(output, output, cv::COLOR_GRAY2BGR);
    } // if

    for (size_t ii = 0; ii < numLineas; ii++) {
        cv::line_descriptor::KeyLine kl = lines[ii];

        /* get a random color */
        int R = (rand() % (int) (255 + 1));
        int G = (rand() % (int) (255 + 1));
        int B = (rand() % (int) (255 + 1));

        cv::Scalar colorAletorio(B, G, R);
        if (banAddTovecColores) {
            vec_colores.push_back(colorAletorio); // añadiendo color al vector de Colores
        } else {
            colorAletorio = vec_colores[ii];
        } //else


        /* get extremes of line */
        cv::Point2f ptStart = cv::Point2f(kl.startPointX, kl.startPointY);
        cv::Point2f ptEnd = cv::Point2f(kl.endPointX, kl.endPointY);

        /* draw line */
        cv::line(output, ptStart, ptEnd, colorAletorio, grosorLinea);

        //line(output, pt1, pt2, cv::Scalar(B, G, R), grosorLinea);

        /* show text */
        if (bandShowLabel) {
            std::string label = int2string(ii) + extraTitulo;
            cv::putText(output, label, kl.pt, cv::FONT_HERSHEY_PLAIN, tamTexto, colorAletorio, grosorTexto);
        } // if 
    }// for
    imageOutput = output.clone();
}// function

cv::Mat plotMatchesInliners_lineas(int idConcat, bool bandGrayScale, bool bandLineasSimilares,
        cv::Mat image1, std::vector<cv::line_descriptor::KeyLine>& keylines1_Match,
        cv::Mat image2, std::vector<cv::line_descriptor::KeyLine>& keylines2_Match,
        std::vector<cv::Scalar>& vec_colores_BH) {

    cv::Mat Img_Output;

    if (bandGrayScale == true) {
        cv::cvtColor(image1, image1, cv::COLOR_RGB2GRAY);
        cv::cvtColor(image2, image2, cv::COLOR_RGB2GRAY);
    }//
    cv::Mat mat_1_Lineas;
    plotLineas(image1, mat_1_Lineas, keylines1_Match, vec_colores_BH);
    //imshow(" Imagen 1 : " + num2string(keylines1_Match.size()) + " Líneas Inliners.", mat_1_Lineas);
    cv::Mat mat_2_Lineas;
    plotLineas(image2, mat_2_Lineas, keylines2_Match, vec_colores_BH);
    //imshow("Imagen 2  : " + num2string(keylines2_Match.size()) + " Líneas Inliners.", mat_2_Lineas);

    int nfil1 = image1.rows;
    int ncol1 = image1.cols;
    int nfil2 = image2.rows;
    int ncol2 = image2.cols;

    int aumentoF1 = 0, aumentoF2 = 0, aumentoC1 = 0, aumentoC2 = 0;
    switch (idConcat) {
        case 1: // Por Derecha
            Img_Output = concatenaMatriz_Horizontal(mat_1_Lineas, mat_2_Lineas);
            aumentoC2 = ncol1;
            break;
        case 2: // Por Izquierda
            Img_Output = concatenaMatriz_Horizontal(mat_2_Lineas, mat_1_Lineas);
            aumentoC1 = ncol2;
            break;
        case 3: // Por Arriba
            Img_Output = concatenaMatriz_Vertical(mat_1_Lineas, mat_2_Lineas);
            aumentoF2 = nfil1;
            break;
        case 4: // Por Abajo
            Img_Output = concatenaMatriz_Vertical(mat_2_Lineas, mat_1_Lineas);
            aumentoF1 = nfil2;
            break;
    }//switch

    if (bandLineasSimilares == true) {
        int numInliners = keylines1_Match.size();
        int grosorLinea = 1;
        for (int ii = 0; ii < numInliners; ii++) {
            /* get extremes of line */
            cv::Point2f pt1 = cv::Point2f(keylines1_Match[ii].startPointX + aumentoC1, keylines1_Match[ii].startPointY + aumentoF1);
            cv::Point2f pt2 = cv::Point2f(keylines2_Match[ii].startPointX + aumentoC2, keylines2_Match[ii].startPointY + aumentoF2);

            /* draw line */
            cv::line(Img_Output, pt1, pt2, vec_colores_BH[ii], grosorLinea);
        }//for
    }//if
    return Img_Output;
}// plotMatchesInliners

cv::Mat plotMatchesInliners_puntos(int idConcat, bool bandGrayScale, bool bandPuntosSimilares,
        cv::Mat image1, std::vector<cv::Point2f>& points1_Match,
        cv::Mat image2, std::vector<cv::Point2f>& points2_Match,
        std::vector<cv::Scalar>& vec_colores_BH, int sizePoint) {

    cv::Mat Img_Output;

    if (bandGrayScale == true) {
        cv::cvtColor(image1, image1, cv::COLOR_RGB2GRAY);
        cv::cvtColor(image2, image2, cv::COLOR_RGB2GRAY);
    }//

    std::vector<std::string> vecEtiquetas;

    cv::Mat mat_1_puntos;
    plotPuntos(image1, mat_1_puntos, points1_Match, vec_colores_BH, vecEtiquetas,
            "", false, sizePoint);
    //imshow(" Imagen 1 : " + num2string(keylines1_Match.size()) + " Líneas Inliners.", mat_1_Lineas);
    cv::Mat mat_2_puntos;
    plotPuntos(image2, mat_2_puntos, points2_Match, vec_colores_BH, vecEtiquetas,
            "", false, sizePoint);
    //imshow("Imagen 2  : " + num2string(keylines2_Match.size()) + " Líneas Inliners.", mat_2_Lineas);

    int nfil1 = image1.rows;
    int ncol1 = image1.cols;
    int nfil2 = image2.rows;
    int ncol2 = image2.cols;

    int aumentoF1 = 0, aumentoF2 = 0, aumentoC1 = 0, aumentoC2 = 0;
    switch (idConcat) {
        case 1: // Por Derecha
            Img_Output = concatenaMatriz_Horizontal(mat_1_puntos, mat_2_puntos);
            aumentoC2 = ncol1;
            break;
        case 2: // Por Izquierda
            Img_Output = concatenaMatriz_Horizontal(mat_2_puntos, mat_1_puntos);
            aumentoC1 = ncol2;
            break;
        case 3: // Por Arriba
            Img_Output = concatenaMatriz_Vertical(mat_1_puntos, mat_2_puntos);
            aumentoF2 = nfil1;
            break;
        case 4: // Por Abajo
            Img_Output = concatenaMatriz_Vertical(mat_2_puntos, mat_1_puntos);
            aumentoF1 = nfil2;
            break;
    }//switch

    if (bandPuntosSimilares == true) {
        int numInliners = points1_Match.size();
        int grosorLinea = 1;
        for (int ii = 0; ii < numInliners; ii++) {
            /* get extremes of line */
            cv::Point2f pt1 = cv::Point2f(points1_Match[ii].x + aumentoC1, points1_Match[ii].y + aumentoF1);
            cv::Point2f pt2 = cv::Point2f(points2_Match[ii].x + aumentoC2, points2_Match[ii].y + aumentoF2);

            /* draw line */
            cv::line(Img_Output, pt1, pt2, vec_colores_BH[ii], grosorLinea);
        }//for
    }//if
    return Img_Output;
}// plotMatchesInliners

cv::Mat concatenaMatriz_Horizontal(cv::Mat& image1, cv::Mat & image2) {
    cv::Mat H;
    cv::hconcat(image1, image2, H);

    return H;
}// function

cv::Mat concatenaMatriz_Vertical(cv::Mat& image1, cv::Mat & image2) {
    cv::Mat V;
    cv::vconcat(image1, image2, V);

    return V;
}// function

//std::vector<cv::Point2f> getVectorPuntos_Ini_Final_Lineas(std::vector<cv::line_descriptor::KeyLine> keyLines) {
//    int numLineas = keyLines.size();
//    int numPuntos = numLineas * 2;
//    std::vector<cv::Point2f> vecPuntos(numPuntos);
//
//    for (int ii = 0; ii < numLineas; ii++) {
//        vecPuntos[ii] = keyLines[ii].getStartPoint(); // la primera mitad son los startPoints
//        vecPuntos[ii + numLineas] = keyLines[ii].getEndPoint(); // la segunda mitad son los endPoints
//    }//for
//
//    return vecPuntos;
//}// function
//
//std::vector<cv::line_descriptor::KeyLine> getVectorLineas_Ini_Final_Puntos(std::vector<cv::Point2f> vecPuntos) {
//    int numPuntos = vecPuntos.size();
//    int numLineas = numPuntos / 2;
//    std::vector<cv::line_descriptor::KeyLine> keyLines(numLineas);
//
//    std::vector<cv::Point2f> vecPuntos_ini = std::vector<cv::Point2f>(vecPuntos.begin(), vecPuntos.begin() + numLineas);
//    std::vector<cv::Point2f> vecPuntos_fin = std::vector<cv::Point2f>(vecPuntos.begin() + numLineas, vecPuntos.end());
//
//
//    for (int ii = 0; ii < numLineas; ii++) {
//        //  index = ii * 2;
//        //cout<<"L_"<<cont<<" -- P_"<<ii<<" y P_"<<ii+1<<endl;
//
//        cv::line_descriptor::KeyLine linea;
//        linea.startPointX = vecPuntos_ini[ii].x;
//        linea.startPointY = vecPuntos_ini[ii].y;
//
//        linea.endPointX = vecPuntos_fin[ii].x;
//        linea.endPointY = vecPuntos_fin[ii].y;
//
//        keyLines[ii] = linea;
//    }//for
//
//    return keyLines;
//}// function

std::vector<cv::Point2f> getVectorPunto_Medio_LineasdioLineas(std::vector<cv::line_descriptor::KeyLine> keyLines) {
    int numLineas = keyLines.size();
    std::vector<cv::Point2f> vecPuntos(numLineas);
    for (int ii = 0; ii < numLineas; ii++) {

        cv::Point2f punto;
        punto.x = (keyLines[ii].startPointX + keyLines[ii].endPointX) / 2;
        punto.y = (keyLines[ii].startPointY + keyLines[ii].endPointY) / 2;
        vecPuntos[ii] = punto;
    }//for
    return vecPuntos;
}//function 

std::vector<cv::Point2f> getSubVect(int ini, int fin, std::vector<cv::Point2f>& vector) {
    int items = (fin - ini);
    std::vector<cv::Point2f> subvect;
    for (int i = ini; i < fin; i++) {

        subvect.push_back(vector[i]);
    } //for
    return subvect;
}//function

void plotPuntos(cv::Mat& imageInput, cv::Mat& imageOutput,
        std::vector<cv::KeyPoint>& vecKeypoints,
        std::vector<cv::Scalar>& vec_colores,
        std::vector<std::string>& vecEtiquetas,
        std::string extraTitulo,
        bool bandUseEtiqueta, int grosorLinea) {

    /* Obteniendo los puntos del vector de Keypoints*/
    std::vector<cv::Point2f> vecPoints = keypoints2Points2f(vecKeypoints);
    /* Plot Puntos */
    plotPuntos(imageInput, imageOutput, vecPoints, vec_colores, vecEtiquetas, extraTitulo, bandUseEtiqueta, grosorLinea);
}// function

std::vector<cv::Point2f> keypoints2Points2f(std::vector<cv::KeyPoint>& vecKeypoints) {
    std::vector<cv::Point2f> vecPoints(vecKeypoints.size());
    for (int ii = 0; ii < vecKeypoints.size(); ii++) {

        vecPoints[ii] = vecKeypoints[ii].pt;
    } //for 
    return vecPoints;
} //function

void plotPuntos(cv::Mat& imageInput, cv::Mat& imageOutput,
        std::vector<cv::Point2f>& puntos, std::vector<cv::Scalar>& vec_colores,
        std::vector<std::string>& vecEtiquetas, std::string extraTitulo,
        bool bandUseEtiqueta, int grosorLinea) {
    //int grosorLinea = 6;
    int grosorTexto = 2;
    int tamTexto = 1;
    int numElementos = puntos.size();

    if (vecEtiquetas.size() != numElementos) {
        vecEtiquetas = vecNum2vecString(generaVectorNumConsecutivos(0, numElementos));
    }//if

    bool banAddTovecColores = false;
    if (vec_colores.size() != numElementos) {
        banAddTovecColores = true;
        vec_colores.clear();
    }//if

    /* draw lines extracted from octave 0 */

    cv::Mat output = imageInput.clone();

    // Convirtiendo en escala de grises
    //    if (output.channels() != 1) {
    //        cv::cvtColor(output, output, cv::COLOR_GRAY2BGR);
    //    } // if

    for (size_t ii = 0; ii < numElementos; ii++) {
        cv::Point2f pnt = puntos[ii];

        /* get a random color */
        int R = (rand() % (int) (255 + 1));
        int G = (rand() % (int) (255 + 1));
        int B = (rand() % (int) (255 + 1));

        cv::Scalar colorAletorio(B, G, R);
        if (banAddTovecColores) {
            vec_colores.push_back(colorAletorio); // añadiendo color al vector de Colores
        } else {

            colorAletorio = vec_colores[ii];
        } //else


        /* get extremes of line */
        cv::Point2f pt1 = cv::Point2f(pnt.x, pnt.y);
        cv::Point2f pt2 = cv::Point2f(pnt.x, pnt.y);

        /* draw line */
        cv::line(output, pt1, pt2, colorAletorio, grosorLinea);

        //line(output, pt1, pt2, cv::Scalar(B, G, R), grosorLinea);

        /* show text */
        if (bandUseEtiqueta) {
            std::string label = vecEtiquetas[ii] + extraTitulo;
            cv::putText(output, label, pt1, cv::FONT_HERSHEY_PLAIN, tamTexto, colorAletorio, grosorTexto);
        }// if 
    }// for
    imageOutput = output;
}// function

void addTextoEnPuntoImagen(cv::Mat& mat, string label, cv::Point2f punto, cv::Scalar color, int tamTexto, int grosorTexto) {
    cv::putText(mat, label, punto, cv::FONT_HERSHEY_PLAIN, tamTexto, color, grosorTexto);
}// function

std::vector<double> calculateVectDistanciaEntrePuntos(bool bandImprimeDist, std::vector<cv::Point2f>& puntos1, std::vector<cv::Point2f>& puntos2) {
    int numPuntos = puntos1.size();
    std::vector<double> vecDistancias(numPuntos);
    if (bandImprimeDist) {
        cout << " ***** Imprimiendo Distancias entre los puntos *****" << endl;
    } //if

    for (int ii = 0; ii < numPuntos; ii++) {
        vecDistancias[ii] = sqrt((pow((puntos1[ii].x - puntos2[ii].x), 2) + pow((puntos1[ii].y - puntos2[ii].y), 2)));

        if (bandImprimeDist) {
            cout << "[" << ii << "] = " << vecDistancias[ii] << endl;
        }//if
    }//for
    cout << endl;

    return vecDistancias;
}//function

void getPuntosAleatoriosDelVectDMatch(int numLineas,
        std::vector<cv::DMatch>& matches,
        std::vector<cv::Point2f> puntos_test,
        std::vector<cv::Point2f> puntos_refe,
        std::vector<cv::Point2f>& puntosSelec_test,
        std::vector<cv::Point2f>& puntosSelec_refe,
        std::vector<int>& p_vecIndMatch_test,
        std::vector<int>& p_vecIndMatch_refe) {

    /* Inicia Funcion*/
    int numMatches = matches.size();
    /* Obteniendo el numero de puntos que se deben generar*/
    int numPuntosAleatorios = (numLineas);

    /* Generando vector de indices aleatorios de matches*/
    std::vector<int> vecIndAleatoriosMatch;
    vecIndAleatoriosMatch = generaVecNumAleatorios(0, numMatches, numPuntosAleatorios);

    //**************************************************
    getVectIndicesMatchesKeyPoints_specific(vecIndAleatoriosMatch,
            matches,
            puntos_test, puntos_refe,
            puntosSelec_test, puntosSelec_refe,
            p_vecIndMatch_test, p_vecIndMatch_refe);

    //**************************************************
}// function

void getVectIndicesMatchesKeyPoints(std::vector<cv::DMatch>& matches,
        std::vector<cv::Point2f>& puntos_test,
        std::vector<cv::Point2f>& puntos_refe,
        std::vector<cv::Point2f>& puntos_test_match,
        std::vector<cv::Point2f>& puntos_refe_match,
        std::vector<int>& indexPuntos_test_match,
        std::vector<int>& indexPuntos_refe_match) {
    /* Limpiando Vectores Donde se almacenan resultados */
    puntos_test_match.clear();
    puntos_refe_match.clear();
    indexPuntos_test_match.clear();
    indexPuntos_refe_match.clear();
    //**************************************************
    int numMatches = matches.size(); // obteniendo numero de correspondencias 
    for (int ii = 0; ii < numMatches; ii++) {

        cv::DMatch match = matches[ii];
        int indMatch_test = match.queryIdx;
        int indMatch_refe = match.trainIdx;

        /* Vectores de Lineas */
        puntos_test_match.push_back(puntos_test[indMatch_test]);
        puntos_refe_match.push_back(puntos_refe[indMatch_refe]);

        /* Modificando Vectores Indices*/
        indexPuntos_test_match.push_back(indMatch_test);
        indexPuntos_refe_match.push_back(indMatch_refe);
    }//for
    //**************************************************
} // function 

void getVectIndicesMatchesKeyPoints_specific(std::vector<int> vecIndEspecificos_Match,
        std::vector<cv::DMatch>& matches,
        std::vector<cv::Point2f>& puntos_test,
        std::vector<cv::Point2f>& puntos_refe,
        std::vector<cv::Point2f>& puntos_test_match,
        std::vector<cv::Point2f>& puntos_refe_match,
        std::vector<int>& indexPuntos_test_match,
        std::vector<int>& indexPuntos_refe_match) {
    /* Limpiando Vectores Donde se almacenan resultados */
    puntos_test_match.clear();
    puntos_refe_match.clear();
    indexPuntos_test_match.clear();
    indexPuntos_refe_match.clear();
    //**************************************************
    int numMatches = vecIndEspecificos_Match.size(); // obteniendo numero de correspondencias 
    for (int ii = 0; ii < numMatches; ii++) {

        cv::DMatch match = matches[vecIndEspecificos_Match[ii]];
        int indMatch_test = match.queryIdx;
        int indMatch_refe = match.trainIdx;

        /* Vectores de Lineas */
        puntos_test_match.push_back(puntos_test[indMatch_test]);
        puntos_refe_match.push_back(puntos_refe[indMatch_refe]);

        /* Modificando Vectores Indices*/
        indexPuntos_test_match.push_back(indMatch_test);
        indexPuntos_refe_match.push_back(indMatch_refe);
    }//for
    //**************************************************
} // function 

std::vector<std::vector<double> > CV_Mat2Std_Vector(cv::Mat & mat) {
    int fils = mat.rows;
    int cols = mat.cols;

    std::vector<std::vector<double> > vector(fils, std::vector<double>(cols));

    for (int ii = 0; ii < fils; ii++) {
        for (int jj = 0; jj < cols; jj++) {

            vector[ii][jj] = mat.at<double>(ii, jj);
        }//for
    }//for
    return vector;
} // function

void printDMatch(std::vector<cv::DMatch>& matches) {
    int numElementos = matches.size();

    cout << " \n Numero de Matches = " << numElementos << endl;
    for (int ii = 0; ii < numElementos; ii++) {

        cv::DMatch match = matches[ii];

        cout << "match[" << ii << "]: queryId = " << match.queryIdx << "  trainId = " << match.trainIdx << "  distancia = " << match.distance << endl;

    } // for
    cout << endl;
} //

void printVectLineas(std::vector<cv::line_descriptor::KeyLine>& vectLineas) {
    int numLineas = vectLineas.size();
    cout << "\n " << numLineas << " Lineas: " << endl;
    for (int ii = 0; ii < numLineas; ii++) {

        cout << " " << ii << " : sX = " << vectLineas[ii].startPointX
                << " sY = " << vectLineas[ii].startPointY << " ; eX = "
                << vectLineas[ii].endPointX << " eY = "
                << vectLineas[ii].endPointY << endl;
    } // for 
    cout << "\n";
} // function 

void printCVMatriz_comoVariableMatlab(cv::Mat mat, std::string nombre) {
    int numFil = mat.rows;
    int numCol = mat.cols;

    //    const int mySizes[3] = {numFil, numCol};
    //    cv::Mat m(2, mySizes, cv::DataType<double>::type);
    //
    //    for (int ii = 0; ii < numFil; ii++) {
    //        for (int jj = 0; jj < numCol; jj++) {
    //
    //            m.at<double>(ii, jj) = vect[ii][jj];
    //        }//for
    //    }//for
    //    return m;
    /*----------------------------------*/
    cout << nombre << " = [";
    for (int f = 0; f < numFil; f++) {
        for (int c = 0; c < numCol; c++) {
            cout << mat.at<double>(f, c);
            if (c < numCol - 1) {
                cout << ", ";
            }//if
        }//for c
        if (f < numFil - 1) {

            cout << ";\n";
        }

    }//for f
    cout << "];" << endl;
}//function

void writeCVMatriz_comoVariableMatlab(cv::Mat mat, std::string nombre, std::ofstream& outfile) {
    int numFil = mat.rows;
    int numCol = mat.cols;
    outfile << nombre << " = [";
    for (int f = 0; f < numFil; f++) {
        for (int c = 0; c < numCol; c++) {
            outfile << mat.at<double>(f, c);
            if (c < numCol - 1) {
                outfile << ", ";
            }//if
        }//for c
        if (f < numFil - 1) {

            outfile << ";\n";
        }

    }//for f
    outfile << "];" << endl;
}//function

void writeVectorCVMatriz_comoVariableMatlab(std::vector<cv::Mat> vectMat, std::string nombre, std::ofstream& outfile) {
    int numM = vectMat.size();
    int numFil;
    int numCol;

    int index;
    for (int ii = 0; ii < numM; ii++) {
        index = ii + 1;
        numFil = vectMat[ii].rows;
        numCol = vectMat[ii].cols;

        outfile << nombre << "{" << index << "}" << " = [";
        for (int f = 0; f < numFil; f++) {
            for (int c = 0; c < numCol; c++) {
                outfile << vectMat[ii].at<double>(f, c);
                if (c < numCol - 1) {
                    outfile << ", ";
                }//if
            }//for c
            if (f < numFil - 1) {

                outfile << ";\n";
            }// if

        }//for f
        outfile << "];" << endl;
    } // for 


}//function

std::vector<cv::Scalar> generateVectorSpecificColor(int numElementos, int R, int G, int B) {
    cv::Scalar color(B, G, R);
    std::vector<cv::Scalar> vectColores(numElementos);

    for (int ii = 0; ii < numElementos; ii++) {

        vectColores[ii] = color;
    } // for 

    return vectColores;
} // function 

void modificaResImg_EnBaseACols(bool bandChangSize, cv::Mat& imgInput, cv::Mat& imgOutput, int widthWanted) {
    if ((bandChangSize == 1) && (imgInput.cols > widthWanted)) {
        double porc = double(widthWanted) / double(imgInput.cols);
        int n_cols = porc * imgInput.cols;
        int n_fils = porc * imgInput.rows;
        modificaResolucionImg(imgInput, imgOutput, n_cols, n_fils);

    } else {

        imgOutput = imgInput.clone();
    } //else-if
}//function

void modificaResImg_EnBaseAFils(bool bandChangSize, cv::Mat& imgInput, cv::Mat& imgOutput, int heightWanted) {
    if ((bandChangSize == 1) && (imgInput.rows > heightWanted)) {
        double porc = double(heightWanted) / double(imgInput.rows);
        int n_cols = porc * imgInput.cols;
        int n_fils = porc * imgInput.rows;
        modificaResolucionImg(imgInput, imgOutput, n_cols, n_fils);

    } else {

        imgOutput = imgInput.clone();
    } //else-if
}//function

void modificaResolucionImg(cv::Mat& r_ImagenOrigen, cv::Mat& r_ImagenDestino, int width, int height) {

    cv::Size size(width, height); //the dst image size,e.g.100x100
    cv::resize(r_ImagenOrigen, r_ImagenDestino, size); //resize image
} //reduceResolucionImg

void extraePointsOfKeypoints(
        std::vector<cv::KeyPoint>& keypoints_test, std::vector<cv::KeyPoint>& keypoints_refe,
        std::vector<cv::Point2f>& puntos_test, std::vector<cv::Point2f>& puntos_refe) {
    /* Limpiando el vector*/
    int numElementos = keypoints_refe.size();

    puntos_test = std::vector<cv::Point2f>(numElementos);
    puntos_refe = std::vector<cv::Point2f>(numElementos);

    for (int ii = 0; ii < numElementos; ii++) {
        puntos_test[ii] = keypoints_test[ii].pt;
        puntos_refe[ii] = keypoints_refe[ii].pt;
    } //for
}//function

bool isThePointInTheRange(cv::Point2f & point, cv::Size & sizeImg) {
    bool bandX = (point.x >= 0) && (point.x <= sizeImg.width);
    bool bandY = (point.y >= 0) && (point.y <= sizeImg.height);
    return (bandX && bandY);
} // function 

std::vector<cv::Point2f> mergePointVectors(std::vector<cv::Point2f>& vectPoints_A,
        std::vector<cv::Point2f>& vectPoints_B) {
    // Creating a new vector with the values of the firs input vector
    std::vector<cv::Point2f> vectPointsMerged(vectPoints_A.begin(),
            vectPoints_A.end());

    // Adding the elments of the second input vector to the output vector
    vectPointsMerged.insert(vectPointsMerged.end(),
            vectPoints_B.begin(), vectPoints_B.end());

    return vectPointsMerged;
} // Function

std::vector<std::pair<int, int> > generaCorrespondenciaPuntosInterseccionLineas_dentroImagen(
        std::vector<cv::line_descriptor::KeyLine>& vectLines_test,
        std::vector<cv::line_descriptor::KeyLine>& vectLines_refe,
        std::vector<cv::Point2f>& vectPuntIntersec_test,
        std::vector<cv::Point2f>& vectPuntIntersec_refe,
        std::vector<cv::line_descriptor::KeyLine>& vectLines_tran,
        std::vector<cv::Point2f>& vectPuntIntersec_tran,
        cv::Size sizeImg) {
    // < Vector Relaciones >
    std::vector<std::pair<int, int> >vectRealcionLineasPI;
    /**/
    vectPuntIntersec_test.clear(); // limpiando vector de puntos de interseccion 
    vectPuntIntersec_refe.clear(); // limpiando vector de puntos de interseccion 
    vectPuntIntersec_tran.clear(); // limpiando vector de puntos de interseccion 

    int numLines = vectLines_test.size(); // Obteniendo el numero de lineas 
    cv::Point2f pIntersec_test, pIntersec_refe, pIntersec_tran;

    bool bandGoodPI_test;
    bool bandGoodPI_refe;
    bool bandGoodPI_tran;
    for (int ii = 0; ii < numLines - 1; ii++) {
        for (int jj = ii + 1; jj < numLines; jj++) {
            pIntersec_tran = getPuntoIntersecLineas(vectLines_tran[ii], vectLines_tran[jj]); // <Tran>
            pIntersec_test = getPuntoIntersecLineas(vectLines_test[ii], vectLines_test[jj]); // <Test>
            pIntersec_refe = getPuntoIntersecLineas(vectLines_refe[ii], vectLines_refe[jj]); // <Refe>          

            bandGoodPI_tran = (isThePointInTheRange(pIntersec_tran, sizeImg) && (getDistanciaEntrePuntos(pIntersec_tran, pIntersec_refe) <= 3));

            // Comprobando si son puntos de interseccion validos  
            bandGoodPI_test = isAValidPoint(pIntersec_test) && (isThePointInTheRange(pIntersec_test, sizeImg));
            bandGoodPI_refe = isAValidPoint(pIntersec_refe) && (isThePointInTheRange(pIntersec_refe, sizeImg));

            if (bandGoodPI_tran && bandGoodPI_test && bandGoodPI_refe) {
                // Agregando punto de interseccion
                vectRealcionLineasPI.push_back(std::make_pair(ii, jj)); // haciendo par 

                vectPuntIntersec_test.push_back(pIntersec_test); // <Test>                                    
                vectPuntIntersec_refe.push_back(pIntersec_refe); // <Refe>                                    
                vectPuntIntersec_tran.push_back(pIntersec_tran); // <Tran>                                    
            }// if 
        } // for       
    } // for       
    return vectRealcionLineasPI;
} //function

void generaCorrespondenciaPuntosInterseccionLineas(std::vector<cv::line_descriptor::KeyLine>& vectLines_test,
        std::vector<cv::line_descriptor::KeyLine>& vectLines_refe,
        std::vector<cv::Point2f>& vectPuntIntersec_test,
        std::vector<cv::Point2f>& vectPuntIntersec_refe,
        cv::Point2f puntoCentroImg, double radioTolerancia) {
    /**/
    vectPuntIntersec_test.clear(); // limpiando vector de puntos de interseccion 
    vectPuntIntersec_refe.clear(); // limpiando vector de puntos de interseccion 

    int numLines = vectLines_test.size(); // Obteniendo el numero de lineas 
    cv::Point2f pIntersec_test, pIntersec_refe;

    bool bandGoodPI_test;
    bool bandGoodPI_refe;
    for (int ii = 0; ii < numLines - 1; ii++) {
        for (int jj = ii + 1; jj < numLines; jj++) {
            pIntersec_test = getPuntoIntersecLineas(vectLines_test[ii], vectLines_test[jj]); // <Test>
            pIntersec_refe = getPuntoIntersecLineas(vectLines_refe[ii], vectLines_refe[jj]); // <Refe>          

            // Comprobando si son puntos de interseccion validos  
            bandGoodPI_test = isAValidPoint(pIntersec_test) && (getDistanciaEntrePuntos(pIntersec_test, puntoCentroImg) < radioTolerancia);
            bandGoodPI_refe = isAValidPoint(pIntersec_refe) && (getDistanciaEntrePuntos(pIntersec_refe, puntoCentroImg) < radioTolerancia);

            if (bandGoodPI_test && bandGoodPI_refe) {
                // Agregando punto de interseccion

                vectPuntIntersec_test.push_back(pIntersec_test); // <Test>                                    
                vectPuntIntersec_refe.push_back(pIntersec_refe); // <Refe>                                    
            }// if 
        } // for       
    } // for       
} //function

std::vector<std::pair<int, int> > generaCorrespondenciaPuntosInterseccionLineas_PI_umbralError(
        std::vector<cv::line_descriptor::KeyLine>& vectLines_test,
        std::vector<cv::line_descriptor::KeyLine>& vectLines_refe,
        std::vector<cv::Point2f>& vectPuntIntersec_test,
        std::vector<cv::Point2f>& vectPuntIntersec_refe,
        std::vector<cv::line_descriptor::KeyLine>& vectLines_tran,
        std::vector<cv::Point2f>& vectPuntIntersec_tran,
        double umbralErrorDist, cv::Point2f puntoCentroImg, double radioTolerancia,
        std::vector<double>& vectDistError) {
    /**/
    vectDistError.clear();
    double factLocal = 1.5;

    vectPuntIntersec_test.clear();
    vectPuntIntersec_tran.clear(); // limpiando vector de puntos de interseccion 
    vectPuntIntersec_refe.clear(); // limpiando vector de puntos de interseccion 

    int numLines = vectLines_tran.size(); // Obteniendo el numero de lineas 
    cv::Point2f pi_tran, pi_refe, pt_test;

    double radioToleranciaPuntos = radioTolerancia * factLocal;
    // < Vector Relaciones >
    std::vector<std::pair<int, int> >vectRealcionLineasPI;

    double dist_aux;
    for (int ii = 0; ii < numLines - 1; ii++) {
        for (int jj = ii + 1; jj < numLines; jj++) {
            // < Obteniendo el PI de las lineas -Tran- >
            pi_tran = getPuntoIntersecLineas(vectLines_tran[ii], vectLines_tran[jj]); // < Tran >

            // <Comprobando si es un punto valido y si esta en el limite establecido >
            if (isAValidPoint(pi_tran) && (getDistanciaEntrePuntos(pi_tran, puntoCentroImg) < radioToleranciaPuntos)) {
                // < Obteniendo el PI de las lineas -Refe- >
                pi_refe = getPuntoIntersecLineas(vectLines_refe[ii], vectLines_refe[jj]); // < Refe >

                // <Comprobando si es un punto valido y si la distancia entre el punto Refe y Tran es menor a un umbral >
                dist_aux = getDistanciaEntrePuntos(pi_refe, pi_tran);
                if (isAValidPoint(pi_refe) && (dist_aux < umbralErrorDist)) {
                    // < Es un punto de interseccion valido y tiene su correspondencia en el conjunto TRAN(Test) y REFE >                    
                    // Guardando distancia 
                    vectDistError.push_back(dist_aux);
                    
                    // Obteniendo punto de interseccion <TEST>
                    pt_test = getPuntoIntersecLineas(vectLines_test[ii], vectLines_test[jj]); // < Test >

                    // Guardando puntos interseccion
                    vectPuntIntersec_test.push_back(pt_test);
                    vectPuntIntersec_refe.push_back(pi_refe);
                    vectPuntIntersec_tran.push_back(pi_tran);

                    // relacion de lineas
                    vectRealcionLineasPI.push_back(std::make_pair(ii, jj)); // haciendo par 
                } // if                 
            } // if 
            // -----------
        } // for 
    } // for

    return vectRealcionLineasPI;
} //function

double getRadioTolerancia(cv::Size imgSize, double factorRadioTolerancia, cv::Point2f& puntoCentroImg) {
    // < Obteniendo centro de la imagen >    
    puntoCentroImg.x = imgSize.width / 2;
    puntoCentroImg.y = imgSize.height / 2;

    double array[] = {imgSize.width, imgSize.height};
    std::vector<double> vectSize = std::vector<double>(array, array + sizeof (array) / sizeof (array[0]));
    double radioTolerancia = (calculateNormVect(vectSize) / 2) * factorRadioTolerancia;

    return radioTolerancia;
} // function 

cv::Mat plotResults_lineas(cv::Mat image_refe, cv::Mat image_test,
        int idFrame_refe, int idFrame_test,
        std::vector<cv::line_descriptor::KeyLine>& keylines_refe_Detected,
        std::vector<cv::line_descriptor::KeyLine>& keylines_test_Detected,
        std::vector<cv::line_descriptor::KeyLine>& keylines_refe_Match,
        std::vector<cv::line_descriptor::KeyLine>& keylines_test_Match,
        std::vector<cv::line_descriptor::KeyLine>& keylines_refe_Inlier,
        std::vector<cv::line_descriptor::KeyLine>& keylines_test_Inlier,
        bool bandLineasSimilares) {

    cv::Mat Img_Output;
    std::vector<cv::Scalar> vectColoresDetected_refe, vectColoresDetected_test, vectColoresMatch, vectColoresInlier;
    cv::Mat img_refe;
    cv::Mat img_test;

    if (image_test.dims > 2) {
        cv::cvtColor(image_refe, img_refe, cv::COLOR_RGB2GRAY);
        cv::cvtColor(image_test, img_test, cv::COLOR_RGB2GRAY);
    } else {
        img_refe = image_refe.clone();
        img_test = image_test.clone();
    } // if - else

    /* <COLORES > */
    int arrColor_Deteccion[] = {
        255, 255, 0
    };
    int arrColor_Match[] = {
        255, 0, 0
    };
    int arrColor_Inlier[] = {
        0, 255, 0
    };
    /*************************************************/
    // < Deteccion >    
    vectColoresDetected_refe = generateVectorSpecificColor(keylines_refe_Detected.size(),
            arrColor_Deteccion[0], arrColor_Deteccion[1],
            arrColor_Deteccion[2]);
    vectColoresDetected_test = generateVectorSpecificColor(keylines_test_Detected.size(),
            arrColor_Deteccion[0], arrColor_Deteccion[1],
            arrColor_Deteccion[2]);

    plotLineas(img_refe, img_refe, keylines_refe_Detected, vectColoresDetected_refe, "", false);
    plotLineas(img_test, img_test, keylines_test_Detected, vectColoresDetected_test, "", false);
    /*************************************************/
    // < Matching >    
    vectColoresMatch = generateVectorSpecificColor(keylines_refe_Match.size(),
            arrColor_Match[0], arrColor_Match[1],
            arrColor_Match[2]);

    plotLineas(img_refe, img_refe, keylines_refe_Match, vectColoresMatch, "", false);
    plotLineas(img_test, img_test, keylines_test_Match, vectColoresMatch, "", false);
    /*************************************************/
    // < RANSAC >    
    vectColoresInlier = generateVectorSpecificColor(keylines_refe_Inlier.size(),
            arrColor_Inlier[0], arrColor_Inlier[1],
            arrColor_Inlier[2]);

    plotLineas(img_refe, img_refe, keylines_refe_Inlier, vectColoresInlier);
    plotLineas(img_test, img_test, keylines_test_Inlier, vectColoresInlier);
    /*************************************************/

    // < Dezplazamiento > 
    int colsSpace = 25;
    int aumentoF_refe = 0, aumentoF_test = 0, aumentoC_refe = 0, aumentoC_test = image_refe.cols + colsSpace;
    //cv::Mat m_space = cv::Mat(img_refe.rows,colsSpace, CV_64F, double(0));
    cv::Mat m_space = cv::Mat(img_refe.rows, colsSpace, img_refe.type(), cv::Scalar(255, 255, 255));


    Img_Output = concatenaMatriz_Horizontal(img_refe, m_space);
    Img_Output = concatenaMatriz_Horizontal(Img_Output, img_test);

    if (bandLineasSimilares) {
        int grosorLinea = 1;
        // <Match>        
        for (int ii = 0; ii < vectColoresMatch.size(); ii++) {
            /* get extremes of line */
            cv::Point2f pt1 = cv::Point2f(keylines_refe_Match[ii].startPointX + aumentoC_refe, keylines_refe_Match[ii].startPointY + aumentoF_refe);
            cv::Point2f pt2 = cv::Point2f(keylines_test_Match[ii].startPointX + aumentoC_test, keylines_test_Match[ii].startPointY + aumentoF_test);

            /* draw line */
            cv::line(Img_Output, pt1, pt2, vectColoresMatch[ii], grosorLinea);
        }//for
        // <RANSAC>
        for (int ii = 0; ii < vectColoresInlier.size(); ii++) {
            /* get extremes of line */
            cv::Point2f pt1 = cv::Point2f(keylines_refe_Inlier[ii].startPointX + aumentoC_refe, keylines_refe_Inlier[ii].startPointY + aumentoF_refe);
            cv::Point2f pt2 = cv::Point2f(keylines_test_Inlier[ii].startPointX + aumentoC_test, keylines_test_Inlier[ii].startPointY + aumentoF_test);

            /* draw line */
            cv::line(Img_Output, pt1, pt2, vectColoresInlier[ii], grosorLinea);
        }//for
    }//if 

    /* Añadiendo etiquetas a las imagenes */
    std::string nDetected_refe = int2string(keylines_refe_Detected.size());
    std::string nDetected_test = int2string(keylines_test_Detected.size());
    std::string nMatches = int2string(keylines_test_Match.size());
    std::string nInliers = int2string(keylines_test_Inlier.size());

    std::vector<std::string> vectEtiquetaSobrePuesta(3);
    vectEtiquetaSobrePuesta[0] = "detected, " + nMatches + " matched, " + nInliers + "Inliers";
    vectEtiquetaSobrePuesta[1] = "detected, " + nMatches + " matched, ";
    vectEtiquetaSobrePuesta[2] = "detected, ";

    std::vector<cv::Scalar> vectColoresSobrePuesta(3);
    vectColoresSobrePuesta[0] = cv::Scalar(arrColor_Inlier[2], arrColor_Inlier[1], arrColor_Inlier[0]);
    vectColoresSobrePuesta[1] = cv::Scalar(arrColor_Match[2], arrColor_Match[1], arrColor_Match[0]);
    vectColoresSobrePuesta[2] = cv::Scalar(arrColor_Deteccion[2], arrColor_Deteccion[1], arrColor_Deteccion[0]);

    cv::Point punto_refe(10, 15);
    cv::Point punto_test(10 + aumentoC_test, 15);
    std::string label_refe;
    std::string label_test;

    std::string id_refe = int2string(idFrame_refe);
    std::string id_test = int2string(idFrame_test);
    for (int ll = 0; ll < 3; ll++) {

        label_refe = id_refe + " - REFE Lines " + nDetected_refe + vectEtiquetaSobrePuesta[ll];
        addTextoEnPuntoImagen(Img_Output, label_refe, punto_refe, vectColoresSobrePuesta[ll], 1, 6);
        addTextoEnPuntoImagen(Img_Output, label_refe, punto_refe, cv::Scalar(0, 0, 0), 1, 2);

        label_test = id_test + " - TEST Lines " + nDetected_test + vectEtiquetaSobrePuesta[ll];
        addTextoEnPuntoImagen(Img_Output, label_test, punto_test, vectColoresSobrePuesta[ll], 1, 6);
        addTextoEnPuntoImagen(Img_Output, label_test, punto_test, cv::Scalar(0, 0, 0), 1, 2);
    } //for 


    return Img_Output;
}// plotMatchesInliners

cv::Mat plotResults_puntos(cv::Mat image_refe, cv::Mat image_test,
        int idFrame_refe, int idFrame_test,
        std::vector<cv::KeyPoint>& puntos_refe_Detected,
        std::vector<cv::KeyPoint>& puntos_test_Detected,
        std::vector<cv::Point2f>& puntos_refe_Match,
        std::vector<cv::Point2f>& puntos_test_Match,
        std::vector<cv::Point2f>& puntos_refe_Inlier,
        std::vector<cv::Point2f>& puntos_test_Inlier,
        bool bandPuntosSimilares) {

    cv::Mat Img_Output;
    std::vector<cv::Scalar> vectColoresDetected_refe, vectColoresDetected_test, vectColoresMatch, vectColoresInlier;
    cv::Mat img_refe;
    cv::Mat img_test;

    if (image_test.dims > 2) {
        cv::cvtColor(image_refe, img_refe, cv::COLOR_RGB2GRAY);
        cv::cvtColor(image_test, img_test, cv::COLOR_RGB2GRAY);
    } else {
        img_refe = image_refe.clone();
        img_test = image_test.clone();
    } // if - else

    /* <COLORES > */
    int arrColor_Deteccion[] = {
        255, 255, 0
    };
    int arrColor_Match[] = {
        255, 0, 0
    };
    int arrColor_Inlier[] = {
        0, 255, 0
    };

    std::vector<std::string> vectEtiquetas;
    /*************************************************/
    // < Deteccion >    
    vectColoresDetected_refe = generateVectorSpecificColor(puntos_refe_Detected.size(),
            arrColor_Deteccion[0], arrColor_Deteccion[1],
            arrColor_Deteccion[2]);
    vectColoresDetected_test = generateVectorSpecificColor(puntos_test_Detected.size(),
            arrColor_Deteccion[0], arrColor_Deteccion[1],
            arrColor_Deteccion[2]);

    plotPuntos(img_refe, img_refe, puntos_refe_Detected, vectColoresDetected_refe, vectEtiquetas, "", false);
    plotPuntos(img_test, img_test, puntos_test_Detected, vectColoresDetected_test, vectEtiquetas, "", false);
    /*************************************************/
    // < Matching >    
    vectColoresMatch = generateVectorSpecificColor(puntos_refe_Match.size(),
            arrColor_Match[0], arrColor_Match[1],
            arrColor_Match[2]);

    plotPuntos(img_refe, img_refe, puntos_refe_Match, vectColoresMatch, vectEtiquetas, "", false);
    plotPuntos(img_test, img_test, puntos_test_Match, vectColoresMatch, vectEtiquetas, "", false);
    /*************************************************/
    // < RANSAC >    
    vectColoresInlier = generateVectorSpecificColor(puntos_refe_Inlier.size(),
            arrColor_Inlier[0], arrColor_Inlier[1],
            arrColor_Inlier[2]);

    plotPuntos(img_refe, img_refe, puntos_refe_Inlier, vectColoresInlier, vectEtiquetas, "", true);
    plotPuntos(img_test, img_test, puntos_test_Inlier, vectColoresInlier, vectEtiquetas, "", true);
    /*************************************************/

    // < Dezplazamiento > 
    int aumentoF_refe = 0, aumentoF_test = 0, aumentoC_refe = 0, aumentoC_test = image_refe.cols;

    Img_Output = concatenaMatriz_Horizontal(img_refe, img_test);

    if (bandPuntosSimilares) {
        int grosorLinea = 1;
        // <Match>        
        for (int ii = 0; ii < vectColoresMatch.size(); ii++) {
            /* get extremes of line */
            cv::Point2f pt1 = cv::Point2f(puntos_refe_Match[ii].x + aumentoC_refe, puntos_refe_Match[ii].y + aumentoF_refe);
            cv::Point2f pt2 = cv::Point2f(puntos_test_Match[ii].x + aumentoC_test, puntos_test_Match[ii].y + aumentoF_test);

            /* draw line */
            cv::line(Img_Output, pt1, pt2, vectColoresMatch[ii], grosorLinea);
        }//for
        // <RANSAC>
        for (int ii = 0; ii < vectColoresInlier.size(); ii++) {
            /* get extremes of line */
            cv::Point2f pt1 = cv::Point2f(puntos_refe_Inlier[ii].x + aumentoC_refe, puntos_refe_Inlier[ii].y + aumentoF_refe);
            cv::Point2f pt2 = cv::Point2f(puntos_test_Inlier[ii].x + aumentoC_test, puntos_test_Inlier[ii].y + aumentoF_test);

            /* draw line */
            cv::line(Img_Output, pt1, pt2, vectColoresInlier[ii], grosorLinea);
        }//for
    }//if 

    /* Añadiendo etiquetas a las imagenes */
    std::string nDetected_refe = int2string(puntos_refe_Detected.size());
    std::string nDetected_test = int2string(puntos_test_Detected.size());
    std::string nMatches = int2string(puntos_test_Match.size());
    std::string nInliers = int2string(puntos_test_Inlier.size());

    std::vector<std::string> vectEtiquetaSobrePuesta(3);
    vectEtiquetaSobrePuesta[0] = "detected, " + nMatches + " matched, " + nInliers + "Inliers";
    vectEtiquetaSobrePuesta[1] = "detected, " + nMatches + " matched, ";
    vectEtiquetaSobrePuesta[2] = "detected, ";

    std::vector<cv::Scalar> vectColoresSobrePuesta(3);
    vectColoresSobrePuesta[0] = cv::Scalar(arrColor_Inlier[2], arrColor_Inlier[1], arrColor_Inlier[0]);
    vectColoresSobrePuesta[1] = cv::Scalar(arrColor_Match[2], arrColor_Match[1], arrColor_Match[0]);
    vectColoresSobrePuesta[2] = cv::Scalar(arrColor_Deteccion[2], arrColor_Deteccion[1], arrColor_Deteccion[0]);

    cv::Point punto_refe(10, 15);
    cv::Point punto_test(10 + image_refe.cols, 15);
    std::string label_refe;
    std::string label_test;

    std::string id_refe = int2string(idFrame_refe);
    std::string id_test = int2string(idFrame_test);
    for (int ll = 0; ll < 3; ll++) {
        label_refe = id_refe + " - REFE Points " + nDetected_refe + vectEtiquetaSobrePuesta[ll];
        addTextoEnPuntoImagen(Img_Output, label_refe, punto_refe, vectColoresSobrePuesta[ll], 1, 6);
        addTextoEnPuntoImagen(Img_Output, label_refe, punto_refe, cv::Scalar(0, 0, 0), 1, 2);

        label_test = id_test + "- TEST Points " + nDetected_test + vectEtiquetaSobrePuesta[ll];
        addTextoEnPuntoImagen(Img_Output, label_test, punto_test, vectColoresSobrePuesta[ll], 1, 6);
        addTextoEnPuntoImagen(Img_Output, label_test, punto_test, cv::Scalar(0, 0, 0), 1, 2);
    } //for 

    return Img_Output;
}// plotMatchesInliners

void getMaxMinCoordinates(std::vector<cv::Point2f>& trans_corners,
        double& maxCols_corner, double& maxRows_corner,
        double& minCols_corner, double& minRows_corner) {
    // Asignando valores iniciales
    maxCols_corner = -99999;
    maxRows_corner = -99999;
    minCols_corner = 99999;
    minRows_corner = 99999;

    for (int ii = 0; ii < trans_corners.size(); ii++) {
        if (maxRows_corner < trans_corners.at(ii).y) {
            maxRows_corner = trans_corners.at(ii).y;
        } // if
        if (maxCols_corner < trans_corners.at(ii).x) {
            maxCols_corner = trans_corners.at(ii).x;
        } // if
        if (minRows_corner > trans_corners.at(ii).y) {
            minRows_corner = trans_corners.at(ii).y;
        } // if
        if (minCols_corner > trans_corners.at(ii).x) {
            minCols_corner = trans_corners.at(ii).x;
        } // if
    } // if
} //function
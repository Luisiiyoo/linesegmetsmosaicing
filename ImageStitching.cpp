/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ImageStitching.cpp
 * Author: luis
 * 
 * Created on 7 de junio de 2018, 08:03 PM
 */

#include "ImageStitching.hpp"

#include "LinePointFunctions.hpp"
#include "BasicFunctions.hpp"
#include "TimeControl.hpp"
#include "Struct_Parametros.hpp"
#include "Struct_Resultados.hpp"


#include <opencv2/xfeatures2d.hpp>
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include <opencv2/line_descriptor.hpp>
#include "opencv2/core/utility.hpp"
#include <opencv2/imgproc.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/highgui.hpp>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "Struct_Parametros.hpp"
#include <iostream>
#include <fstream>
#include <string.h>

#include <time.h>
#include <sys/time.h>

#include "opencv2/stitching/detail/autocalib.hpp"
#include "opencv2/stitching/detail/blenders.hpp"
#include "opencv2/stitching/detail/camera.hpp"
#include "opencv2/stitching/detail/exposure_compensate.hpp"
#include "opencv2/stitching/detail/matchers.hpp"
#include "opencv2/stitching/detail/motion_estimators.hpp"
#include "opencv2/stitching/detail/seam_finders.hpp"
#include "opencv2/stitching/detail/util.hpp"
#include "opencv2/stitching/detail/warpers.hpp"

void ImageStitching::createTextFileEspecificResults(std::string fullName,
        std::vector<std::string>& vectTiemposEjecucion_label,
        std::vector<std::vector<double> >& vectTiemposEjecucion_Total_Iter,
        double runtimeStitchingBlending,
        int numFramesTotal, int numImgsUtilizadasMosaicing
        ) {
    // < abriendo archivo >
    cout << "Saving txt file in :" << fullName << endl;
    std::ofstream outfile(fullName.c_str());

    outfile << "clc; clear; close all;" << std::endl;
    // <escribiendo informacion> 
    outfile << "pathOfSourceMedia = " << "'" << parametros.SaveResults.pathOfSourceMedia << "'" << std::endl;
    outfile << "\n % ------------------- \n" << std::endl;

    // Homografias
    /*
    writeVectorCVMatriz_comoVariableMatlab(vectHomo_NoMejorada, "vectHomo_NoMejorada", outfile);
    outfile << "\n % ------------------- \n" << std::endl;
    writeVectorCVMatriz_comoVariableMatlab(vectHomo_SiMejorada, "vectHomo_SiMejorada", outfile);
    outfile << "\n % ------------------- \n" << std::endl;
    writeVector2D_comoVariableMatlab(vectDista_NoMejorada, "vectDista_NoMejorada", outfile);
    outfile << "\n % ------------------- \n" << std::endl;
    writeVector2D_comoVariableMatlab(vectDista_SiMejorada, "vectDista_SiMejorada", outfile);
    outfile << "\n % ------------------- \n" << std::endl;
    writeVector2D_comoVariableMatlab(vectAngle_NoMejorada, "vectAngle_NoMejorada", outfile);
    outfile << "\n % ------------------- \n" << std::endl;
    writeVector2D_comoVariableMatlab(vectAngle_SiMejorada, "vectAngle_SiMejorada", outfile);
    outfile << "\n % ------------------- \n" << std::endl;
    writeVector_comoVariableMatlab(vectInliers_NoMejorado, "vectInliers_NoMejorado", outfile);
    outfile << "\n % ------------------- \n" << std::endl;
    writeVector_comoVariableMatlab(vectInliers_SiMejorado, "vectInliers_SiMejorado", outfile);
    outfile << "\n % ------------------- \n" << std::endl;
     */

    /* < Imagenes - Homografias > */

    //    outfile << " \n %% Modelos parciales (Obtenidos en cada iteracion considerada) " << std::endl;
    //    writeVectorCVMatriz_comoVariableMatlab(vectHomografias_parcial, "vectHomografias_parcial", outfile);
    //    outfile << "\n % ------------------- \n" << std::endl;
    //
    //    outfile << " \n %% Modelos Completas (Considerando los anteriores modelos) " << std::endl;
    //    writeVectorCVMatriz_comoVariableMatlab(vectHomografias_completas, "vectHomografias_completas", outfile);
    //    outfile << "\n % ------------------- \n" << std::endl;
    //
    //    // < Indices considerados >
    //    outfile << " \n %% Indices Modelos Considerados " << std::endl;
    //    writeVector_comoVariableMatlab(vectIndexLocal, "vectIndexLocal", outfile);
    //    outfile << "\n % ------------------- \n" << std::endl;
    //
    //    writeVector_comoVariableMatlab(vect1DConsiderarStitching, "vect1DConsiderarStitching", outfile);
    //    outfile << "\n % ------------------- \n" << std::endl;
    //
    //    writeVector_comoVariableMatlab(vect1DBandModelFound, "vect1DBandHomoOK", outfile);
    //    outfile << "\n % ------------------- \n" << std::endl;
    //
    //    writeVector2D_comoVariableMatlab(vect2DRelacionHomografias, "vect2DRelacionHomografias", outfile);
    //    outfile << "\n % ------------------- \n" << std::endl;
    //
    //    // < Desplazamiento >
    //    outfile << " \n %% Desplazamiento " << std::endl;
    //    writeVector_comoVariableMatlab(vectDespIzq_global, "vectDespIzq_global", outfile);
    //    outfile << "\n % ------------------- \n" << std::endl;
    //    writeVector_comoVariableMatlab(vectDespDer_global, "vectDespDer_global", outfile);
    //    outfile << "\n % ------------------- \n" << std::endl;
    //    writeVector_comoVariableMatlab(vectDespArr_global, "vectDespArr_global", outfile);
    //    outfile << "\n % ------------------- \n" << std::endl;
    //    writeVector_comoVariableMatlab(vectDespAba_global, "vectDespAba_global", outfile);
    //    outfile << "\n % ------------------- \n" << std::endl;

    // < Tiempos de ejecucion >
    outfile << " \n %%  Tiempos de ejecucion desglosados  " << std::endl;
    std::vector<double> vectTiempoEtapasPromedio_ms(vectTiemposEjecucion_label.size());
    double tiempoStitchingBlendingPromedio_ms;

    for (int ii = 0; ii < vectTiemposEjecucion_label.size(); ii++) {
        std::vector<double> vect = getColFromVector2D(vectTiemposEjecucion_Total_Iter, ii);
        vectTiempoEtapasPromedio_ms[ii] = (getSumatoriaDeVector(vect) / numFramesTotal) * 1000;
    } // for

    tiempoStitchingBlendingPromedio_ms = (runtimeStitchingBlending / numImgsUtilizadasMosaicing)* 1000;

    // Guardando 
    outfile << "numFramesTotal = " << numFramesTotal << std::endl;
    outfile << "numImgsUtilizadasMosaicing = " << numImgsUtilizadasMosaicing << std::endl;
    outfile << "\n % ------------------- \n" << std::endl;
    writeVector_comoVariableMatlab(vectTiemposEjecucion_label, "vectTiemposEjecucion_label", outfile);
    writeVector_comoVariableMatlab(vectTiempoEtapasPromedio_ms, "vectTiempoEtapasPromedio_ms", outfile);
    outfile << "\n % ------------------- \n" << std::endl;
    outfile << "tiempoDetecDescMatchRansacPromedio_ms = " << getSumatoriaDeVector(vectTiempoEtapasPromedio_ms) << std::endl;
    outfile << "tiempoStitchingBlendingPromedio_ms = " << tiempoStitchingBlendingPromedio_ms << std::endl;
    outfile << "\n % ------------------- \n" << std::endl;
    outfile << "tiempoTotalEjecucion_s = " << getSumatoriaDeVector2D(vectTiemposEjecucion_Total_Iter) + runtimeStitchingBlending << std::endl;
    outfile << "tiempoTotalPromedioEjecucion_s = " << (getSumatoriaDeVector(vectTiempoEtapasPromedio_ms) + tiempoStitchingBlendingPromedio_ms) / 1000 << std::endl;
    outfile << "\n % ------------------- \n" << std::endl;

    // < cerrando archivo >
    outfile.close();
} // function 

ImageStitching::ImageStitching(ParametrosEjecucion& parametros, int numFramesTotal) {
    this->parametros = parametros;

    this->vect2DRelacionHomografias = std::vector<std::vector<int> >(numFramesTotal, std::vector<int>(4));
    this->vect1DBandModelFound = generaVectorFalse(numFramesTotal);
    this->vect1DConsiderarStitching = generaVectorFalse(numFramesTotal);
} //

ImageStitching::ImageStitching(const ImageStitching& orig) {
} //

ImageStitching::~ImageStitching() {
} //


std::vector<cv::Point2f> ImageStitching::getCornersImage(int width, int height) {
    std::vector<cv::Point2f> corners_test(4);
    //cvPoint(width, 0);
    corners_test[0] = cv::Point2f(0, 0); // Esquina Sup-Izq
    corners_test[1] = cv::Point2f(width, 0); // Esquina Sup-Der
    corners_test[2] = cv::Point2f(width, height); // Esquina Inf-Der
    corners_test[3] = cv::Point2f(0, height); // Esquina Inf-Izq

    return corners_test;
} // function 

std::vector<cv::Point2f> ImageStitching::transformPointsByMatTransformation(std::vector<cv::Point2f> & points, cv::Mat& matTransformation) {
    std::vector<cv::Point2f> points_trans(points.size());
    cv::perspectiveTransform(points, points_trans, matTransformation);

    return points_trans;
} // function 

cv::Size ImageStitching::getSizeSquareFromPoints(std::vector<cv::Point2f>& points) {
    /* Maximos y Minimos de columnas y filas */
    double maxCols_corner_test, maxRows_corner_test;
    double minCols_corner_test, minRows_corner_test;

    /* < Obteniendo maximos y minimos de la transformacion > */
    getMaxMinCoordinates(points, maxCols_corner_test, maxRows_corner_test,
            minCols_corner_test, minRows_corner_test);

    double ancho = maxCols_corner_test - minCols_corner_test;
    double alto = maxRows_corner_test - minRows_corner_test;

    cv::Size sizeFromPoints(ancho, alto);
    return sizeFromPoints;
} // function 

std::vector<cv::Point2f> ImageStitching::transformCornersImageByTransformationMat(int width, int height,
        cv::Mat& matTransformation, cv::Size& sizeImage_trans) {
    std::vector<cv::Point2f> corners_test = getCornersImage(width, height);
    std::vector<cv::Point2f> corners_test_trans = transformPointsByMatTransformation(corners_test, matTransformation);

    sizeImage_trans = getSizeSquareFromPoints(corners_test_trans);

    return corners_test_trans;
} // fuction

cv::Size ImageStitching::getSizeCanvas_TransformationMat_DisplacementVector(std::vector<cv::Point2f> points,
        cv::Size imgSize,
        cv::Mat& matTraslacion,
        std::vector<double>& vectDesplazamiento,
        std::vector<double>& vectExtremeCordinates) {

    /**/
    cv::Size sizeCanvas;

    /* Maximos y Minimos de columnas y filas */
    double maxCols_corner_test = -99999, maxRows_corner_test = -99999;
    double minCols_corner_test = 99999, minRows_corner_test = 99999;

    /* < Obteniendo maximos y minimos de la transformacion > */
    getMaxMinCoordinates(points, maxCols_corner_test, maxRows_corner_test,
            minCols_corner_test, minRows_corner_test);

    /*     double arr[] = {desp_colsIzq, desp_colsDer, desp_filsArr, desp_filsAba}; */
    double arrColsFils[] = {minCols_corner_test, maxCols_corner_test, minRows_corner_test, maxRows_corner_test};
    vectExtremeCordinates = std::vector<double>(arrColsFils, arrColsFils + sizeof (arrColsFils) / sizeof (arrColsFils[0]));

    //    printVector_comoVariableMatlab(vectExtremeCordinates, "vectExtremeCordinates");

    /* < Calculando desplazamiento para realizar el ajuste de la Homografia > */
    double desp_colsIzq = 0, desp_colsDer = 0;
    double desp_filsArr = 0, desp_filsAba = 0;

    if (minCols_corner_test < 0) {
        desp_colsIzq = std::abs(minCols_corner_test);
    }
    if (maxCols_corner_test > imgSize.width) {
        desp_colsDer = maxCols_corner_test - imgSize.width;
    }
    if (minRows_corner_test < 0) {
        desp_filsArr = std::abs(minRows_corner_test);
    } // if 
    if (maxRows_corner_test > imgSize.height) {
        desp_filsAba = maxRows_corner_test - imgSize.height;
    }
    /* <Vector de Desplazamiento> */
    double arr[] = {desp_colsIzq, desp_colsDer, desp_filsArr, desp_filsAba};
    vectDesplazamiento = std::vector<double>(arr, arr + sizeof (arr) / sizeof (arr[0]));

    /* < Calculando matriz de Desplazamiento > */
    matTraslacion = cv::Mat::eye(3, 3, CV_64F);
    matTraslacion.at<double>(0, 2) = desp_colsIzq; // desp_colsIzq
    matTraslacion.at<double>(1, 2) = desp_filsArr; // desp_filsArr

    /* < Definiendo tamaño del CANVAS >*/
    int anchoCanvas = round(imgSize.width + (desp_colsIzq + desp_colsDer));
    int altoCanvas = round(imgSize.height + (desp_filsArr + desp_filsAba));

    sizeCanvas = cv::Size(anchoCanvas, altoCanvas);

    /* <Retornando el tamaño del CANVAS> */
    return sizeCanvas;

} // function

cv::Size ImageStitching::getSizeCanvasFromImageTransformations(cv::Mat& matHomografia,
        cv::Size sizeImg_test, cv::Size sizeImg_refe,
        cv::Mat& matTraslacion,
        std::vector<double>& vectDesplazamiento) {
    /* Obteniendo Esquinas Transformadas*/
    cv::Size sizeImageTest_trans;
    std::vector<cv::Point2f> corners_test_trans = transformCornersImageByTransformationMat(
            sizeImg_test.width, sizeImg_test.height,
            matHomografia, sizeImageTest_trans);

    /* Obteniendo El tamaño del Canvas ya con la transformacion de la Imagen */
    std::vector<double> vectExtremeCordinates;
    cv::Size sizeCanvas = getSizeCanvas_TransformationMat_DisplacementVector(corners_test_trans, sizeImg_refe,
            matTraslacion, vectDesplazamiento, vectExtremeCordinates);

    return sizeCanvas;
} //fucntion

cv::Mat ImageStitching::stitchingTwoImages(
        cv::Mat& img_test, cv::Mat& img_test_mask,
        cv::Mat& img_refe, cv::Mat& img_refe_mask,
        cv::Mat& matHomografia,
        cv::Mat& imgMosaicMask,
        int opcionBlender, bool try_gpu) {
    /* <~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~> */
    /* Obteniendo tamaño del canvas entre las dos imágenes */
    cv::Mat matTraslacion;
    std::vector<double> vectDesplazamiento;

    cv::Size sizeCanvas = getSizeCanvasFromImageTransformations(matHomografia,
            img_test.size(), img_refe.size(),
            matTraslacion, vectDesplazamiento);

    // < Revisando el tamaño del Mosaico >
    checkSizeMosaicGlobal(sizeCanvas);

    cv::Mat matHomografia_ajustada = matTraslacion * matHomografia;
    /* <~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~> */

    /* < Obteniendo la transformacion de las imagenes ajustadas al CANVAS > */
    // < Matrices >
    cv::Mat imgTran_ajust, img_refe_ajust; // Imagenes  Ajustadas
    cv::Mat imgTran_mask_ajust, imgRefe_mask_ajust; // Mascara transformada con la Homografia ajustada

    // < Refe Transformation >
    cv::warpPerspective(img_refe, img_refe_ajust, matTraslacion, sizeCanvas); // <Aplicando ajuste para que se pueda visualizar (Traslacion solamente)>
    cv::warpPerspective(img_refe_mask, imgRefe_mask_ajust, matTraslacion, sizeCanvas); // aplicando transformacion con la Homografia ajustada

    // < Test Transformation >
    cv::warpPerspective(img_test, imgTran_ajust, matHomografia_ajustada, sizeCanvas); // <Aplicando ajuste para que se pueda visualizar (Traslacion + Homografia)>
    cv::warpPerspective(img_test_mask, imgTran_mask_ajust, matHomografia_ajustada, sizeCanvas); // aplicando transformacion con la Homografia ajustada

    /* < Generando unicamente el Stitching > */
    cv::Mat imgStitching = img_refe_ajust.clone();
    imgTran_ajust.copyTo(imgStitching, imgTran_mask_ajust); // Copiando los pixeles a la region marcada por la mascara

    /* < Generando Stitching + Blending >*/
    cv::Mat imgMosaic;
    imgMosaic = blendingTwoImages(imgTran_ajust, img_refe_ajust,
            imgTran_mask_ajust, imgRefe_mask_ajust,
            imgMosaicMask, opcionBlender, try_gpu);


    //    CanvasMask = blendingMask.clone();
    /* < ***************************** >*/
    /* Mostrando resultado */
    //        cv::imshow("Image Stitching", imgStitching);
    //        cv::imshow("Image Blending", imgBlending);
    //    cv::imshow("Stitching-Blending Mask", blendingMask);
    /* < ***************************** >*/

    /* < Retornando la Imagen Canvas */
    return imgMosaic;
} // function 

cv::Mat ImageStitching::addImageToMosaic(
        cv::Mat& newImage, cv::Mat& newImage_mask,
        cv::Mat& oldMosaico, cv::Mat& oldMosaico_mask,
        cv::Size& sizeCanvas, cv::Mat& matHomografia_ajustada,
        cv::Mat& matTraslacion_ajustada,
        cv::Mat& newMosaicMask,
        int opcionBlender, bool try_gpu) {

    /* < Obteniendo la transformacion de las imagenes ajustadas al CANVAS > */
    // < Matrices >
    cv::Mat newImgTran_ajust, newMosaico_ajust; // Imagenes  Ajustadas
    cv::Mat newImgTran_mask_ajust, newMosaico_mask_ajust; // Mascara transformada con la Homografia ajustada

    // < Refe Transformation >
    cv::warpPerspective(oldMosaico, newMosaico_ajust, matTraslacion_ajustada, sizeCanvas); // <Aplicando ajuste para que se pueda visualizar (Traslacion solamente)>
    cv::warpPerspective(oldMosaico_mask, newMosaico_mask_ajust, matTraslacion_ajustada, sizeCanvas); // aplicando transformacion con la Homografia ajustada

    // < Test Transformation >
    cv::warpPerspective(newImage, newImgTran_ajust, matHomografia_ajustada, sizeCanvas); // <Aplicando ajuste para que se pueda visualizar (Traslacion + Homografia)>
    cv::warpPerspective(newImage_mask, newImgTran_mask_ajust, matHomografia_ajustada, sizeCanvas); // aplicando transformacion con la Homografia ajustada

    /* < Generando unicamente el Stitching > */
    cv::Mat imgStitching = newMosaico_ajust.clone();
    newImgTran_ajust.copyTo(imgStitching, newImgTran_mask_ajust); // Copiando los pixeles a la region marcada por la mascara

    /* < Generando Stitching ++ Blending >*/
    cv::Mat newMosaic;
    newMosaic = blendingTwoImages(newImgTran_ajust, newMosaico_ajust,
            newImgTran_mask_ajust, newMosaico_mask_ajust,
            newMosaicMask, opcionBlender, try_gpu);


    //    CanvasMask = blendingMask.clone();
    /* < ***************************** >*/
    /* Mostrando resultado */
    //    cv::imshow("newImgTran_mask_ajust", newImgTran_mask_ajust);
    //    cv::imshow("newMosaico_mask_ajust", newMosaico_mask_ajust);
    //    cv::imshow("Stitching-Blending Mask", newMosaicMask);
    /* < ***************************** >*/

    /* < Retornando la Imagen Canvas */
    return newMosaic;
} // function 

cv::Mat ImageStitching::stitchingImages(std::vector<cv::Mat>& vectImagenes,
        std::vector<cv::Mat>& vectHomografias, cv::Mat& matTraslacion,
        cv::Size sizeCanvas,
        cv::Mat& CanvasMask, int opcionBlender, bool try_gpu) {
    /* <---------------------------------> */
    int numImagenes = vectImagenes.size();

    /* Transformando las imagenes */
    // < Matrices Sin Ajuste >
    cv::Mat matImg;
    cv::Mat matImg_mask;
    // < Matrices Con Ajuste >
    cv::Mat matImg_ajust; // Imagenes  Ajustadas
    cv::Mat matImg_mask_ajust; // Mascara transformada con la Homografia ajustada

    cv::Mat matHomografia;
    cv::Mat matHomografia_ajust;

    std::vector<cv::Mat> vectImagenes_mask(vectImagenes.size());

    for (int hh = 0; hh < numImagenes; hh++) {
        cout << "Ajustando Img_" << hh + 1 << " de " << numImagenes << " imagenes." << endl;
        /* < Imagen y su Mascara Sin transformar> */
        matImg = vectImagenes[hh].clone();
        matImg_mask = cv::Mat(matImg.size(), CV_8UC1, cv::Scalar(255)); // creando mascara para copiar los elementos 

        /* < Obteniendo Homografia Ajustada > */
        matHomografia = vectHomografias[hh].clone(); // obteniendo la Homografia de la iteracion 
        matHomografia_ajust = matTraslacion * matHomografia;

        // < Transformando Imagen y Mascara con "matHomografia_ajustada">
        cv::warpPerspective(matImg, matImg_ajust, matHomografia_ajust, sizeCanvas); // <Aplicando ajuste para que se pueda visualizar (Traslacion + Homografia)>
        cv::warpPerspective(matImg_mask, matImg_mask_ajust, matHomografia_ajust, sizeCanvas); // aplicando transformacion con la Homografia ajustada

        /* < Reasignando transformacion> */
        vectImagenes[hh] = matImg_ajust.clone();
        vectImagenes_mask[hh] = matImg_mask_ajust.clone();


    } // for 

    /* < Combinando las imagenes > */
    cv::Mat CanvasImg = blendingImages(sizeCanvas, vectImagenes,
            vectImagenes_mask, CanvasMask, opcionBlender, try_gpu);

    /* < ***************************** >*/
    /* Mostrando resultado */
    //     cv::imshow("Stitching-Blending Image ", imgBlending);
    //    cv::imshow("Stitching-Blending Mask", blendingMask);
    /* < ***************************** >*/

    /* < Retornando la Imagen Canvas */
    return CanvasImg;
} // function 

cv::Mat ImageStitching::blendingImages(cv::Size& sizeCanvas, std::vector<cv::Mat>& vectMatImages,
        std::vector<cv::Mat>& vectMatImages_mask,
        cv::Mat& blendingMask, int opcionBlender, bool try_gpu) {
    /* <~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~> */
    bool bandBlendingPorPares = true;
    /* <~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~> */
    /* < ***************************** >*/
    /* < Choosing Blender Algorithm > */
    cv::Ptr<cv::detail::Blender> blender = createBlenderObject(opcionBlender, try_gpu);

    /* < Preparando Blender > */
    //blender->prepare(cv::Rect(0, 0, max(imgRefe_aux.cols, imgTest_aux.cols), max(imgRefe_aux.rows, imgTest_aux.rows)));
    blender->prepare(cv::Rect(0, 0, sizeCanvas.width, sizeCanvas.height));
    cv::Point pt(0, 0);

    /* < ---------------------> */
    cv::Mat ImgBlended, ImgBlended_mask;
    for (int ii = 0; ii < vectMatImages.size(); ii++) {
        cout << "Stitching/Blending Img_" << ii + 1 << " de " << vectMatImages.size() << " imagenes." << endl;
        /* Transformando al formato adecuado */
        cv::Mat matImg_s;
        vectMatImages[ii].convertTo(matImg_s, CV_16S);

        if (bandBlendingPorPares == false) {
            /* < Procesando imagenes para el Blender > */
            blender->feed(matImg_s, vectMatImages_mask[ii], pt);
            //        cv::imshow("vectMatImages["+int2string(ii)+"]",vectMatImages[ii]);
        } else {
            if (ii == 0) {
                ImgBlended = matImg_s.clone();
                ImgBlended_mask = vectMatImages_mask[ii].clone();

                blender->feed(ImgBlended, ImgBlended_mask, pt);
            } else {
                /* Alimentando con las dos imagemes, la actual y la anterior */
                blender->feed(ImgBlended, ImgBlended_mask, pt);
                blender->feed(matImg_s, vectMatImages_mask[ii], pt);

                /* Mezclar las imagenes */
                blender->blend(ImgBlended, ImgBlended_mask);

                /* Limpiando Puntero y Creando uno nuevo*/
                blender.release();
                blender = createBlenderObject(opcionBlender, try_gpu);
                blender->prepare(cv::Rect(0, 0, sizeCanvas.width, sizeCanvas.height));
            } // if - else        
        } // if - else
    } // for 

    //    cv::Mat matImg_s;
    //    int ii;
    //
    //    ii = 0;
    //    //    cv::imshow("vectMatImages["+int2string(ii)+"]",vectMatImages[ii]);
    //    vectMatImages[ii].convertTo(matImg_s, CV_16S);
    //    blender->feed(matImg_s, vectMatImages_mask[ii], pt);
    //    ii = 2;
    //    //    cv::imshow("vectMatImages["+int2string(ii)+"]",vectMatImages[ii]);
    //    vectMatImages[ii].convertTo(matImg_s, CV_16S);
    //    blender->feed(matImg_s, vectMatImages_mask[ii], pt);
    /* < ---------------------> */

    cv::Mat imgBlending;


    if (bandBlendingPorPares == false) {
        /* < Mezclando > */
        cv::Mat imgResult_s;
        blender->blend(imgResult_s, blendingMask);

        /* Destransformando imagenes */

        imgResult_s.convertTo(imgBlending, CV_8U);
    } else {
        blendingMask = ImgBlended_mask.clone();
        ImgBlended.convertTo(imgBlending, CV_8U);
    } // if - else

    //    cv::imshow("blendingMask", blendingMask);
    //    cv::waitKey(0);

    return imgBlending;
} // function 

cv::Mat ImageStitching::blendingTwoImages(cv::Mat& imgTest, cv::Mat& imgRefe,
        cv::Mat& imgTest_mask, cv::Mat& imgRefe_mask,
        cv::Mat& blendingMask, int opcionBlender, bool try_gpu) {
    /* Transformando al formato adecuado */
    cv::Mat imgRefe_aux, imgTest_aux;
    imgRefe.convertTo(imgRefe_aux, CV_16S);
    imgTest.convertTo(imgTest_aux, CV_16S);

    /* < Choosing Blender Algorithm > */
    cv::Ptr<cv::detail::Blender> blender = createBlenderObject(opcionBlender, try_gpu);

    /* < Preparando Blender > */
    blender->prepare(cv::Rect(0, 0, max(imgRefe_aux.cols, imgTest_aux.cols), max(imgRefe_aux.rows, imgTest_aux.rows)));

    /* < Procesando imagenes para el Blender > */
    cv::Point pt(0, 0);
    blender->feed(imgRefe_aux, imgRefe_mask, pt);
    blender->feed(imgTest_aux, imgTest_mask, pt);

    /* < Mezclando > */
    cv::Mat imgResult_s;
    blender->blend(imgResult_s, blendingMask);

    /* Destransformando imagenes */
    cv::Mat imgBlending;
    imgResult_s.convertTo(imgBlending, CV_8U);

    return imgBlending;
} // function 

cv::Ptr<cv::detail::Blender> ImageStitching::createBlenderObject(int opcionBlender, bool try_gpu) {
    cv::Ptr<cv::detail::Blender> blender;
    int blend_type;
    switch (opcionBlender) {
        case 1:
        {
            blend_type = cv::detail::Blender::FEATHER;
            break;
        }// case 
        case 2:
        {
            blend_type = cv::detail::Blender::MULTI_BAND;
            break;
        }// case 
        default:
        case 0:
        {
            blend_type = cv::detail::Blender::NO;
            break;
        } // case 
    } // switch 

    blender = cv::detail::Blender::createDefault(blend_type, try_gpu);

    return blender;
} // function

void ImageStitching::agregarImagenAlMosaico(Struct_Matching& structMatch) {
    /*+++++++++++++++++++++++++++++++++++++++*/
    /* Midiendo tiempos */
    TimeControl tc;
    tc.start(); // <--------
    /*+++++++++++++++++++++++++++++++++++++++*/
    int iterFrame_actual = structMatch.iterFramePares + 1;

    int indx_test = vect2DRelacionHomografias[iterFrame_actual][1]; // posTest 
    int indx_refe = vect2DRelacionHomografias[iterFrame_actual][2]; // posRefe

    int indx_Test_local = findIndexOfValInVector(indx_test, vectIndexLocal);
    int indx_Refe_local = findIndexOfValInVector(indx_refe, vectIndexLocal);

    cv::Mat newImage = structMatch.imageMat_test.clone(); //vectImagenes[indx_Test_local];
    cv::Mat newImage_mask = cv::Mat(newImage.size(), CV_8UC1, cv::Scalar(255)); // creando mascara para copiar los elementos          

    cv::Mat oldMosaico, oldMosaico_mask;
    if (indx_Test_local == 1) { // Si es el primer par de imagenes
        oldMosaico = structMatch.imageMat_refe.clone(); // vectImagenes[indx_Refe_local]; 
        oldMosaico_mask = cv::Mat(oldMosaico.size(), CV_8UC1, cv::Scalar(255)); // creando mascara para copiar los elementos 

        vectTraslacion_completas_mosaicoAcumulado.push_back(cv::Mat::eye(3, 3, CV_64F));
    } else {
        oldMosaico = MosaicoAculumado_img.clone();
        oldMosaico_mask = MosaicoAculumado_mask.clone();
    }//if-else

    cv::Mat matH_ajustadaAnterior_newImage, matT_newImage;
    cv::Size sizeCanvas;
    cv::Mat matT_canvas;
    std::vector<double> vectMaxDesplazamiento;

    // Homografia Global
    cv::Mat HG = vectHomografias_completas[indx_Test_local];
    //    HG.at<double>(2, 0) = 0;
    //    HG.at<double>(2, 1) = 0;

    // Ajustar imagen con todas las homografias y con la traslacion de la anterior iteracion
    matH_ajustadaAnterior_newImage = vectTraslacion_completas_mosaicoAcumulado[indx_Refe_local] * HG;
    //    cout << matH_ajustadaAnterior_newImage << endl;

    // Obteniendo la nueva traslacion de esta iteracion que sera aplicada al <Canvas>
    sizeCanvas = getSizeCanvasFromImageTransformations(matH_ajustadaAnterior_newImage, newImage.size(),
            oldMosaico.size(), matT_canvas, vectMaxDesplazamiento);

    // <Desplazamiento para la nueva imagen>
    // Obteniendo la nueva traslacion global que sera aplicada a <newImage>
    cv::Size sizeDD = getSizeCanvasAndMatTraslacion(oldMosaico.size(),
            matT_newImage, vectMaxDesplazamiento);

    // < Ajustando Homografias con desplazamiento para el CANVAS >
    cv::Mat mAjuste_canvas = matT_canvas;
    cv::Mat mAjuste_newImagen = matT_newImage * HG;

    // < Guardando traslacion aplicada en esta iteracion a newImage >
    vectTraslacion_completas_mosaicoAcumulado.push_back(matT_newImage);

    // <Agregando newImage al Mosaico >
    MosaicoAculumado_img = addImageToMosaic(newImage, newImage_mask,
            oldMosaico, oldMosaico_mask,
            sizeCanvas, mAjuste_newImagen, mAjuste_canvas,
            MosaicoAculumado_mask,
            parametros.Mosaic.idTypeBlending, false);

    /*+++++++++++++++++++++++++++++++++++++++*/
    vectRuntimeMosaicing_mosaicoacumulado.push_back(tc.finish(false, ""));
    /*+++++++++++++++++++++++++++++++++++++++*/
} //function 

double ImageStitching::ajustarHomografias(Struct_Matching& structMatch) {
    /*+++++++++++++++++++++++++++++++++++++++*/
    /* Midiendo tiempos */
    TimeControl tc;
    tc.start(); // <--------
    /*+++++++++++++++++++++++++++++++++++++++*/
    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    int iterFrame_actual = structMatch.iterFramePares + 1;

    if (structMatch.iterFramePares == 0) {
        /* < Establece relacion entre los frames y las iteraciones >*/
        vect2DRelacionHomografias[structMatch.iterFramePares][0] = structMatch.iterFramePares; // numeroIteracion
        vect2DRelacionHomografias[structMatch.iterFramePares][1] = structMatch.idFrame_refe; // posTest 
        vect2DRelacionHomografias[structMatch.iterFramePares][2] = structMatch.idFrame_refe; // posRefe
        vect2DRelacionHomografias[structMatch.iterFramePares][3] = 100; // porcentajeMatch
        /* <Banderas para considerar en el mosaico> */
        vect1DBandModelFound[structMatch.iterFramePares] = true;
        vect1DConsiderarStitching[structMatch.iterFramePares] = true;

        /* < Frame Inicial valido > */
        vectIndexLocal.push_back(structMatch.iterFramePares);
        vectHomografias_parcial.push_back(cv::Mat::eye(3, 3, CV_64F)); // Identidad
        vectHomografias_completas.push_back(cv::Mat::eye(3, 3, CV_64F)); // copia la referencia a la imagenreferencia a la imagen                

        // < Agregar imagen si no se realizara el mosaico acumulado >
        if (!parametros.Mosaic.bandMosaicoAcumulado) {
            vectImagenes.push_back(structMatch.imageMat_refe.clone()); // copia la referencia a la imagen
        } //if 

        vectDespIzq_global.push_back(0); //[indice] = vectDesplazamiento[0];
        vectDespDer_global.push_back(0);
        vectDespArr_global.push_back(0);
        vectDespAba_global.push_back(0);
    } // 

    // <Guardando Bandera de frames considerados>
    vect1DConsiderarStitching[iterFrame_actual] = structMatch.bandConsiderarStitching;

    /* < Establece relacion entre los frames y las iteraciones >*/
    vect2DRelacionHomografias[iterFrame_actual][0] = iterFrame_actual; // numeroIteracion
    vect2DRelacionHomografias[iterFrame_actual][1] = structMatch.idFrame_test; // posTest 
    vect2DRelacionHomografias[iterFrame_actual][2] = structMatch.idFrame_refe; // posRefe
    vect2DRelacionHomografias[iterFrame_actual][3] = structMatch.porcentajeMatchInliers; // porcentajeMatch

    /* <Banderas para considerar en el mosaico> */
    vect1DBandModelFound[iterFrame_actual] = structMatch.bandModelFound;
    int indx_test;

    if (structMatch.bandConsiderarStitching) {
        // < Iteracion que cumple con los criterios de acuerdo a la seleccion dinamica de keyframe >    
        indx_test = iterFrame_actual; // idTest + 1 si es el actual                                

        // Ese frame se considerara        
        vect1DConsiderarStitching[indx_test] = true;

        /* Obteniendo indice Homografia Referencia */
        int indx_refe = vect2DRelacionHomografias[indx_test][2]; // Id Refe (actual)

        /* Guardar imagen */
        vectIndexLocal.push_back(indx_test);

        /* < Obteniendo el indice correspondiente > */
        vectHomografias_parcial.push_back(structMatch.Model); // copia la homografia correspondiente
        // < Agregar imagen si no se realizara el mosaico acumulado >
        if (!parametros.Mosaic.bandMosaicoAcumulado) {
            vectImagenes.push_back(structMatch.imageMat_test); // copia la imagen 
        }// if 

        /* <-------------------- Ajustando Homografia --------------------> */
        int indx_Test_local = findIndexOfValInVector(indx_test, vectIndexLocal);
        int indx_Refe_local = findIndexOfValInVector(indx_refe, vectIndexLocal);

        cv::Mat matH = vectHomografias_completas[indx_Refe_local] * vectHomografias_parcial[indx_Test_local];
        matH = matH / matH.at<double>(2, 2);

        vectHomografias_completas.push_back(matH);
        /* <-------------------- Desplazamiento --------------------> */
        /*  Encontrando Puntos Extremos para definir el tamaño del CANVAS */
        std::vector<double> vectDesplazamiento_global;
        cv::Mat matTraslacion_global;

        getSizeCanvasFromImageTransformations(vectHomografias_completas[indx_Test_local],
                structMatch.imageMat_test.size(), structMatch.imageMat_refe.size(),
                matTraslacion_global, vectDesplazamiento_global);

        vectDespIzq_global.push_back(vectDesplazamiento_global[0]); //[indice] = vectDesplazamiento[0];
        vectDespDer_global.push_back(vectDesplazamiento_global[1]);
        vectDespArr_global.push_back(vectDesplazamiento_global[2]);
        vectDespAba_global.push_back(vectDesplazamiento_global[3]);

        //        cout << "--------------------" << endl;
        //        printVector_comoVariableMatlab(vectDesplazamiento_global, "vectDesplazamiento");
        //        cout << "--------------------" << endl;
        /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
        //        std::vector<int> subVect = getRowFromVector2D(vect2DRelacionHomografias, indx_test);
        //        printVector_comoVariableMatlab(subVect, "[Iter, TestF, RefeF, PrcMatch]");
        //        cout << "H_ajust[" << indx_test << "] = " << "H_ajust[" << indx_refe << "] * H[" << indx_test << "]" << endl;
        //        cout << " ** Modelo de la Iter " << indx_test << " " << ((vect1DConsiderarStitching[indx_test]) ? "SI" : "NO") << " considerado en el Mosaico **\n" << endl;
        /* < ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ > */
    } // if    

    /*+++++++++++++++++++++++++++++++++++++++*/
    return tc.finish(false, "");
    /*+++++++++++++++++++++++++++++++++++++++*/
} // function 

double ImageStitching::crearCanvasDeHomografiasAjustadas(cv::Mat& CanvasImg, cv::Mat& CanvasMask, int & numImagesMosaico) {
    /*+++++++++++++++++++++++++++++++++++++++*/
    /* Midiendo tiempos */
    TimeControl tc;
    tc.start(); // <--------
    /*+++++++++++++++++++++++++++++++++++++++*/
    /* < Relaciones entre las iteraciones > */
    //    printVector2D_comoVariableMatlab(vect2DRelacionHomografias, "vect2DRelacionHomografias");

    /* < Extrayendo las iteraciones validas > */
    std::vector<int> vectIndicesConsiderados = findElementosSatisfacenExpresion(vect1DConsiderarStitching);
    numImagesMosaico = vectIndicesConsiderados.size();

    if (vectIndicesConsiderados.empty()) {
        cout << "No se puede formar el mosaico, el porcentaje del match entre las "
                << "iteraciones tiene que ser como maximo " << this->parametros.ControlVent.porcMinMantenerImg << "%." << endl;
        this->parametros.Mosaic.bandStitching = false;
    } else {

        /* Obteniendo solo los datos necesarios */
        vect2DRelacionHomografias = getSpecific_STDVector2d_FromVector(vect2DRelacionHomografias, vectIndicesConsiderados); //    vectImagenes, vectHomografias_completas , vectHomografias_parcial
        vect1DBandModelFound = getElementsFromVector(vect1DBandModelFound, vectIndicesConsiderados);


        /* <Obteniendo el tamaño del mosaico Final y la matriz para ajustar la imagen de referencia> */
        cv::Mat matTraslacion;
        std::vector<double> vectMaxDesplazamiento;
        cv::Size sizeCanvas = getSizeCanvasAndMatTraslacion(vectImagenes[0].size(),
                matTraslacion, vectMaxDesplazamiento);


        /* <---------------------------------> */
        //vectHomografias_parcial.clear();
        /* <---------------------------------> */
        // < Revisando el tamaño del Mosaico >
        checkSizeMosaicGlobal(sizeCanvas);

        cout << "\t** Generando Mosaico **" << endl;
        /* < ~~~~~~~~~~~~~~~ Realizando Image Mosaicing  con Homografias ajustadas ~~~~~~~~~~~~~~~ >*/
        cout << "\n ~~~~~~ Pares de Imagenes Considerados ~~~~~~ " << endl;
        printVector_comoVariableMatlab(vectIndicesConsiderados, "vectIndicesIteracionesConsideradas");
        printVector2D_comoVariableMatlab(vect2DRelacionHomografias, "vect2DRelacionHomografias");


        cout << "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~";
        cout << "\n~~~~~~~~~~~~~ Image Mosaicing ~~~~~~~~~~~~~";
        cout << "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
        printVector_comoVariableMatlab(vectMaxDesplazamiento, "vectDesplazamiento");
        cout << "--------------------------" << endl;
        printCVMatriz_comoVariableMatlab(matTraslacion, "matTraslacion");
        cout << "--------------------------" << endl;
        cout << "  1 - SizeCanvas : Width = " << sizeCanvas.width << ", Rows = " << sizeCanvas.height << endl;
        cout << "  2 - Size Mosaico Acumulado : Width = " << MosaicoAculumado_img.cols << ", Rows = " << MosaicoAculumado_img.rows << endl;
        cout << endl;

        CanvasImg = stitchingImages(vectImagenes, vectHomografias_completas,
                matTraslacion, sizeCanvas, CanvasMask, this->parametros.Mosaic.idTypeBlending, false);
        /* <---------------------------------> */
    }// if - else 
    /* <---------------------------------> */

    /*+++++++++++++++++++++++++++++++++++++++*/
    return tc.finish(false, "");
    /*+++++++++++++++++++++++++++++++++++++++*/
} // function

cv::Size ImageStitching::getSizeCanvasAndMatTraslacion(cv::Size sizeImagen,
        cv::Mat& matTraslacion, std::vector<double>& vectMaxDesplazamiento) {
    /* < Obteniendo el maximo desplzamiento > */
    double maxDespIzq = getMaxElementVec(this->vectDespIzq_global);
    double maxDespDer = getMaxElementVec(this->vectDespDer_global);
    double maxDespArr = getMaxElementVec(this->vectDespArr_global);
    double maxDespAba = getMaxElementVec(this->vectDespAba_global);

    double arrDesp[] = {maxDespIzq, maxDespDer, maxDespArr, maxDespAba};
    vectMaxDesplazamiento = std::vector<double>(arrDesp, arrDesp + sizeof (arrDesp) / sizeof (arrDesp[0]));

    /* < Generando Matriz de Traslacion > */
    matTraslacion = cv::Mat::eye(3, 3, CV_64F);
    matTraslacion.at<double>(0, 2) = maxDespIzq; // desp_colsIzq
    matTraslacion.at<double>(1, 2) = maxDespArr; // desp_filsArr

    /* Obteniendo el tañamo del CANVAS */
    double widhtCanvas = round(sizeImagen.width + maxDespIzq + maxDespDer);
    double heightCanvas = round(sizeImagen.height + maxDespArr + maxDespAba);
    cv::Size sizeCanvas(widhtCanvas, heightCanvas);

    return sizeCanvas;
} //function

void ImageStitching::checkSizeMosaicGlobal(cv::Size sizeCanvas) {
    bool bandLimiteWidht = (sizeCanvas.width < (this->parametros.Mosaic.limitSizeCanvas));
    bool bandLimiteHeight = (sizeCanvas.height < (this->parametros.Mosaic.limitSizeCanvas));

    if (!bandLimiteWidht && !bandLimiteHeight) {
        cout << "\n** El mosaico excede el ancho o el alto permitido por el mosaico, modifique este parametro.**" << endl;
        cout << "  SizeCanvas : Width = " << sizeCanvas.width << ", Rows = " << sizeCanvas.height << endl;
        cout << endl;
        cv::waitKey(0);
        exit(0);
    } // if 

} // function
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   LineFunctions.hpp
 * Author: luis
 *
 * Created on 13 de septiembre de 2017, 02:37 PM
 */

#ifndef LINEFUNCTIONS_HPP
#define LINEFUNCTIONS_HPP

#include <opencv2/line_descriptor.hpp>
#include "opencv2/core/utility.hpp"
#include <opencv2/imgproc.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/highgui.hpp>

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <string>
#include <iostream>
#include <fstream>

std::pair<double, double > getPorcentajesModel(bool bandModelFound,
            cv::Mat& Model, double averageSizeImage,
            int numMatches, int numInliers);

cv::line_descriptor::KeyLine perspectiveTransform_KeyLine(cv::line_descriptor::KeyLine& linea, cv::Mat& matTransformacion);
void perspectiveTransform_vectKeyLines(std::vector<cv::line_descriptor::KeyLine> vectKeylines_input,
        std::vector<cv::line_descriptor::KeyLine>& vectKeylines_output, cv::Mat& matTransformacion);

cv::Mat mat1D_To_mat2D(cv::Mat& mat1D, int nFil, int nCol);

std::vector< std::vector< double > > transform_cvMat2stdVector(cv::Mat mat);
cv::Mat transform_stdVector2cvMat(std::vector< std::vector< double > > vect);

std::vector<std::vector<double> > CV_Mat2Std_Vector(cv::Mat& mat);

std::vector<cv::Mat> getSpecific_CVMat_FromVector(std::vector<cv::Mat>& vectMat, std::vector<int> vecIndices);
std::vector<std::vector<int> > getSpecific_STDVector2d_FromVector(std::vector<std::vector<int> >& vect2D, std::vector<int> vecIndices);

std::vector<cv::line_descriptor::KeyLine> getSpecificLinesFromVector(std::vector<cv::line_descriptor::KeyLine>& vectKeylines, std::vector<int> vecIndices);
std::vector<cv::Point2f> getSpecificPointsFromVector(std::vector<cv::Point2f>& vectPuntos, std::vector<int> vecIndices);


double getDistanciaEvalucionInlier_Linea(int tipoEval, cv::line_descriptor::KeyLine& linea_tran, cv::line_descriptor::KeyLine& linea_refe);

std::vector<cv::line_descriptor::KeyLine> getLinesTransformationsByModel(int tipoEvalDistancia,
        cv::Mat& H,
        std::vector<bool>vectorBanderasConsiderarLinea,
        std::vector<cv::line_descriptor::KeyLine>& lineas_test,
        std::vector<cv::line_descriptor::KeyLine>& lineas_refe,
        std::vector<double>& vectDistanciaError,
        std::vector<double>& vectAngulosError,
        bool bandPrintSomething);

std::vector<double> getPointsTransformationsByModel(cv::Mat& H,
        std::vector<bool>vectorBanderasConsiderarLinea,
        std::vector<cv::Point2f>& puntos_test,
        std::vector<cv::Point2f>& puntos_refe,
        std::vector<cv::Point2f>& puntos_tran,
        bool bandPrintSomething);

void printKeylinesAsPointsVector_varMatlab(std::vector<cv::line_descriptor::KeyLine>& lineas, std::string nomVar);
void printPointsVector_varMatlab(std::vector<cv::Point2f>& puntos, std::string nomVar);

std::vector<std::vector<double> > transformPoints2Mat(std::vector<cv::Point2f>& vectPoints);

std::vector<cv::Point2f> vectLineSegments2vectPoints(std::vector<cv::line_descriptor::KeyLine>& vectKeylines);
std::vector<cv::line_descriptor::KeyLine> vectPoints2vectLineSegments(std::vector<cv::Point2f>& vectPointsIniFin);

void getVectIndicesMatchesKeyPoints_specific(std::vector<int> vecIndEspecificos_Match,
        std::vector<cv::DMatch>& matches,
        std::vector<cv::Point2f>& puntos_I1,
        std::vector<cv::Point2f>& puntos_I2,
        std::vector<cv::Point2f>& puntos_I1_Match,
        std::vector<cv::Point2f>& puntos_I2_Match,
        std::vector<int>& puntos_I1_Match_index,
        std::vector<int>& puntos_I2_Match_index);

void getVectIndicesMatchesKeyPoints(std::vector<cv::DMatch>& matches,
        std::vector<cv::Point2f> puntos_I1,
        std::vector<cv::Point2f> puntos_I2,
        std::vector<cv::Point2f>& puntos_I1_Match,
        std::vector<cv::Point2f>& puntos_I2_Match,
        std::vector<int>& puntos_I1_Match_index,
        std::vector<int>& puntos_I2_Match_index);

void getVectPointsInliners(std::vector<int>& vecIndicesValidos_BH,
        std::vector<cv::Point2f>& puntos_I1_Match,
        std::vector<cv::Point2f>& puntos_I2_Match,
        std::vector<cv::Point2f>& puntos_I1_Match_Inliners,
        std::vector<cv::Point2f>& puntos_I2_Match_Inliners);

void getVectKeyLineasInliners(std::vector<int>& vecIndicesValidos_BH,
        std::vector<cv::line_descriptor::KeyLine>& keylines1_Match,
        std::vector<cv::line_descriptor::KeyLine>& keylines2_Match,
        std::vector<cv::line_descriptor::KeyLine>& keylines1_Match_Inliners,
        std::vector<cv::line_descriptor::KeyLine>& keylines2_Match_Inliners);

void getPuntosAleatoriosDelVectDMatch(int numLineas,
        std::vector<cv::DMatch>& matches,
        std::vector<cv::Point2f> puntos_I1,
        std::vector<cv::Point2f> puntos_I2,
        std::vector<cv::Point2f>& puntosSelecI1,
        std::vector<cv::Point2f>& puntosSelecI2,
        std::vector<int>& p_vecIndMatchImg1,
        std::vector<int>& p_vecIndMatchImg2);

bool isThePointInTheRange(cv::Point2f & point, cv::Size & sizeImg);

std::vector<cv::Point2f> mergePointVectors(std::vector<cv::Point2f>& vectPoints_A,
        std::vector<cv::Point2f>& vectPoints_B);

std::vector<std::pair<int, int> > generaCorrespondenciaPuntosInterseccionLineas_dentroImagen(
        std::vector<cv::line_descriptor::KeyLine>& vectLines_test,
        std::vector<cv::line_descriptor::KeyLine>& vectLines_refe,
        std::vector<cv::Point2f>& vectPuntIntersec_test,
        std::vector<cv::Point2f>& vectPuntIntersec_refe,
        std::vector<cv::line_descriptor::KeyLine>& vectLines_tran,
        std::vector<cv::Point2f>& vectPuntIntersec_tran,
        cv::Size sizeImg);

void generaCorrespondenciaPuntosInterseccionLineas(std::vector<cv::line_descriptor::KeyLine>& vectLines_test,
        std::vector<cv::line_descriptor::KeyLine>& vectLines_refe,
        std::vector<cv::Point2f>& vectPuntIntersec_test,
        std::vector<cv::Point2f>& vectPuntIntersec_refe,
        cv::Point2f puntoCentroImg, double radioTolerancia);

std::vector<std::pair<int, int> > generaCorrespondenciaPuntosInterseccionLineas_PI_umbralError(
        std::vector<cv::line_descriptor::KeyLine>& vectLines_test,
        std::vector<cv::line_descriptor::KeyLine>& vectLines_refe,
        std::vector<cv::Point2f>& vectPuntIntersec_test,
        std::vector<cv::Point2f>& vectPuntIntersec_refe,
        std::vector<cv::line_descriptor::KeyLine>& vectLines_tran,
        std::vector<cv::Point2f>& vectPuntIntersec_tran,
        double umbralErrorDist, cv::Point2f puntoCentroImg, double radioTolerancia,
        std::vector<double>& vectDistError);

//std::vector<std::pair<int, int> > generaCorrespondenciaPuntosInterseccionLineas_radio(
//        std::vector<cv::line_descriptor::KeyLine>& vectLines_test,
//        std::vector<cv::line_descriptor::KeyLine>& vectLines_refe,
//        cv::Point2f pc, double radioTolerancia, int distanciaRefeTest_PI,
//        std::vector<cv::Point2f>& vectPuntIntersec_test,
//        std::vector<cv::Point2f>& vectPuntIntersec_refe);

//void getValidIntersectionPointsFromLinesTransformation(cv::Size sizeImg, double validRatio,
//        std::vector<cv::line_descriptor::KeyLine>& vectLines_test,
//        std::vector<cv::line_descriptor::KeyLine>& vectLines_refe,
//        std::vector<cv::line_descriptor::KeyLine>& vectLines_tran,
//        std::vector<cv::Point2f>& vectPuntIntersec_test,
//        std::vector<cv::Point2f>& vectPuntIntersec_refe,
//        std::vector<cv::Point2f>& vectPuntIntersec_tran,
//        bool bandCheck);

bool isAValidPoint(cv::Point2f& punto);

double getAngleBetween2Lines(cv::line_descriptor::KeyLine& linea_test, cv::line_descriptor::KeyLine& linea_refe, bool bandShowMess);

cv::Point2f getPuntoIntersecLineas(cv::line_descriptor::KeyLine& Linea1, cv::line_descriptor::KeyLine& Linea2);
//cv::Point2f getPuntoInterParam(double m1, double b1, double m2, double b2);

void getParametrosEcuacionRecta_Explicitos(double &pendiente, double &b, cv::line_descriptor::KeyLine& Linea);

void plotLineas(cv::Mat imageInput, cv::Mat& imageOutput,
        std::vector<cv::line_descriptor::KeyLine> lines,
        std::vector<cv::Scalar>& vec_colores, std::string extraTitulo = "",
        bool bandShowLabel = true);

void plotLineasRectas(cv::Mat imageInput, cv::Mat& imageOutput,
        std::vector<cv::line_descriptor::KeyLine> lines,
        std::vector<cv::Scalar>& vec_colores,
        std::string extraTitulo = "", bool bandLabel = true, int factorLineaRecta = 100);
//void plotLineas(cv::Mat& imageInput, cv::Mat& imageOutput, std::vector<cv::line_descriptor::KeyLine>& lines, std::vector<cv::Scalar>& vec_colores);

void plotPuntoInterseccion(cv::Mat& imageInput, cv::Mat& imageOutput, std::vector<cv::Point2f>& puntosIntersec, std::vector< std::vector<int> >& matLineasIntersec);

void removeElementosDeVectorAleatorio(std::vector<int>& vecIndAleatoriosMatch, std::vector<cv::DMatch>& vecMatches);
//bool isOK_Intersecciones(std::vector<cv::Point2f>& vecPoints1, std::vector<cv::Point2f>& vecPoints2);

cv::Point2f getPuntoMedioLinea(cv::line_descriptor::KeyLine& Linea);
double getDistanciaEntrePuntos(cv::Point2f& punto1, cv::Point2f& punto2);
std::vector<double> getDistanciaEntrePuntos(std::vector<cv::Point2f>& punto1, std::vector<cv::Point2f>& punto2);
std::vector<double> getVectDistanciaEntrePuntos(std::vector<cv::Point2f>& vectPuntos_test, std::vector<cv::Point2f>& vectPuntos_refe);
double getMinDistanciaEntreSegmentosLinea(cv::line_descriptor::KeyLine& linea_tran, cv::line_descriptor::KeyLine& linea_refe);

int string2int(std::string cadena);
std::string int2string(int n);
std::string double2string(double n);
std::vector<std::string> vecNum2vecString(std::vector<int> vecN);

cv::Mat concatenaMatriz_Horizontal(cv::Mat& image1, cv::Mat& image2);
cv::Mat concatenaMatriz_Vertical(cv::Mat& image1, cv::Mat& image2);

//std::vector<cv::Point2f> getVectorPuntos_Ini_Final_Lineas(std::vector<cv::line_descriptor::KeyLine> keyLines);
//std::vector<cv::line_descriptor::KeyLine> getVectorLineas_Ini_Final_Puntos(std::vector<cv::Point2f> vecPuntos);

std::vector<cv::Point2f> getVectorPunto_Medio_LineasdioLineas(std::vector<cv::line_descriptor::KeyLine> keyLines);
std::vector<cv::Point2f> getSubVect(int ini, int fin, std::vector<cv::Point2f>& vector);
std::vector<cv::Point2f> keypoints2Points2f(std::vector<cv::KeyPoint>& vecKeypoints);

void plotPuntos(cv::Mat& imageInput, cv::Mat& imageOutput,
        std::vector<cv::KeyPoint>& vecKeypoints,
        std::vector<cv::Scalar>& vec_colores, std::vector<std::string>& vecEtiquetas, std::string extraTitulo = "",
        bool bandUseEtiqueta = true, int grosorLinea = 4);

void plotPuntos(cv::Mat& imageInput, cv::Mat& imageOutput,
        std::vector<cv::Point2f>& puntos, std::vector<cv::Scalar>& vec_colores,
        std::vector<std::string>& vecEtiquetas, std::string extraTitulo = "",
        bool bandUseEtiqueta = true, int grosorLinea = 4);

std::vector<double> calculateVectDistanciaEntrePuntos(bool bandImprimeDist, std::vector<cv::Point2f>& puntos1, std::vector<cv::Point2f>& puntos2);
void getParametrosEcuacionRecta_Implicitos(cv::line_descriptor::KeyLine& Linea, double &A, double &B, double &C);
double getDistanciaEntrePuntoRecta(cv::Point2f& punto, cv::line_descriptor::KeyLine& Linea);
std::vector<double> getVectDistanciaEntrePuntosRecta(std::vector<cv::Point2f>& vecPuntos, cv::line_descriptor::KeyLine& Linea);
std::vector<cv::Point2f> muestreaNPuntosRespectoLineas(int numPuntos, cv::line_descriptor::KeyLine& Linea);
std::vector<cv::Scalar> generaVectorColorDuplicado(int numDuplicas, cv::Scalar color);

void addTextoEnPuntoImagen(cv::Mat& mat, std::string label, cv::Point2f punto, cv::Scalar color, int tamTexto = 1, int grosorTexto = 2);

cv::Mat plotMatchesInliners_lineas(int idConcat, bool bandGrayScale, bool bandLineasSimilares,
        cv::Mat image1, std::vector<cv::line_descriptor::KeyLine>& keylines1_Match,
        cv::Mat image2, std::vector<cv::line_descriptor::KeyLine>& keylines2_Match,
        std::vector<cv::Scalar>& vec_colores);

cv::Mat plotMatchesInliners_puntos(int idConcat, bool bandGrayScale, bool bandPuntosSimilares,
        cv::Mat image1, std::vector<cv::Point2f>& points1_Match,
        cv::Mat image2, std::vector<cv::Point2f>& points2_Match,
        std::vector<cv::Scalar>& vec_colores_BH, int sizePoint = 4);

void printDMatch(std::vector<cv::DMatch>& matches);

void printVectLineas(std::vector<cv::line_descriptor::KeyLine>& vectLineas);

void printCVMatriz_comoVariableMatlab(cv::Mat mat, std::string nombre);
void writeCVMatriz_comoVariableMatlab(cv::Mat mat, std::string nombre, std::ofstream& outfile);
void writeVectorCVMatriz_comoVariableMatlab(std::vector<cv::Mat> vectMat, std::string nombre, std::ofstream& outfile);

std::vector<cv::Scalar> generateVectorSpecificColor(int numElementos, int R, int G, int B);

void modificaResImg_EnBaseACols(bool bandChangSize, cv::Mat& r_ImagenOrigen, cv::Mat& r_ImagenDestino, int widthWanted);
void modificaResImg_EnBaseAFils(bool bandChangSize, cv::Mat& r_ImagenOrigen, cv::Mat& r_ImagenDestino, int heightWanted);
void modificaResolucionImg(cv::Mat& r_ImagenOrigen, cv::Mat& r_ImagenDestino, int width, int height);

void extraePointsOfKeypoints(std::vector<cv::KeyPoint>& keypoints_test,
        std::vector<cv::KeyPoint>& keypoints_refe,
        std::vector<cv::Point2f>& puntos_test,
        std::vector<cv::Point2f>& puntos_refe);

double getRadioTolerancia(cv::Size imgSize, double factorRadioTolerancia, cv::Point2f& puntoCentroImg);

cv::Mat plotResults_lineas(cv::Mat image_refe, cv::Mat image_test,
        int idFrame_refe, int idFrame_test,
        std::vector<cv::line_descriptor::KeyLine>& keylines_refe_Detected,
        std::vector<cv::line_descriptor::KeyLine>& keylines_test_Detected,
        std::vector<cv::line_descriptor::KeyLine>& keylines_refe_Match,
        std::vector<cv::line_descriptor::KeyLine>& keylines_test_Match,
        std::vector<cv::line_descriptor::KeyLine>& keylines_refe_Inlier,
        std::vector<cv::line_descriptor::KeyLine>& keylines_test_Inlier,
        bool bandLineasSimilares = true);

cv::Mat plotResults_puntos(cv::Mat image_refe, cv::Mat image_test,
        int idFrame_refe, int idFrame_test,
        std::vector<cv::KeyPoint>& puntos_refe_Detected,
        std::vector<cv::KeyPoint>& puntos_test_Detected,
        std::vector<cv::Point2f>& puntos_refe_Match,
        std::vector<cv::Point2f>& puntos_test_Match,
        std::vector<cv::Point2f>& puntos_refe_Inlier,
        std::vector<cv::Point2f>& puntos_test_Inlier,
        bool bandPuntosSimilares = true);


void getMaxMinCoordinates(std::vector<cv::Point2f>& trans_corners,
            double& maxCols_corner, double& maxRows_corner,
            double& minCols_corner, double& minRows_corner);
#endif /* LINEFUNCTIONS_HPP */


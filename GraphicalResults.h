/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   GraphicalResults.h
 * Author: luis
 *
 * Created on 20 de agosto de 2018, 06:19 PM
 */

#ifndef GRAPHICALRESULTS_H
#define GRAPHICALRESULTS_H

#include "Struct_Parametros.hpp"
#include "Struct_Resultados.hpp"
#include "TimeControl.hpp"
#include "LinePointFunctions.hpp"
#include "BasicFunctions.hpp"
#include "ImageStitching.hpp"
#include "SavingResultsMatio.hpp"
#include "QuadTree_Keypoints.h"

class GraphicalResults {
public:
    GraphicalResults();

    GraphicalResults(ParametrosEjecucion& parametros);

    virtual ~GraphicalResults();

    void controlaVentanas(Struct_Matching& structMatch, ImageStitching& stitcher, Struct_ResultsMATIO& struResultMatio);

    void plotingFeaturesDetected(Struct_Matching& structMatch, Struct_ResultsMATIO& struResultMatio);

    void plotingMatchesRANSAC(Struct_Matching& structMatch, Struct_ResultsMATIO& struResultMatio);

    void ploting_ValidIntersectionPoints(Struct_Matching & structMatch);

    void plotingFeaturesInliersTransformed(ImageStitching& stitcher, Struct_Matching& structMatch);

    void plotingMosaicoParImagenes(ImageStitching& stitcher, Struct_Matching& structMatch);

    void plotingQuadTreeResults(Struct_QuadTreeKP& structQuadTree);

private:

    ParametrosEjecucion parametros;
    bool bandNoWaitFirstWindow;
};
/* ------------------------------------------- */
void muestraVentanaCanvas(cv::Mat& Canvas, std::string nombreVentanaCanvas,
        bool bandAplicarSiempre = false, int anchoDeseado = 960, int altoDeseado = 700);

/* ------------------------------------------- */

#endif /* GRAPHICALRESULTS_H */


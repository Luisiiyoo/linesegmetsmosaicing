/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: luis
 *
 * Created on 6 de diciembre de 2018, 09:34 PM
 */

#include <dirent.h> 
#include <iosfwd> 
#include <sys/stat.h> 
#include <dirent.h>
#include <errno.h>

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string.h>
#include <cstdlib>
#include<vector>
#include<algorithm>
#include <sstream>


//#include "BasicFunctions.hpp"


using namespace std;

static std::string int2string(int num) {
    // Converting number to string 
    std::stringstream ss;
    ss << num;
    return ss.str();
}// function 

static std::string double2string(double num) {
    // Converting number to string 
    std::stringstream ss;
    ss << num;
    return ss.str();
}// function 

static std::vector<std::string> getFilesInDirectory(const std::string directory) {
    /* Vector de Directorios */
    std::vector<std::string> vectFullFileName;
    std::vector<std::string> vectFileName;
#ifdef WINDOWS
    HANDLE dir;
    WIN32_FIND_DATA file_data;

    if ((dir = FindFirstFile((directory + "/*").c_str(), &file_data)) == INVALID_HANDLE_VALUE)
        return; /* No files found */

    do {
        const string file_name = file_data.cFileName;
        const string full_file_name = directory + "/" + file_name;
        const bool is_directory = (file_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0;

        if (file_name[0] == '.')
            continue;

        if (is_directory)
            continue;

        vectFullFileName.push_back(full_file_name);
    } while (FindNextFile(dir, &file_data));

    FindClose(dir);
#else
    DIR *dir;
    class dirent *ent;
    class stat st;

    dir = opendir(directory.c_str());
    while ((ent = readdir(dir)) != NULL) {
        const std::string file_name = ent->d_name;
        const std::string full_file_name = directory + "/" + file_name;

        /* Si es un archivo que comienza con . lo omite*/
        if (file_name[0] == '.')
            continue;

        if (stat(full_file_name.c_str(), &st) == -1)
            continue;

        const bool is_directory = (st.st_mode & S_IFDIR) != 0;

        /* Si es un subDirectorio lo omite*/
        if (is_directory)
            continue;

        vectFullFileName.push_back(full_file_name);
        vectFileName.push_back(file_name);
    }
    closedir(dir);

    /* Ordenando el vector para trabjar deacuerdo al nombre especificado por cada archivo*/
    std::sort(vectFullFileName.begin(), vectFullFileName.end());
    std::sort(vectFileName.begin(), vectFileName.end());

    return vectFileName;
#endif
} // GetFilesInDirectory

static void printVectorStrings_Directorios(std::vector<std::string> vect, std::string cad) {
    cout << cad << endl;
    if (vect.size() > 0) {
        for (int ii = 0; ii < vect.size(); ii++) {
            cout << ii << " : " << vect[ii] << endl;
        } // for
    } else {
        cout << "  Wrong directory or there are no files !!" << endl;
    } // if - else
} // function 

static bool renameFiles(std::string newName_Format, int numZeros_Format,
        std::string fullPathFiles, std::vector<std::string>& vectNamesImages) {

    bool bandOK = true;
    // < Checking if one element of the list has the same name desired >
    std::string cadAux = vectNamesImages[0];

    // Buscando nombre
    std::size_t found = cadAux.find(newName_Format);
    if (found != std::string::npos) {
        std::cout << " The files have relative name with the newName: \"" << newName_Format << "\"\n";
        bandOK = false;
    } else {
        // finding extension
        found = cadAux.find('.');
        std::string extension = cadAux.substr(found, cadAux.length() - found);

        // Rename      
        std::string oldName;
        std::string newName;
        std::string numberIter;

        cout << " ------------------- " << endl;
        for (int ff = 0; ff < vectNamesImages.size(); ff++) {
            oldName = vectNamesImages[ff]; // getting the old name

            // Converting number to string             
            numberIter = int2string(ff);

            newName = newName_Format + std::string(numZeros_Format - numberIter.length(), '0') +
                    numberIter + extension;

            // rename
            cout << ff << " : oldName: " << oldName << " ... newName: " << newName << endl;
            oldName = fullPathFiles + oldName;
            newName = fullPathFiles + newName;

            int result = rename(oldName.c_str(), newName.c_str());
            if (result == 0) {
                puts("\t File successfully renamed");
            } else {
                perror("Error renaming file");
                return false;
            }// if - else             
        } // for 
    }// if - else
    return bandOK;
} // function 

static int findString(std::string cad1, std::string cad2) {
    std::size_t found = cad1.find(cad2);
    if (found == std::string::npos) {
        return -1;
    } else {
        return found;
    }
}// function

static std::string getNameSequence(std::string& fullPathFiles, std::vector<std::string>& vectNamesImages) {
    // < Important: the name of the image sequence must be similar to below > 
    // < name_%04d.extension>
    std::string nameImageSequence = "";

    int numImages = vectNamesImages.size();

    if (numImages < 2) {
        cout << " Error : No enought number of images. " << endl;
        //    } else if (numImages == 2) {
        //        cout << " Image Mosaicing using two images. " << endl;
    } else {
        cout << " Image Mosaicing using " << int2string(numImages) << " images. " << endl;

        // obteniendo primeras cadenas
        std::string cad_1 = vectNamesImages[0];
        std::string cad_2 = vectNamesImages[1];

        // obteniendo nombre de la secuencia
        std::string cadAux, extension;

        // Conditions to check if the image secuence names are valid
        bool bandLengthSimilar = (cad_1.length() == cad_2.length()); // < Longitud similar >

        int posSlash_1 = findString(cad_1, "_");
        int posSlash_2 = findString(cad_2, "_");
        bool bandSlash = (posSlash_1 == posSlash_2); // < misma posicion de Slash >


        std::string subcad_1 = cad_1.substr(0, posSlash_1 + 1);
        std::string subcad_2 = cad_2.substr(0, posSlash_2 + 1);
        bool bandSameSubstring = !(subcad_1.compare(subcad_2)); // < misma substring >

        int posExtension_1 = findString(cad_1, ".");
        int posExtension_2 = findString(cad_2, ".");
        bool bandSameExtension = (posExtension_1 == posExtension_2); // < Misma extension >       

        // < Revisando si inicia en 0 o en 1 >
        int numDigitos = posExtension_1 - posSlash_1 - 1;
        std::string primeraNumeracion = cad_1.substr(posSlash_1 + 1, numDigitos);
        bool c0 = !primeraNumeracion.compare(std::string(numDigitos, '0'));
        bool c1 = !primeraNumeracion.compare(std::string(numDigitos - 1, '0') + "1");
        bool bandInicioBien = c0 || c1;


        bool isOK = false;
        extension = cad_1.substr(posExtension_1, cad_1.length() - posExtension_1);

        if (bandLengthSimilar && bandSlash && bandSameSubstring && bandSameExtension && bandInicioBien) {
            cadAux = cad_1.substr(0, posSlash_1 + 1); // subCadena
            isOK = true;

            // Checking all the files if they have relative names
            int valIni = (c0) ? 0 : 1;
            int indicadorLimite = 10;
            int contIndicadorLimite = 1;
            for (int ff = 0; ff < vectNamesImages.size(); ff++) {
                //if (isItInTheString(vectNamesImages[ff], cadAux) < 0) {

                if (ff >= indicadorLimite) {
                    indicadorLimite = indicadorLimite * 10;
                    contIndicadorLimite++;
                }// if 

                std::string cadd = cadAux + std::string(numDigitos - contIndicadorLimite, '0') + int2string(valIni)+extension;
                if (vectNamesImages[ff].compare(cadd)) {
                    isOK = false;
                    break;
                } // if 

                valIni++;
            } // for 
        }// if 

        // < ---------------------------------------- >

        if (isOK) {
            // std::string(numZeros_Format - numberIter.length(), '0') +
            //            cout<<vectNamesImages[0]<<endl;
            //            cout<<posExtension_1<< " ----- "<< posSlash_1<<endl;
            nameImageSequence = cadAux + "%0" + int2string(numDigitos) + "d" + extension;
            cout << "\n Name of image sequence : " << nameImageSequence << endl;
        } else {
            // < Renombrar >
            bool bandInvalidResponse = true;

            char response;
            while (bandInvalidResponse) {
                cout << " There is not a valid name sequence. \n Do you want to rename all the files with \"imgseq_%04."<<extension<<"\" (Y/n)?" << endl;
                cin>>response;

                // < Positive response >
                if (response == 'y' || response == 'Y') {
                    // < Renombrar >
                    numDigitos = 4;
                    std::string cadRename;
                    bool bandErrorRename = true;
                    int contIntentos = 0;
                    while (bandErrorRename) {
                        cadRename = "imgseq" + int2string(contIntentos) + "_";
                        bandErrorRename = !(renameFiles(cadRename, numDigitos, fullPathFiles, vectNamesImages));
                        contIntentos++;
                    }// while 
                    nameImageSequence = cadRename + "%0" + int2string(numDigitos) + extension;
                    bandInvalidResponse = false;
                }// if 

                    // < Negative response >
                else if (response == 'n' || response == 'N') {
                    // < Salir >
                    bandInvalidResponse = false;
                    nameImageSequence = "Error";
                } // if else
            } //while 
        }// if - else
    } // if - else if  - else

    return nameImageSequence;
} // function

int main(int argc, char** argv) {
    // < Definiendo ruta >
    std::string newName = "inspection_";
    int numZeros = 4;


    //std::string fullPathFiles = "/home/luis/Documentos/Datasets_Mosaicos/INAOE_DATASET/INAOE_SecuenciaImagenes/Turbina/";
    //std::string fullPathFiles = "/home/luis/Descargas/Prueba/";
    std::string fullPathFiles = "/home/luis/Descargas/s/";

    // < Obteniendo lista de archivos en el directorio>
    std::vector<std::string> vectNamesImages = getFilesInDirectory(fullPathFiles);

    // Imprimiendo lista de archivos
    printVectorStrings_Directorios(vectNamesImages, fullPathFiles);


    // < Renombrar archivos >
    //    renameFiles(newName, numZeros, fullPathFiles, vectNamesImages);

    // < Obtener nombre de secuencia >
    getNameSequence(fullPathFiles, vectNamesImages);

    return 0;
}


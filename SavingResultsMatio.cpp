/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SavingResultsMatio.cpp
 * Author: luis
 * 
 * Created on 10 de junio de 2018, 02:04 PM
 */

#include "SavingResultsMatio.hpp"
#include "BasicFunctions.hpp"
#include "Struct_Parametros.hpp"
#include "Struct_Resultados.hpp"

#include <opencv2/xfeatures2d.hpp>
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include <opencv2/line_descriptor.hpp>
#include "opencv2/core/utility.hpp"
#include <opencv2/imgproc.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/highgui.hpp>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>
#include <fstream>
#include <string.h>

SavingResultsMatio::SavingResultsMatio(ParametrosEjecucion& parametros) {
    this->parametros = parametros;
} //

SavingResultsMatio::SavingResultsMatio(const SavingResultsMatio& orig) {
} //

SavingResultsMatio::~SavingResultsMatio() {
} //

void SavingResultsMatio::closeMatio_FilePointer(mat_t* matio_FilePointer) {
    //Close file
    Mat_Close(matio_FilePointer);
} //function

mat_t* SavingResultsMatio::createMatio_FilePointer(std::string nombreArchivo) {
    mat_t* matio_FilePointer;

    std::size_t foundVal = nombreArchivo.find(".mat");
    if (foundVal == std::string::npos) {
        nombreArchivo = nombreArchivo + ".mat";
    }//if
    const char *FILENAME = nombreArchivo.c_str();

    //Open file
    matio_FilePointer = Mat_CreateVer(FILENAME, NULL, MAT_FT_MAT5);

    return matio_FilePointer;
} //function

void SavingResultsMatio::write_Int(string nombreVar, int valorVariable, mat_t *matio_FilePointer) {
    //integer
    char* fieldname2 = string2char(nombreVar);
    int myinteger = valorVariable;
    size_t dim2[2] = {1, 1};

    matvar_t *variableInt = Mat_VarCreate(fieldname2, MAT_C_INT32, MAT_T_INT32, 2, dim2, &myinteger, 0);
    Mat_VarWrite(matio_FilePointer, variableInt, MAT_COMPRESSION_NONE); //or MAT_COMPRESSION_ZLIB
    Mat_VarFree(variableInt);
}//function

void SavingResultsMatio::write_Int_vec1D(string nombreVar, std::vector<int>& vec1D_valorVariable, mat_t *matio_FilePointer) {
    int fils = vec1D_valorVariable.size();
    int array1D[fils];

    for (int i = 0; i < vec1D_valorVariable.size(); i++) {
        array1D[i] = vec1D_valorVariable[i];
    }//for

    //integer
    char* fieldname = string2char(nombreVar);
    size_t dim[1] = {fils};

    matvar_t *variable_vec1D = Mat_VarCreate(fieldname, MAT_C_INT32, MAT_T_INT32, 1, dim, &array1D, 0);
    Mat_VarWrite(matio_FilePointer, variable_vec1D, MAT_COMPRESSION_NONE); //or MAT_COMPRESSION_ZLIB
    Mat_VarFree(variable_vec1D);
}//function

void SavingResultsMatio::write_Double(string nombreVar, double valorVariable, mat_t *matio_FilePointer) {
    //double
    char* fieldname3 = string2char(nombreVar);
    double mydouble = valorVariable;
    size_t dim3[2] = {1, 1};
    matvar_t *variableDouble = Mat_VarCreate(fieldname3, MAT_C_DOUBLE, MAT_T_DOUBLE, 2, dim3, &mydouble, 0);
    Mat_VarWrite(matio_FilePointer, variableDouble, MAT_COMPRESSION_NONE); //or MAT_COMPRESSION_ZLIB
    Mat_VarFree(variableDouble);
}//function

void SavingResultsMatio::write_Double_vec1D(string nombreVar, std::vector<double>& vec1D_valorVariable, mat_t *matio_FilePointer) {
    int fils = vec1D_valorVariable.size();
    double array1D[fils];

    for (int i = 0; i < vec1D_valorVariable.size(); i++) {
        array1D[i] = vec1D_valorVariable[i];
    }//for

    //integer
    char* fieldname = string2char(nombreVar);
    size_t dim[1] = {fils};

    matvar_t *variable_vec1D = Mat_VarCreate(fieldname, MAT_C_DOUBLE, MAT_T_DOUBLE, 1, dim, &array1D, 0);
    Mat_VarWrite(matio_FilePointer, variable_vec1D, MAT_COMPRESSION_NONE); //or MAT_COMPRESSION_ZLIB
    Mat_VarFree(variable_vec1D);
}//function

void SavingResultsMatio::write_Bool(string nombreVar, bool valorVariable, mat_t *matio_FilePointer) {
    //bool
    char* fieldname5 = string2char(nombreVar);
    bool mybool = valorVariable;
    size_t dim5[2] = {1, 1};
    matvar_t *variableBool = Mat_VarCreate(fieldname5, MAT_C_INT16, MAT_T_INT16, 2, dim5, &mybool, MAT_F_LOGICAL);
    Mat_VarWrite(matio_FilePointer, variableBool, MAT_COMPRESSION_NONE); //or MAT_COMPRESSION_ZLIB
    Mat_VarFree(variableBool);
}//function

void SavingResultsMatio::write_Bool_vec1D(string nombreVar, std::vector<bool>& vec1D_valorVariable, mat_t *matio_FilePointer) {
    int fils = vec1D_valorVariable.size();
    bool array1D[fils];

    for (int i = 0; i < vec1D_valorVariable.size(); i++) {
        array1D[i] = vec1D_valorVariable[i];
    }//for

    //integer
    char* fieldname = string2char(nombreVar);
    size_t dim[1] = {fils};

    matvar_t *variable_vec1DInt = Mat_VarCreate(fieldname, MAT_C_INT16, MAT_T_INT16, 1, dim, &array1D, 0);
    Mat_VarWrite(matio_FilePointer, variable_vec1DInt, MAT_COMPRESSION_NONE); //or MAT_COMPRESSION_ZLIB
    Mat_VarFree(variable_vec1DInt);
}//function

void SavingResultsMatio::write_String(string nombreVar, string valorVariable, mat_t *matio_FilePointer) {
    //string
    char* fieldname1 = string2char(nombreVar);
    char* mystring = string2char(valorVariable);
    //size_t dim1[2] = {1, sizeof (mystring) / sizeof (mystring[0])};
    size_t dim1[2] = {1, valorVariable.size()};

    matvar_t *variableString = Mat_VarCreate(fieldname1, MAT_C_CHAR, MAT_T_UTF8, 2, dim1, mystring, 0);
    Mat_VarWrite(matio_FilePointer, variableString, MAT_COMPRESSION_NONE); //or MAT_COMPRESSION_ZLIB
    Mat_VarFree(variableString);
}//function

char* SavingResultsMatio::string2char(std::string cadena) {

    char * writable = new char[cadena.size() + 1];
    std::copy(cadena.begin(), cadena.end(), writable);
    writable[cadena.size()] = '\0'; // don't forget the terminating 0

    // don't forget to free the string after finished using it
    //delete[] writable;
    return writable;
}//function

void SavingResultsMatio::saveResults(Struct_ResultsMATIO& strutResultsMatio) {
    /* Revisando Directorio */
    bool bandDirectorioOK = makeSureDirectoryExist(this->parametros.SaveResults.dirResultados_FileMat);

    if (bandDirectorioOK) {
        std::string fullName = this->parametros.SaveResults.dirResultados_FileMat +
                this->parametros.SaveResults.nameExecution;

        /* < Creando y Abriendo Archivo MATIO > */
        mat_t* matio_FilePointer = createMatio_FilePointer(fullName);

        if (matio_FilePointer == NULL) {
            cout << "\n¡¡Error: The MATIO File was not created !!" << endl;
            cout << "Dir: " << this->parametros.SaveResults.dirResultados_FileMat << endl << endl;
            cout << "File: " << this->parametros.SaveResults.nameExecution << endl << endl;
            exit(0);
        } // if 

        /* < Guardando variables en el archivo creado > */
        saveImportantVariables(matio_FilePointer, strutResultsMatio);

        /* <Cerrando Archivo MATIO> */
        closeMatio_FilePointer(matio_FilePointer);

    } else {
        cout << "\n ** Error: It's no possible use the directory given ** " << endl;
    }// if 


} // function 

void SavingResultsMatio::saveImportantVariables(mat_t* matio_FilePointer, Struct_ResultsMATIO& struResultMatio) {
    /* Guarda Variables*/
    write_Int("primitive_bandLineas", (int) parametros.Features.bandHacerConLineas, matio_FilePointer);

    write_Double("exec_tiempoTotEjecucion", struResultMatio.totalRunTime, matio_FilePointer);

    write_Int("ransac_numIteraciones", parametros.RANSAC.numIteraciones, matio_FilePointer);
    write_Int("ransac_numMuestras", parametros.RANSAC.numMinMuestras, matio_FilePointer);
    write_Int("ransac_umbralDistancia", parametros.RANSAC.umbralDistancia, matio_FilePointer);
    write_Int("ransac_umbralAngulo", parametros.RANSAC.umbralAngulo, matio_FilePointer);
    write_Double("ransac_radioDeInliners", parametros.RANSAC.radioDeInliners, matio_FilePointer);

    write_String("video_nombre", struResultMatio.nomVideo, matio_FilePointer);
    write_Int("video_numFramesTotal", struResultMatio.numFramesTotal, matio_FilePointer);
    write_Int("video_numFramesIterations", struResultMatio.numFramesIterations, matio_FilePointer);
    write_Double("video_framesPerSeconds", struResultMatio.framesPerSecond, matio_FilePointer);
    write_Double("video_duracionSeconds", struResultMatio.videoDuration, matio_FilePointer);

    write_Int("video_bandFijarImagen", (int) parametros.ControlVent.bandDynamicKeyFrame, matio_FilePointer);
    write_Double("video_porcMinMantenerImg", parametros.ControlVent.porcMinMantenerImg, matio_FilePointer);

    write_Int("matching_maxNumFeatures", parametros.Features.maxNumFeatures, matio_FilePointer);
    write_String("matching_detectorDescritor", struResultMatio.nomDetecDesc, matio_FilePointer);
    write_String("matching_Matcher", struResultMatio.nomMatcher, matio_FilePointer);
    write_String("ransac_TypeModel", struResultMatio.typeModel, matio_FilePointer);
    write_String("blending_Blending", struResultMatio.typeBlending, matio_FilePointer);


    /* <Tiempos de ejecucion desglosados> */
    for (int ii = 0; ii < struResultMatio.vectTiemposEjecucion_label.size(); ii++) {
        std::vector<double> vect = getColFromVector2D(struResultMatio.vectTiemposEjecucion_Total_Iter, ii);
        write_Double_vec1D(struResultMatio.vectTiemposEjecucion_label[ii], vect, matio_FilePointer);
    } // for

    /* Detalles Mosaico*/
    write_Double("mosaico_runtimeStitchingBlending", struResultMatio.runtimeStitchingBlending, matio_FilePointer);
    write_Int("mosaico_numImgsUtilizadasMosaicing", struResultMatio.numImgsUtilizadasMosaicing, matio_FilePointer);

    write_Int("mosaico_width", struResultMatio.MosaicoImg.cols, matio_FilePointer);
    write_Int("mosaico_height", struResultMatio.MosaicoImg.rows, matio_FilePointer);

    /* <-------------------------------------------------------------------> */
    write_Double_vec1D("vectPorcentajeMatch", struResultMatio.vectPorcentajeMatchInliers, matio_FilePointer);
    write_Double_vec1D("vectPorcentajeDesplz", struResultMatio.vectPorcentajeDesplModel, matio_FilePointer);
    /* <-------------------------------------------------------------------> */

    write_Int_vec1D("vectNumFeaturesDetected", struResultMatio.vectNum_Features_iter, matio_FilePointer);
    write_Int_vec1D("vectNumMatches", struResultMatio.vectNum_Matches_iter, matio_FilePointer);
    write_Int_vec1D("vectNumInliers", struResultMatio.vectNum_Inliers_iter, matio_FilePointer);
    write_Int_vec1D("vectNumPuntosValidos", struResultMatio.vectNum_PuntosValidos_iter, matio_FilePointer);

    // QuadTree
    write_Bool("quadTree_BandUsedQuadTree", parametros.Features.bandUseQuadTreeKP, matio_FilePointer);
    write_Int_vec1D("vectIdModelUsed", struResultMatio.vectIdModelUsed, matio_FilePointer);

    // Error Puntos validos 
    write_Double_vec1D("vectMean_DistError_Inliers", struResultMatio.vectMean_distanceInliers_iter, matio_FilePointer);
    write_Double_vec1D("vectSD_DistError_Inliers", struResultMatio.vectSD_distanceInliers_iter, matio_FilePointer);

    // Error Features 
    write_Double_vec1D("vectMean_Features_DistError_Inliers", struResultMatio.vectMean_Feature_distanceInliers_iter, matio_FilePointer);
    write_Double_vec1D("vectSD_Features_DistError_Inliers", struResultMatio.vectSD_Feature_distanceInliers_iter, matio_FilePointer);
    write_Double_vec1D("vectMean_Features_AngleError_Inliers", struResultMatio.vectMean_Feature_angleInliers_iter, matio_FilePointer);
    write_Double_vec1D("vectSD_Features_AngleError_Inliers", struResultMatio.vectSD_Feature_angleInliers_iter, matio_FilePointer);

    // <Extra>
    write_Int_vec1D("vectNumFeaturesDetected_lines", struResultMatio.vectNumDetect_Lineas, matio_FilePointer);
    write_Int_vec1D("vectNumFeaturesDetected_points", struResultMatio.vectNumDetect_Puntos, matio_FilePointer);
    write_Int_vec1D("vectNumMatches_lines", struResultMatio.vectNumMatch_Lineas, matio_FilePointer);
    write_Int_vec1D("vectNumMatches_points", struResultMatio.vectNumMatch_Puntos, matio_FilePointer);
    write_Int_vec1D("vectNumInliers_lines", struResultMatio.vectNumInlier_Lineas, matio_FilePointer);
    write_Int_vec1D("vectNumInliers_points", struResultMatio.vectNumInlier_Puntos, matio_FilePointer);
} // function
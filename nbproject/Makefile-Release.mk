#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/BasicFunctions.o \
	${OBJECTDIR}/FeatureDetection.o \
	${OBJECTDIR}/FeatureMatching.o \
	${OBJECTDIR}/FeatureMosaicing.o \
	${OBJECTDIR}/GraphicalResults.o \
	${OBJECTDIR}/ImageStitching.o \
	${OBJECTDIR}/Jacobi_eigenvalue_modif.o \
	${OBJECTDIR}/LinePointFunctions.o \
	${OBJECTDIR}/ModelEstimator.o \
	${OBJECTDIR}/QuadTree_Keypoints.o \
	${OBJECTDIR}/RANSAC.o \
	${OBJECTDIR}/SavingResultsMatio.o \
	${OBJECTDIR}/Struct_Parametros.o \
	${OBJECTDIR}/TimeControl.o \
	${OBJECTDIR}/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/linesegmetsmosaicing

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/linesegmetsmosaicing: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/linesegmetsmosaicing ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/BasicFunctions.o: BasicFunctions.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/BasicFunctions.o BasicFunctions.cpp

${OBJECTDIR}/FeatureDetection.o: FeatureDetection.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/FeatureDetection.o FeatureDetection.cpp

${OBJECTDIR}/FeatureMatching.o: FeatureMatching.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/FeatureMatching.o FeatureMatching.cpp

${OBJECTDIR}/FeatureMosaicing.o: FeatureMosaicing.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/FeatureMosaicing.o FeatureMosaicing.cpp

${OBJECTDIR}/GraphicalResults.o: GraphicalResults.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/GraphicalResults.o GraphicalResults.cpp

${OBJECTDIR}/ImageStitching.o: ImageStitching.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ImageStitching.o ImageStitching.cpp

${OBJECTDIR}/Jacobi_eigenvalue_modif.o: Jacobi_eigenvalue_modif.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Jacobi_eigenvalue_modif.o Jacobi_eigenvalue_modif.cpp

${OBJECTDIR}/LinePointFunctions.o: LinePointFunctions.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/LinePointFunctions.o LinePointFunctions.cpp

${OBJECTDIR}/ModelEstimator.o: ModelEstimator.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ModelEstimator.o ModelEstimator.cpp

${OBJECTDIR}/QuadTree_Keypoints.o: QuadTree_Keypoints.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/QuadTree_Keypoints.o QuadTree_Keypoints.cpp

${OBJECTDIR}/RANSAC.o: RANSAC.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/RANSAC.o RANSAC.cpp

${OBJECTDIR}/SavingResultsMatio.o: SavingResultsMatio.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/SavingResultsMatio.o SavingResultsMatio.cpp

${OBJECTDIR}/Struct_Parametros.o: Struct_Parametros.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Struct_Parametros.o Struct_Parametros.cpp

${OBJECTDIR}/TimeControl.o: TimeControl.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/TimeControl.o TimeControl.cpp

${OBJECTDIR}/main.o: main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/linesegmetsmosaicing

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
